/**
  ******************************************************************************
  * @file    DataHandle.c
  * @author  liuhong
  * @version V1.0
  * @date    20 - March - 2018
  * @brief   the source code:DataHandle.cpp
  ******************************************************************************
  **/

#include "stdafx.h"
#include "DataHandle.h"  
#include "math.h"

//--------------------------------------------------------------------
#define  DIGIT_UNITS                   1                       //个位
#define  DIGIT_TENS                    10                      //十位
#define  DIGIT_HUNDREDS                100                     //百位
#define  DIGIT_THOUSANDS               1000                    //千位
#define  DIGIT_TEN_THOUSANDS           10000                   //万位
#define  DIGIT_HUNDRED_THOUSANDS       100000                  //十万位
#define  DIGIT_MILLION                 1000000                 //百万位
#define  DIGIT_TEN_MILLION             10000000                //千万位
#define  DIGIT_HUNDRED_MILLION         100000000               //亿位
#define  DIGIT_BILLION                 1000000000              //十亿位
#define  DIGIT_TEN_BILLION             10000000000             //百亿位
#define  DIGIT_HUNDRED_BILLION         100000000000            //千亿位
#define  DIGIT_THOUSANDS_BILLION       1000000000000           //万亿位
//--------------------------------------------------------------------
// constructor function
CDataHandle::CDataHandle()
{
}
//----------------------------------
// destructor function
CDataHandle::~CDataHandle()
{
}
//------------------------------------------------------------------------------
//copy data buffer part
//------------------------------------------------------------------------------
// copy data for byte
// *target <- *source   (cnt)
void CDataHandle::CopyDataForByte(unsigned char *target,unsigned char *source,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
   {
      target[i] = source[i];
   }
}
// copy data for half word
// *target <- *source   (cnt)
void CDataHandle::CopyDataForHalfWord(unsigned short *target,unsigned short *source,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
   {
      target[i] = source[i];
   }

}
// copy data for word
// *target <- *source   (cnt)
void CDataHandle::CopyDataForWord(unsigned int *target,unsigned int *source,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
      target[i] = source[i];
}
//------------------------------------------------------------------------------
//clear data buffer part
//------------------------------------------------------------------------------
// clear data for zero(BYTE)
// *target <- 0   (cnt)
void CDataHandle::ClearDataForByte(unsigned char *target,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
   {
      target[i] = 0;
   }
}
// clear data for zero(half word)
// *target <- 0   (cnt)
void CDataHandle::ClearDataForHalfWord(unsigned short *target,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
   {
      target[i] = 0;
   }
}
// clear data for zero(word)
// *target <- 0   (cnt)
void CDataHandle::ClearDataForWord(unsigned int *target,unsigned int cnt)
{
   unsigned int i;
   for(i=0;i<cnt;i++)
   {
      target[i] = 0;
   }
}
//===============================================
#if CPP_MODE_FLAG
//------------------------------------------------------------------------------
//delay time part
//------------------------------------------------------------------------------
//  delay long time
//  delaycnt: delay tick
void CDataHandle::DelayLongTime(unsigned int delaycnt)
{
   unsigned char i;
   unsigned int j;
   
   for(j=0;j<delaycnt;j++)
   {
      for(i=0;i<100;i++); 
   }
}
//  delay short time
//  delaycnt: delay tick
void CDataHandle::DelayShortTime(unsigned int delaycnt)
{
   unsigned int i;
   for(i=0;i<delaycnt;i++);
}
#endif
//===============================================
//------------------------------------------------------------------------------
// ascii part
//------------------------------------------------------------------------------
//byte To ascii
//input:changedata
unsigned char CDataHandle::ByteToAscii(unsigned char changedata)
{
   if(changedata<10)
      changedata=changedata+'0';
   else if(changedata>=10 && changedata<16)
      changedata=changedata+'A'-10;
   else 
      return 0xff;
      
   return  changedata;
}
//ascii To byte
//input:changedata
unsigned char CDataHandle::AsciiToByte(unsigned char changedata)
{
   if(changedata>='0' && changedata<='9')
      changedata=changedata-'0';
   else if(changedata>='a' && changedata<='z')
      changedata=changedata-'a'+10;
   else if(changedata>='A' && changedata<='Z')
      changedata=changedata-'A'+10;
   else
      changedata=0xff;
    
   return changedata;
}
//------------------------------------------------------------------------------
// data handle part
//------------------------------------------------------------------------------
//
//  greatest common divisor
int CDataHandle::Gcd(int x,int y)
{
   if(y)
   {
      return Gcd(y,x%y);
   }
   else
   {
      return x;
   }
}
//
// Least common multiple
int CDataHandle::Lcm(int x,int y)
{
   return (x * y / Gcd(x,y));
}
//
//
void CDataHandle::SignalInterpalationHandler(int insertCnt,short *targetBuf,short *sourceBuf,int len)
{
   int i,j;
   int index;
   int stepVal;

   index = 0;
   for(i=0;i<len;i++)
   {
      if( 0 == i )
	  {
          targetBuf[index++] = sourceBuf[i];
          continue;
	  }

	  stepVal = (sourceBuf[i] - sourceBuf[i-1])/insertCnt;
	  for(j = 1;j <insertCnt;j++)
	  {
		  targetBuf[index++] = sourceBuf[i-1] + stepVal * j;
	  }
      targetBuf[index++] = sourceBuf[i];
   }
}
//
//
void CDataHandle::SignalSampleHandler(int sampleCnt,short *targetBuf,short *sourceBuf,int len)
{
   int i;
   int index;

   index = 0;
   for(i = 0;i<len;i++)
   {
      if( 0 == i%sampleCnt)
	  {
         targetBuf[index++] = sourceBuf[i];
	  }
   }
}
//
// data mean (for float)
float CDataHandle::DataMeanForFloat(float *data, int len)
{
   int i;
   float avg = 0;
   float sum = 0;
   for (i = 0; i<len; i++)
   {
      sum += data[i];
   }
   avg = sum / len;
   return avg;
}
//-----------------------------------------
//  Standard Deviation
float CDataHandle::StandardDeviation(float *data, int len)
{
   int i;
   float avg, s;
   float sum = 0, e = 0;
   for (i = 0; i<len; i++)
   {
      sum += data[i];
   }
   avg = sum / len;
  
   for (i = 0; i<len; i++)
   {
      e += (data[i] - avg)*(data[i] - avg);
   }
   e /= len - 1;
   s = (float)(sqrt(e));
   return s;
}     
//------------------------------------------------------------------------------
// data 
// get max data (for float)
float CDataHandle::MaxDataForFloat(float *data, int len)
{
   int i;
   float maxd = data[0];
   for(i = 1; i < len; i++)
   {
      if(maxd < data[i])
      {
         maxd = data[i];
      }
   }
   return maxd;
}
float CDataHandle::MaxDataForFABS(float *data, int len)
{
   int i;
   float maxd;
   
   maxd = (float)(fabs(data[0]));
   for(i = 1; i < len; i++)
   {
      if(maxd < fabs(data[i]))
      {
         maxd = (float)(fabs(data[i]));
      }
   }
   return maxd;
}
//---------------------------------
// get max index (for float)
int CDataHandle::MaxIndexForFloat(float *data,int len)
{
   int i;
   int index = 0;
   float maxd = data[0];
   for(i = 1; i < len; i++)
   {
      if(maxd < data[i])
      {
         maxd = data[i];
         index = i;
      }
   }
   return index;
}
//---------------------------------
// get min data (for float)
float CDataHandle::MinDataForFloat(float *data, int len)
{
   int i;
   float mind = data[0];
   for (i = 1; i < len; i++)
   {
      if( mind > data[i] )
      {
          mind = data[i];
      }
   }
   return mind;
}
float CDataHandle::MinDataForFABS(float *data, int len)
{
   int i;
   float mind = (float)(fabs(data[0]));
   for (i = 1; i < len; i++)
   {
      if( mind > fabs(data[i]) )
      {
         mind = (float)(fabs(data[i]));
      }
   }
   return mind;
}
//------------------------------
// get min index (for float)
int CDataHandle::MinIndexForFloat(float *data,int len)
{
   int i;
   int index = 0;
   float mind = data[0];
   for (i = 1; i < len; i++)
   {
      if( mind > data[i] )
      {
          mind = data[i];
          index = i;
      }
   }
   return index;
}
//-----------------------------
// Max (for int)
int CDataHandle::MaxForInt(int a, int b)
{
   return a>b ? a : b;
}
//-----------------------------
// Min (for int)
int CDataHandle::MinForInt(int a, int b)
{
   return a<b ? a : b;
}
//-----------------------------
// quick sort (for float)
void CDataHandle::QuickSortingForFloat(float *data, int left, int right)
{
   int i = left, j = right;
   float x = data[left];
   if( left < right )
   {
      while (i < j)
      {
         while (i < j && data[j] >= x)       // Right to Left <x  
               j--;

         if (i < j)
            data[i++] = data[j];

         while (i < j && data[i]< x)         // Left to Right >=x
            i++;
         if (i < j)
            data[j--] = data[i];
      }
      data[i] = x;
      QuickSortingForFloat(data, left, i - 1);     // Recursion
      QuickSortingForFloat(data, i + 1, right);
   }
}
//
//
void CDataHandle::BubbleSortHandler(unsigned int *arr, int len) 
{
    unsigned int temp;
    int i, j;
    for (i=0; i<len-1; i++) /* 外循环为排序趟数，len个数进行len-1趟 */
	{
        for (j=0; j<len-1-i; j++) 
		{ /* 内循环为每趟比较的次数，第i趟比较len-i次 */
            if (arr[j] > arr[j+1]) 
			{ 
				/* 相邻元素比较，若逆序则交换（升序为左大于右，降序反之） */
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
	}
}
//
//
void CDataHandle::BubbleSortWithIndexHandler(unsigned int *arr,int *index,int len) 
{
    unsigned int temp;
	int indexTmp;
    int i, j;
    for (i=0; i<len-1; i++) /* 外循环为排序趟数，len个数进行len-1趟 */
	{
        for (j=0; j<len-1-i; j++) 
		{ /* 内循环为每趟比较的次数，第i趟比较len-i次 */
            if (arr[j] > arr[j+1]) 
			{ 
				/* 相邻元素比较，若逆序则交换（升序为左大于右，降序反之） */
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
				
				indexTmp = index[j];
				index[j] = index[j+1];
				index[j+1] = indexTmp;
            }
        }
	}
}
 
//------------------------------------------
// median fetch from array  (for float)
float CDataHandle::MedianFetchForFloat(float *input, int len)
{
   float medianvalue;

   QuickSortingForFloat(input, 0, len - 1);

   if (len % 2 == 0)
      medianvalue = (input[(len / 2) - 1] + input[(len / 2)]) / (float)(2.0);
   else
      medianvalue = input[(len / 2)];

   return medianvalue;
}
//-------------------------------------------------
//  calculate small value cnt (for signed short)
unsigned short CDataHandle::CalSmallValueCntForFloat(float *data,unsigned short len,float small_threshold)
{
   unsigned short i;
   unsigned short small_signal_cnt;
   float midtmp;
   
   small_signal_cnt = 0;
   for(i=0;i<len;i++)
   { 
      midtmp = data[i];
      if( midtmp < 0 )
      {
         midtmp = 0 - midtmp;
      }
      if( midtmp < small_threshold )
      {
         small_signal_cnt++;
      }
   }
   return small_signal_cnt;
}
//---------------------------------------------------
//  calculate small value cnt (for signed short)
unsigned short CDataHandle::CalBigValueCntForFloat(float *data,unsigned short len,float big_threshold)
{
   unsigned short i;
   unsigned short big_signal_cnt;
   float midtmp;
   
   big_signal_cnt = 0;
   for(i=0;i<len;i++)
   { 
      midtmp = data[i];
      if( midtmp < 0 )
      {
         midtmp = 0 - midtmp;
      }
      if( midtmp > big_threshold )
      {
         big_signal_cnt++;
      }
   }
   return big_signal_cnt;
}
//------------------------------------
// Text to bin func
unsigned char CDataHandle::TextToBinFunc(char *inbuf,int inlen,char *targetbuf,int *targetlen,int maxChangeLen)
{
   int i;
   unsigned char intemp,val;
   unsigned char temp;
   unsigned char flag;
   int cnt,index;

   flag = 0;
   cnt = 0;
   index = 0;
   val = 0;
   for(i=0;i<inlen;i++)
   {
      intemp = inbuf[i];
      temp = AsciiToByte(intemp);
      if( 0xFF == temp )
	  {
         if( (0 == cnt) && (0 == index) ) 
		 {
		    return 0;
		 }
//------------------------------------------------
         if( (' ' == intemp) || (',' == intemp) || (0x0d == intemp) || (0x0A == intemp) )
         {
            if( 0 == index )
            {
               return 0;
            }
            targetbuf[cnt++] = val;
            index = 0;
            flag = 0;
            val = 0;
		 }
	  }
	  else
	  {
         val = val * 16 + temp;
		 index++;
		 if(index>2)
		 {
			return 0;
		 }
	  }
   }
   if( 0 != index )
   {
      targetbuf[cnt++] = val;
   }
   *targetlen = cnt;
   return 1;
}
//--------------------------------------------------------------------
//	float to string
int CDataHandle::GetFloatToCharCnt( float val,int pointCnt )
{
   int cnt;
   int integralPart;

   cnt = 0;
   if( val<0 )
   {
	  cnt = 1;
	  val = 0 - val;
   }
   integralPart = (int)(val);
//--------------------------------------------
   if(integralPart < DIGIT_TENS )
	  cnt = cnt + 2;                                         // 十位(包括小数点) 
   else if(integralPart < DIGIT_HUNDREDS )
      cnt = cnt + 3;                                         // 百位(包括小数点) 
   else if(integralPart < DIGIT_THOUSANDS )         
      cnt = cnt + 4;                                         // 千位(包括小数点) 
   else if(integralPart < DIGIT_TEN_THOUSANDS )     
      cnt = cnt + 5;                                         // 万位(包括小数点) 
   else if(integralPart < DIGIT_HUNDRED_THOUSANDS )
      cnt = cnt + 6;                                         // 十万位(包括小数点) 
   else if(integralPart < DIGIT_MILLION )
      cnt = cnt + 7;                                         // 百万位(包括小数点) 
   else if(integralPart < DIGIT_TEN_MILLION )
      cnt = cnt + 8;                                         // 千万位(包括小数点) 
   else if(integralPart < DIGIT_HUNDRED_MILLION )
      cnt = cnt + 9;                                         // 亿位(包括小数点) 
   else if(integralPart < DIGIT_BILLION )
      cnt = cnt + 10;                                        // 十亿位(包括小数点) 
   else if(integralPart < DIGIT_TEN_BILLION )
      cnt = cnt + 11;                                        // 百亿位(包括小数点) 
   else if(integralPart < DIGIT_HUNDRED_BILLION )
      cnt = cnt + 12;                                        // 千亿位(包括小数点) 
   else if(integralPart < DIGIT_THOUSANDS_BILLION )
      cnt = cnt + 13;                                        // 万亿位(包括小数点) 

   cnt = cnt + pointCnt;
   return cnt;   
}
//---------------------------------
// change int to string
int CDataHandle::ChangeIntToString( int val,char *buf )
{
   int index;

   index = 0;
   if( val < 0 )
   {
      buf[index++] = '-';
	  val = 0 - val;
   }
   GetIntegerPartString(val,buf,&index);
   return index;
}
//--------------------------------------
// change float to string
int CDataHandle::ChangeFloatToString( float val,unsigned char pointCnt,char *buf )
{
   int integerPart;
   float pointPart;
   int index;

   index = 0;
   if( val<0 )
   {
	  val = 0 - val;
	  buf[index++] = '-';
   }
   integerPart = (int)(val);
   pointPart = val - (float)(integerPart);

//----------------------------------------------------
   GetIntegerPartString(integerPart,buf,&index);         //integer part
   buf[index++] = '.';
   GetDecimalPartString(pointPart,pointCnt,buf,&index);  //decimal part
//-----------------------------------------------------
   return index;
}
//-----------------------------
//chinese char change
BOOL CDataHandle::UnicodeToUtf_8(unsigned char *unicodeBuf, unsigned char *utf_8Buf, int length)
{
   if (length % 2 != 0)
      return FALSE;

   int i;
   int index;
   unsigned short tmp;

   index = 0;
   tmp = 0;
   i = 0;
   for (i = 0; i <length; i++)
   {
      if (0 != (i + 1) % 2)
      {
         tmp = unicodeBuf[i];
         continue;
      }
      tmp = (tmp <<8) + unicodeBuf[i];
      utf_8Buf[index++] = 0xE0 | (tmp >> 12);
      utf_8Buf[index++] = 0x80 | ((tmp >> 6) & 0x3F);
      utf_8Buf[index++] = 0x80 | (tmp & 0x3F);
   }
   return TRUE;
}
BOOL CDataHandle::UnicodeStrToUtf_8(CString unicodeStr, unsigned char *utf_8Buf)
{
   int length;
   int i,index;
   unsigned short tmp;
   unsigned char *strBuf;
   length = unicodeStr.GetLength();

   if (0 != length % 2)
      return FALSE;

   strBuf = new unsigned char[length * 2];
   index = 0;
   for (i = 0; i < length; i++)
   {
      tmp = (unsigned short)(unicodeStr[i]);
      strBuf[index++] = (unsigned char)((tmp >> 8) & 0xFF);
      strBuf[index++] = (unsigned char)(tmp  & 0xFF);
   }
   UnicodeToUtf_8(strBuf, utf_8Buf, length * 2);
   delete[] strBuf;
   return TRUE;
}
//
//
BOOL CDataHandle::Utf_8ToUnicode(unsigned char *unicodeBuf, unsigned char *utf_8Buf, int length)
{
   int i;
   for (i = 0; i < length; i++)
   {
      if (0 == utf_8Buf[i])
         break;
   }
   length = i;
   if (length % 3 != 0)
      return FALSE;
//--------------------------------------
   int index;
   unsigned short tmp;

   index = 0;
   tmp = 0;
   i = 0;
   for (i = 0; i <length/3; i++)
   {
      tmp = (utf_8Buf[i * 3] & 0x1F) << 12;
      tmp = tmp | (utf_8Buf[i * 3 + 1] & 0x3F) << 6;
      tmp = tmp | (utf_8Buf[i * 3 + 2] & 0x3F);

      unicodeBuf[index++] = (unsigned char)((tmp >> 8) & 0xFF);
      unicodeBuf[index++] = (unsigned char)(tmp  & 0xFF);
   }
   return TRUE;

}
void CDataHandle::GetIntegerPartString(int integerPart,char *buf,int *index)
{
   BOOL flag;
   char tmp;
   int cmpval;
//---------------------------------------------------------
   cmpval = DIGIT_BILLION;
   if( integerPart < DIGIT_THOUSANDS )
   {
      cmpval = DIGIT_HUNDREDS;
   }
   else if( integerPart < DIGIT_MILLION )                         //<1000000
   {
      cmpval = DIGIT_HUNDRED_THOUSANDS;
   }
   else if ( integerPart < DIGIT_BILLION )
   {
      cmpval = DIGIT_HUNDRED_MILLION;
   }
//----------------------------------------------------------
   flag = FALSE;
   do
   {
      tmp = (char)(integerPart/cmpval);
      if( (0!= tmp) || (TRUE == flag) )
      {
         buf[*index] = tmp + '0';
	     *index = *index + 1;
	     flag = TRUE;
      }
      integerPart = integerPart % cmpval;
//-------------------------------------------
	  cmpval = cmpval / DIGIT_TENS;
	  if( integerPart < DIGIT_TENS )
	  {
         if( DIGIT_UNITS == cmpval )
		 {
            buf[*index] = (char)(integerPart) + '0';
            *index = *index + 1;
	        break;
		 }
	  }
   }while(1);
}
//-------------------------------------------------
// get decimal part string
void CDataHandle::GetDecimalPartString(float decimalPart,int pointCnt,char *buf,int *index)
{
   int floatTmp;
   int i;
   char tmp;

   floatTmp = DIGIT_TENS;
   i = 0;
   do
   {
      tmp = (int)(decimalPart * floatTmp) % DIGIT_TENS;
	  buf[*index] = tmp + '0';

	  *index = *index + 1;

	  floatTmp = floatTmp * DIGIT_TENS; 
      i++;
   }while(i<pointCnt);
}
//------------------------------------------------------------------------------
// cal xortmp for communicate buffer
void CDataHandle::CalXortmpForBuffer(unsigned char *target,unsigned char len)
{
   unsigned char xortmp;
   unsigned char i;
   
   xortmp = 0;
   for(i=0;i<len-1;i++)
   {
      xortmp = xortmp ^ target[i];
   }
   target[i] = xortmp;
}
//
// UTF-8 to GB2312
char* CDataHandle::Utf8_To_GB2312(char* utf8)
{
   int len = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0);
   wchar_t* wstr = new wchar_t[len + 1];
   memset(wstr, 0, len + 1);
   MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wstr, len);
   len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
   char* str = new char[len + 1];
   memset(str, 0, len + 1);
   WideCharToMultiByte(CP_ACP, 0, wstr, -1, str, len, NULL, NULL);
   if (wstr)
   {
      delete[] wstr;
   }
   return str;
}
//
//GB2312 to UTF-8 
char* CDataHandle::GB2312_To_Utf8(char* gb2312)
{
   int len = MultiByteToWideChar(CP_ACP, 0, gb2312, -1, NULL, 0);
   wchar_t* wstr = new wchar_t[len + 1];
   memset(wstr, 0, len + 1);
   MultiByteToWideChar(CP_ACP, 0, gb2312, -1, wstr, len);
   len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
   char* str = new char[len + 1];
   memset(str, 0, len + 1);
   WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
   if (wstr)
   {
      delete[] wstr;
   }
   return str;
}