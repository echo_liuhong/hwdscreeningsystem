#include "windows.h"

#ifndef __FILE_HANDLER_H_H__
#define __FILE_HANDLER_H_H__

#define  FILE_HANDLER_VERSION_NAME              _T("File handler Version: 00:09")

#define  ECG_FILE_UPPER_CASE_SUFFIX             _T(".ECG")
#define  ECG_FILE_LOWER_CASE_SUFFIX             _T(".ecg")

#define  ECG_FILE_BINARY_UPPER_CASE_SUFFIX      _T(".BIN")
#define  ECG_FILE_BINARY_LOWER_CASE_SUFFIX      _T(".bin")

#define  TXT_FILE_SUFFIX                        _T(".txt")

#define  SAVE_DATA_STR_LENGTH                   5000
//------------------------------------------------------------
#define  HEX_FILE_MAX_LINE_CNT                  256

#define  DATA_RECORD                            0x00
#define  END_OF_FILE_RECORD                     0x01
#define  EXTENDED_SEGMENT_ADDRESS_RECORD        0x02
#define  START_SEGMENT_ADDRESS_RECORD           0x03
#define  EXTENDED_LINEAR_ADDRESS_RECORD         0x04
#define  START_LINEAR_ADDRESS_RECORD            0x05
//------------------------------------------------------------
#define  MAX_BUFFER_CNT                         12
//------------------------------------------------------------
enum{
   ECG_DATA_FILE = 0,
   REVERSE_DATA_FILE,
};
enum{
   ONE_LEAD_DATA_STA = 1,
   TWO_LEAD_DATA_STA,
   THREE_LEAD_DATA_STA,
   FOUR_LEAD_DATA_STA,
   FIVE_LEAD_DATA_STA,
   SIX_LEAD_DATA_STA,
   SEVEN_LEAD_DATA_STA,
   EIGHT_LEAD_DATA_STA,
};
enum{
   ECG_DATA_AFTER_FILTER = 0x00,
   ECG_DATA_ORIGINAL = 0x10
};
enum{
   SAVE_DATA_FOR_DECIMAL = 0,
   SAVE_DATA_FOR_HEXADECIMAL,
};
//---------------------------------------------------------------
typedef struct
{
    int Length;
    unsigned char *DatBuf;
}DATA_BUFFER;
//-------------------------------------------------------------
//hex 文件的使用
typedef struct
{
   BOOL TransformrResultFlag;            // TRUE:transform success,    FALSE:transform fail
   unsigned int Length;                  // 二进制文件的总长度
   unsigned char SubSectionCnt;          // hex文件分段的个数
   unsigned int *SubSectionStartAddr;    // 分段的起始地址
   unsigned int *SubSectionLength;       // 分段的长度 
   unsigned char *DatBuf;

}BINARY_DATA;
typedef struct
{
   unsigned char Cmd;
   unsigned char DataCnt;
   unsigned int StartAddr;
   unsigned int OffsetAddr;
   unsigned char Databuf[256];
}LINE_CONTENT;

typedef struct{
   BOOL flag;
   unsigned int subsectioncnt;
   unsigned int subsectionindex;
   unsigned int contentcnt;
   unsigned int subsection_offsetaddr[256];
   unsigned int subsection_bufcnt[256];

}SUB_SECIONT_INFOR;

typedef struct
{
   CString FolderName;    //file folder name
   CString RouteName;     //file route name
   CString FileName;      //file name

}FILENAME_STRUCT;
//-----------------------------------------------------
#define  ECG_FILE_DATA_START_INDEX              1536
#define  LEAD_CNT_SAVE_INDEX                    0x0E
#define  MAX_SAVE_LEAD_CNT                      0x09

#define  TWO_BYTE_IN_ONE_DATA                   2
#define  THREE_BYTE_IN_ONE_DATA                 3


#define  DATA_TYPE_PART                         0xF0
#define  ORIGINAL_SIGNAL_DATA_TYPE              0x01
#define  LEAD_CNT_ERROR_TYPE                    0x02
#define  DATA_LENGTH_ERROR                      0x03

#define ORIGINAL_DATA_STR_TIP                   _T("该文件中的心电数据类型不匹配")
#define ERROR_LEAD_CNT_STR_TIP                  _T("ECG文件中的导联个数信息错误")
#define ERROR_LEAD_DATA_CNT_STR_TIP             _T("ECG文件中错误的导联的数据个数")

#define READ_ORIGINAL_COMMAND                   1
#define READ_WAVE_COMMAND                       2
//-------------------------------
typedef struct
{
	BOOL TransformFlag;
	unsigned char SaveCmd;                    //save command
	unsigned char ErrorInfo;                  //1:数据是3个字节原始信息，不能保存, 2：导联数太多
	unsigned char SaveMode;
	unsigned char LeadCntValue;
	unsigned char DataType;

	unsigned int DataLen;
	unsigned int EveryLeadLen;

	unsigned char EcgLeadCnt;
	int *EveryLeadDataBuf[MAX_BUFFER_CNT];
}ECG_FILE_STRUCT;
//------------------------------------

class CFileHandler
{														 
public:
	CString CurModuleRoute;

    CFileHandler();
    virtual ~CFileHandler();

    CString GetCurrentProgramPath();
//------------------------------------------------------------------------------------
    BOOL CreateNewFileFolder(CString cur_route,CString route);
//------------------------------------------------------------------------------------
	void SaveDataToSpecifiedPos(CString route,CString cur_route,CString filename,int pos,CString savedata);
    void SaveDataToSpecifiedPos(CString filename,int pos,CString savedata);
    void SaveDataToSpecifiedPos(CString route,CString cur_route,CString filename,int pos,unsigned char *savedata,int len);
    void SaveDataToSpecifiedPos(CString filename,int pos,unsigned char *savedata,int len);

    void SaveDataAppendWrite(CString route,CString cur_route,CString filename,CString savedata);
    void SaveDataAppendWrite(CString filename,CString savedata);
    void SaveDataAppendWrite(CString route,CString cur_route,CString filename,unsigned char *savedata,int len);
    void SaveDataAppendWrite(CString route,CString cur_route,CString filename,char *savedata,int len);
    void SaveDataAppendWrite(CString filename,unsigned char *savedata,int len);
    void SaveDataAppendWrite(CString filename,char *savedata,int len);

    void SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,CString savedata);
    void SaveDataOverwriteWrite(CString filename,CString savedata);
    void SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,unsigned char *savedata,int len);
    void SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,char *savedata,int len);
    void SaveDataOverwriteWrite(CString filename,unsigned char *savedata,int len);
    void SaveDataOverwriteWrite(CString filename,char *savedata,int len);

    void SaveIntBufToTxt(FILENAME_STRUCT *fileNameStruct,int *buf,int len);
	void SaveIntBufToTxt(CString fileNameStr,int *buf,int len);

	void SaveShortBufToTxt(FILENAME_STRUCT *fileNameStruct,signed short *buf,int len);
	void SaveShortBufToTxt(CString fileNameStr,signed short *buf,int len);
//------------------------------------------------------------------------------------
	BOOL CheckNumberStrValid(CString str);
//------------------------------------------------------------------------------------
//  Hex file to Binary 
	void HexTransformBin(unsigned char *source,int len,BINARY_DATA *BinaryData);
//------------------------------------------------------------------------------------
//  ascii file to binary 
	void AsciiDataToBinaryData(unsigned char *indata,int inlen,DATA_BUFFER *outdat);
//------------------------------------------------------------------------------------
//  ecg file to binary
	void GetWaveBuffer(unsigned char *buf,int length,ECG_FILE_STRUCT *EcgDataHandler);
	void SaveLeadWaveValue(CString filename,int *ecgdata,int length,unsigned char savemode);
	void SaveLeadWaveValue(CString filename,int *ecgdata,int length);
//------------------------------------------------------------------------------------------------
//  txt 转换成 signal文件的功能函数
    void ConvertTxtToSignal(unsigned char *buf,int length,int *target);
    int GetSignalDataCnt(unsigned char *buf,int length);   //Get singal data count
private:
//------
    BOOL CreateNewFileRoute(CString fileroute);
//------------------------------------------------------------------------------------------------
//  hex 转换成BIN文件所需要的功能函数
	BOOL GetLineBufValue(unsigned char *source,int len,unsigned char *linebuf);         //将一行中的两个ascii码转换成1个十六进制文件
	BOOL GetBinLenAndStartAddr(unsigned char *source,int len,BINARY_DATA *BinaryData);  //获取HEX文件中二进制的个数,以及相对应的起始地址
	BOOL GetSubSectionCntAndLength(unsigned char *source,int linecnt,SUB_SECIONT_INFOR *subsectioninfor);//  获取子部分的个数以及长度
	BOOL GetLineContent(unsigned char *linebuf,int len,LINE_CONTENT *linecontent);      //将一行中的所有数据对应于HEX文件的格式命令
    void GetAllBinaryData(unsigned char *source,unsigned int sourcelen,BINARY_DATA *binarydata);

    unsigned char ByteToAscii(unsigned char changedata);
    unsigned char AsciiToByte(unsigned char changedata);
//------------------------------------------------------------------------------------
	void SaveDataBufferWithCompleteRoute(BOOL flag,FILENAME_STRUCT *fileNameStruct,char *buf,int len);
	void SaveDataBufferWithoutRoute(BOOL flag,CString fileNameStr,char *buf,int len);
};
#endif //__FILE_HANDLER_H_H__


