/**
  ******************************************************************************
  * @file    DataHandle.h
  * @author  liuhong
  * @version V1.0
  * @date    20 - March - 2018
  * @brief   the header of source code DataHandle.c or DataHandle.cpp
  ******************************************************************************
  **/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DATA_HANDLE_H_H__
#define __DATA_HANDLE_H_H__

#include "windows.h"
#include "stdint.h"

#define DATA_BASE_OPERATION_VERSION              _T("Data Handler ver:00.2")
#define CPP_MODE_FLAG                            0

#define COUNT_MODE_DECIMAL_CNT                   10
#define COUNT_MODE_HEXADECIMAL_CNT               16
//------------------------------------------------------------------------------
typedef union __WORD_TYPE__ 
{
   unsigned char DataByte[4];
   unsigned int DataUint;
   signed int DataInt;
}WORD_TYPE;

typedef union __HALFWORD_TYPE__
{
   unsigned char DataByte[2];
   unsigned short DataUShort;
   signed short DataShort;
   
}HALFWORD_TYPE;  
//---------------------------------------
typedef struct
{
	int DataLen;
	unsigned char *DataContent;

}DATA_CONTENT_STRUCT;
//-----------------------
typedef struct
{
	int sourceFreq;
	int targetFreq;
	int sourceLen;
	int targetLen;
	int insertCnt;
	int sampleCnt;
	HALFWORD_TYPE *sourceBuf;
	HALFWORD_TYPE *MidBuf;
	HALFWORD_TYPE *TargetBuf;

}SIGNAL_SAMPLERATE_CHANGE_STRUCT;

//------------------------------------------------------------------------------
class CDataHandle
{
public:
    CDataHandle();
    ~CDataHandle();

   void CopyDataForByte(unsigned char *target,unsigned char *source,unsigned int cnt);
   void CopyDataForHalfWord(unsigned short *target,unsigned short *source,unsigned int cnt);
   void CopyDataForWord(unsigned int *target,unsigned int *source,unsigned int cnt);

   void ClearDataForByte(unsigned char *target,unsigned int cnt);
   void ClearDataForHalfWord(unsigned short *target,unsigned int cnt);
   void ClearDataForWord(unsigned int *target,unsigned int cnt);
//--------------------------
#if CPP_MODE_FLAG
   void DelayLongTime(unsigned int delaycnt);
   void DelayShortTime(unsigned int delaycnt);
#endif
//--------------------------
   unsigned char ByteToAscii(unsigned char changedata);
   unsigned char AsciiToByte(unsigned char changedata);
//------------------------------------------------------------------------------
   int Gcd(int x,int y);
   int Lcm(int x,int y);
   void SignalInterpalationHandler(int insertCnt,short *targetBuf,short *sourceBuf,int len);
   void SignalSampleHandler(int sampleCnt,short *targetBuf,short *sourceBuf,int len);
//   void SignalSamplerateChangeHandler(SIGNAL_SAMPLERATE_CHANGE_STRUCT *SignalSamplerateChangeStruct);

   float DataMeanForFloat(float *data,int len);
   float StandardDeviation(float *data,int len);

   float MaxDataForFloat(float *data,int len);
   float MaxDataForFABS(float *data,int len);
   int MaxIndexForFloat(float *data,int len);

   float MinDataForFloat(float *data,int len);
   float MinDataForFABS(float *data,int len);
   int MinIndexForFloat(float *data,int len);

   int MaxForInt(int a, int b);
   int MinForInt(int a, int b);

   void QuickSortingForFloat(float *data,int left,int right);     // quick sort
   float MedianFetchForFloat(float *input,int len);                // median fetch 
   void BubbleSortHandler(unsigned int *arr, int len);
   void BubbleSortWithIndexHandler(unsigned int *arr,int *index, int len);
   //void BubbleSortWithIndexHandler(unsigned int *arr,int *index, int len);

   unsigned short CalSmallValueCntForFloat(float *data,unsigned short len,float small_threshold);
   unsigned short CalBigValueCntForFloat(float *data,unsigned short len,float big_threshold);

   void CalXortmpForBuffer(unsigned char *target,unsigned char len);
//------------------------------------------------------------------------------
   unsigned char TextToBinFunc(char *inbuf,int inlen,char *targetbuf,int *targetlen,int maxChangeLen);
//-------------------------------------------------------------------------------------------
//	float to string
   int GetFloatToCharCnt( float val,int pointCnt );
   int ChangeIntToString( int val,char *buf );
   int ChangeFloatToString( float val,unsigned char pointCnt,char *buf );
//---------------------------
//chinese char change
   BOOL UnicodeToUtf_8(unsigned char *unicodeBuf, unsigned char *utf_8Buf, int length);
   BOOL UnicodeStrToUtf_8(CString unicodeStr, unsigned char *utf_8Buf);
   BOOL Utf_8ToUnicode(unsigned char *unicodeBuf, unsigned char *utf_8Buf, int length);

   char* CDataHandle::Utf8_To_GB2312(char* utf8);
   char* CDataHandle::GB2312_To_Utf8(char* gb2312);

protected:
private:
   void GetIntegerPartString(int integerPart,char *buf,int *index);
   void GetDecimalPartString(float decimalPart,int pointCnt,char *buf,int *index);

};
#endif //__DATA_HANDLE_H_H__
