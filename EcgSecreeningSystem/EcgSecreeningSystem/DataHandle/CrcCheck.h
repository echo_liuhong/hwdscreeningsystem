//
// CrcCheck.h : Defines the entry point for the console application.
//
#include "windows.h"

//-----------------------------------------------------------------------------------
#ifndef __CRC_CHECK_H_H__
#define __CRC_CHECK_H_H__

//----------------------------------------------------------------
void MakeCrc32Table(void);
unsigned int CalcCrc32(unsigned char *data, unsigned int size);
unsigned int CalcCrc32_buf(unsigned char *data, unsigned int size,unsigned int crcTmp);
//----------------------------------------------------------------------------------

#endif //__CRC_CHECK_H_H__