
#include "stdafx.h"
#include "FileHandler.h"
#include "DataHandle.h"
#include <assert.h>

#pragma warning(disable:4996)
//-----------------------------------------------------------
#define TEMPORARY_SAVE_BUFFER_CNT               100000
#define SAVE_TEMPORARY_SAVE_BUFFER_CNT          (TEMPORARY_SAVE_BUFFER_CNT-200)
//-----------------------------------------------------------
// constructor function
CFileHandler::CFileHandler()
{
}
//
// destructor function
//
CFileHandler::~CFileHandler()
{
}
//
// get program current path
// return:current route
//
CString CFileHandler::GetCurrentProgramPath()
{
   CString m_FilePath;
   int m_iPosIndex;

   GetModuleFileName(NULL,m_FilePath.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
   m_FilePath.ReleaseBuffer();
   m_iPosIndex = m_FilePath.ReverseFind('\\');
   m_FilePath = m_FilePath.Left(m_iPosIndex);

   CurModuleRoute = m_FilePath;
   return m_FilePath;
}
//
//
BOOL CFileHandler::CreateNewFileFolder(CString cur_route,CString route)
{
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   return CreateNewFileRoute(cur_route);
   }
   return FALSE;
}
//
//  create new file route
//
BOOL CFileHandler::CreateNewFileRoute(CString fileroute)
{
   if(!PathFileExists(fileroute))       //文件夹不存在则创建
   {
      return CreateDirectory(fileroute,NULL);
   }
   return TRUE;
}
/*******************************************************************************
* Function Name  : SaveDataToSpecifiedPos
* Description    : save data to specified pos
* Input          : CString route:创建一个新的路径
                   CString cur_route: 当前保存的路径
				   CString filenam：保存的文件名

				   int pos 从文件开始处的pos位置开始保存
				   CString savedata： 保存数据的内容
				   unsigned char *savedata, 保存的数据的内容的指针
				   int len   数据内容的长度
* Output         : None
* Return         : None
*******************************************************************************/
void CFileHandler::SaveDataToSpecifiedPos(CString route,CString cur_route,CString filename,int pos,CString savedata)
{
//---------------------------------------------------------
// get save file name
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   CreateNewFileRoute(cur_route);

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
//-----------------------------------------
   SaveDataToSpecifiedPos(filename,pos,savedata);
}
void CFileHandler::SaveDataToSpecifiedPos(CString filename,int pos,CString savedata)
{
   CFile wfile;
   int length,i;
   unsigned char *buf;

   length = savedata.GetLength();
   buf = new unsigned char[length + 1];       //20181217
   for(i=0;i<length;i++)
   {
      buf[i] = (unsigned char)(savedata[i]);
   }

   SaveDataToSpecifiedPos(filename,pos,buf,length);

   delete []buf;
}

void CFileHandler::SaveDataToSpecifiedPos(CString route,CString cur_route,CString filename,int pos,unsigned char *savedata,int len)
{
//---------------------------------------------------------
// get save file name
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

       CreateNewFileRoute(cur_route);    

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
//-----------------------------------------
   SaveDataToSpecifiedPos(filename,pos,savedata,len);
}
void CFileHandler::SaveDataToSpecifiedPos(CString filename,int pos,unsigned char *savedata,int len)
{
   CFile wfile;
   if( FALSE == wfile.Open( filename,CFile::modeRead))
   {
      wfile.Open(filename,CFile::modeWrite | CFile::modeCreate);
   }
   else
   {
      wfile.Close();
      wfile.Open(filename,CFile::modeWrite);
      wfile.Seek(pos,CFile::begin); 
   }
   wfile.Write(savedata,len);
   wfile.Close();
}
/*******************************************************************************
* Function Name  : SaveDataAppendWrite
* Description    : save data append write
* Input          : CString route:创建一个新的路径
                   CString cur_route: 当前保存的路径
				   CString filenam：保存的文件名
				   CString savedata： 保存数据的内容

				   unsigned char *savedata, 保存的数据的内容的指针
				   int len   数据内容的长度
* Output         : None
* Return         : None
*******************************************************************************/
void CFileHandler::SaveDataAppendWrite(CString route,CString cur_route,
									   CString filename,CString savedata)
{
//---------------------------------------------------------
// get save file name
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   CreateNewFileRoute(cur_route);

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
//-----------------------------------------
   SaveDataAppendWrite(filename,savedata);
}
//-----------------------------------------
void CFileHandler::SaveDataAppendWrite(CString filename,CString savedata)
{
   CFile wfile;
   int length,i;
   unsigned char *buf;

   length = savedata.GetLength();
   buf = new unsigned char[length + 1];       //20181217
   for(i=0;i<length;i++)
   {
      buf[i] = (unsigned char)(savedata[i]);
   }

   SaveDataAppendWrite(filename,buf,length);

   delete []buf;
}
void CFileHandler::SaveDataAppendWrite(CString route,CString cur_route,
	                                   CString filename,unsigned char *savedata,int len)
{
//---------------------------------------------------------
// get save file name
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

       CreateNewFileRoute(cur_route);    

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
//-----------------------------------------
   SaveDataAppendWrite(filename,savedata,len);
}
void CFileHandler::SaveDataAppendWrite(CString route,CString cur_route,
	                                   CString filename,char *savedata,int len)
{
//---------------------------------------------------------
// get save file name
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

       CreateNewFileRoute(cur_route);    

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
//-----------------------------------------
   SaveDataAppendWrite(filename,savedata,len);
}
void CFileHandler::SaveDataAppendWrite(CString filename,unsigned char *savedata,int len)
{
   CFile wfile;
   if( FALSE == wfile.Open( filename,CFile::modeRead))
   {
      wfile.Open(filename,CFile::modeWrite | CFile::modeCreate);
   }
   else
   {
      wfile.Close();
      wfile.Open(filename,CFile::modeWrite);
      wfile.SeekToEnd(); 
   }
   wfile.Write(savedata,len);
   wfile.Close();
}
void CFileHandler::SaveDataAppendWrite(CString filename,char *savedata,int len)
{
   CFile wfile;
   if( FALSE == wfile.Open( filename,CFile::modeRead))
   {
      wfile.Open(filename,CFile::modeWrite | CFile::modeCreate);
   }
   else
   {
      wfile.Close();
      wfile.Open(filename,CFile::modeWrite);
      wfile.SeekToEnd(); 
   }
   wfile.Write(savedata,len);
   wfile.Close();
}

/*******************************************************************************
* Function Name  : SaveDataOverwriteWrite
* Description    : save data overwrite write (保存数据覆盖写)
* Input          : CString route:创建一个新的路径
                   CString cur_route: 当前保存的路径
				   CString filenam：保存的文件名
				   CString savedata： 保存数据的内容

				   unsigned char *savedata, 保存的数据的内容的指针
				   int len   数据内容的长度
* Output         : None
* Return         : None
*******************************************************************************/
void CFileHandler::SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,CString savedata)
{
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   CreateNewFileRoute(cur_route);

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
   SaveDataOverwriteWrite(filename,savedata);
}
void CFileHandler::SaveDataOverwriteWrite(CString filename,CString savedata)
{
   CFile wfile;
   int length,i;
   unsigned char *buf;

   length = savedata.GetLength();
   buf = new unsigned char[length + 1];   //20181217
   for(i=0;i<length;i++)
   {
      buf[i] = (unsigned char)(savedata[i]);
   }
   SaveDataOverwriteWrite(filename,buf,length);

   delete []buf;
}
void CFileHandler::SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,unsigned char *savedata,int len)
{
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   CreateNewFileRoute(cur_route);

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
   SaveDataOverwriteWrite(filename,savedata,len);
}
void CFileHandler::SaveDataOverwriteWrite(CString route,CString cur_route,CString filename,char *savedata,int len)
{
   if( (0 == route.IsEmpty()) && (0 == cur_route.IsEmpty()) )   
   {
	   cur_route = cur_route + _T("\\");
	   cur_route = cur_route + route;

	   CreateNewFileRoute(cur_route);

	   cur_route = cur_route + _T("\\");
	   filename = cur_route + filename;
   }
   SaveDataOverwriteWrite(filename,savedata,len);
}
void CFileHandler::SaveDataOverwriteWrite(CString filename,unsigned char *savedata,int len)
{
   CFile wfile;

   if(TRUE == wfile.Open(filename,CFile::modeRead) )
   {
     wfile.Close();
	 wfile.Remove(filename);
   }
   wfile.Open( filename,CFile::modeWrite | CFile::modeCreate);
   wfile.Write(savedata,len);
   wfile.Close();
}
void CFileHandler::SaveDataOverwriteWrite(CString filename,char *savedata,int len)
{
   CFile wfile;

   if(TRUE == wfile.Open(filename,CFile::modeRead) )
   {
     wfile.Close();
	 wfile.Remove(filename);
   }
   wfile.Open( filename,CFile::modeWrite | CFile::modeCreate);
   wfile.Write(savedata,len);
   wfile.Close();
}
void CFileHandler::SaveIntBufToTxt(FILENAME_STRUCT *fileNameStruct,int *buf,int len)
{
   int i;
   BOOL flag;
   CString str;
   CString saveStr;

   flag = FALSE;
   for(i = 0;i<len;i++)
   {
      str.Format(_T("%d\r\n"),buf[i]);
	  saveStr = saveStr + str;

	  if( saveStr.GetLength() > SAVE_DATA_STR_LENGTH )
	  {
	     if( FALSE == flag )
	     {
            flag = TRUE;
			SaveDataOverwriteWrite(fileNameStruct->FolderName,fileNameStruct->RouteName,fileNameStruct->FileName,saveStr);
            saveStr.Empty();
		 }
		 else
         {
            SaveDataAppendWrite(fileNameStruct->FolderName,fileNameStruct->RouteName,fileNameStruct->FileName,saveStr);
            saveStr.Empty();
         }
	  }
   }
//------------------------------------
   if( 0 != saveStr.GetLength() )
   {
      if( FALSE == flag )
         SaveDataOverwriteWrite(fileNameStruct->FolderName,fileNameStruct->RouteName,fileNameStruct->FileName,saveStr);
	  else
         SaveDataAppendWrite(fileNameStruct->FolderName,fileNameStruct->RouteName,fileNameStruct->FileName,saveStr);
   }
   saveStr.Empty();
//------------------------------------
}
void CFileHandler::SaveIntBufToTxt(CString fileNameStr,int *buf,int len)
{
   int i;
   BOOL flag;
   CString str;
   CString saveStr;

   flag = FALSE;
   for( i=0;i<len;i++)
   {
      str.Format(_T("%d\r\n"),buf[i]);
	  saveStr = saveStr + str;

	  if( saveStr.GetLength() > SAVE_DATA_STR_LENGTH )
	  {
	     if( FALSE == flag )
	     {
            flag = TRUE;
            SaveDataOverwriteWrite(fileNameStr,saveStr);
            saveStr.Empty();
		 }
		 else
         {
            SaveDataAppendWrite(fileNameStr,saveStr);
            saveStr.Empty();
         }
	  }
   }
//-------------------------------------------------------------
   if( 0 != saveStr.GetLength() )
   {
      if( FALSE == flag )
         SaveDataOverwriteWrite(fileNameStr,saveStr);
	  else
         SaveDataAppendWrite(fileNameStr,saveStr);
   }
   saveStr.Empty();
//-------------------------------------------------------------
}
//
//
void CFileHandler::SaveShortBufToTxt(FILENAME_STRUCT *fileNameStruct,signed short *buf,int len)
{
   int i;
   BOOL flag;
   
   int index;
   char *saveBuffer;
   saveBuffer = new char[TEMPORARY_SAVE_BUFFER_CNT];
//------------------------------------------------------
   flag = FALSE;
   index = 0;
   for(i = 0;i<len;i++)
   {
      index += sprintf(saveBuffer + index,"%d\r\n",buf[i]);
//----
	  if( index>SAVE_TEMPORARY_SAVE_BUFFER_CNT )
	  {
         SaveDataBufferWithCompleteRoute(flag,fileNameStruct,saveBuffer,index);
         flag = TRUE;
		 index = 0;
	  }
   }
//------------------------------------
   if( 0 != index )
   {
      SaveDataBufferWithCompleteRoute(flag,fileNameStruct,saveBuffer,index);
   }
   delete [] saveBuffer;
//------------------------------------
}
//
//
void CFileHandler::SaveShortBufToTxt(CString fileNameStr,signed short *buf,int len)
{
   int i;
   int index;
   char *saveBuffer;
   CFile wfile;
   if( 0 == len )
	   return;

   if( TRUE == wfile.Open(fileNameStr,CFile::modeRead) )
   {
      wfile.Close();
      wfile.Remove(fileNameStr);
   }
   wfile.Open( fileNameStr,CFile::modeWrite | CFile::modeCreate);

   saveBuffer = new char[TEMPORARY_SAVE_BUFFER_CNT];
   
   index = 0;
   for(i = 0;i<len;i++)
   {
      index += sprintf(saveBuffer + index,"%d\r\n",buf[i]);
	  if( index>SAVE_TEMPORARY_SAVE_BUFFER_CNT )
	  {
         wfile.Write(saveBuffer,index);
		 index = 0;
	  }
   }
   if( 0 != index )
   {
	   wfile.Write(saveBuffer,index);
   }
   wfile.Close();
   delete [] saveBuffer;
}
//
//
void CFileHandler::SaveDataBufferWithCompleteRoute(BOOL flag,FILENAME_STRUCT *fileNameStruct,char *buf,int len)
{
   if( FALSE == flag )
   { 
      SaveDataOverwriteWrite( fileNameStruct->FolderName,
                              fileNameStruct->RouteName,
                              fileNameStruct->FileName,
                              buf,
                              len);
   }
   else
   {
      SaveDataAppendWrite(fileNameStruct->FolderName,
                          fileNameStruct->RouteName,
                          fileNameStruct->FileName,
                          buf,
                          len);
   }
}
/*******************************************************************************
* Function Name  : HexTransformBin
* Description    : hex file transform binary file (保存数据覆盖写)
* Input          : *source,     *source 
                   int len,     source 的长度
				   *binarydata  一个BIN文件的结构体
* Output         : None
* Return         : None
*******************************************************************************/
void CFileHandler::HexTransformBin(unsigned char *source,int len,BINARY_DATA *binarydata)
{
   BOOL flag;
   unsigned int length;
   unsigned int subsectioncnt;
   flag = GetBinLenAndStartAddr(source,len,binarydata);
   if( FALSE == flag )
   {
      binarydata->TransformrResultFlag = FALSE;
	  return ;
   }
   subsectioncnt = binarydata->SubSectionCnt;
   length = binarydata->SubSectionStartAddr[subsectioncnt-1] - binarydata->SubSectionStartAddr[0];
   length = length + binarydata->SubSectionLength[subsectioncnt-1];
   binarydata->Length = length;
   binarydata->DatBuf = new unsigned char[length];
   binarydata->TransformrResultFlag = TRUE;

   GetAllBinaryData(source,len,binarydata);
}
//
//  get bin length and start addr
//
BOOL CFileHandler::GetBinLenAndStartAddr(unsigned char *source,int len,BINARY_DATA *binarydata)
{
   unsigned int i;
   int index;
   int linecnt,linestartindex;
   SUB_SECIONT_INFOR subsectioninfor;

   index = 0;
   subsectioninfor.flag = FALSE;
   subsectioninfor.contentcnt = 0;
   subsectioninfor.subsectioncnt = 0;
   subsectioninfor.subsectionindex = 0;
//--------------------------------------------------
// 获取子块的个数,子块的起始地址,相应子块的长度
   do{
      if( ':' != source[index] )
         return FALSE;
	  else
         linestartindex = index + 1;
//----------------------------------------
      linecnt = 0;
      index++;
      while(1)
      {
         if( linecnt >= HEX_FILE_MAX_LINE_CNT )
            return FALSE;

		 if( (source[index] == 0x0D) && (source[index+1] == 0x0A) )
		 {
            index++;
			if( FALSE == GetSubSectionCntAndLength(&source[linestartindex],linecnt,&subsectioninfor))
			{
               return FALSE;
			}
            index++;
			break;
		 }
		 else
		 {
            linecnt++; 
		 }
         index++;
      }
   }while(index<len);
//--------------------------------------------------
   binarydata->SubSectionCnt = subsectioninfor.subsectioncnt;
   binarydata->SubSectionStartAddr = new unsigned int[subsectioninfor.subsectioncnt];
   binarydata->SubSectionLength = new unsigned int[subsectioninfor.subsectioncnt];
   for(i=0;i<subsectioninfor.subsectioncnt;i++)
   {
	   binarydata->SubSectionStartAddr[i] = subsectioninfor.subsection_offsetaddr[i];
	   binarydata->SubSectionLength[i] = subsectioninfor.subsection_bufcnt[i];
   }
   return TRUE;
}
//
// get sub section cnt and len
//
BOOL CFileHandler::GetSubSectionCntAndLength(unsigned char *source,int linecnt,SUB_SECIONT_INFOR *subsectioninfor)
{
   LINE_CONTENT LineContent;
   unsigned char linebuf[256];

   if( 1 == (linecnt%2) )
       return FALSE; 

   if(FALSE == GetLineBufValue(source,linecnt,linebuf))
       return FALSE;

   if(FALSE == GetLineContent(linebuf,linecnt/2,&LineContent))
       return FALSE;

   if(DATA_RECORD == LineContent.Cmd)
   {
	  subsectioninfor->contentcnt = subsectioninfor->contentcnt + LineContent.DataCnt;
   }
   else if( EXTENDED_LINEAR_ADDRESS_RECORD == LineContent.Cmd )
   {
      subsectioninfor->subsectioncnt++;
      subsectioninfor->subsection_offsetaddr[subsectioninfor->subsectionindex] = LineContent.OffsetAddr;
      if( FALSE == subsectioninfor->flag )
	  {
		 subsectioninfor->flag = TRUE;
	  }
	  else
	  {
		 subsectioninfor->subsection_bufcnt[subsectioninfor->subsectionindex-1] = subsectioninfor->contentcnt;
	  }
      subsectioninfor->subsectionindex++;
	  subsectioninfor->contentcnt = 0;
	}
	else if( END_OF_FILE_RECORD ==  LineContent.Cmd )
	{
      subsectioninfor->subsection_bufcnt[subsectioninfor->subsectionindex-1] = subsectioninfor->contentcnt;
	}
    return TRUE;
}
//
// get line buf value
//
BOOL CFileHandler::GetLineBufValue(unsigned char *source,int len,unsigned char *linebuf)
{
   int i;
   int index;
   BOOL flag;
   unsigned char buftmp;

   flag = FALSE;
   buftmp = 0; 
   index = 0;
   for(i=0;i<len;i++)
   {
      if( FALSE == flag )
	  {
         flag = TRUE;

         buftmp = AsciiToByte(source[i]);
		 if( 0xFF == buftmp )
            return FALSE;
	  }
	  else 
	  {
		 flag = FALSE;

         linebuf[index] = buftmp * 16;
         buftmp = AsciiToByte(source[i]);
		 if( 0xFF == buftmp )
            return FALSE;

		 linebuf[index] = linebuf[index] + buftmp;
		 index++;
	  }
   }
   return TRUE;
}
//
// get all binary data 
//
void CFileHandler::GetAllBinaryData(unsigned char *source,unsigned int sourcelen,BINARY_DATA *binarydata)
{
   unsigned int index;
   unsigned int i,saveindex,supplecnt;
   int linecnt,linestartindex;

   BOOL flag;
   unsigned int preaddr;

   LINE_CONTENT LineContent;
   unsigned char linebuf[256];

   flag = FALSE;
   index = 0;
   saveindex = 0;
//--------------------------------------------------
// 获取子块的个数,子块的起始地址,相应子块的长度
   do{
      if( ':' != source[index] )
         return;
	  else
         linestartindex = index + 1;
//---------------------------------------------------
      linecnt = 0;
      index++;
      while(1)
      {
		if( (source[index] == 0x0D) && (source[index+1] == 0x0A) )
		{
           index++;
		   GetLineBufValue(&source[linestartindex],linecnt,linebuf);
           GetLineContent(linebuf,linecnt/2,&LineContent);

           if(DATA_RECORD == LineContent.Cmd)
           {
			   for(i=0;i<LineContent.DataCnt;i++)
			   {
                  binarydata->DatBuf[saveindex++] = LineContent.Databuf[i];
			   }
           }
           else if( EXTENDED_LINEAR_ADDRESS_RECORD == LineContent.Cmd )
           {
			   if( FALSE == flag )
			   {
                  flag = TRUE;
			      preaddr = LineContent.OffsetAddr;
			   }
			   else
			   {
                  supplecnt = LineContent.OffsetAddr - preaddr - saveindex;
                  for(i=0;i<supplecnt;i++)
                     binarydata->DatBuf[saveindex++] = 0xFF;
			   }
           }
		   index++;
		   break;
		 }
		 else
		 {
            linecnt++; 
		 }
         index++;
      }
   }while(index<sourcelen);
//--------------------------------------------------
}
//
// get subsectioncnt 
//
BOOL CFileHandler::GetLineContent(unsigned char *linebuf,int len,LINE_CONTENT *linecontent)
{
   int i;
   unsigned char checksum;

   unsigned char cnt;
   unsigned char cmd;

   checksum = 0;
   for(i=0;i<len-1;i++)
   {
      checksum = checksum + linebuf[i];
   }
   checksum = 0x00 - checksum;
   if( checksum != linebuf[len-1] )
   {
      return FALSE;
   }
   cnt = linebuf[0];
   linecontent->DataCnt = cnt;
   cmd = linebuf[3];
   linecontent->Cmd = cmd;
   linecontent->StartAddr = linebuf[0]*256 + linebuf[1];

   switch(cmd)
   {
      case DATA_RECORD:linecontent->DataCnt = cnt;
		               for(i=0;i<cnt;i++)
					   {
                          linecontent->Databuf[i] = linebuf[4+i];
					   }
		               break;
      case END_OF_FILE_RECORD:
		                     break;
      case EXTENDED_SEGMENT_ADDRESS_RECORD:
		                                   break;
      case START_SEGMENT_ADDRESS_RECORD:
		                                break;
	  case EXTENDED_LINEAR_ADDRESS_RECORD:linecontent->OffsetAddr = (linebuf[4]*256 + linebuf[5])*0x10000;
		                                  break;
	  case START_LINEAR_ADDRESS_RECORD:

		                               break;
   }
   return TRUE;
}
//byte To ascii
//input:changedata
unsigned char CFileHandler::ByteToAscii(unsigned char changedata)
{
   if(changedata<10)
      changedata=changedata+'0';
   else if(changedata>=10 && changedata<16)
      changedata=changedata+'A'-10;
   else 
      return 0xff;
      
   return  changedata;
}
//ascii To byte
//input:changedata
unsigned char CFileHandler::AsciiToByte(unsigned char changedata)
{
   if(changedata>='0' && changedata<='9')
      changedata=changedata-'0';
   else if(changedata>='a' && changedata<='z')
      changedata=changedata-'a'+10;
   else if(changedata>='A' && changedata<='Z')
      changedata=changedata-'A'+10;
   else
      changedata=0xff;
    
   return changedata;
}
/*******************************************************************************
* Function Name  : AsciiDataToBinaryData
* Description    : Ascii data to binary data
* Input          : *indata   输入数据的指针 
                   inlen,    indata 的长度
				   *outdat   结构体包含长度和数据指针
* Output         : None
* Return         : None
*******************************************************************************/
 void CFileHandler::AsciiDataToBinaryData(unsigned char *indata,int inlen,DATA_BUFFER *outdat)
{
   int i;
   int length,index;
   BOOL flag;
   unsigned char tempval;
   unsigned char tmpmory;

   length = 0;
   for(i = 0;i<inlen;i++)
   {
      tempval = AsciiToByte(indata[i]);
	  if( 0xFF != tempval )
	  {
		  length++;
	  }
   }
   if( length%2 !=0 )
   {
	  outdat->Length = 0;
	  return;
   }
   else
   {
      outdat->Length = length/2;
   }
   outdat->DatBuf = new unsigned char[outdat->Length];
   length = length/2;

   flag = FALSE;
   index = 0;
   for(i=0;i<inlen;i++)
   {
      tempval = AsciiToByte(indata[i]);
	  if( 0xFF == tempval )
		  continue;
//----------------------------------------------
	  if(FALSE == flag)
	  {
         tmpmory = tempval;
		 flag = TRUE;
	  }
	  else
	  {
         tmpmory = tmpmory * 16 + tempval;
		 flag = FALSE;
		 outdat->DatBuf[index++] = tmpmory;
	  }
//----------------------------------------------
   }
}
/*******************************************************************************
* Function Name  : GetWaveBuffer
* Description    : get wave buffer
* Input          : *buf   输入数据的指针 
                   length, 输入指针的数据
				   *EcgDataHandler ECGdata handler  用于返回转换后的数据
* Output         : None
* Return         : None
*******************************************************************************/
void CFileHandler::GetWaveBuffer(unsigned char *buf,int length,ECG_FILE_STRUCT *EcgDataHandler)
{
   int i;
   unsigned char tmpory;
   unsigned char leadcnt;
   unsigned char everypoint_datacnt;

   tmpory = buf[LEAD_CNT_SAVE_INDEX];
   if( 0!= (tmpory & DATA_TYPE_PART) )
   {
	  if( READ_WAVE_COMMAND == EcgDataHandler->SaveCmd )
	  {
         EcgDataHandler->TransformFlag = FALSE;
	     EcgDataHandler->ErrorInfo = ORIGINAL_SIGNAL_DATA_TYPE;
         return;
	  }
   }
   else
   {
	  if( READ_ORIGINAL_COMMAND == EcgDataHandler->SaveCmd )
	  {
         EcgDataHandler->TransformFlag = FALSE;
	     EcgDataHandler->ErrorInfo = ORIGINAL_SIGNAL_DATA_TYPE;
         return;
	  }
   }
//---------------------------
   tmpory = tmpory & 0x0F;
   if( (tmpory > MAX_SAVE_LEAD_CNT) || (tmpory == 0) )
   {
      EcgDataHandler->TransformFlag = FALSE;
	  EcgDataHandler->ErrorInfo = LEAD_CNT_ERROR_TYPE;
	  return;
   }
   leadcnt = tmpory;
   EcgDataHandler->LeadCntValue = tmpory;
   EcgDataHandler->DataType = buf[LEAD_CNT_SAVE_INDEX] & 0x10;

   everypoint_datacnt = TWO_BYTE_IN_ONE_DATA;
   if( ECG_DATA_ORIGINAL == EcgDataHandler->DataType )
      everypoint_datacnt = THREE_BYTE_IN_ONE_DATA;

   if((length - ECG_FILE_DATA_START_INDEX) %(leadcnt * everypoint_datacnt) != 0)
   {
	  EcgDataHandler->TransformFlag = FALSE;
	  EcgDataHandler->ErrorInfo = DATA_LENGTH_ERROR;
	  return;
   }
   EcgDataHandler->EcgLeadCnt = EcgDataHandler->LeadCntValue;
   EcgDataHandler->DataLen = length;
   EcgDataHandler->EveryLeadLen = (length - ECG_FILE_DATA_START_INDEX)/(leadcnt * everypoint_datacnt);

   for(i=0;i<EcgDataHandler->EcgLeadCnt;i++)
   {
      EcgDataHandler->EveryLeadDataBuf[i] = new int[EcgDataHandler->EveryLeadLen];
   }
   EcgDataHandler->TransformFlag = TRUE;
//-----------------------------------------------------------------------------------------
   WORD_TYPE midwordtmp;
   HALFWORD_TYPE midhalfwordtmp;
   unsigned char *target;
   unsigned char j;
   int leadsaveindex;

   leadsaveindex = 0;
   target = &buf[ECG_FILE_DATA_START_INDEX];
   length = length - ECG_FILE_DATA_START_INDEX;
   i = 0;
   do{
//      for(j=0;j<EcgDataHandler->EcgLeadCnt;j++)
//	  {
//         midhalfwordtmp.DataUShort = (target[i+1]<<8) + (target[i]);
//         i = i + TWO_BYTE_IN_ONE_DATA;
//         EcgDataHandler->EveryLeadDataBuf[j][leadsaveindex] = midhalfwordtmp.DataShort;
//	  }
      for(j=0;j<EcgDataHandler->EcgLeadCnt;j++)
      {
		  if( TWO_BYTE_IN_ONE_DATA == everypoint_datacnt )
	      {
             midhalfwordtmp.DataUShort = (target[i+1]<<8) + (target[i]);
             i = i + TWO_BYTE_IN_ONE_DATA;
             EcgDataHandler->EveryLeadDataBuf[j][leadsaveindex] = midhalfwordtmp.DataShort;
          }
          else if( THREE_BYTE_IN_ONE_DATA == everypoint_datacnt )
          {
             midwordtmp.DataUint = (target[i]<<16) + (target[i+1]<<8) + target[i+2];
             i = i + THREE_BYTE_IN_ONE_DATA;
             EcgDataHandler->EveryLeadDataBuf[j][leadsaveindex] = midwordtmp.DataInt;
          }
      }
	  leadsaveindex++;
   }while(i<length);
}
//---------------------------------------
// save lead wave value
void CFileHandler::SaveLeadWaveValue(CString filename,int *ecgdata,int length,unsigned char savemode)
{
   int i,j;
   CString str;
   unsigned char *savebuf;
   unsigned short saveindex,lineindex;
   WORD_TYPE midwordtmp;

   BOOL flag,writeflag;
   CFile wfile;

   savebuf = new unsigned char[500];

   saveindex = 0;
   lineindex = 0;
   flag = FALSE;
   writeflag = FALSE;
   for(i=0;i<length;i++)
   {
      midwordtmp.DataInt = ecgdata[i];
      if( SAVE_DATA_FOR_DECIMAL == savemode )
	  {
         str.Format(_T("%d,"),midwordtmp.DataInt);
	  }
	  else if( SAVE_DATA_FOR_HEXADECIMAL == savemode )
      {
		 str.Format(_T("0x%06X,"),midwordtmp.DataUint); 
	  }

	  for(j=0;j<str.GetLength();j++)
	  {
		  savebuf[saveindex++] = (unsigned char)(str[j]);
	  }
	  if(0==(i+1)%10)
	  {
         savebuf[saveindex++] = '\r';
		 savebuf[saveindex++] = '\n';
		 lineindex++;
		 if( lineindex>3 )
		 {
			writeflag  = TRUE;
			if(flag == FALSE)
			{
			   flag = TRUE;
               if(TRUE == wfile.Open(filename,CFile::modeRead) )
               {
                  wfile.Close();
	              wfile.Remove(filename);
               }
               wfile.Open( filename,CFile::modeWrite | CFile::modeCreate);
               wfile.Write(savebuf,saveindex);
			}
			else
			{
               wfile.Write(savebuf,saveindex);
			}
			saveindex = 0;
			lineindex = 0;
		 }
	  }
	  else
	  {
		  writeflag = FALSE;
	  }
   }
   if( FALSE == writeflag )
   {
      wfile.Write(savebuf,saveindex);
   }
   wfile.Close();
   delete []savebuf;
}
//---------------------------------------
// save lead wave value
void CFileHandler::SaveLeadWaveValue(CString filename,int *ecgdata,int length)
{
   int i,j;
   CString str;
   unsigned char *savebuf;
   unsigned short saveindex,lineindex;
   WORD_TYPE midwordtmp;

   BOOL flag,writeflag;
   CFile wfile;

   savebuf = new unsigned char[500];

   saveindex = 0;
   lineindex = 0;
   flag = FALSE;
   writeflag = FALSE;
   for(i=0;i<length;i++)
   {
      midwordtmp.DataInt = ecgdata[i];
      str.Format(_T("%d,"),midwordtmp.DataInt);

	  for( j=0;j<str.GetLength();j++)
	  {
         savebuf[saveindex++] = (unsigned char)(str[j]);
	  }
      savebuf[saveindex++] = '\r';
      savebuf[saveindex++] = '\n';
      lineindex++;
      if( lineindex>10 )
      {
         writeflag  = TRUE;
         if( FALSE == flag )
         {
            flag = TRUE;
            if(TRUE == wfile.Open(filename,CFile::modeRead) )
            {
               wfile.Close();
               wfile.Remove(filename);
            }
            wfile.Open( filename,CFile::modeWrite | CFile::modeCreate);
            wfile.Write(savebuf,saveindex);
	     }
         else
         {
            wfile.Write(savebuf,saveindex);
         }
         saveindex = 0;
         lineindex = 0;
      }
	  else
	  {
		  writeflag = FALSE;
	  }
   }
   if( FALSE == writeflag )
   {
      wfile.Write(savebuf,saveindex);
   }
   wfile.Close();
   delete []savebuf;
}
//--------------------------------------------
// convert text to signal
// include negative,float 
void CFileHandler::ConvertTxtToSignal(unsigned char *buf,int length,int *target)   //20181212
{
//-------------------------------------------------------------   
   BOOL numberSymbolFlag;
   BOOL decimalFlag;
   int decimalCnt;
   int changeStep;
   int dataSignalIndex,i;
   int j;
   float dattmp,midtmp;
   unsigned char temp;

   numberSymbolFlag = FALSE;
   
   dataSignalIndex = 0;
   dattmp = 0.0;
   decimalCnt = 0;
   changeStep = 0;
   for( i=0;i<length;i++ )
   {
      temp = buf[i];
      if( 0 == changeStep )
      {
         decimalFlag = FALSE;
         if( '-' == temp )
         {
            numberSymbolFlag = TRUE;
         }
         else 
         {
            temp = AsciiToByte(temp);
            if( 0xFF != temp )
            {
               dattmp = temp;
			   changeStep = 1;
			}
		 }
	  }
	  else if( 1 == changeStep )
	  {
         if( ',' == temp)  
		 {
			 if(numberSymbolFlag)                              //保存的是整数
			 {
                dattmp = 0 - dattmp;
				numberSymbolFlag = FALSE;
			 }
			 target[dataSignalIndex++] = (int)(dattmp);
			 dattmp = 0;
			 changeStep = 0;
		 }
		 else if( (0x0D == temp) && (0x0A == buf[i+1]) )
		 {
			 i++;                                               //保存的小数 
			 if(numberSymbolFlag)
			 {
                dattmp = 0 - dattmp;
				numberSymbolFlag = FALSE;
			 }
			 target[dataSignalIndex++] = (int)(dattmp);
			 dattmp = 0;
			 changeStep = 0;
		 }
		 else if( 0x0A == temp )
		 {
			 if(numberSymbolFlag)
			 {
                dattmp = 0 - dattmp;
				numberSymbolFlag = FALSE;
			 }
			 target[dataSignalIndex++] = (int)(dattmp);
			 dattmp = 0;
			 changeStep = 0;
		 }
		 else if('.' == temp)
		 {
		    decimalFlag = TRUE;
			decimalCnt = 0;
		 }
		 else
		 {
			 temp = AsciiToByte(temp);
			 if( 0xFF != temp)
			 {
                if( FALSE == decimalFlag )
				{
                   dattmp = dattmp * 10 + temp;
				}
				else
				{
                   midtmp = temp;
				   decimalCnt++;
				   for( j=0;j<decimalCnt;j++ )
				   {
                      midtmp = midtmp /(float)(10.0);    
				   }
				   dattmp = dattmp + midtmp;
				}
			 }
			 else
			 {
                changeStep = 2;
			 }
         }
      }
      else if( 2 == changeStep )
      {
         if(',' == temp)
            changeStep = 0;
	  }
   }
}
//--------------------------------------------
// get signal data cnt
int CFileHandler::GetSignalDataCnt(unsigned char *buf,int length)
{
   int i;
   int cnt;

   cnt = 0;
   for(i=0;i<length;i++)
   {
      if( ',' == buf[i]  )
         cnt++;
   }
   if( 0 != cnt )
   {
      return cnt;
   }
//-------------------------------------------------
// '\r' = 0x0D ,'\n' = 0x0A 
   for(i=0;i<length;i++)
   {
      if( (0x0d == buf[i]) && (0x0A == buf[i+1]) )
	  {
         i++;                      
         cnt++;
	  }
	  else if( 0x0A == buf[i] )    
	  {
         cnt++;                  //一个0x0A进行换行  \r 回车键，\n 是换行
	  }
   }
   return cnt;
}
//
//
BOOL CFileHandler::CheckNumberStrValid(CString str)
{
   int length;
   int nCount;

   length = str.GetLength();
   nCount = str.Replace('.','.');
   if( (nCount >= 2) || ('.' == str[0]) || ('.' == str[length-1] ))
   {
	   return FALSE;
   }
//--------------------------------------------------
   for( nCount = 0;nCount<str.GetLength();nCount++ )
   {
	   if((str[nCount]>= '0') && (str[nCount]<= '9'))
	   {
		  continue;
	   }
	   else if(str[nCount] == '.')
	   {
		  continue;
	   }
	   else 
	   {
		  return FALSE;
	   }
   }
   return TRUE;
}
