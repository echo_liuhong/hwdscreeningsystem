
#ifndef __USB_BULK_DRIVER_CONFIG_H_H__
#define __USB_BULK_DRIVER_CONFIG_H_H__

#include "windows.h"

#include "dbt.h"
#include "lusb0_usb.h"

//-------------------------------------------------------------
#define USB_BULK_DRIVER_VERSION             _T("Usb bulk driver version:00.01")
//version: 00.01  
//-----------------------------------------------------------------------------
#define USB_ID_VENDOR                       0x1483        // USB ID VENDOR
#define USB_ID_PRODUCT                      0x5751        // USB ID PRODUCT
#define USB_ID_PVN                          0x0100
// Device endpoint(s)
#define USB_BULK_EP0	                    0x00

#define USB_BULK_EP1_IN                     0x81          // wMaxPacketSize:   16  Int
#define USB_BULK_EP1_OUT                    0x01          // wMaxPacketSize:   16  Int
#define USB_BULK_EP2_IN                     0x82          // wMaxPacketSize:   64  Bulk
#define USB_BULK_EP2_OUT                    0x02          // wMaxPacketSize:   64  Bulk

#define MAX_HANDLE_USB_BULK_DEVICE          100

//------------------------------------------------------------
#define READ_BUFFER_LEN                     64

#define MAX_WRITE_BLK_CNT                   16
#define EVERY_WRITE_BUFFER_LEN              READ_BUFFER_LEN
//-----------------------------------------------------------------------------
//THREAD_PRIORITY_TIME_CRITICAL               
//THREAD_PRIORITY_HIGHEST
//THREAD_PRIORITY_ABOVE_NORMAL
//THREAD_PRIORITY_NORMAL
//THREAD_PRIORITY_BELOW_NORMAL
//THREAD_PRIORITY_LOWEST
//THREAD_PRIORITY_IDLE 
//-------------------------------------------------------------
enum{
   NO_NEED_INITIAL_BEFORE_SCAN = 0,
   NEED_INITIAL_BEFORE_SCAN,
};
typedef struct __USB_BULK_DEVICE_INFO__
{
   CString MyDevPathName;                  // device path name
   BOOL MyDevFound;                        // my deivce found
   BOOL DataInSending;                     // USB send flag
   BOOL OpenDeviceFlag;                    // 是否打开设备标志
   BOOL TestFlag;

   unsigned char DeviceNum;        
   unsigned short MyVid;                   //
   unsigned short MyPid;                   //
   unsigned short MyPvn;                   //

   HANDLE hReadHandle;                     //USB read handle
   HANDLE hWriteHandle;                    //USB write handle

}USB_BULK_DEVICE_INFO;
enum{
   NO_DATA_NEED_SEND = 0,
   HAVE_DATA_NEED_SEND,
};
typedef struct
{
   unsigned char SendFlag;
   unsigned char SendBagCnt;
   unsigned char SendBagIndex;

   char WriteContent[MAX_WRITE_BLK_CNT][EVERY_WRITE_BUFFER_LEN];

}USB_BULK_DRIVER_COMMUNICATE_STRUCT;
//-------------------------------------------------------------
//
typedef void (__stdcall* FnCallBack)(char *ribuf,DWORD cnt,LPVOID pData);

class CUsbBulkDriverConfig
{														 
public:
    void  *MPData;
    FnCallBack  FunctionBack;
    volatile BOOL ThreadFlag;

    struct usb_device *pBoard[MAX_HANDLE_USB_BULK_DEVICE];
    usb_dev_handle *pBoardHandle[MAX_HANDLE_USB_BULK_DEVICE];

    OVERLAPPED WriteOverlapped;        // 发送报告用的OVERLAPPED。
    OVERLAPPED ReadOverlapped;         // 接收报告用的OVERLAPPED。

    CWinThread * pReadReportThread;    // 指向读报告线程的指针
    CWinThread * pWriteReportThread;   // 指向写报告线程的指针

    USB_BULK_DEVICE_INFO UsbBulkDeviceInfor;
    USB_BULK_DRIVER_COMMUNICATE_STRUCT UsbBulkDriverCommunicateStruct;

    void InitialUsbBulkRWThread(void);
//--------------------------------------	
	void ScanAllUsbBulkDevice(void);
	void OpenRelativeUsbBulkDevice(void);
    void CloseRelativeUsbBulkDev(void);
//--------------------------------------	
    int BulkWriteBuffer(char *readBuffer,int len);
    int USBBulkReadData(unsigned int nBoardID,int pipenum,char *writebuffer,int len,int waittime);

    CUsbBulkDriverConfig();
    ~CUsbBulkDriverConfig();
protected:
    int ScanUsbBulkDevice(int NeedInit);
    int OpenUsbBulkDev(int DevIndex);
    int CloseUsbBulkDev(int DevIndex);

    int scan_dev(struct usb_device *pBoard[]);
    usb_dev_handle *open_dev(int devNum,struct usb_device *pBoard[]);
    int UsbBulkWriteData(unsigned int nBoardID,int pipenum,char *sendbuffer,int len,int waittime);
    int close_dev(int devNum,struct usb_dev_handle *pBoardHandle[]);
};

#endif   //__USB_BULK_DRIVER_CONFIG_H_H__
//---------------------------------------

