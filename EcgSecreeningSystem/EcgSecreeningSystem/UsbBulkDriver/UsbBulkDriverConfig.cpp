#include "stdafx.h"
#include "windows.h"
#include "UsbBulkDriverConfig.h"
#include <assert.h>

#include "lusb0_usb.h"
#pragma comment(lib, "libusb.lib")
//-----------------------------------------------------------------
#define  READ_WAIT_TIMECNT            100   //200
//-----------------------------------------------------------------
#define  TEST_CLAIM_INTERFACE
//
//
//
UINT ReadReportThread(LPVOID pParam)
{
   CUsbBulkDriverConfig *pApp;
   int Length;
   pApp = (CUsbBulkDriverConfig *)pParam;   //强制转换

   char *readBuffer;
   readBuffer = new char[READ_BUFFER_LEN];

   while( pApp->ThreadFlag )
   {
       if( FALSE == pApp->UsbBulkDeviceInfor.MyDevFound )
           continue;

       Length = pApp->USBBulkReadData(0,USB_BULK_EP1_IN,readBuffer,READ_BUFFER_LEN,READ_WAIT_TIMECNT);
       if( Length > 0 )
       {
          if( NULL != pApp->FunctionBack )
          {
              pApp->FunctionBack(readBuffer,Length,pApp->MPData);
          }
       }
   }
   return 0;
}
//////////////////////////////End of function//////////////////////
//
//
UINT WriteReportThread(LPVOID pParam)
{
   CUsbBulkDriverConfig *pt;
   pt = (CUsbBulkDriverConfig*)pParam; 
   int length;
   unsigned char sendBagIndex;

   while( pt->ThreadFlag )
   {
       if( FALSE == pt->UsbBulkDeviceInfor.MyDevFound )
           continue;
       
	   if( NO_DATA_NEED_SEND == pt->UsbBulkDriverCommunicateStruct.SendFlag )
		   continue;

	   sendBagIndex = pt->UsbBulkDriverCommunicateStruct.SendBagIndex;
	   length = pt->BulkWriteBuffer(pt->UsbBulkDriverCommunicateStruct.WriteContent[sendBagIndex],EVERY_WRITE_BUFFER_LEN);
	   if(length == EVERY_WRITE_BUFFER_LEN)
	   {
		   sendBagIndex++;
		   sendBagIndex = sendBagIndex % MAX_WRITE_BLK_CNT;
		   if(sendBagIndex == pt->UsbBulkDriverCommunicateStruct.SendBagCnt)
		   {
			  pt->UsbBulkDriverCommunicateStruct.SendBagCnt = 0;
			  pt->UsbBulkDriverCommunicateStruct.SendBagIndex = 0;
			  pt->UsbBulkDriverCommunicateStruct.SendFlag = NO_DATA_NEED_SEND;
		   }
		   else
		   {
		      pt->UsbBulkDriverCommunicateStruct.SendBagIndex = sendBagIndex;
		   }
	   }
   }
   return 0;
}
//
// Constructor
//
CUsbBulkDriverConfig::CUsbBulkDriverConfig()
{
   int i;
   MPData = NULL;
   FunctionBack = NULL;
   ThreadFlag = FALSE;

    for(i=0;i<MAX_HANDLE_USB_BULK_DEVICE;i++)
	{
		pBoard[i] = NULL;
		pBoardHandle[i] = NULL;
	}
//--------------------------------------------------------------
    UsbBulkDeviceInfor.MyDevFound = FALSE;
    UsbBulkDeviceInfor.OpenDeviceFlag = FALSE;
	UsbBulkDeviceInfor.TestFlag = FALSE;

	UsbBulkDeviceInfor.DeviceNum = 0;

	UsbBulkDeviceInfor.MyVid = USB_ID_VENDOR;
	UsbBulkDeviceInfor.MyPid = USB_ID_PRODUCT;
	UsbBulkDeviceInfor.MyPvn = USB_ID_PVN;

	UsbBulkDeviceInfor.hReadHandle = INVALID_HANDLE_VALUE;   
    UsbBulkDeviceInfor.hWriteHandle = INVALID_HANDLE_VALUE;  
//-----------------------------------------------------------------
	UsbBulkDriverCommunicateStruct.SendFlag = NO_DATA_NEED_SEND;
	UsbBulkDriverCommunicateStruct.SendBagCnt = 0;
	UsbBulkDriverCommunicateStruct.SendBagIndex = 0;
}
//
// Delete dynamic memory
//
CUsbBulkDriverConfig::~CUsbBulkDriverConfig()
{

}
//
// initial usb hid control
void CUsbBulkDriverConfig::InitialUsbBulkRWThread(void)
{
   if( NULL == MPData )      //20211021
      return;

   WriteOverlapped.Offset = 0;     
   WriteOverlapped.OffsetHigh = 0;
   WriteOverlapped.hEvent = CreateEvent(NULL,TRUE,FALSE,NULL);

   ReadOverlapped.Offset = 0;      
   ReadOverlapped.OffsetHigh = 0;
   ReadOverlapped.hEvent=CreateEvent(NULL,TRUE,FALSE,NULL);

   pWriteReportThread = AfxBeginThread(WriteReportThread,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL);
   if( pWriteReportThread!=NULL )
   {
      pWriteReportThread->ResumeThread();
   }
   ThreadFlag = TRUE;  //20210927 for read thread
   pReadReportThread = AfxBeginThread(ReadReportThread,this, THREAD_PRIORITY_TIME_CRITICAL,0,CREATE_SUSPENDED,NULL);
   if( NULL != pReadReportThread )
   {
       pReadReportThread->ResumeThread();
   }
}
//
//
void CUsbBulkDriverConfig::ScanAllUsbBulkDevice(void)
{
   ScanUsbBulkDevice(NEED_INITIAL_BEFORE_SCAN);
}
//
// scan usb bulk device
//
int CUsbBulkDriverConfig::ScanUsbBulkDevice(int NeedInit)
{
   if( NeedInit )
   {
       usb_init();         // initialize the library 
       usb_find_busses();  // find all busses 
       usb_find_devices(); // find all connected devices 
   }
   UsbBulkDeviceInfor.DeviceNum = scan_dev(pBoard);
   return UsbBulkDeviceInfor.DeviceNum;
}
/**
  * @brief  查找指定的USB设备个数
  * @param  pBoard 设备句柄存放地址
  * @retval 识别到的指定设备个数
  **/
int CUsbBulkDriverConfig::scan_dev(struct usb_device *pBoard[])
{
   struct usb_bus *bus;
   struct usb_device *dev;
   int devnum=0,i=0;
   for (bus = usb_get_busses(); bus; bus = bus->next)
   {
       for(dev = bus->devices; dev; dev = dev->next)
       {
           if( dev->descriptor.idVendor == USB_ID_VENDOR 
            && dev->descriptor.idProduct == USB_ID_PRODUCT )
            {
                pBoard[devnum] = dev;
                devnum++;
            }
        }
    }
	for(i = devnum;i<MAX_HANDLE_USB_BULK_DEVICE;i++)
	{
	   pBoard[i] = NULL;
	}
    return devnum;
}
//
//
void CUsbBulkDriverConfig::OpenRelativeUsbBulkDevice(void)
{
   int DevNum;
   DevNum = ScanUsbBulkDevice(NEED_INITIAL_BEFORE_SCAN);	  	//scan usb bulk device
   if( 0 == DevNum )
	   return;
//--------------------
   int OpenSuccess;
   OpenSuccess = OpenUsbBulkDev(0);
   if( 0 == OpenSuccess )
   {
	  UsbBulkDeviceInfor.MyDevFound = TRUE;
   }
}
/**
  * @brief  打开指定的USB设备
  * @param  devNum	需要打开的设备号
  * @retval 打开状态
  **/
int CUsbBulkDriverConfig::OpenUsbBulkDev(int DevIndex)
{
	pBoardHandle[DevIndex] = open_dev(DevIndex,pBoard);
	if( NULL == pBoardHandle[DevIndex] )
	{
	   return SEVERITY_ERROR;
	}
	else
	{
	   return SEVERITY_SUCCESS;
	}
}
/**
  * @brief  打开指定的USB设备
  * @param  devNum	需要打开的设备号
  * @param	pBoard	设备句柄存放地址
  * @retval 已经打开了的设备句柄
  **/
usb_dev_handle * CUsbBulkDriverConfig::open_dev(int devNum,struct usb_device *pBoard[])
{
   struct usb_bus *bus;
   struct usb_device *dev;
   int devnum=0;
   for( bus = usb_get_busses(); bus; bus = bus->next )
   {
       for(dev = bus->devices; dev; dev = dev->next)
       {
          if( dev->descriptor.idVendor == UsbBulkDeviceInfor.MyVid
           && dev->descriptor.idProduct == UsbBulkDeviceInfor.MyPid )
          {
             if( devnum == devNum ) 
			 {
                return usb_open(pBoard[devNum]);
			 }
		     devnum++;
           }
       }
    }
    return NULL;
}
/**
  * @brief  USB Bulk读数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  readbuffer 读取数据缓冲区
  * @param  len 读取数据字节数
  * @param  waittime 超时时间
  * @retval 读到的数据字节数
  */
int CUsbBulkDriverConfig::USBBulkReadData(unsigned int nBoardID,int pipenum,char *readbuffer,int len,int waittime)
{
   int ret=0;
   if( NULL == pBoardHandle[nBoardID] )
   {
      return SEVERITY_ERROR;
   }
#ifdef TEST_SET_CONFIGURATION
   if(usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
   {
       usb_close(pBoardHandle[nBoardID]);
       return SEVERITY_ERROR;
   }
#endif

#ifdef TEST_CLAIM_INTERFACE
   if(usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
   {
       //usb_close(pBoardHandle[nBoardID]);
	   //CloseUsbBulkDev(nBoardID);
	   UsbBulkDeviceInfor.TestFlag = TRUE;
       return SEVERITY_ERROR;
   }
#endif

#if TEST_ASYNC
   // Running an async read test
   ret = transfer_bulk_async(pGinkgoBoardHandle[nBoardID], pipenum, sendbuffer, len, waittime);
#else
   ret = usb_bulk_read(pBoardHandle[nBoardID], pipenum, readbuffer, len, waittime);
#endif
#ifdef TEST_CLAIM_INTERFACE
   usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
   return ret;
}
//
//
int CUsbBulkDriverConfig::BulkWriteBuffer(char *writeBuffer,int len)
{
   return UsbBulkWriteData(0,USB_BULK_EP2_OUT,writeBuffer,len,10);
}
/**
  * @brief  USB Bulk端点写数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  sendbuffer 发送数据缓冲区
  * @param  len 发送数据字节数
  * @param  waittime 超时时间
  * @retval 成功发送的数据字节数
  **/
int CUsbBulkDriverConfig::UsbBulkWriteData(unsigned int nBoardID,int pipenum,char *sendbuffer,int len,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL)
	{
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if(usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
        return SEVERITY_ERROR;
    }
#endif

#if TEST_ASYNC
    // Running an async write test
    ret = transfer_bulk_async(dev, pipenum, sendbuffer, len, waittime);
#else
	ret = usb_bulk_write(pBoardHandle[nBoardID], pipenum, sendbuffer, len, waittime);
	/*if((len%64) == 0){
		usb_bulk_write(pBoardHandle[nBoardID], pipenum, sendbuffer, 0, waittime);
	}*/
#endif
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}
//
//
void CUsbBulkDriverConfig::CloseRelativeUsbBulkDev()
{
   int value;
   value = CloseUsbBulkDev(0);
   if( SEVERITY_SUCCESS == value)
   {
      UsbBulkDeviceInfor.MyDevFound = FALSE;
   }
}
//
//
int CUsbBulkDriverConfig::CloseUsbBulkDev(int DevIndex)
{
	return close_dev(DevIndex,pBoardHandle);
}
/**
  * @brief  关闭指定的USB设备
  * @param  devNum	需要关闭的设备号
  * @param  pBoardHandle	开打了的设备句柄
  * @retval 已经打开了的设备句柄
  **/
int CUsbBulkDriverConfig::close_dev(int devNum,struct usb_dev_handle *pBoardHandle[])
{
   struct usb_bus *bus;
   struct usb_device *dev;
   int devnum = 0;
   for( bus = usb_get_busses(); bus; bus = bus->next)
   {
      for(dev = bus->devices; dev; dev = dev->next)
      {
          if( dev->descriptor.idVendor == UsbBulkDeviceInfor.MyVid
           && dev->descriptor.idProduct == UsbBulkDeviceInfor.MyPid)
          {
              if( devnum == devNum )
              {
                  return usb_close(pBoardHandle[devNum]);
              }
		      devnum++;
          }
      }
   }
   return NULL;
}

