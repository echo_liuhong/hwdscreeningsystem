
// EcgSecreeningSystemDlg.h : 头文件
//
#include "UsbBulkDriverConfig.h"
#include "UsbBulkProtocolHandler.h"
#include "DataProtocolHandler.h"
#include "DataHandle.h"

#include "RapidScreenSimplifyDlg.h"
#include "DeviceRecoverSimplifyDlg.h"
#include "RapidScreenDetailedDlg.h"
#include "DeviceRecoverDetailedDlg.h"
#include "DeviceConfigDlg.h"

#include "EcgShowDlg.h"

#include "CrcCheck.h"
#include "ProjDefine.h"
#include "DrawPicHandler.h"

#include "EncipherHandler.h"
#include "CrcCheck.h"


#pragma once

//-------------------------------------------------------------------------
#define MAX_INSERT_DLG_COUNT                            5
//----------------------------
#define RAPID_SCREEN_SIMPLIFY_INDEX                     0
#define DEVICE_RECOVERY_SIMPLIFY_INDEX                  1
#define RAPID_SCREEN_DETAILED_INDEX                     2
#define DEVICE_RECOVERY_DETAILED_INDEX                  3
#define DEVICE_CONFIG_DETAILED_INDEX                    4

#define MAX_INSERT_SHOW_DLG_COUNT                       1
#define ECG_DISPLAY_INDEX                               0
//-------------------------------------------
enum {
	INVALID_CONNECT_STEP = 0,
	WRITE_READ_FIX_ADDR,
	CHECK_BULK_SERIAL_NUMBER,
	ACTIVE_USB_BULK_HANDLE,
	READ_USB_BULK_DATA_MODE,
};
typedef struct
{
   int InsertItemCnt;                                   // insert item index
   int SelectIndex;                                     // select index
   int *DlgId;                                          // dlg's id
   CDialog *DlgPt[MAX_INSERT_DLG_COUNT];                // dlg pt            
   CString *TabNameStr;                                 // insert item str

   CRapidScreenSimplifyDlg *RapidScreenSimplifyDlg;
   CDeviceRecoverSimplifyDlg *DeviceRecoverSimplifyDlg;
   CRapidScreenDetailedDlg *RapidScreenDetailedDlg;
   CDeviceRecoverDetailedDlg *DeviceRecoverDetailedDlg;
   CDeviceConfigDlg *DeviceConfigDlg;
//------------------------------------------------------------
   int ShowInsertItemCnt;
   int ShowSelectIndex;
   int *ShowDlgId;
   CDialog *ShowDlgPt[MAX_INSERT_SHOW_DLG_COUNT];            // dlg pt      
   CString *ShowTabNameStr;                              // insert item str
   CEcgShowDlg *EcgShowDlg;

}CTABCTRL_PARAMETER;
//------------------------------------
typedef struct
{
   BOOL WindowInitialFlag;
   int CurDlgStatus;

   CRect myWindowRect;
   CRect ShowRect;

}VIEW_WINDOW_INFOR;
//------------------------------------
typedef struct
{
//  20220728
//	BOOL HexShowFlag;
//	BOOL DisplayStatusFlag;
//	int Cnt;

	BOOL HexShowFlag;
	BOOL DisplayStatusFlag;
	unsigned char activeVal;
	int ProgramStartTimeCnt;
	int FirstConnectHandleStep;
	int Cnt;
	int FlushIntervalCnt;

}BULK_RECEIVE_CONTENT_SHOW_STRUCT;
//------------------------------------
// CEcgSecreeningSystemDlg 对话框
class CEcgSecreeningSystemDlg : public CDialogEx
{
// 构造
public:
    VIEW_WINDOW_INFOR ViewWindowInfor;
    CTABCTRL_PARAMETER CTabCtrlParameter;
//----------------------------------------------------------
    CUsbBulkDriverConfig UsbBulkControl;
	CEncipherHandler EncipherHandler;

    void PollingOpenUsbBulkDevice(void);
    void InitialUsbBulkDriverPart(void);
    void ConnectCommunicateHandler(void);

	void GetUsbBulkFixSerialNumHandler(void);
	void CheckUsbBulkFixSerialNumHandler(void);
	void ActiveUsbBulkDevReadHandler(void);
	void InActiveUsbBulkDevReadHandler(void);
//----------------------------------------------------------
    CUsbBulkProtocolHandler BulkProtocolPart;
    friend void __stdcall UsbBulkDataProc(char *buffer, DWORD cnt, LPVOID pData);
    void ReceiveBufferData(char *buffer, DWORD cnt);

    CDataProtocolHandler ProtocolHandler;
    BOOL HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
    void HandleDisplayDataCmd(COMMUNICATE_BAG_STRUCT *receivebag);
    BOOL TransformCommunicateBagToDiffDlg(COMMUNICATE_BAG_STRUCT *receivebag);
    void ShowAllReceiveContent(unsigned char *ribuf, DWORD cnt);

	void HandleGetFixSerialNumCmd(unsigned char *ribuf, DWORD cnt);
	void HandleFixSerialNumValidtyCmd(unsigned char *ribuf, DWORD cnt);
	void HandleActiveUsbBulkCmd(unsigned char *ribuf, DWORD cnt);
	void HandleGetBleDevInforCmd(unsigned char *ribuf, DWORD cnt);
	void GetAllScanBleDeviceInfor(COMMUNICATE_BAG_STRUCT *receivebag);
//----------------------------------------------------------
	BULK_RECEIVE_CONTENT_SHOW_STRUCT BulkShowContent;
	void InitialBulkReceiveContentStruct();
	void InitialAllControl(void);
	void InitialDlgWindowInfo(void);

   void InitialTabControl(void);
   void ChangeTabControlShow(void);
   void InitialEcgAndUserTabControl(void);
   void ChangeEcgAndUserTabControlShow(void);
   BOOL HandleKeyPressDown(MSG *pMsg);

   void SendBufToBleDevice(void);
   void SendBufToUsbBulkDev(void);
   void SendBuffer(unsigned char type, unsigned char *sbuf, DWORD cnt);
//-----------------------------------------------------------
public:
	CEcgSecreeningSystemDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ECGSECREENINGSYSTEM_DIALOG };
#endif
public:
	BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
// 实现
protected:
	HICON m_hIcon;
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnDeviceChange(WPARAM nEventType, LPARAM dwData);
   afx_msg void OnTcnSelchangeChangeDeviceHandleType(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedConnectBleDevice();
	afx_msg void OnBnClickedDisconnectEcgDevice();
	afx_msg void OnBnClickedClearBtn();
	afx_msg void OnBnClickedPauseBtn();
	afx_msg void OnBnClickedHexshowflag();
};
