
#include "stdafx.h"
#include "ExcelHandler.h"
#include <assert.h>

//Excel.cpp
#include "stdafx.h"
#include <tchar.h>
 
//
//
CExcelHandler::CExcelHandler()
{

}
//
//
CExcelHandler::~CExcelHandler()
{
}

void CExcelHandler::CreateExcelListHead(int ActiveOrRecoverFlag,CString sheetName)
{
   SetSheetName(1, sheetName);
   if(INDEX_ACTIVE_DEVICE_LIST_NAME == ActiveOrRecoverFlag )
   {
      //active device
      SetSheetTableContent(1, 1, _T("序号"));
      SetSheetTableContent(1, 2, _T("设备号"));
      SetSheetTableContent(1, 3, _T("开始时间"));
      SetSheetTableContent(1, 4, _T("结束时间"));
      SetSheetTableContent(1, 5, _T("总块数"));
      SetSheetTableContent(1, 6, _T("姓名"));
      SetSheetTableContent(1, 7, _T("性别"));
      SetSheetTableContent(1, 8, _T("年龄"));
      SetSheetTableContent(1, 9, _T("身高"));
      SetSheetTableContent(1, 10, _T("体重"));
      SetSheetTableContent(1, 11, _T("手机号"));
   }
   else if(INDEX_RECOVER_DEVICE_DATA_LIST_NAME == ActiveOrRecoverFlag)
   {
      //recover 
      SetSheetTableContent(1, 1, _T("序号"));
      SetSheetTableContent(1, 2, _T("设备号"));
      SetSheetTableContent(1, 3, _T("开始时间"));
      SetSheetTableContent(1, 4, _T("结束时间"));
      SetSheetTableContent(1, 5, _T("总块数"));
      SetSheetTableContent(1, 6, _T("姓名"));
      SetSheetTableContent(1, 7, _T("性别"));
      SetSheetTableContent(1, 8, _T("年龄"));
      SetSheetTableContent(1, 9, _T("身高"));
      SetSheetTableContent(1, 10, _T("体重"));
      SetSheetTableContent(1, 11, _T("手机号"));
      SetSheetTableContent(1, 12, _T("文件路径"));
   }
}

 
BOOL CExcelHandler::InitExcel()
{
   LPDISPATCH lpdisp = NULL;
   //1.创建Excel实例
   if (!App.CreateDispatch(_T("Excel.Application"), NULL))
   {
      exit(-1);
      return FALSE;
   }
   return TRUE;	
}
//
// get workd container
void CExcelHandler::GetWorksContainer(void)
{
   COleVariant
      covTrue((short)TRUE),
      covFalse((short)FALSE),
      covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR);

   Books.AttachDispatch(App.get_Workbooks());
   Book.AttachDispatch(Books.Add(covOptional));
   sheets.AttachDispatch(Book.get_Worksheets());
}
//
// load all cells
void CExcelHandler::LoadAllCells(CString fileName)
{
   LPDISPATCH lpdisp = NULL;
   Books.AttachDispatch(App.get_Workbooks(), true);
   lpdisp = Books.Add(COleVariant(CString(fileName)));

   if (lpdisp)
   {
      Book.AttachDispatch(lpdisp);
      sheets.AttachDispatch(Book.get_Worksheets());
   }
   else
   {
      return;
   }
   sheet.AttachDispatch(sheets.get_Item(COleVariant((short)1)));
   CurrentRange.AttachDispatch(sheet.get_Cells(), TRUE);				//加载所有单元格
}
//
// get row cnt
int CExcelHandler::GetRowCnt(void)
{
   CRange usedRange;
   CRange range;
   int nRow;
   usedRange.AttachDispatch(sheet.get_UsedRange(), true);
   range.AttachDispatch(usedRange.get_Rows(),true);
   nRow = range.get_Count();

   usedRange.ReleaseDispatch();
   range.ReleaseDispatch();
   return nRow;
}
//
//
void CExcelHandler::SetSheetName(int tabID, CString sheetName)
{
   sheet.AttachDispatch(sheets.get_Item(COleVariant((short)tabID)));	//获取sheet1
   sheet.put_Name(sheetName);	                                       //设置sheet1名字
}
//
//
void CExcelHandler::SetSheetTableContent(long rowIndex,long colIndex, CString str)
{
   CurrentRange.AttachDispatch(sheet.get_Cells(), TRUE);
   CurrentRange.put_Item(COleVariant((long)rowIndex), COleVariant((long)colIndex), COleVariant(str));
}
//
//
void CExcelHandler::SaveExcelFile(CString fileName)
{
   CurrentRange.AttachDispatch(sheet.get_UsedRange());//加载已使用的单元格
   CurrentRange.put_WrapText(COleVariant((long)1));   //设置文本自动换行

   //5.设置对齐方式
   //水平对齐：默认 1 居中 -4108， 左= -4131，右=-4152
   //垂直对齐：默认 2 居中 -4108， 左= -4160，右=-4107
   CurrentRange.put_VerticalAlignment(COleVariant((long)-4108));
   CurrentRange.put_HorizontalAlignment(COleVariant((long)-4108));

   Book.SaveCopyAs(COleVariant(fileName)); //保存
   Book.put_Saved(TRUE);
}
//
//
void CExcelHandler::release()
{
   CurrentRange.ReleaseDispatch();
   sheet.ReleaseDispatch();
   sheets.ReleaseDispatch();
   Book.ReleaseDispatch();
   Books.ReleaseDispatch();
   App.Quit();
   App.ReleaseDispatch();
}
 
 
