//#include "windows.h"

#ifndef __OFFICE_EXCEL_HANDLER_H_H__
#define __OFFICE_EXCEL_HANDLER_H_H__

#include "CApplication.h"
#include "CFont0.h"
#include "CRange.h"
#include "CWorkbook.h"
#include "CWorkbooks.h"
#include "CWorksheet.h"
#include "CWorksheets.h"

#define  OFFICE_HANDLER_VERSION_NAME              _T("Office excel handler Version: 1.00:00")
//------------------------------------------------------------
#define  INDEX_ACTIVE_DEVICE_LIST_NAME            0
#define  INDEX_RECOVER_DEVICE_DATA_LIST_NAME      1

class CExcelHandler
{
public:
   CApplication App;        // 创建应用程序实例
   CWorkbooks Books;        // 工作簿，多个Excel文件
   CWorkbook Book;          // 单个工作簿
   CWorksheets sheets;      // 多个sheet页面
   CWorksheet sheet;        // 单个sheet页面
   CRange CurrentRange;     // 操作单元格

   //void CreateNewExcelFile(CString excelFileName);

   void CreateExcelListHead(int ActiveOrRecoverFlag, CString sheetName);

//-------------------------------------------
   BOOL InitExcel();
   void GetWorksContainer(void);
   void LoadAllCells(CString fileName);
   int GetRowCnt(void);

   void SetSheetName(int tabID,CString sheetName);
   void SetSheetTableContent(long rowIndex, long colIndex, CString str);
   void SaveExcelFile(CString fileName);
   void release();
private:
 
protected:
 
public:
   CExcelHandler();
	virtual ~CExcelHandler();
public:
protected:

};
#endif