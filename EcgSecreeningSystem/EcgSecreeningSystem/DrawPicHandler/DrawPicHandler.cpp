
#include "stdafx.h"
#include "DrawPicHandler.h"
#include "EcgShowDlg.h"
#include <assert.h>

//------------------------------------------------------------------
#define   BATTERY_PERCENTAGE_INVALID_STR    _T(",Battery Percentage:Invalid,")
#define   DONGLE_CAPACITY_INVALID_STR       _T(",Dongle capacity:Invalid,")

const CString CDrawPicHandler::EcgSingalLeadName[MAX_ECG_LEAD_CNT] = 
{
   LEAD_I_SAVE_NAME,LEAD_II_SAVE_NAME,LEAD_III_SAVE_NAME,
   LEAD_AVR_SAVE_NAME,LEAD_AVL_SAVE_NAME,LEAD_AVF_SAVE_NAME,
   LEAD_V1_SAVE_NAME,LEAD_V2_SAVE_NAME,LEAD_V3_SAVE_NAME,
   LEAD_V4_SAVE_NAME,LEAD_V5_SAVE_NAME,LEAD_V6_SAVE_NAME,
   LEAD_V7_SAVE_NAME,LEAD_V8_SAVE_NAME,LEAD_V9_SAVE_NAME,
   LEAD_V3R_SAVE_NAME,LEAD_V4R_SAVE_NAME,LEAD_V5R_SAVE_NAME,
};
const int CDrawPicHandler::ZoomTable[ZOOM_RATIO_RANGE] = 
{
   ZOOM_OUT_MULTIPLE_0,ZOOM_OUT_MULTIPLE_1,ZOOM_OUT_MULTIPLE_2,ZOOM_OUT_MULTIPLE_3, 
   ZOOM_OUT_MULTIPLE_4,ZOOM_OUT_MULTIPLE_5,ZOOM_OUT_MULTIPLE_6,ZOOM_OUT_MULTIPLE_7, 
   ZOOM_OUT_MULTIPLE_8,ZOOM_OUT_MULTIPLE_9,ZOOM_OUT_MULTIPLE_10,ZOOM_OUT_MULTIPLE_11,
   ZOOM_OUT_MULTIPLE_12,ZOOM_OUT_MULTIPLE_13,ZOOM_OUT_MULTIPLE_14,
};
//---------------------------------------------------------------------------
// constructor function
CDrawPicHandler::CDrawPicHandler()
{
   ParentPt =NULL;
   PicDriver.m_hwnd = NULL; 

   CurShowLeadIndex = 0;

   PicDriver.WaveContentProperty.ResolutionValue = DEFAULT_RESOLUTION_VALUE;
   PicDriver.WaveContentProperty.NoGridZoomDenominator = ZoomTable[DEFAULT_ZOOM_OUT_INDEX];
//------------------------------------------------
   ShowPlaybackFlag = REAL_TIME_DISPLAY_SIGNAL;   //default
   ShowStartPos = 0;
}
//-----------------------------------------------
// destructor function
CDrawPicHandler::~CDrawPicHandler()
{

}
//-----------------------------------------------
// get every show len
void CDrawPicHandler::GetEveryShowLen(void)
{
   int xdirectcnt,ydirectcnt;
   int x_DrawLen;

   xdirectcnt = PicDriver.AreaSizeDetails.XDirectCnt;
   ydirectcnt = PicDriver.AreaSizeDetails.YDirectCnt;

   PicDriver.AreaSizeDetails.XDrawLen = PicDriver.AreaSizeDetails.DrawMappingRect.Width();
   PicDriver.AreaSizeDetails.YDrawLen = PicDriver.AreaSizeDetails.DrawMappingRect.Height();
   PicDriver.AreaSizeDetails.X_EveryLen = PicDriver.AreaSizeDetails.DrawMappingRect.Width();
   PicDriver.AreaSizeDetails.Y_EveryLen = PicDriver.AreaSizeDetails.DrawMappingRect.Height() / PicDriver.AreaSizeDetails.YDirectCnt;  //20210813

   x_DrawLen = PicDriver.AreaSizeDetails.X_EveryLen;
   if( FALSE == PicDriver.DisplayWaveContent.DistribufFlag )
   {
       //PicDriver.DisplayWaveContent.ShowCnt = x_DrawLen;
	    PicDriver.DisplayWaveContent.WaveType = new unsigned int[x_DrawLen];    //20210812
       PicDriver.DisplayWaveContent.ShowBuf = new int[x_DrawLen];

	    PicDriver.DisplayWaveContent.DistribufFlag = TRUE;
   }
   else
   {   delete [] PicDriver.DisplayWaveContent.ShowBuf;
       delete [] PicDriver.DisplayWaveContent.WaveType;

       //PicDriver.DisplayWaveContent.ShowCnt = x_DrawLen;
	    PicDriver.DisplayWaveContent.WaveType = new unsigned int[x_DrawLen];    //20210812
       PicDriver.DisplayWaveContent.ShowBuf = new int[x_DrawLen];              

 	   PicDriver.DisplayWaveContent.DistribufFlag = TRUE;
   }
}
//-------------------------------------------
// draw signal wave
void CDrawPicHandler::DrawSignalWave(void)
{
   if (NULL == ParentPt)
      return;

   CEcgShowDlg *pData;
   pData = (CEcgShowDlg *)ParentPt;
   //-------------------------------
   int *source;
   int showcnt;
   int maxsavecnt;

   source = &pData->EcgLeadSignal.LeadSignalData[0].DataInt;
   showcnt = pData->EcgLeadSignal.ShowCnt;
   maxsavecnt = pData->EcgLeadSignal.MaxSavePtCnt;

   GetDrawSignalWaveContent(maxsavecnt, source, showcnt);
   DrawAllWave();
}
//----------------------------------------------
// configure draw property
/*
void CDrawPicHandler::ConfigureDrawProperty(int yindex)
{
   if( NULL == ParentPt )	 
	   return;

   CEcgWaveShowView *pData;
   pData =(CEcgWaveShowView *)ParentPt;
//-------------------------------------------------------
   unsigned char batteryPercentage;
   unsigned char dongleLeftCapacity;
   CString str;
   CString leadnamestr;
   int acceVal;
   unsigned char saveStatus;
   unsigned int saveLefttime;
   unsigned char lefthour,leftminute,leftsecond;
   
   PicDriver.WaveContentProperty.Y_DirectIndex = yindex;
   if(TRUE == PicDriver.FilterDisplayFlag && (1 == yindex) )
   {
	   PicDriver.WaveContentProperty.ShowLeadInforStr = _T("filter wave:");
	   return;
   }


   PicDriver.WaveContentProperty.HorizontalCnt = AMPLITUDE_DIVIDE_CNT;
   PicDriver.WaveContentProperty.ResolutionValue = DEFAULT_RESOLUTION_VALUE;

   leadnamestr = EcgSingalLeadName[CurShowLeadIndex + yindex];
//------------------------------------------------------------------
   unsigned short heartRateCnt;
   heartRateCnt = pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.HeartRataCnt;
   if( heartRateCnt  & HEART_RATE_NOT_READY_FLAG )
   {
      str = _T(",Heartrate not ready.");
   }
   else if( heartRateCnt & INVALID_ECG_SAMPLERATE_VAL )
   {
      str = _T(",invalid ecg samplerate value,");
   }
   else
   {
      str.Format(_T(",Heartrate = %d"),pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.HeartRataCnt);
   }
   
   leadnamestr = leadnamestr + str;
//-------------------------------------------------------------------
   batteryPercentage = pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.BatteryPercentage;
   if( batteryPercentage > 100 )
   {
      str = BATTERY_PERCENTAGE_INVALID_STR;
	  str = str + _T(",");
   }
   else
   {
	  str.Format(_T(",battery Percentage = %d%% "),batteryPercentage);   //格式化不能单独出现"%"
   }
   leadnamestr = leadnamestr + str;
//-------------------------------------------------------------------
   dongleLeftCapacity = pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.DongleLeftCapacity;
   if( dongleLeftCapacity > 100 )
   {
      str = DONGLE_CAPACITY_INVALID_STR;
	  str = str + _T(",");
   }
   else
   {
	  str.Format(_T(",dongle left capacity = %d%%,"),(100 - dongleLeftCapacity));   //格式化不能单独出现"%"
   }
   leadnamestr = leadnamestr + str;
//-------------------------------------------------------------------
   if( 1 == pData->SignalDataHandler.AccelerationVal.AcceFlag)
   {
       acceVal = pData->SignalDataHandler.AccelerationVal.X_Acceleration.DataShort;
       str.Format(_T("X_acc = %d,"),acceVal);
       leadnamestr = leadnamestr + str;

       acceVal = pData->SignalDataHandler.AccelerationVal.Y_Acceleration.DataShort;
       str.Format(_T("Y_acc = %d,"),acceVal);
       leadnamestr = leadnamestr + str;

       acceVal = pData->SignalDataHandler.AccelerationVal.Z_Acceleration.DataShort;
       str.Format(_T("Z_acc = %d,"),acceVal);
       leadnamestr = leadnamestr + str;
   }
//-------------------------------------------------------------------
   saveStatus = pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.saveStatus;
   if( SAVE_ECG_IS_PROGRESS == saveStatus )
   {
	  saveLefttime = pData->SignalDataHandler.EcgLeadSignal.DeviceSpecialStatus.saveLeftTime;
	  lefthour = saveLefttime /3600;
	  leftminute = (saveLefttime%3600/60);
	  leftsecond = (saveLefttime%60);
	  str.Format(_T("保存剩余时间:%02d - %02d - %02d"),lefthour,leftminute,leftsecond);
	  leadnamestr = leadnamestr + str;
   }
   else if( SAVE_ECG_IS_COMPLETE == saveStatus )
   {
	  str.Format(_T("保存完成"));
	  leadnamestr = leadnamestr + str;
   }
   else
   {

   }
//-------------------------------------------------------------------
   PicDriver.WaveContentProperty.ShowLeadInforStr = leadnamestr;
}*/
//--------------------------------------------------
// draw signal for all ecg signal
void CDrawPicHandler::GetDrawSignalWaveContent(int maxsavecnt,int *source,int sourcelen)
{
   int i;
   int drawindex,drawcnt,showindex;
   int x_everylen;
   int *lead_signal_buf;

   int startindex;
   lead_signal_buf = source;
//-------------------------------------------------------------------------
   x_everylen = PicDriver.AreaSizeDetails.X_EveryLen;
   startindex = sourcelen;
   DisplayBuf.ShowLength = 0;
   if( 0 == startindex )
      return;

   if( startindex <= x_everylen )
   {
	   DisplayBuf.ShowLength = startindex;
      for(i=0;i<startindex;i++)
	   {
         PicDriver.DisplayWaveContent.ShowBuf[i] = lead_signal_buf[i];
	   }
	   PicDriver.DisplayWaveContent.ShowCnt = DisplayBuf.ShowLength;       //20181127
	   return;
   }
   if(startindex > x_everylen )
   {
	  DisplayBuf.ShowLength = x_everylen;

     drawcnt = startindex % x_everylen;
	  if( startindex>maxsavecnt )
	  {
        drawindex = 0;
	  }
	  else
	  {
        drawindex = (startindex/x_everylen) * x_everylen;
	  }

	  showindex = 0;
	  for(i=0;i<drawcnt;i++)
     {
        PicDriver.DisplayWaveContent.ShowBuf[showindex] = lead_signal_buf[drawindex+i];
		  showindex++;
     }
	  for(i = 0;i<DRAW_BLANK_INTERVAL;i++)
	  {
	     PicDriver.DisplayWaveContent.ShowBuf[showindex] = 0x7FFFFFFF; 
		  showindex++;
         
		 if( showindex >= x_everylen)
		 {
		     PicDriver.DisplayWaveContent.ShowCnt = DisplayBuf.ShowLength;       //20181127
		 	  return;
		 }
	  }

	  drawindex = (startindex/x_everylen-1) * x_everylen;
	  drawindex = drawindex + drawcnt + DRAW_BLANK_INTERVAL;
	  while(1)
	  {
         PicDriver.DisplayWaveContent.ShowBuf[showindex] = lead_signal_buf[drawindex];
         drawindex++;
         showindex++;
         if( showindex >= x_everylen )
	      {
		       PicDriver.DisplayWaveContent.ShowCnt = DisplayBuf.ShowLength;       //20181127
             return;
         }
	   }
   }
}
//-----------------------------------------------------------------
// get draw playback signal
/*
void CDrawPicHandler::GetDrawPlaybackSignal(int index,int *source)
{  
   if( NULL == ParentPt )	 
       return;

   CEcgWaveShowView *pData;
   pData =(CEcgWaveShowView *)ParentPt;

   int i;
   int x_everylen;
   int *lead_signal_buf;

   lead_signal_buf = &source[index];
   x_everylen = PicDriver.AreaSizeDetails.XDrawLen;

   PicDriver.DisplayWaveContent.ShowFeatureFlag = FALSE;

   if( 0 != pData->SignalDataHandler.EcgSignalPlayback.DataLen )
   {
       DisplayBuf.ShowLength = x_everylen;
       for(i=0;i<x_everylen;i++)
       {
          PicDriver.DisplayWaveContent.ShowBuf[i] = lead_signal_buf[i];
       }
   }
}*/
/*
//-----------------------------------------------------------------
// get draw playback signal
void CDrawPicHandler::GetDrawPlaybackSignalWithFeature(int index,int *source,unsigned int *typeSource)
{  
   if( NULL == ParentPt )	 
       return;

   CEcgWaveShowView *pData;
   pData =(CEcgWaveShowView *)ParentPt;

   int i;
   int x_everylen;
   int *lead_signal_buf;
   unsigned int *typeBuf;

   lead_signal_buf = &source[index];
   typeBuf = &typeSource[index];
   x_everylen = PicDriver.AreaSizeDetails.XDrawLen;

   PicDriver.DisplayWaveContent.ShowFeatureFlag = TRUE;

   if( 0 != pData->SignalDataHandler.EcgSignalPlayback.DataLen )
   {
       DisplayBuf.ShowLength = x_everylen;
       for(i=0;i<x_everylen;i++)
       {
          PicDriver.DisplayWaveContent.ShowBuf[i] = lead_signal_buf[i];
		  PicDriver.DisplayWaveContent.WaveType[i] = typeBuf[i];
       }
   }
}
*/
//-----------------------------------------
// Draw all wave
void CDrawPicHandler::DrawAllWave(void)
{
   BOOL ecgShowFlag;
   HPEN framepen;
   HPEN amplitudepen;
   HPEN midpen;
   HPEN drawpen;
   HPEN zeropen;
   HPEN energypen;

   HBRUSH brush;

   ecgShowFlag = DisplayBuf.ShowFlag;
//--------------------------------------------------------------------------
   framepen = CreatePen(PS_SOLID,DRAW_PEN_WIDTH,DRAW_FRAME_COLOR);
   amplitudepen = CreatePen(PS_SOLID,DRAW_AMPLITUDE_LINE_WIDTH,DRAW_AMPLITUDE_LINE_COLOR);
   midpen = CreatePen(PS_SOLID,DRAW_MIDLINE_WIDTH,DRAW_MIDLINE_COLOR);
   drawpen = CreatePen(PS_SOLID,DRAW_PEN_WIDTH,DRAW_WAVE_COLOR);
   zeropen = CreatePen(PS_SOLID,DRAW_ZERO_LINE_WIDTH,DRAW_ZERO_LINE_COLOR);
   energypen = CreatePen(PS_SOLID,DRAW_ENERGY_LINE_WIDTH,DRAW_ENERGY_LINE_COLOR);

   brush = CreateSolidBrush(WHITE_COLOR); 
//--------------------------------------------------------------------------
   PicDriver.ShowHandlerContent.FramePen = &framepen;
   PicDriver.ShowHandlerContent.MidPen = &midpen;
   PicDriver.ShowHandlerContent.AmplitudePen = &amplitudepen;
   PicDriver.ShowHandlerContent.DrawPen = &drawpen;
   PicDriver.ShowHandlerContent.ZeroPen = &zeropen;
   PicDriver.ShowHandlerContent.EnergyPen = &energypen;

   PicDriver.ShowHandlerContent.Brush = &brush;
//--------------------------------------------------------------------------
   PicDriver.DrawAllSignalWave();
//--------------------------------------------------------------------------
   DeleteObject(energypen);
   DeleteObject(zeropen);
   DeleteObject(framepen);
   DeleteObject(amplitudepen);
   DeleteObject(midpen);
   DeleteObject(drawpen);
   DeleteObject(brush);
}
//
//
void CDrawPicHandler::ChangeFilterDisplay(BOOL filterDisplayFlag)
{
   PicDriver.FilterDisplayFlag = filterDisplayFlag;
   if( TRUE == PicDriver.FilterDisplayFlag )
   {
      PicDriver.AreaSizeDetails.YDirectCnt = 2;
   }
   else
   {
      PicDriver.AreaSizeDetails.YDirectCnt = 1;
   }

}