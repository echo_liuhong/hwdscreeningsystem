#include "windows.h"

#ifndef __DRAW_PIC_DRIVER_H_H__
#define __DRAW_PIC_DRIVER_H_H__

#define DRAW_PICTURE_DRIVER_VERSION_NAME      "DRAW Picture driver version: 00:02"
//-------------------------------------------------------------------------------
#define AMPLITUDE_DIVIDE_CNT                  6

#define RED_COLOR                             ((COLORREF)(0x000000FF))
#define PINK2_COLOR                           ((COLORREF)(0x00A0A0FF))
#define PINK1_COLOR                           ((COLORREF)(0x00D0D0FF))
#define PINK_COLOR                            ((COLORREF)(0x00E8E8FF)) //((COLORREF)(0x008080FF))  
#define GREEN_COLOR                           ((COLORREF)(0x0000FF00))
#define BLUE_COLOR                            ((COLORREF)(0x00FF0000))
#define VIOLET_COLOR                          ((COLORREF)(0x00800080))
#define YELLOW_COLOR                          ((COLORREF)(0x0000FFFF))
#define BLACK_COLOR                           ((COLORREF)(0x00000000))
#define WHITE_COLOR                           ((COLORREF)(0x00FFFFFF))

#define DRAW_FRAME_COLOR                      PINK1_COLOR
#define DRAW_WAVE_COLOR                       BLACK_COLOR
#define DRAW_POWER_COLOR                      VIOLET_COLOR
#define DRAW_MIDLINE_COLOR                    PINK_COLOR
#define DRAW_AMPLITUDE_LINE_COLOR             PINK1_COLOR        //VIOLET_COLOR
#define DRAW_ZERO_LINE_COLOR                  PINK2_COLOR
#define DRAW_ENERGY_LINE_COLOR                VIOLET_COLOR

#define DRAW_PEN_WIDTH                        1
#define DRAW_POWER_WIDTH                      4
#define DRAW_MIDLINE_WIDTH                    1
#define DRAW_AMPLITUDE_LINE_WIDTH             2
#define DRAW_ZERO_LINE_WIDTH                  1
#define DRAW_ENERGY_LINE_WIDTH                4

#define DEFAULT_X_DIRECT_CNT                  1
#define DEFAULT_Y_DIRECT_CNT                  1
//---------------------------------------------------------------
#define DRAW_X_LENGTH                         2000
#define DRAW_VERTICAL_INTERVAL                20

#define DRAW_Y_LENGTH                         1000
#define DRAW_HORIZONTAL_INTERVAL              20
//-----------------------------------------------
#define ZOOM_INFOR_I                          _T("25mm/s  5mm/mv")
#define ZOOM_INFOR_II                         _T("25mm/s  10mm/mv")
#define ZOOM_INFOR_III                        _T("25mm/s  20mm/mv")
#define DISPLAY_ZOOMINFOR_X_OFFSET_VALUE      200
#define DISPLAY_ZOOMINFOR_Y_OFFSET_VALUE      50
//---------------------------------------------------------------
typedef struct
{
   BOOL DistribufFlag;
   BOOL ShowFeatureFlag;

   int ShowCnt;
   unsigned int *WaveType;
   int *ShowBuf;

}DISPLAY_WAVE_BUFFER;
#define DISPLAY_NO_GRID_FLAG       0
#define DISPLAY_HAVE_GRID_FLAG     1

#define DEFAULT_DISPLAY_ECG_WAVE_GRID      DISPLAY_NO_GRID_FLAG
//---------------------------------------------------------------
typedef struct
{
   unsigned char ShowFlag;
   unsigned char GridFlag;
   BOOL ReverseFlag;

   int X_DirectIndex;
   int Y_DirectIndex;

   int NoGridZoomDenominator;
   int HaveGridZoomDenominator;

   int StartPos;
   int HorizontalCnt;
   float ResolutionValue;
   CString ShowLeadInforStr;

}DRAW_WAVE_CONTENT_PROPERTY;
//----------------------------
typedef struct
{
   HPEN *FramePen;
   HPEN *AmplitudePen;
   HPEN *MidPen;
   HPEN *DrawPen;
   HPEN *ZeroPen;
   HPEN *EnergyPen;

   HBRUSH *Brush;

}DRAW_WAVE_HANDLER_PROPERTY;
//---------------------------------------------------------------
typedef struct
{
   CRect DrawMappingRect;

//   int XStartPos;                  // draw area's size
   int XDrawLen;                   // draw area's horizontal len
   int YDrawLen;                   // draw area's vertical len

   int XDirectCnt;                 // X方向的个数  
   int YDirectCnt;                 // Y方向的个数  

   int X_EveryLen;                 // X方向每个块的长度
   int Y_EveryLen;                 // Y方向每个块的长度
   
}DRAW_AREA_SIZE_DETAILS;
//-------------------------------------------------------------------------------
class CDrawPicDriver
{														 
public:
	HWND m_hwnd;
	BOOL FilterDisplayFlag;                       //for playback
	DISPLAY_WAVE_BUFFER DisplayWaveContent;

	DRAW_WAVE_HANDLER_PROPERTY ShowHandlerContent;
   DRAW_WAVE_CONTENT_PROPERTY WaveContentProperty;

	DRAW_AREA_SIZE_DETAILS AreaSizeDetails;
//---------------------------------------------------------------------------
    CDrawPicDriver();
    ~CDrawPicDriver();
//---------------------------------------------------------------------------
	void DrawAllSignalWave(void);
	void TestWindowDrawLine(void);
//---------------------------------------------------------------------------
protected:
private:
    HDC pDC;
    HDC memDC;
    HBITMAP memBmp;

	BOOL CreateCompatibleBitmapForDraw(void);           // create compatible bitmap for draw
//-----------------------------
//  no grid
	void DrawRectangleForCompatibleDc_NoGrid(void);     // draw rectantgle for compatibel 
	void DrawHorizontalLine_NoGrid(void);               // draw horizontal line
	void DrawAmplitudeLevel_NoGrid(void);               // draw amplitude level line
	void DrawSignalWave_NoGrid(void);                   // draw signal wave
	void DisplayCompatibleBitbmp_NoGrid(void);          // display compatibel bitmap 
//-----------------------------
//  Have grid
	void DrawRectangleForCompatibleDc_HaveGrid(void);   // draw rectantgle for compatibel 
	void DrawGrid_ForWave(void);                        // draw grid for wave 
	void DrawGrid_LeadInfor(void);                      // draw grid lead infor
	void DrawSignalWave_HaveGrid(void);                 // draw signal wave
	void DisplayCompatibleBitbmp_HaveGrid(void);        // display compatibel bitmap 
};

#endif //__DRAW_PIC_DRIVER_H_H__


