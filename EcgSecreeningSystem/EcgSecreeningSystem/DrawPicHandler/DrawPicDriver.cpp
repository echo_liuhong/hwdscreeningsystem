
#include "stdafx.h"
#include "DrawPicDriver.h"
#include <assert.h>

#define   ZOOM_MOLECULE_VALUE                    7
#define   ZOOM_DEMOMINATOR_VALUE                 8

#define   X_LEN_TEST_DIVIDE_CNT                  4
#define   Y_LEN_TEST_DIVIDE_CNT                  4    
#define   R_CIRCLE_SIZE                          15

#define   PIC_DRIVER_DEFAULT_RESOLUTION_VALUE    ((float)(4.62))

#define   DEFAULT_GRID_VALUE                     DISPLAY_HAVE_GRID_FLAG      
//
// constructor function
//
CDrawPicDriver::CDrawPicDriver()
{
   m_hwnd = NULL;
   
   DisplayWaveContent.DistribufFlag = FALSE;
   DisplayWaveContent.ShowFeatureFlag = FALSE;
   DisplayWaveContent.ShowCnt = 0;

   WaveContentProperty.NoGridZoomDenominator = 1;
   WaveContentProperty.HaveGridZoomDenominator = 1;
   WaveContentProperty.GridFlag = DEFAULT_DISPLAY_ECG_WAVE_GRID;

   WaveContentProperty.HorizontalCnt = AMPLITUDE_DIVIDE_CNT;
   WaveContentProperty.ResolutionValue = PIC_DRIVER_DEFAULT_RESOLUTION_VALUE;

   AreaSizeDetails.XDirectCnt = DEFAULT_X_DIRECT_CNT;
   AreaSizeDetails.YDirectCnt = DEFAULT_Y_DIRECT_CNT;

   FilterDisplayFlag = FALSE;

   WaveContentProperty.GridFlag = DEFAULT_GRID_VALUE;

}
//
// destructor function
//
CDrawPicDriver::~CDrawPicDriver()
{

}
//--------------------------------------
// Draw all signal wave
void CDrawPicDriver::DrawAllSignalWave(void)
{
   BOOL flag;
   if( NULL == m_hwnd )
       return;
//-----------------------------------------------
   flag = CreateCompatibleBitmapForDraw();         // 1st
   if( flag == FALSE )
	   return;
//-----------------------------------------------
   if( DISPLAY_NO_GRID_FLAG == WaveContentProperty.GridFlag )
   {
      DrawRectangleForCompatibleDc_NoGrid();                 // mid step
      DrawHorizontalLine_NoGrid();                           // draw horizontal line
      DrawAmplitudeLevel_NoGrid();                           //
      DrawSignalWave_NoGrid();                               // draw signal wave
      DisplayCompatibleBitbmp_NoGrid();
   }
   else if( DISPLAY_HAVE_GRID_FLAG == WaveContentProperty.GridFlag )
   {
      DrawRectangleForCompatibleDc_HaveGrid();                 // mid step
      DrawGrid_ForWave();
	   DrawGrid_LeadInfor();                                    // draw grid lead infor
      DrawSignalWave_HaveGrid();                               // draw signal wave
      DisplayCompatibleBitbmp_HaveGrid();
   }
}
//--------------------------------------
// draw					
BOOL CDrawPicDriver::CreateCompatibleBitmapForDraw(void)
{
   pDC = GetDC(m_hwnd);
   if( NULL == pDC  )
      return FALSE;

   memDC = CreateCompatibleDC(pDC);
   if( NULL == memDC )
	   return FALSE;

   return TRUE;
}
//-------------------------------------------------------
// draw no grid
// draw rectangle for compatible DC
void CDrawPicDriver::DrawRectangleForCompatibleDc_NoGrid(void)
{
   memBmp = CreateCompatibleBitmap(pDC,AreaSizeDetails.X_EveryLen,AreaSizeDetails.Y_EveryLen);
   SelectObject(memDC,memBmp);

   RECT rect;
   rect.left = 0; 
   rect.top = 0;
   rect.right = AreaSizeDetails.X_EveryLen - 1; 
   rect.bottom = AreaSizeDetails.Y_EveryLen - 1;
   FillRect(memDC,&rect,ShowHandlerContent.Brush[0]);           // fill color
//-----------------------------------------------------------------------------
//  DRAW FRAME
   SelectObject(memDC,ShowHandlerContent.FramePen[0]);

   rect.left = 0;
   rect.top = 0;
   rect.right = AreaSizeDetails.X_EveryLen - 1;
   rect.bottom = AreaSizeDetails.Y_EveryLen - 1;
   Rectangle(memDC,rect.left,rect.top,rect.right,rect.bottom); //draw rectangle
}
//--------------------------------------------------
// draw horizontal line
void CDrawPicDriver::DrawHorizontalLine_NoGrid(void)
{
   CString str;
   int linecnt;
   int interval,yoffset;
   int i; 

   linecnt = WaveContentProperty.HorizontalCnt;
   interval = AreaSizeDetails.Y_EveryLen/linecnt;
   for(i = 1;i<linecnt;i++)
   {
      if( i == (linecnt/2) )
      {
         //  draw midline
         SelectObject(memDC,ShowHandlerContent.MidPen[0]);
		   MoveToEx(memDC,0,((i * AreaSizeDetails.Y_EveryLen)/linecnt),NULL);
		   LineTo(memDC,AreaSizeDetails.X_EveryLen,((i * AreaSizeDetails.Y_EveryLen)/linecnt));
      }
      else if( i != (linecnt/2))
      {
         //draw amplitude line
         interval = ((i * AreaSizeDetails.Y_EveryLen)/linecnt);
         yoffset = interval;

         SelectObject(memDC,ShowHandlerContent.AmplitudePen[0]);
         MoveToEx(memDC,0,interval,NULL);
         LineTo(memDC,AreaSizeDetails.X_EveryLen,interval);
      }
   }
}
//--------------------------------------------------
// draw amplitude level
void CDrawPicDriver::DrawAmplitudeLevel_NoGrid(void)
{
   CString str;
   CString showleadinforstr;
   int interval,yoffset;
   float divide_line_amplitude_value;
   int i; 
   int linecnt;
   int height;
   float resolution_value;

   height = AreaSizeDetails.Y_EveryLen;
   linecnt = WaveContentProperty.HorizontalCnt;
   resolution_value = WaveContentProperty.ResolutionValue;
   interval = height/linecnt;

   showleadinforstr = WaveContentProperty.ShowLeadInforStr;
   for( i = 0;i<linecnt;i++ )
   {
      if( i == 0 )
	  {
         TextOut(memDC,1,1,showleadinforstr,showleadinforstr.GetLength());
         continue;
	  }
      if( i == (linecnt/2))
      {
		 continue;
	  }
	  //draw amplitude level
      interval = ((i *  height)/linecnt);
      yoffset = interval;
        
      if( interval >(height/2) )
      {
         interval = 0 - (interval - height/2);
      }
	  else
	  {
	     interval = (height/2 - interval);
	  }
	  interval = interval * WaveContentProperty.NoGridZoomDenominator;
 	  divide_line_amplitude_value = interval * resolution_value;

	  str.Format(_T("%.1fuV"),divide_line_amplitude_value);
	  TextOut(memDC,1,yoffset+1,str,str.GetLength());
   }
}
//
// draw all signal wave
//
void CDrawPicDriver::DrawSignalWave_NoGrid(void)
{
   if( FALSE == DisplayWaveContent.DistribufFlag )
	   return;

   if( 0 == DisplayWaveContent.ShowCnt )
	   return;

   unsigned char flag;
   int i;
   int tmp;
   int zoom_demominator;

   int showCnt;
   int *showBuf;
   unsigned int *ecgFeature;
   POINT startpt,endpt;

   zoom_demominator = WaveContentProperty.NoGridZoomDenominator;

   startpt.x = 0;
   endpt.x = 0;

   flag = 0;
   showCnt = DisplayWaveContent.ShowCnt;
   showBuf = DisplayWaveContent.ShowBuf;
   ecgFeature = DisplayWaveContent.WaveType;
//--------------------------------------------------------------------------
   for(i=0;i<showCnt;i++)
   {
      if( 0x7fffffff == showBuf[i] )
	   {
		   if(0 == flag)
         {
            flag = 1;
		   }
		   endpt.x++;
		   continue;
	   }
//----------------------------------------------------------------------
      tmp = showBuf[i]/zoom_demominator;                  // SHOW DATA
	   if( TRUE == WaveContentProperty.ReverseFlag )
	      tmp = tmp + AreaSizeDetails.Y_EveryLen/2;
	   else
	      tmp = AreaSizeDetails.Y_EveryLen/2 - tmp;

	   if( 0 == i )
      {
         startpt.y = tmp;
      }
      endpt.y = tmp;
//----------------------------------------------------------------------
      if(1 == flag )
      {
         flag = 0;
         startpt.x = endpt.x;
         startpt.y = tmp;
	   }
//----------------------------------------------------------------------
      SelectObject(memDC,ShowHandlerContent.DrawPen[0]);
	   MoveToEx(memDC,startpt.x,startpt.y,NULL);
	   LineTo(memDC,endpt.x,endpt.y);

	   if( TRUE == DisplayWaveContent.ShowFeatureFlag )
	   {
		   if( 1 == ecgFeature[i])
		   {
             SelectObject(memDC,ShowHandlerContent.MidPen[0]);
		       Ellipse(memDC,endpt.x - R_CIRCLE_SIZE,endpt.y - R_CIRCLE_SIZE,endpt.x + R_CIRCLE_SIZE,endpt.y + R_CIRCLE_SIZE);
		   }
	   }
//--------------------------------------------------------------------------
	   startpt.x = endpt.x;
      startpt.y = endpt.y;
      endpt.x++;
//--------------------------------------------------------------------------
   }
}
//--------------------------------------------------
// display compatible Bitbmp
void CDrawPicDriver::DisplayCompatibleBitbmp_NoGrid(void)
{
   CRect rect;
   CString str;

   if( 0 == WaveContentProperty.Y_DirectIndex)
   {
      rect.left = AreaSizeDetails.DrawMappingRect.left;        //!!!!!20181121
      rect.top =  AreaSizeDetails.DrawMappingRect.top;
      rect.right = AreaSizeDetails.DrawMappingRect.right;
      rect.bottom = AreaSizeDetails.DrawMappingRect.bottom;
   }
   else
   {
      rect.left = AreaSizeDetails.DrawMappingRect.left;        //!!!!!20181121
      rect.top =  AreaSizeDetails.DrawMappingRect.bottom/(WaveContentProperty.Y_DirectIndex+1);
      rect.right = AreaSizeDetails.DrawMappingRect.right;
      rect.bottom = AreaSizeDetails.DrawMappingRect.bottom;
   }

   BitBlt(pDC,
          rect.left,rect.top,rect.Width(),rect.Height(),
	       memDC,
          0,0,SRCCOPY);
   WaveContentProperty.ShowLeadInforStr.Empty();
   DeleteObject(memBmp);
   DeleteDC(memDC);
   DeleteDC(pDC);
}
//-------------------------------------------------------
// draw have grid
// draw rectangle for compatible DC
void CDrawPicDriver::DrawRectangleForCompatibleDc_HaveGrid(void)
{
   memBmp = CreateCompatibleBitmap(pDC,AreaSizeDetails.X_EveryLen,AreaSizeDetails.Y_EveryLen);
   SelectObject(memDC,memBmp);

   RECT rect;
   rect.left = 0; 
   rect.top = 0;
   rect.right = AreaSizeDetails.X_EveryLen - 1;
   rect.bottom = AreaSizeDetails.Y_EveryLen - 1;
   FillRect(memDC,&rect,ShowHandlerContent.Brush[0]);           // fill color
//-----------------------------------------------------------------------------
//  DRAW FRAME
   SelectObject(memDC,ShowHandlerContent.FramePen[0]);

   rect.left = 0;
   rect.top = 0;
   rect.right = AreaSizeDetails.X_EveryLen- 1;
   rect.bottom = AreaSizeDetails.Y_EveryLen - 1;
   Rectangle(memDC,rect.left,rect.top,rect.right,rect.bottom); //draw rectangle
}
//
void CDrawPicDriver::DrawGrid_ForWave()
{
   int i;
   int linecnt;
   int midVerticalPos,tmpPos;
//----------------------------------------
//draw horizontal line
//draw mid line
   SelectObject(memDC,ShowHandlerContent.AmplitudePen[0]);

   midVerticalPos = AreaSizeDetails.DrawMappingRect.Height()/2;//DRAW_VERTICAL_INTERVAL;
   MoveToEx(memDC,0,midVerticalPos,NULL);
   LineTo(memDC,AreaSizeDetails.DrawMappingRect.Width(),midVerticalPos);

   midVerticalPos = AreaSizeDetails.DrawMappingRect.Height()/2;
//--------------------------------
   i = 1;
   while(1)
   {
      if( 0 == i%5 )
         SelectObject(memDC,ShowHandlerContent.AmplitudePen[0]);
	  else
         SelectObject(memDC,ShowHandlerContent.MidPen[0]);

	  tmpPos = midVerticalPos - i * DRAW_VERTICAL_INTERVAL;
      if( tmpPos < 0 )
	  {
		  break;
	  }
	  MoveToEx(memDC,0,tmpPos,NULL);
      LineTo(memDC,AreaSizeDetails.DrawMappingRect.Width(),tmpPos);
	  i++;
   }
   i = 1;
   while(1)
   {
      if( 0 == i%5 )
         SelectObject(memDC,ShowHandlerContent.AmplitudePen[0]);
	   else
         SelectObject(memDC,ShowHandlerContent.MidPen[0]);

	   tmpPos = midVerticalPos + i * DRAW_VERTICAL_INTERVAL;
      if( tmpPos > AreaSizeDetails.DrawMappingRect.Height() )
	   {
		   break;
	   }
	   MoveToEx(memDC,0,tmpPos,NULL);
      LineTo(memDC,AreaSizeDetails.DrawMappingRect.Width(),tmpPos);
	   i++;
   }
//----------------------------------------
//draw vertical line
   linecnt = AreaSizeDetails.DrawMappingRect.Width()/DRAW_HORIZONTAL_INTERVAL;
   for(i = 1;i<linecnt;i++)
   {
      if( 0 == i%5 )
         SelectObject(memDC,ShowHandlerContent.AmplitudePen[0]);
	   else
         SelectObject(memDC,ShowHandlerContent.MidPen[0]);

      MoveToEx(memDC,(i * DRAW_HORIZONTAL_INTERVAL),0,NULL);
	   LineTo(memDC,(i * DRAW_HORIZONTAL_INTERVAL),AreaSizeDetails.DrawMappingRect.Height());
   }
}
//
//
void CDrawPicDriver::DrawGrid_LeadInfor()
{
   CString str;
   CString showleadinforstr;
   int zoom_demominator;

   str = WaveContentProperty.ShowLeadInforStr;
   TextOut(memDC,1,1,str,str.GetLength());
     
   zoom_demominator = WaveContentProperty.HaveGridZoomDenominator;
   if( 0 == zoom_demominator )
	  str = ZOOM_INFOR_I;
   else if( 1 == zoom_demominator )
	  str = ZOOM_INFOR_II;
   else if( 2 == zoom_demominator )
	  str = ZOOM_INFOR_III;

   TextOut(memDC,
	        AreaSizeDetails.X_EveryLen - DISPLAY_ZOOMINFOR_X_OFFSET_VALUE,
	        AreaSizeDetails.Y_EveryLen - DISPLAY_ZOOMINFOR_Y_OFFSET_VALUE,
		     str,
		     str.GetLength());
}
//
//
void CDrawPicDriver::DrawSignalWave_HaveGrid()
{
   if( FALSE == DisplayWaveContent.DistribufFlag )
	   return;

   if( 0 == DisplayWaveContent.ShowCnt )
	   return;

   unsigned char flag;
   int i;
   int tmp;
   int zoom_demominator;

   int showCnt;
   int *showBuf;
   float resolutionValue;
   POINT startpt,endpt;

   zoom_demominator = WaveContentProperty.HaveGridZoomDenominator;
   resolutionValue = WaveContentProperty.ResolutionValue;

   startpt.x = 0;
   endpt.x = 0;

   flag = 0;
   showCnt = DisplayWaveContent.ShowCnt;
   showBuf = DisplayWaveContent.ShowBuf;
//--------------------------------------------------------------------------
   for(i=0;i<showCnt;i++)
   {
      if( 0x7fffffff == showBuf[i] )
	  {
		 if(0 == flag)
         {
            flag = 1;
		 }
		 endpt.x++;
		 continue;
	  }
//----------------------------------------------------------------------
	  if( 0 == zoom_demominator)
         tmp = (int)((showBuf[i] * resolutionValue * 100) /1000);                  // SHOW DATA
	  else if( 1 == zoom_demominator )
         tmp = (int)((showBuf[i] * resolutionValue * 200) /1000);                  // SHOW DATA
	  else if( 2 == zoom_demominator )
         tmp = (int)((showBuf[i] * resolutionValue * 400) /1000);                  // SHOW DATA

	  if( TRUE == WaveContentProperty.ReverseFlag )
	     tmp = tmp + AreaSizeDetails.Y_EveryLen/2;
	  else
	     tmp = AreaSizeDetails.Y_EveryLen/2 - tmp;

	  if( 0 == i )
      {
         startpt.y = tmp;
      }
      endpt.y = tmp;
//----------------------------------------------------------------------
      if(1 == flag )
      {
         flag = 0;
         startpt.x = endpt.x;
         startpt.y = tmp;
	  }
//----------------------------------------------------------------------
      SelectObject(memDC,ShowHandlerContent.DrawPen[0]);
	  MoveToEx(memDC,startpt.x,startpt.y,NULL);
	  LineTo(memDC,endpt.x,endpt.y);
//--------------------------------------------------------------------------
	  startpt.x = endpt.x;
      startpt.y = endpt.y;
      endpt.x++;
//--------------------------------------------------------------------------
   }
}
//
void CDrawPicDriver::DisplayCompatibleBitbmp_HaveGrid()
{
   CRect rect;
   CString str;

   rect.left = AreaSizeDetails.DrawMappingRect.left;        //!!!!!20181121
   rect.top =  AreaSizeDetails.DrawMappingRect.top;
   rect.right = AreaSizeDetails.DrawMappingRect.right;
   rect.bottom = AreaSizeDetails.DrawMappingRect.bottom;
   BitBlt(pDC,
	      rect.left,rect.top,rect.Width(),rect.Height(),
		  memDC,
		  0,0,SRCCOPY);

   WaveContentProperty.ShowLeadInforStr.Empty();
   DeleteObject(memBmp);
   DeleteDC(memDC);
   DeleteDC(pDC);
}
