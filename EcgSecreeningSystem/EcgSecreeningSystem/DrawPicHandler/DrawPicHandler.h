#include "windows.h"
#include "DrawPicDriver.h"

#ifndef __DRAW_PICTURE_HANDLER_H_H__
#define __DRAW_PICTURE_HANDLER_H_H__

#define DRAW_PICTURE_HANDLER_VERSION_NAME     "DRAW Picture Version:00:02"
//-------------------------------------------------------------------------------
#define MAX_ECG_LEAD_CNT                      18

#define LEAD_I_SAVE_NAME                      _T("Lead_I")
#define LEAD_II_SAVE_NAME                     _T("Lead_II")
#define LEAD_III_SAVE_NAME                    _T("Lead_III")
#define LEAD_AVR_SAVE_NAME                    _T("Lead_AVR")
#define LEAD_AVL_SAVE_NAME                    _T("Lead_AVL")
#define LEAD_AVF_SAVE_NAME                    _T("Lead_AVF")
#define LEAD_V1_SAVE_NAME                     _T("Lead_V1")
#define LEAD_V2_SAVE_NAME                     _T("Lead_V2")
#define LEAD_V3_SAVE_NAME                     _T("Lead_V3")
#define LEAD_V4_SAVE_NAME                     _T("Lead_V4")
#define LEAD_V5_SAVE_NAME                     _T("Lead_V5")
#define LEAD_V6_SAVE_NAME                     _T("Lead_V6")
#define LEAD_V7_SAVE_NAME                     _T("Lead_V7")
#define LEAD_V8_SAVE_NAME                     _T("Lead_V8")
#define LEAD_V9_SAVE_NAME                     _T("Lead_V9")
#define LEAD_V3R_SAVE_NAME                    _T("Lead_V3R")
#define LEAD_V4R_SAVE_NAME                    _T("Lead_V4R")
#define LEAD_V5R_SAVE_NAME                    _T("Lead_V5R")
//-------------------
//-----------------------------------------------------
#define ZOOM_RATIO_RANGE                      15
#define ZOOM_RATIO_MIDTMP                     (ZOOM_RATIO_RANGE - 1)
#define ZOOM_OUT_MULTIPLE_0                   256
#define ZOOM_OUT_MULTIPLE_1                   128
#define ZOOM_OUT_MULTIPLE_2                   64
#define ZOOM_OUT_MULTIPLE_3                   56
#define ZOOM_OUT_MULTIPLE_4                   48
#define ZOOM_OUT_MULTIPLE_5                   40
#define ZOOM_OUT_MULTIPLE_6                   32
#define ZOOM_OUT_MULTIPLE_7                   24
#define ZOOM_OUT_MULTIPLE_8                   16
#define ZOOM_OUT_MULTIPLE_9                   8
#define ZOOM_OUT_MULTIPLE_10                  6
#define ZOOM_OUT_MULTIPLE_11                  4
#define ZOOM_OUT_MULTIPLE_12                  3
#define ZOOM_OUT_MULTIPLE_13                  2
#define ZOOM_OUT_MULTIPLE_14                  1

#define DEFAULT_ZOOM_OUT_INDEX                ZOOM_RATIO_MIDTMP
#define DEFAULT_RESOLUTION_VALUE              (float)(4.62)   //20201216

#define DRAW_BLANK_INTERVAL                   40 
//-------------------------------------------------------------------------------
enum{
   SHOW_ECG_SIGNAL_WAVE = 0,
   SHOW_ECG_ORIGINAL_DATA,
};
enum{
   REAL_TIME_DISPLAY_SIGNAL = 0,
   PLAYBACK_DISPLAY_SIGNAL,
};
typedef struct
{
   unsigned char ShowFlag;

   BOOL ReverseFlag;

   int *ZoomTable;
   int ShowLength;
   //int *ShowContentBuffer;
   BOOL *ZeroMarkBuf;
   BOOL *EnergyTipBuf;

}DISPLAY_STRUCT;
//-------------------------------------------------------------------------------
class CDrawPicHandler
{														 
public:
//	BOOL FilterDisplayFlag;                       //for playback
    void *ParentPt;                               //主窗口类的指针
    DISPLAY_STRUCT DisplayBuf;
	CDrawPicDriver PicDriver;

	static const CString EcgSingalLeadName[MAX_ECG_LEAD_CNT];
	static const int ZoomTable[ZOOM_RATIO_RANGE];
//------------------------------------------------
    volatile BOOL DisplayFlag;
    unsigned char ShowPlaybackFlag;

	int ShowStartPos;
//------------------------------------------------
	int CurShowLeadIndex;              //Current show lead index 
public:
//---------------------------------------------------------------------------
    CDrawPicHandler();
    ~CDrawPicHandler();

	void GetEveryShowLen(void);
	void DrawSignalWave(void);
	void ChangeFilterDisplay(BOOL filterDisplayFlag);
//---------------------------------------------------------------------------
protected:
private:
	void ConfigureDrawProperty(int yindex);
	void DrawAllWave(void);
	void GetDrawSignalWaveContent(int maxsavecnt,int *source,int sourcelen);
	void GetDrawPlaybackSignal(int index,int *source);
	void GetDrawPlaybackSignalWithFeature(int index,int *source,unsigned int *typeSource);
//---------------------------------------------------------------------------
};
#endif //__DRAW_PICTURE_HANDLER_H_H__


