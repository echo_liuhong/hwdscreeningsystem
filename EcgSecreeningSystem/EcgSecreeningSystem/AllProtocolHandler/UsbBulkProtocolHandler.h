
#ifndef __USB_BULK_PROTOCOL_HANDLER_H_H__
#define __USB_BULK_PROTOCOL_HANDLER_H_H__

#include "windows.h"
#include "UsbBulkProtocolHandler.h"
//-------------------------------------------------------------------------
#define BULK_RECEIVE_BAG_CNT_PART                  0x0F
#define BULK_RECEIVE_BAG_INDEX_PART                0x0F 

#define RECEIVE_DATA_FOR_BLE_PART                  0 
#define RECEIVE_DATA_FOR_USB_PART                  1
#define RECEIVE_DATA_FOR_OBJECT_INDEX              5

#define BLE_TRANSPARENT_TRANSIMISSION_VAL          0 
#define BLE_MODULE_OPERATION_VAL                   1
#define BLE_DATA_OBJECT_INDEX                      6

#define DEVICE_TO_MASTER_VAL                       0
#define MASTER_TO_DEVICE_VAL                       1
#define BULK_DATA_DIRECTION_INDEX                  7
//--------------------------------------------------------------------------
#define SINGLE_USB_BULK_DATA_CNT                   64
#define BLE_INTERACTIVE_DATA_CNT                   60
typedef union
{
   unsigned char uBulkBuf[SINGLE_USB_BULK_DATA_CNT];
   char sBulkBuf[SINGLE_USB_BULK_DATA_CNT];
   struct
   {
      unsigned char Type;
      unsigned char BagIndex;
      unsigned char Len;
      unsigned char CheckSum;
      unsigned char DataBuffer[BLE_INTERACTIVE_DATA_CNT];
      
   }BulkBagStruct;
   
}BULK_SINGLE_BAG_STRUCT;
//----------------------------------------------------
#define MAX_BLE_DATA_CONTENT_CNT             255

#define MAC_DATA_LENGTH                      8
#define BLE_DATA_LENGTH                      (MAX_BLE_DATA_CONTENT_CNT - MAC_DATA_LENGTH)
typedef union
{
   unsigned char Buffer[MAX_BLE_DATA_CONTENT_CNT];
   struct{
       unsigned char MacAddr[MAC_DATA_LENGTH];
       unsigned char DataBuf[BLE_DATA_LENGTH];
   }DataContent;

}SINGLE_BLE_DATA_BAG;
//------------------------------------------------
#define MAX_BULK_BAG_CNT                     16
#define RECEIVE_DATA_FOR_BLE                 0
#define RECEIVE_DATA_FOR_USB_BULK            1

#define RECEIVE_DATA_FOR_BLE_DEVICE          0
#define RECEIVE_DATA_FOR_BLE_MASTER          1
typedef struct
{
//--------------------------------------
// receive part
   unsigned char Flag;
   unsigned char BulkDataDirect;                 //0:for ble,1:for device
   unsigned char BleDataDirect;                  //0:for ble,1:for device
   int Step;
   int BulkBagCnt;
   int SaveIndex;
   int SingleBagCntLen;
//--------------------------
   BULK_SINGLE_BAG_STRUCT BulkSingleBag;
   BULK_SINGLE_BAG_STRUCT BulkBagBuffer[MAX_BULK_BAG_CNT];

   SINGLE_BLE_DATA_BAG SingleBag;

}BULK_RECEIVE_STRUCT;
//-------------------------------------------------------------------------------------------------
typedef struct
{
//--------------------------------------
// Send part
   unsigned char SendBagCnt;                 //0:for ble,1:for device
//--------------------------
   BULK_SINGLE_BAG_STRUCT BulkBagBuffer[MAX_BULK_BAG_CNT];

}BULK_SEND_STRUCT;
//-------------------------------------------------------------------------------------------------
//
// CComDataHandler
//
class CUsbBulkProtocolHandler
{
public:
   BULK_RECEIVE_STRUCT UsbBulkReceiveBag;
   BULK_SEND_STRUCT UsbBulkSendBag;
//---------------
   void InitialUsbBulkCommunicateBag(void);
   void HandlerReceiveBuffer(char *rbuf,DWORD cnt);
   void ChangeSendBufferToBulkProtocol(unsigned char type,unsigned char *sbuf,DWORD cnt);
//---------------
public:
   // Constructor and Destructor
   CUsbBulkProtocolHandler();
   virtual ~CUsbBulkProtocolHandler();
private:
   void HandleBulkFirstBag(void);
   void HandleBulkOtherBag(void);
};
//-----------------------------------------------------
#endif