//
// HandleComData.cpp : implementation file
//

#include "stdafx.h"
#include "UsbBulkProtocolHandler.h"

#ifdef _DEBUG
  #define new DEBUG_NEW
#endif

//
//
CUsbBulkProtocolHandler::CUsbBulkProtocolHandler(void)
{
   InitialUsbBulkCommunicateBag();
}
//
//
void CUsbBulkProtocolHandler::InitialUsbBulkCommunicateBag(void)
{
   UsbBulkReceiveBag.Flag = 0;
   UsbBulkReceiveBag.BulkBagCnt = 0;
   UsbBulkReceiveBag.Step = 0;
}
//----------------------------------------
// destruction function
CUsbBulkProtocolHandler::~CUsbBulkProtocolHandler()
{
}
//
//
void CUsbBulkProtocolHandler::HandlerReceiveBuffer(char *rbuf,DWORD cnt)
{
   if( SINGLE_USB_BULK_DATA_CNT != cnt )
	   return;
//---------------------------
   int i;
   unsigned char type;
   unsigned char bagIndex;
   unsigned char length,checkSum;
   for( i = 0;i<SINGLE_USB_BULK_DATA_CNT;i++)
   {
      UsbBulkReceiveBag.BulkSingleBag.sBulkBuf[i] = rbuf[i];
   }
   type = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Type;
   bagIndex = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.BagIndex;
   length = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Len;
   checkSum = 0;
   for( i = 0;i<length;i++)
   {
	   checkSum = checkSum + UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.DataBuffer[i];
   }
   if(checkSum != UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.CheckSum )
   {
      UsbBulkReceiveBag.Step = 0;
	  return;
   }
   if( 0 == (type & BULK_RECEIVE_BAG_CNT_PART) )
   {
	   // bag cnt error
	   return;
   }
//-------------------------------------------------
   if( (1 == (type & BULK_RECEIVE_BAG_CNT_PART)) || (0 == bagIndex) )
   {
      HandleBulkFirstBag();
   }
   else
   {
      if( 1 == UsbBulkReceiveBag.Step )
      {
         HandleBulkOtherBag();
      }
      else
      {
         UsbBulkReceiveBag.Step = 0;
         return;
      }
   }
}
//
//
void CUsbBulkProtocolHandler::HandleBulkFirstBag(void)
{
   unsigned char receiveBagCnt;
   unsigned char receiveBagIndex;
   unsigned char *sourceBuf;
   int i;
   int length;
   unsigned char type;
   receiveBagCnt = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Type & BULK_RECEIVE_BAG_CNT_PART;
   receiveBagIndex = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.BagIndex & BULK_RECEIVE_BAG_INDEX_PART; 
//--------------------
   sourceBuf = UsbBulkReceiveBag.BulkSingleBag.uBulkBuf;
   if(0 != receiveBagIndex )
      return;

   UsbBulkReceiveBag.BulkBagCnt = receiveBagCnt;
   type = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Type;
   UsbBulkReceiveBag.BulkDataDirect = (type>>RECEIVE_DATA_FOR_OBJECT_INDEX) & 0x01;
   UsbBulkReceiveBag.BleDataDirect = (type>>BLE_DATA_OBJECT_INDEX) & 0x01;
   if( 1 == receiveBagCnt )
   {
	   length = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Len;
	   sourceBuf = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.DataBuffer;
	   UsbBulkReceiveBag.SingleBagCntLen = length;
 	   for(i = 0;i<length;i++)
	   {
	       UsbBulkReceiveBag.SingleBag.Buffer[i] = sourceBuf[i];
	   }
       UsbBulkReceiveBag.Step = 0;
       UsbBulkReceiveBag.Flag = 1;
   }
   else
   {
      for(i=0;i<SINGLE_USB_BULK_DATA_CNT;i++)
      {
		  UsbBulkReceiveBag.BulkBagBuffer[receiveBagIndex].uBulkBuf[i] = sourceBuf[i];
      }
      UsbBulkReceiveBag.Step = 1;
   }
}
//
//
void CUsbBulkProtocolHandler::HandleBulkOtherBag(void)
{
   unsigned char receiveBagCnt;
   unsigned char receiveBagIndex;
   unsigned char *sourceBuf;
   int i,j;
   receiveBagCnt = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.Type & BULK_RECEIVE_BAG_CNT_PART;
   receiveBagIndex = UsbBulkReceiveBag.BulkSingleBag.BulkBagStruct.BagIndex & BULK_RECEIVE_BAG_INDEX_PART; 

   sourceBuf = UsbBulkReceiveBag.BulkSingleBag.uBulkBuf;
   if( 0 == receiveBagIndex )
   {
      UsbBulkReceiveBag.Step = 0;
      return;
   }
   UsbBulkReceiveBag.BulkBagCnt = receiveBagCnt;
   for(i=0;i<SINGLE_USB_BULK_DATA_CNT;i++)
   {
	   UsbBulkReceiveBag.BulkBagBuffer[receiveBagIndex].uBulkBuf[i] = sourceBuf[i];
   }
//------------------------------------------------------------
   if( receiveBagIndex != (receiveBagCnt - 1) )
	   return;

   int length;
   int saveIndex;
   saveIndex = 0;
   for( i = 0;i<receiveBagCnt;i++)
   {
	   length = UsbBulkReceiveBag.BulkBagBuffer[i].BulkBagStruct.Len;
	   sourceBuf = UsbBulkReceiveBag.BulkBagBuffer[i].BulkBagStruct.DataBuffer;
	   for(j = 0;j<length;j++)
	   {
		   UsbBulkReceiveBag.SingleBag.Buffer[saveIndex] = sourceBuf[j];
		   saveIndex++;
	   }
   }
   UsbBulkReceiveBag.SingleBagCntLen = saveIndex;
   UsbBulkReceiveBag.Flag = 1;
   UsbBulkReceiveBag.Step = 0;
//------------------------------------------------------------
}
//
//
void CUsbBulkProtocolHandler::ChangeSendBufferToBulkProtocol(unsigned char type,unsigned char *sbuf,DWORD cnt)
{
   unsigned char i,j;
   unsigned char copyLen;
   unsigned char checkSum;
   unsigned char *targetBuf;
   unsigned short saveIndex;
   int bagCnt;
   
   if( 0 == cnt )
   {
	  UsbBulkSendBag.SendBagCnt = 0;
	  return;
   }
//------------------------------------------------------------------------------   
// 1st get BagCnt;  
   bagCnt = cnt / BLE_INTERACTIVE_DATA_CNT;
   if( 0 != (cnt % BLE_INTERACTIVE_DATA_CNT) )
   {
      bagCnt++;   
   }
   if( bagCnt > MAX_BULK_BAG_CNT )
	  return;
//------------------------------------------------------------------------------   
// 2nd Send
   saveIndex = 0;
   for(i=0;i<bagCnt;i++)
   {
      type = (type & 0xF0) | bagCnt;
      
	  UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.Type = type | bagCnt;
	  UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.BagIndex = i;
      if( i != (bagCnt-1) )
      {
         copyLen = BLE_INTERACTIVE_DATA_CNT;
         UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.Len = BLE_INTERACTIVE_DATA_CNT;
      }
      else
      {
         if(1 == bagCnt)
         {
            copyLen = (unsigned char)cnt;
         }
         else
         {
            copyLen = (unsigned char)(cnt - i * BLE_INTERACTIVE_DATA_CNT);   //20211206
         }
         UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.Len = copyLen; //cnt % BLE_INTERACTIVE_DATA_CNT;
      }
//-----------------------------------------------
	  targetBuf = UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.DataBuffer;
      for(j=0;j<BLE_INTERACTIVE_DATA_CNT;j++)
      {
         targetBuf[j] = 0x00; 
      }
      for(j=0;j<copyLen;j++)
      {
         targetBuf[j] = sbuf[saveIndex+j];
      }   
      saveIndex = saveIndex + copyLen;
      checkSum = 0;
      for(j=0;j<BLE_INTERACTIVE_DATA_CNT;j++)
      {
         checkSum = checkSum + targetBuf[j];
      }
	  UsbBulkSendBag.BulkBagBuffer[i].BulkBagStruct.CheckSum = checkSum;
   }
   UsbBulkSendBag.SendBagCnt = bagCnt;
}

