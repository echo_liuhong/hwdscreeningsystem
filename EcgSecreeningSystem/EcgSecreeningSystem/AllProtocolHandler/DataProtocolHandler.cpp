//
// HandleComData.cpp : implementation file
//

#include "stdafx.h"
#include "UsbBulkDriver\UsbBulkDriverConfig.h"
#include "DataProtocolHandler.h"

#ifdef _DEBUG
  #define new DEBUG_NEW
#endif

//----------------------------------------------------------------
const unsigned char CDataProtocolHandler::ReceTypeAndLenTable[RECEIVE_UART_CMD_CNT * 2] =
{
   0x01,0xE1,  //20211028 ADD
   CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE,CMD_ECG_ONEHALF_FOR_BAGINDEX_DATA_LEN,
   CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE,CMD_EMG_ONEHALF_FOR_BAGINDEX_DATA_LEN,
   CMD_BLE_MODULE_INTERACTIVE,CMD_BLE_MODULE_INTERACTIVE_DATA_LEN,
   CMD_SINGLE_ECG_WAVE,CMD_SINGLE_ECG_WAVE_DATA_LEN,
   CMD_ORIGINAL_SINGLE_ECG_WAVE,CMD_ORIGINAL_SINGLE_ECG_WAVE_DATA_LEN,
   CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE,CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE_DATA_LEN,
   CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE,CMD_ONEHALF_EMG_AND_ACCELERATOR_WAVE_DATA_LEN,

   CMD_GET_DONGLE_POWER,CMD_GET_DONGLE_POWER_DATA_LEN,               // 0
   CMD_DEV_SELF_CHECKING,CMD_DEV_SELF_CHECKING_SEND_DATA_LEN,        
//---------------------------
   CMD_GET_CUR_TIMESTAMP,CMD_GET_CUR_TIMESTAMP_RECEIVE_DATA_LEN,
   CMD_SET_CUR_TIMESTAMP,CMD_SET_CUR_TIMESTAMP_DATA_LEN,         
   CMD_READ_TIMESTAMP_LOG,CMD_READ_TIMESTAMP_LOG_SEND_DATA_LEN,
   CMD_ERASE_TIMESTAMP_LOG_AREA,CMD_ERASE_TIMESTAMP_LOG_AREA_DATA_LEN,

   CMD_ERROR_LOG_HANDLER,CMD_ERROR_LOG_SEND_DATA_LEN,
//--------------------
   CMD_GET_CONNECT_STATUS,CMD_GET_CONNECT_STATUS_DATA_LEN,    
   CMD_CHANGE_STATUS_CMD,CMD_CHANGE_STATUS_CMD_DATA_LEN,                
   CMD_GET_RUN_MODE,CMD_GET_RUN_MODE_SEND_DATA_LEN,                

   CMD_GET_DONGLE_VERSION,CMD_ACK_GET_DONGLE_VERSION_DATA_LEN, 
   CMD_GET_BULK_DEVICE_VERSION,CMD_ACK_GET_BULK_DEVICE_VERSION_DATA_LEN,

   CMD_GET_DONGLE_BOOT_VERSION,CMD_ACK_GET_DONGLE_BOOT_VERSION_DATA_LEN,
   CMD_SET_WIFI_INFOR,CMD_SET_WIFI_INFOR_DATA_LEN,
   CMD_GET_WIFI_INFOR,CMD_GET_WIFI_INFOR_DATA_LEN,
   CMD_SET_DONGLE_QUIT_STA,CMD_SET_DONGLE_QUIT_STA_DATA_LEN,
   CMD_GET_SAVE_STATUS,CMD_GET_SAVE_STATUS_RECEIVE_DATA_LEN,
   CMD_SET_SAVE_STATUS,CMD_SET_SAVE_STATUS_SEND_DATA_LEN,
   CMD_SET_SAMPLERATE_VAL,CMD_SET_SAMPLERATE_VAL_DATA_LEN,
//---------------------------
// boot cmd part
   CMD_GET_CODE_PROPERTY,CMD_GET_CODE_PROPERTY_SEND_DATA_LEN,
   CMD_ACTIVE_BOOTLOADER,CMD_ACTIVE_BOOTLOADER_DATA_LEN,
   CMD_UPDATE_HEAD_BAG,CMD_UPDATE_HEAD_BAG_ACK_DATA_LEN,
   CMD_UPDATE_DATA_BAG,CMD_UPDATE_DATA_BAG_ACK_DATA_LEN,
   CMD_UPDATE_FINISH_BAG,CMD_UPDATE_FINISH_BAG_DATA_LEN,

   CMD_GET_SAVE_LEFTTIME,CMD_GET_SAVE_LEFTTIME_DATA_LEN,

   CMD_SET_DEVICE_INFO,CMD_SET_DEVICE_INFO_DATA_LEN,
   CMD_GET_DEVICE_INFO,CMD_GET_DEVICE_INFO_SEND_DATA_LEN,
   CMD_GET_BLE_MAC_ADDR,CMD_GET_BLE_MAC_INFO_SEND_DATA_LEN,
   CMD_GET_UPLOAD_ADDR,CMD_GET_UPLOAD_ADDR_SEND_DATA_LEN,
   CMD_SET_UPLOAD_ADDR,CMD_SET_UPLOAD_ADDR_DATA_LEN,
   CMD_GET_BASE_VERSION,CMD_GET_BASE_VERSION_SEND_DATA_LEN,

   CMD_SET_USER_INFOR,CMD_SET_USER_INFOR_DATA_LEN,             //20201015
   CMD_GET_USER_INFOR,CMD_GET_USER_INFOR_SEND_DATA_LEN,
//--------------------------------------------------------------
   CMD_GET_BASE_COMMUNICATE_INFO,CMD_GET_BASE_COMMUNICATE_INFO_DATA_LEN,
   CMD_GET_ECG_BLK_CNT,CMD_GET_ECG_BLK_CNT_SEND_DATA_LEN,
   CMD_GET_OR_WRITE_ECG_BLK_INFOR,CMD_GET_ECG_BLK_INFOR_SEND_DATA_LEN,

   CMD_GET_GAIN_VALUE,CMD_GET_GAIN_VALUE_SEND_DATA_LEN,
//---------------------------------------------------------------
   CMD_GET_ECG_BLK_CONTENT,CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN_FOR_SHORTBAG,
   CMD_GET_ECG_BLK_CONTENT,CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN_FOR_LONGBAG,
   CMD_GET_ECG_BLK_CONTENT,CMD_GET_ECG_BLK_CONTENT_SEND_DATA_LEN_FOR_COMPLETE_PAGE,
};
//-----------------------------------------------
//  construction function
CDataProtocolHandler::CDataProtocolHandler()
{
   InitialCompleteCommunicateBag();
}
//----------------------------------------
// destruction function
CDataProtocolHandler::~CDataProtocolHandler()
{
}
//
// initial complete communicate bag
//
void CDataProtocolHandler::InitialCompleteCommunicateBag()
{
   unsigned short i;
   unsigned char *target1,*target2;
//-----------------------------------	
   DataCommunicateStruct.ReceiveFlag = NO_RECEIVE_BAG_RECEIVE;
   DataCommunicateStruct.CheckHeadFlag = NO_CHECK_HEAD_FLAG;
   DataCommunicateStruct.AckFlag = NO_NEED_ACK_CMD;
   DataCommunicateStruct.ProtocolRestartFlag = NO_NEED_PROTOCOL_RESTART;
   DataCommunicateStruct.ReceiveIndex = 0;
   DataCommunicateStruct.ReceiveCount = 0;
   DataCommunicateStruct.ReceiveStep = 0;
	
   DataCommunicateStruct.ReceiveCmdCnt = 0;
   for(i=0;i<RECEIVE_SAME_CMD_TYPE_CNT;i++)
   {
      DataCommunicateStruct.ReceiveCmdIndex[i] = 0;
   }

   DataCommunicateStruct.Xortmp = 0;
   DataCommunicateStruct.ChckCmdIndex = 0;
   DataCommunicateStruct.CheckCmdType = 0;
   DataCommunicateStruct.ReserveByte = 0;
   DataCommunicateStruct.SendSingleBagLen = 0;

   target1 = &DataCommunicateStruct.ReceiveBag.HeadHigh;
   target2 = &DataCommunicateStruct.SendBag.HeadHigh;
   for( i=0;i<COMMUNICATE_BAG_LENGTH;i++ )
   {
      target1[i] = 0;
      target2[i] = 0;
   }
}
//----------------------------------------------------------
//  handle bel communicate bag
void CDataProtocolHandler::HandlerCommunicateBag(unsigned char ribuf)
{
   if( DataCommunicateStruct.ReceiveStep < CMD_HEAD_PART_CNT )
   {
      AnalysisReceiveHead(ribuf);
   }
   else
   {
      AnalysisReceiveDataContent(ribuf);
   }
}
//---------------------------------------
// anylysis receive head
void CDataProtocolHandler::AnalysisReceiveHead(unsigned char ribuf)
{
   unsigned char receivestep;

   receivestep = DataCommunicateStruct.ReceiveStep;
   if( 0 == receivestep )
   {
      Step_FirstHandler(ribuf);
   }
   else if( 1 == receivestep )
   {
      Step_SecondHandler(ribuf);
   }
   else if( 2 == receivestep )
   {
      Step_ThirdHandler(ribuf);
   }
   else if( 3 == receivestep )
   {
      Step_ForthHandler(ribuf);
   }
}
//-------------------------
// step first handler
void CDataProtocolHandler::Step_FirstHandler(unsigned char ribuf)
{
   if( ECG_START_HIGH_TMP == ribuf )
   {
      DataCommunicateStruct.ReceiveStep = 1;
      DataCommunicateStruct.ReceiveBag.HeadHigh = ribuf;
   }
   else
   {
      DataCommunicateStruct.ReceiveStep = 0;  
   }
}
//-------------------------
// step second handler 
void CDataProtocolHandler::Step_SecondHandler(unsigned char ribuf)
{
   if( ECG_START_LOW_TMP == ribuf )
   {
      DataCommunicateStruct.ReceiveStep = 2;
      DataCommunicateStruct.ReceiveBag.HeadLow = ribuf;
   }
   else if( ECG_START_HIGH_TMP != ribuf )
   {
      DataCommunicateStruct.ReceiveStep = 0;  
   }
}
//------------------------------------
// step third handler
void CDataProtocolHandler::Step_ThirdHandler(unsigned char ribuf)
{
   int i,index;
   int flag;

   if( ECG_START_HIGH_TMP == ribuf  )
   {
      DataCommunicateStruct.ReceiveStep = 1;
      DataCommunicateStruct.ReceiveBag.HeadHigh = ribuf;
      return;
   }
   
   flag = 0;
   index = 0;
   for(i=0;i<RECEIVE_UART_CMD_CNT;i++)
   {
      if( ribuf == ReceTypeAndLenTable[i*2] )
      {
         flag = 1;
         DataCommunicateStruct.ReceiveCmdIndex[index++] = i;   //20210120 modify
         //break;     //20210120
	  }
   }
   DataCommunicateStruct.ReceiveCmdCnt = index;
//-------------------------
   if( 1 == flag )
   {
      DataCommunicateStruct.ReceiveStep = 3;
      DataCommunicateStruct.ReceiveBag.Type = ribuf;
      DataCommunicateStruct.Xortmp = 0xFF ^ ribuf;
   }
   else
   {
      DataCommunicateStruct.ReceiveStep = 0;
   }
}
//----------------------
// STEP FORTH HANDLER
void CDataProtocolHandler::Step_ForthHandler(unsigned char ribuf)
{
//-------------------------
//20210120 modify
   int i;
   if( ECG_START_HIGH_TMP == ribuf )
   {
      DataCommunicateStruct.ReceiveStep = 1;
      DataCommunicateStruct.ReceiveBag.HeadHigh = ribuf;
      return;
   }

   for(i= 0;i<DataCommunicateStruct.ReceiveCmdCnt;i++)
   {
      if( ribuf == ReceTypeAndLenTable[ DataCommunicateStruct.ReceiveCmdIndex[i]*2+1 ] )
      {
         DataCommunicateStruct.ReceiveStep = 4;
         DataCommunicateStruct.ReceiveBag.Length = ribuf;
         DataCommunicateStruct.ReceiveCount = ribuf;
         DataCommunicateStruct.ReceiveIndex = 0;

         DataCommunicateStruct.Xortmp = DataCommunicateStruct.Xortmp ^ ribuf;
         DataCommunicateStruct.ChckHeadStep = 0;
		 return;
      }    
   }
   DataCommunicateStruct.ReceiveStep = 0;
}
//
// analysis data content
//
void CDataProtocolHandler::AnalysisReceiveDataContent(unsigned char ribuf)
{
   unsigned char receivestep;
//------------------------------------
   //CheckCmdHeadInDataContent(ribuf);    //20220309 delete?
   if( NEED_PROTOCOL_RESTART == DataCommunicateStruct.ProtocolRestartFlag )    //20191105
   {
	   DataCommunicateStruct.ProtocolRestartFlag = NO_NEED_PROTOCOL_RESTART;   //20191105
       return;
   }
//------------------------------------
   receivestep = DataCommunicateStruct.ReceiveStep;
   if( 4 == receivestep )
   {
      Step_FifthHandler(ribuf);
   }
   else if( 5 == receivestep )
   {
      Step_SixthHandler(ribuf);
   }
//------------------------------------
}
//
// check cmd head in data content
//
void CDataProtocolHandler::CheckCmdHeadInDataContent(unsigned char ribuf)
{
   unsigned char chckheadstep;
   unsigned char i; 
   unsigned char flag; 
    
   chckheadstep = DataCommunicateStruct.ChckHeadStep;
   DataCommunicateStruct.ProtocolRestartFlag = NO_NEED_PROTOCOL_RESTART;
   if( 0 == chckheadstep ) 
   {
      if( ECG_START_HIGH_TMP == ribuf )
      {
          DataCommunicateStruct.ChckHeadStep = 1;
      }
      return;      
   }
//----------------   
   if( 1 == chckheadstep ) 
   {
      if(ECG_START_LOW_TMP == ribuf)
      {
         DataCommunicateStruct.ChckHeadStep = 2;
      }
      else if(ECG_START_HIGH_TMP != ribuf)
      {
         DataCommunicateStruct.ChckHeadStep = 0;         
      }
      return;      
   }
//----------------   
   if( 2 == chckheadstep ) 
   {
      if( ECG_START_HIGH_TMP == ribuf )
      {
         DataCommunicateStruct.ChckHeadStep = 1;
      }
      else
      {
         flag = 0;
         for(i=0;i<RECEIVE_UART_CMD_CNT;i++)
         {
            if( ribuf == ReceTypeAndLenTable[i*2] )
            {
               flag = 1;
               DataCommunicateStruct.ChckCmdIndex = i;
               break;     
	        }
         } 
         if( 1 == flag )
         {
            DataCommunicateStruct.ChckHeadStep = 3; 
            DataCommunicateStruct.CheckCmdType = ribuf;
         }
         else
         {
            DataCommunicateStruct.ChckHeadStep = 0; 
         }
      }
      return;      
   }
   if( 3 == chckheadstep ) 
   {
      if(ECG_START_HIGH_TMP == ribuf)
      {
         DataCommunicateStruct.ChckHeadStep = 1;  
      }   
      else
      {
         if( ribuf == ReceTypeAndLenTable[DataCommunicateStruct.ChckCmdIndex*2 +1] )
         {
            DataCommunicateStruct.ReceiveStep = 4;
            DataCommunicateStruct.ReceiveBag.Type = DataCommunicateStruct.CheckCmdType;
            DataCommunicateStruct.ReceiveBag.Length = ribuf;
            DataCommunicateStruct.ReceiveCount = ribuf;                         //20191105
            DataCommunicateStruct.ReceiveIndex = 0;                             //20191105
            DataCommunicateStruct.Xortmp = 0xFF ^ ribuf ^ DataCommunicateStruct.CheckCmdType;
            DataCommunicateStruct.ProtocolRestartFlag = NEED_PROTOCOL_RESTART;  //20191105
         }           
         DataCommunicateStruct.ChckHeadStep = 0;         
      }          
   }
}

//-----------------------------------------------
// step fifth handler
void CDataProtocolHandler::Step_FifthHandler(unsigned char ribuf)
{
   DataCommunicateStruct.Xortmp = DataCommunicateStruct.Xortmp ^ ribuf;
   DataCommunicateStruct.ReceiveBag.Buffer[DataCommunicateStruct.ReceiveIndex] = ribuf;
   DataCommunicateStruct.ReceiveIndex++;
   if( DataCommunicateStruct.ReceiveIndex == DataCommunicateStruct.ReceiveCount)
   {
      DataCommunicateStruct.ReceiveStep++;
   }
}
//-----------------------------------------------
// step sixth handle
void CDataProtocolHandler::Step_SixthHandler(unsigned char ribuf)
{
   if( DataCommunicateStruct.Xortmp == ribuf )
   {
      DataCommunicateStruct.ReceiveBag.Buffer[DataCommunicateStruct.ReceiveIndex] = ribuf;
      DataCommunicateStruct.ReceiveFlag = HAVE_RECEIVE_BAG_RECEIVE;
   }
   else
   {
      DataCommunicateStruct.ReceiveFlag = NO_RECEIVE_BAG_RECEIVE;
   }
   DataCommunicateStruct.ReceiveStep = 0;
}
//
//
void CDataProtocolHandler::GenerateWithBulkInteractiveCmd(uint8_t type, uint8_t runFlag, unsigned char *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	//---------------
	index = RESERVE_MAC_BUFFER_CNT;
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
	sendbuffer[index++] = PC_WITH_BULK_INTERACTIVE_CMD_INDEX/10 + 0x30;
	sendbuffer[index++] = PC_WITH_BULK_INTERACTIVE_CMD_INDEX%10 + 0x30;

    sendbuffer[index++] = runFlag/10 + 0x30;
	sendbuffer[index++] = runFlag%10 + 0x30;

	if ( ACTIVE_USB_BULK_READ == runFlag)
	{
       sendbuffer[index++] = ACTIVE_USB_BULK_READ / 10 + 0x30;
       sendbuffer[index++] = ACTIVE_USB_BULK_READ % 10 + 0x30;
	}
	else
	{
       sendbuffer[index++] = 0x30;
	   sendbuffer[index++] = 0x30;
	}
	i = index - RESERVE_MAC_BUFFER_CNT;
	for(; i< CMD_BLE_MODULE_INTERACTIVE_LEN; i++)
	{
       sendbuffer[index++] = 0x00;
	}
	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], index - RESERVE_MAC_BUFFER_CNT);
	DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateGetBulkFixSerialNumCmd(uint8_t type,uint8_t *buff,unsigned char *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	//---------------
	index = RESERVE_MAC_BUFFER_CNT;
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
	sendbuffer[index++] = GET_BULK_FIX_SERIAL_NUM_CMD_INDEX / 10 + 0x30;
	sendbuffer[index++] = GET_BULK_FIX_SERIAL_NUM_CMD_INDEX % 10 + 0x30;

	for (i = 0; i <GET_BULK_FIX_SERIAL_NUM_CMD_BUFFER_LEN; i++)
	{
		sendbuffer[index++] = buff[i];
	}

	i = index - RESERVE_MAC_BUFFER_CNT;
	for (; i< CMD_BLE_MODULE_INTERACTIVE_LEN; i++)
	{
		sendbuffer[index++] = 0x00;
	}
	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], index - RESERVE_MAC_BUFFER_CNT);
	DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateCheckBulkFixSerialNumValidityCmd(uint8_t type, uint8_t *buff, unsigned char *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	//---------------
	index = RESERVE_MAC_BUFFER_CNT;
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
	sendbuffer[index++] = GET_CHCK_BULK_FIX_SERIAL_NUM_CMD_INDEX / 10 + 0x30;
	sendbuffer[index++] = GET_CHCK_BULK_FIX_SERIAL_NUM_CMD_INDEX % 10 + 0x30;

	for (i = 0; i <GET_BULK_FIX_SERIAL_NUM_CMD_BUFFER_LEN; i++)
	{
		sendbuffer[index++] = buff[i];
	}

	i = index - RESERVE_MAC_BUFFER_CNT;
	for (; i< CMD_BLE_MODULE_INTERACTIVE_LEN; i++)
	{
		sendbuffer[index++] = 0x00;
	}
	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], index - RESERVE_MAC_BUFFER_CNT);
	DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateGetBleDevConnectStaCmd(uint8_t type, unsigned char *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	//---------------
	index = RESERVE_MAC_BUFFER_CNT;
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
	sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
	sendbuffer[index++] = GET_BLE_DEV_INFOR_CMD_INDEX / 10 + 0x30;
	sendbuffer[index++] = GET_BLE_DEV_INFOR_CMD_INDEX % 10 + 0x30;
	sendbuffer[index++] = GET_BLE_DEV_CONNECT_STATUS / 10 + 0x30;
	sendbuffer[index++] = GET_BLE_DEV_CONNECT_STATUS % 10 + 0x30;

	i = index - RESERVE_MAC_BUFFER_CNT;
	for (; i< CMD_BLE_MODULE_INTERACTIVE_LEN; i++)
	{
		sendbuffer[index++] = 0x00;
	}
	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], index - RESERVE_MAC_BUFFER_CNT);
	DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateControlStatusCmd(unsigned char type,unsigned char statusSel,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   if( FALSE == TransformMacBuf(type,sendbuffer,macBuf) )
   {
      return;
   }
//---------------
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_SET_DONGLE_QUIT_STA;
   sendbuffer[index++] = CMD_SET_DONGLE_QUIT_STA_DATA_LEN;

   if( 0 == statusSel )
   {
      sendbuffer[index++] = ENTER_INTO_POWEROFF_STA;
      sendbuffer[index++] = 0x7F - ENTER_INTO_POWEROFF_STA;
   }
   else if( 1 == statusSel )
   {
      sendbuffer[index++] = CANCEL_POWEROFF_STA;
      sendbuffer[index++] = 0x7F - CANCEL_POWEROFF_STA;
   }
   else if( 2 == statusSel )
   {
      sendbuffer[index++] = ENTER_INTO_RESET_STA;
      sendbuffer[index++] = 0x7F - ENTER_INTO_RESET_STA;
   }
   else if( 3 == statusSel )
   {
      sendbuffer[index++] = CANCEL_AUTO_SLEEP_STA;
      sendbuffer[index++] = 0x7F - CANCEL_AUTO_SLEEP_STA;
   }
   else if( 4 == statusSel )
   {
      sendbuffer[index++] = RESUME_AUTO_SLEEP_STA;
      sendbuffer[index++] = 0x7F - RESUME_AUTO_SLEEP_STA;
   }
   index++;
   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],index - RESERVE_MAC_BUFFER_CNT);
   DataCommunicateStruct.SendSingleBagLen = index;
}
void CDataProtocolHandler::GenerateActiveBootloaderCmd(unsigned char type,unsigned char status,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   if( FALSE == TransformMacBuf(type,sendbuffer,macBuf) )
   {
      return;
   }
//---------------
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_ACTIVE_BOOTLOADER;
   sendbuffer[index++] = CMD_ACTIVE_BOOTLOADER_DATA_LEN;

   if( (ACTIVE_BULK_FIRMWARE_BOOT == status) || (ACTIVE_BULK_FIRMWARE_APP == status) )
   {
      sendbuffer[index++] = status;
      sendbuffer[index++] = 0x7F - status;
   }
   else 
   {
	  return;
   }
   index++;
   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],index - RESERVE_MAC_BUFFER_CNT);
   DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateGetCodePropertyCmd(unsigned char type,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   if( FALSE == TransformMacBuf(type,sendbuffer,macBuf) )
   {
      return;
   }
//---------------
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_CODE_PROPERTY;
   sendbuffer[index++] = CMD_GET_CODE_PROPERTY_RECEIVE_DATA_LEN;
   index++;
   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],index - RESERVE_MAC_BUFFER_CNT);
   DataCommunicateStruct.SendSingleBagLen = index;
}
void CDataProtocolHandler::GenerateGetBootVerCmd(unsigned char type,unsigned char verType,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   if(FALSE == TransformMacBuf(type,sendbuffer,macBuf))
   {
      return;
   }
//---------------
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_BULK_DEVICE_VERSION;
   sendbuffer[index++] = CMD_GET_BULK_DEVICE_VERSION_VERSION_DATA_LEN;
   sendbuffer[index++] = verType;
   sendbuffer[index++] = 0x7F - verType;
   index++;
   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],index - RESERVE_MAC_BUFFER_CNT);
   DataCommunicateStruct.SendSingleBagLen = index;
}
//
//
void CDataProtocolHandler::GenerateConnectBleCmd(unsigned char type,unsigned char connectIndex,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;
   int i;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for(i=0;i<(CMD_BLE_MODULE_INTERACTIVE_LEN + RESERVE_MAC_BUFFER_CNT);i++)
   {
       sendbuffer[i] = 0x00;
   }
   if( FALSE == TransformMacBuf(type,sendbuffer,macBuf) )
   {
       return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
   sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
   sendbuffer[index++] = CONNECT_OR_DISCONNECT_CMD_INDEX/10 + 0x30;   //cmdIndex
   sendbuffer[index++] = CONNECT_OR_DISCONNECT_CMD_INDEX%10 + 0x30;     

   sendbuffer[index++] = CONNECT_BLE_DEV_TYPE/10 + 0x30;
   sendbuffer[index++] = CONNECT_BLE_DEV_TYPE%10 + 0x30;
   sendbuffer[index++] = connectIndex/10 + 0x30;
   sendbuffer[index++] = connectIndex%10 + 0x30;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],i-RESERVE_MAC_BUFFER_CNT );
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateDisconnectBleCmd(unsigned char type,unsigned char connectIndex,unsigned char *macBuf)
{
   int index;
   unsigned char *sendbuffer;
   int i;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for(i=0;i<(CMD_BLE_MODULE_INTERACTIVE_LEN + RESERVE_MAC_BUFFER_CNT);i++)
   {
       sendbuffer[i] = 0x00;
   }
   if( FALSE == TransformMacBuf(type,sendbuffer,macBuf) )
   {
       return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE;
   sendbuffer[index++] = CMD_BLE_MODULE_INTERACTIVE_DATA_LEN;
   sendbuffer[index++] = CONNECT_OR_DISCONNECT_CMD_INDEX/10 + 0x30;   //cmdIndex
   sendbuffer[index++] = CONNECT_OR_DISCONNECT_CMD_INDEX%10 + 0x30;     

   sendbuffer[index++] = DISCONNECT_BLE_DEV_TYPE/10 + 0x30;
   sendbuffer[index++] = DISCONNECT_BLE_DEV_TYPE%10 + 0x30;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],i-RESERVE_MAC_BUFFER_CNT );
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateCommunicateCmd(unsigned char type,unsigned char *buffer,int length)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   if(FALSE == TransformMacBuf(type,sendbuffer,NULL))
   {
      return;
   }
//---------------
   index = RESERVE_MAC_BUFFER_CNT;
   for(i=0;i<length;i++)
   {
      sendbuffer[index++] = buffer[i];
   }
   DataCommunicateStruct.SendSingleBagLen = index;
}
//---------------------------------
// generate write user infor cmd
void CDataProtocolHandler::GenerateWriteUserInforCmd(unsigned char type,int internalIndex, unsigned char *wBuf, int length,unsigned char *macBuf)
{
   int index;
   int i,j;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_WRITE_ECG_BLK_INFOR_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
//------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_OR_WRITE_ECG_BLK_INFOR;
   sendbuffer[index++] = CMD_WRITE_ECG_BLK_INFOR_RECEIVE_DATA_LEN;
   
   sendbuffer[index++] = 0x00;
   sendbuffer[index++] = 0x00;
   sendbuffer[index++] = internalIndex | CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG;
   for (j = 0; j<length; j++)
   {
      sendbuffer[index++] = wBuf[j];
   }
   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],i - RESERVE_MAC_BUFFER_CNT);
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetDevMacAddrCmd(unsigned char type, unsigned char *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_BLE_MAC_ADDR_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_BLE_MAC_ADDR;
   sendbuffer[index++] = CMD_GET_BLE_MAC_ADDR_RECEIVE_DATA_LEN;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetDongleRunStaCmd(unsigned char type, unsigned char *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_RUN_MODE_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_RUN_MODE;
   sendbuffer[index++] = CMD_GET_RUN_MODE_RECEIVE_DATA_LEN;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetDongleSaveStaCmd(unsigned char type, unsigned char *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_SAVE_STATUS_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_SAVE_STATUS;
   sendbuffer[index++] = CMD_GET_SAVE_STATUS_SEND_DATA_LEN;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateSetDongleSaveStaCmd(uint8_t type, uint8_t saveMode,uint8_t saveTimeCnt,uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_SET_SAVE_STATUS_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
//------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_SET_SAVE_STATUS;
   sendbuffer[index++] = CMD_SET_SAVE_STATUS_RECEIVE_DATA_LEN;

   sendbuffer[index++] = saveMode;
   sendbuffer[index++] = saveTimeCnt;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateSendTimeStampCmd(uint8_t type, time_t timeStamp, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_SET_CUR_TIMESTAMP_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_SET_CUR_TIMESTAMP;
   sendbuffer[index++] = CMD_SET_CUR_TIMESTAMP_DATA_LEN;

   sendbuffer[index++] = (unsigned char)(timeStamp & 0xFF);
   sendbuffer[index++] = (unsigned char)((timeStamp >> 8) & 0xFF);
   sendbuffer[index++] = (unsigned char)((timeStamp >> 16) & 0xFF);
   sendbuffer[index++] = (unsigned char)((timeStamp >> 24) & 0xFF);

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
void CDataProtocolHandler::GenerateReadTimeStampCmd(uint8_t type,uint8_t *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	for (i = 0; i<(CMD_GET_CUR_TIMESTAMP_LEN + RESERVE_MAC_BUFFER_CNT); i++)
	{
		sendbuffer[i] = 0x00;
	}
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	index = RESERVE_MAC_BUFFER_CNT;
	//------------------------------------
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_GET_CUR_TIMESTAMP;
	sendbuffer[index++] = CMD_GET_CUR_TIMESTAMP_SEND_DATA_LEN;

	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
	DataCommunicateStruct.SendSingleBagLen = i;
}

void CDataProtocolHandler::GenerateActiveEcgSampleCmd(uint8_t type, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_CHANGE_STATUS_CMD_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
//------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD_DATA_LEN;

   sendbuffer[index++] = 0x01;
   sendbuffer[index++] = 0x7E;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateActiveSampleCmd(uint8_t type,uint8_t value,uint8_t *macBuf)
{
    int index;
    int i;
    unsigned char *sendbuffer;

    sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
    for (i = 0; i<(CMD_CHANGE_STATUS_CMD_LEN + RESERVE_MAC_BUFFER_CNT); i++)
    {
       sendbuffer[i] = 0x00;
    }
    if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
    {
       return;
    }
    index = RESERVE_MAC_BUFFER_CNT;
//------------------------------------
    sendbuffer[index++] = ECG_START_HIGH_TMP;
    sendbuffer[index++] = ECG_START_LOW_TMP;
    sendbuffer[index++] = CMD_CHANGE_STATUS_CMD;
    sendbuffer[index++] = CMD_CHANGE_STATUS_CMD_DATA_LEN;

    sendbuffer[index++] = value;
    sendbuffer[index++] = 0x7F - value;

    CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
    DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetActiveSampleCmd(uint8_t type, uint8_t *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	for (i = 0; i<(CMD_GET_RUN_MODE_LEN + RESERVE_MAC_BUFFER_CNT); i++)
	{
		sendbuffer[i] = 0x00;
	}
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	index = RESERVE_MAC_BUFFER_CNT;
	//------------------------------------
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_GET_RUN_MODE;
	sendbuffer[index++] = CMD_GET_RUN_MODE_RECEIVE_DATA_LEN;

	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
	DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateDevSelfCheckCmd(uint8_t type, uint8_t cmd, uint8_t *macBuf)
{
	int index;
	int i;
	unsigned char *sendbuffer;

	sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
	for (i = 0; i<(CMD_DEV_SELF_CHECKING_LEN + RESERVE_MAC_BUFFER_CNT); i++)
	{
		sendbuffer[i] = 0x00;
	}
	if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
	{
		return;
	}
	index = RESERVE_MAC_BUFFER_CNT;
	//------------------------------------
	sendbuffer[index++] = ECG_START_HIGH_TMP;
	sendbuffer[index++] = ECG_START_LOW_TMP;
	sendbuffer[index++] = CMD_DEV_SELF_CHECKING;
	sendbuffer[index++] = CMD_DEV_SELF_CHECKING_RECEIVE_DATA_LEN;
	sendbuffer[index++] = cmd;

	CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
	DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetDongleVerCmd(unsigned char type, unsigned char *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_DONGLE_VERSION_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_DONGLE_VERSION;
   sendbuffer[index++] = CMD_GET_DONGLE_VERSION_SEND_DATA_LEN;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT],(i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//
void CDataProtocolHandler::GenerateGetDongleFixInforCmd(uint8_t type,uint8_t idIndex,uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_DEVICE_INFO_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_DEVICE_INFO;
   sendbuffer[index++] = CMD_GET_DEVICE_INFO_RECEIVE_DATA_LEN;
   sendbuffer[index++] = idIndex;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
//============================================================
//
void CDataProtocolHandler::GenerateGetSaveBlkCnt(unsigned char type, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_ECG_BLK_CNT_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_ECG_BLK_CNT;
   sendbuffer[index++] = CMD_GET_ECG_BLK_CNT_RECEIVE_DATA_LEN;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateEnterReadMode(unsigned char type, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_CHANGE_STATUS_CMD_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD_DATA_LEN;

   sendbuffer[index++] = 0x03;                  //read mode
   sendbuffer[index++] = (char)(0x7F - 0x03);

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateQuitReadMode(unsigned char type, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_CHANGE_STATUS_CMD_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD_DATA_LEN;

   sendbuffer[index++] = 0x01;                  //start mode
   sendbuffer[index++] = (char)(0x7F - 0x01);

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateEnterStopMode(unsigned char type, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_CHANGE_STATUS_CMD_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
   //------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD;
   sendbuffer[index++] = CMD_CHANGE_STATUS_CMD_DATA_LEN;

   sendbuffer[index++] = 0x02;                  //start mode
   sendbuffer[index++] = (char)(0x7F - 0x02);

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
void CDataProtocolHandler::GenerateGetEveryBlkInforCmd(uint8_t type, uint8_t ecgBlkIndex,uint8_t internalIndex, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_ECG_BLK_INFOR_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
//------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_OR_WRITE_ECG_BLK_INFOR;
   sendbuffer[index++] = CMD_GET_ECG_BLK_INFOR_RECEIVE_DATA_LEN;

   sendbuffer[index++] = (unsigned char)(ecgBlkIndex & 0xFF);
   sendbuffer[index++] = 0;

   sendbuffer[index++] = internalIndex;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
// generate get every blk content cmd
void CDataProtocolHandler::GenerateGetEveryBlkContentCmd(uint8_t type, uint8_t feature, int sPageIndex, int ePageIndex, uint8_t *macBuf)
{
   int index;
   int i;
   unsigned char *sendbuffer;

   sendbuffer = DataCommunicateStruct.SendBag.BufferForMac;
   for (i = 0; i<(CMD_GET_ECG_BLK_CONTENT_LEN + RESERVE_MAC_BUFFER_CNT); i++)
   {
      sendbuffer[i] = 0x00;
   }
   if (FALSE == TransformMacBuf(type, sendbuffer, macBuf))
   {
      return;
   }
   index = RESERVE_MAC_BUFFER_CNT;
//--------------------------------------
   sendbuffer[index++] = ECG_START_HIGH_TMP;
   sendbuffer[index++] = ECG_START_LOW_TMP;
   sendbuffer[index++] = CMD_GET_ECG_BLK_CONTENT;
   sendbuffer[index++] = CMD_GET_ECG_BLK_CONTENT_SEND_DATA_LEN;

   sendbuffer[index++] = feature;

   sendbuffer[index++] = (unsigned char)(sPageIndex & 0x7F);
   sendbuffer[index++] = (unsigned char)((sPageIndex >> 7) & 0x7F);
   sendbuffer[index++] = (unsigned char)((sPageIndex >> 14) & 0x7F);

   sendbuffer[index++] = (unsigned char)(ePageIndex & 0x7F);
   sendbuffer[index++] = (unsigned char)((ePageIndex >> 7) & 0x7F);
   sendbuffer[index++] = (unsigned char)((ePageIndex >> 14) & 0x7F);

   sendbuffer[index++] = 0x00;         //speedVal;
   sendbuffer[index++] = 0x00;

   CalXortmpForSendBuffer(&sendbuffer[RESERVE_MAC_BUFFER_CNT], (i - RESERVE_MAC_BUFFER_CNT));
   DataCommunicateStruct.SendSingleBagLen = i;
}
//
// 
BOOL CDataProtocolHandler::TransformMacBuf(unsigned char type,unsigned char *sendBuf,unsigned char *macBuf)
{
   int i;
   if( TYPE_COMMUNICATE_WITHOUT_MACADDR == type )
   {
      for( i = 0;i<RESERVE_MAC_BUFFER_CNT;i++)
         sendBuf[i] = 0x00;
   }
   else if(TYPE_COMMUNICATE_WITH_MACADDR == type)
   {
      for( i = 0;i<RESERVE_MAC_BUFFER_CNT;i++)
         sendBuf[i] = macBuf[i];
   }
   else
   {
      return FALSE;
   }
   return TRUE;
}
//------------------------------------------------------------------------------
// cal xortmp for sendbuffer
void CDataProtocolHandler::CalXortmpForSendBuffer(unsigned char *target,unsigned char len)
{
   unsigned char xortmp;
   unsigned char i;
   
   xortmp = 0;
   for(i=0;i<len-1;i++)
   {
      xortmp = xortmp ^ target[i];
   }
   target[i] = xortmp;
}

