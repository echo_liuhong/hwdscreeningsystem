//#pragma once

#ifndef __SAVE_ECG_STRUCT_H_H__
#define __SAVE_ECG_STRUCT_H_H__

#define MAX_SAVE_BLK_CNT                     64
#define SAVE_DATA_PROPERTY_LEN               128
#define VALID_SAVE_DATA_PROPERTY_LENGTH      124    //20200623
#define SAVE_DATA_PROPERTY_CRC_LEN           (SAVE_DATA_PROPERTY_LEN - 4 )
//-------------------------------------------------------------------
#define HEAD_BUFFER_LEN                      1024
#define FIRST_HEAD_LENGTH                    128
#define EVERY_BAG_LEN                        128
#define GET_HEAD_BAG_CNT                     (HEAD_BUFFER_LEN/EVERY_BAG_LEN)

#define VALID_SAVE_DATA_PROPERTY_LEN         32
#define RESERVE_HEAD_DATA_LEN                ( HEAD_BUFFER_LEN - VALID_SAVE_DATA_PROPERTY_LEN )
#define MAC_LEN                              8
//---------------------------------------------------
#define ID_NUM_BUFFER_LEN                    23
#define USER_ID_BUFFER_LEN                   32
#define SAVE_DATA_ONLY_FLAG_LEN              20
#define BEGIN_BAG_INDEX_LEN                  4
#define END_BAG_INDEX_LEN                    4

#define END_SAVE_BLK_VALUE                   0x55
//----------------------------------------------------
#define DEFAULT_POS_OFFSET_VAL               100

#define DONGLE_CHOOSE_ECG                    0
#define DONGLE_CHOOSE_EMG                    1
#define DONGLE_CHOOSE_EEG                    2
//----------------------------------------------------
typedef struct
{
   unsigned short SampleRate;   
   unsigned char LeadCnt;
   unsigned char LeadType;                                          //...3
   unsigned int StartTimeStamp;                                     //...7
   unsigned int EndTimeStamp;                                       //...11 
 
   unsigned int SaveDataLen;                                        //...15
   unsigned int BeginPageIndex;                                     //...19 

   unsigned int EndPageIndex;                                       //...23
   unsigned char MacAddr[MAC_LEN];                                  //...31
    
   unsigned char IDNumberLen;                                       //...32 
   unsigned char IdNumberBuf[ID_NUM_BUFFER_LEN];                    //...55   

   unsigned char UserIdLen;                                         //
   unsigned char SaveDataOnlyFlagLen;    
   unsigned char EndBlkValue;    
   unsigned char DivideVal;                                         //...59
    
   unsigned char UserIDStrBuf[USER_ID_BUFFER_LEN];                  //...91
   unsigned char SaveDataOnlyFlagBuf[SAVE_DATA_ONLY_FLAG_LEN];      //...111    
   unsigned char AuxType0;                                          //
   unsigned char AuxType1;                                          // 
   unsigned char AuxType2;                                          //
   unsigned char AuxType3;                                          //...115
   unsigned char StartBagIndex[BEGIN_BAG_INDEX_LEN];                //...119
   unsigned char EndBagIndex[END_BAG_INDEX_LEN];                    //...123 
   unsigned int CrcValue;                                           //...127
}SAVE_DATA_PROPERTY_T;
//--------------------
typedef union
{
   unsigned char HeadBuffer[HEAD_BUFFER_LEN];
   SAVE_DATA_PROPERTY_T DataHead;

}SAVE_DATA_HEAD;
//-------------------------------------------------------------------
#define USER_NAME_STR_LEN               20
#define USER_PHONE_NUMBER_STR_LEN       16
#define USER_GENDER_STR_LEN             4
#define USER_AGE_STR_LEN                4
#define USER_HEIGHT_STR_LEN             4
#define USER_WEIGHT_STR_LEN             4 
#define USER_INFOR_SUM_LEN              (USER_NAME_STR_LEN + \
                                        USER_PHONE_NUMBER_STR_LEN + \
                                        USER_GENDER_STR_LEN + \
                                        USER_AGE_STR_LEN + \
                                        USER_HEIGHT_STR_LEN + \
                                        USER_WEIGHT_STR_LEN )
#define USER_INFOR_RESERVE_LEN          (VALID_SAVE_DATA_PROPERTY_LENGTH - USER_INFOR_SUM_LEN)
typedef union
{
   unsigned char Buffer[SAVE_DATA_PROPERTY_LEN];
   struct
   {
      union {
         unsigned char Data[VALID_SAVE_DATA_PROPERTY_LENGTH];
         struct
         {
            unsigned char Name[USER_NAME_STR_LEN];
            unsigned char PhoneNo[USER_PHONE_NUMBER_STR_LEN];
            unsigned char Gender[USER_GENDER_STR_LEN];
            unsigned char Age[USER_AGE_STR_LEN];
            unsigned char Height[USER_HEIGHT_STR_LEN];
            unsigned char Weight[USER_WEIGHT_STR_LEN];
            unsigned char ReserveInfor[USER_INFOR_RESERVE_LEN];
         }Detailed;
       }UserInfor;
       unsigned int BagCrc;
       
   }Content;
	
}SAVE_BLK_DETAILED_INFOR;
//-------------------------------------------------------------------

#endif
