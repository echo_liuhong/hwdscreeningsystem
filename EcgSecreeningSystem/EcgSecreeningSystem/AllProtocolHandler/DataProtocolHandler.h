
#ifndef __DATA_PROTOCOL_HANDLER_H_H__
#define __DATA_PROTOCOL_HANDLER_H_H__

#include "windows.h"
#include "CompleteCommunicateProtocol.h"
#include "stdint.h"

//-------------------------------------------------------------------------
#define TYPE_COMMUNICATE_WITH_MACADDR              0
#define TYPE_COMMUNICATE_WITHOUT_MACADDR           1
//-------------------------------------------------------------------------
#define CONNECT_OR_DISCONNECT_CMD_INDEX            0x02

#define CONNECT_BLE_DEV_TYPE                       0x01
#define DISCONNECT_BLE_DEV_TYPE                    0x02

#define PC_WITH_BULK_INTERACTIVE_CMD_INDEX         0x04
#define ACTIVE_USB_BULK_READ                       0x01
#define INACTIVE_USB_BULK_READ                     0x02

#define GET_BLE_DEV_INFOR_CMD_INDEX                0x05
#define GET_BLE_DEV_CONNECT_STATUS                 0x01

#define GET_BULK_FIX_SERIAL_NUM_CMD_INDEX               0x06
#define GET_BULK_FIX_SERIAL_NUM_CMD_BUFFER_LEN          64

#define GET_CHCK_BULK_FIX_SERIAL_NUM_CMD_INDEX          0x07
#define GET_CHCK_BULK_FIX_SERIAL_NUM_CMD_BUFFER_LEN     64
//-------------------------------------------------------------------------
enum{
   NO_RECEIVE_BAG_RECEIVE = 0,
   HAVE_RECEIVE_BAG_RECEIVE, 
};
enum{
   NO_CHECK_HEAD_FLAG = 0,
   HAVE_CHECK_HEAD_FLAG
};
enum{
   RECEIVE_VIA_PC_USART = 0,
   RECEIVE_VIA_BLUETOOTH
};
enum{
   NO_NEED_ACK_CMD = 0,
   NEED_ACK_CMD,
};
enum{
   NO_NEED_PROTOCOL_RESTART = 0,
   NEED_PROTOCOL_RESTART,
};
//--------------------------------------------
#define RECEIVE_SAME_CMD_TYPE_CNT         8
typedef struct
{
   unsigned char ReceiveFlag:1;
   unsigned char CheckHeadFlag:1;
   unsigned char AckFlag:1;
   unsigned char ProtocolRestartFlag:1;
   unsigned char ReserveBit:4;

   unsigned char ReceiveStep;
   unsigned char ReceiveIndex;
   unsigned char ReceiveCount;
	 
   unsigned char ReceiveCmdCnt;           //20210120
   unsigned char ReceiveCmdIndex[8];      //20210120

   unsigned char Xortmp;
   unsigned char ChckHeadStep;
   unsigned char ChckCmdIndex; 
   unsigned char CheckCmdType;
   unsigned char ReserveByte;
   unsigned short SendSingleBagLen;    

   COMMUNICATE_BAG_STRUCT ReceiveBag; 
   COMMUNICATE_BAG_STRUCT SendBag; 

}COMPLETE_COMMUNICATE_BAG;
//-------------------------------------------------------------------------------------------------
//
// CComDataHandler
//
class CDataProtocolHandler
{
//-----------------------------------------------------
public:
   static const unsigned char ReceTypeAndLenTable[RECEIVE_UART_CMD_CNT*2];
   COMPLETE_COMMUNICATE_BAG DataCommunicateStruct;
//-------------------------------------------
   void InitialCompleteCommunicateBag(void);
   void HandlerCommunicateBag(unsigned char ribuf);

   void GenerateWithBulkInteractiveCmd(uint8_t type, uint8_t runFlag, unsigned char *macBuf);
   void GenerateGetBulkFixSerialNumCmd(uint8_t type, uint8_t *buff, unsigned char *macBuf);
   void GenerateCheckBulkFixSerialNumValidityCmd(uint8_t type, uint8_t *buff, unsigned char *macBuf);
   void GenerateGetBleDevConnectStaCmd(uint8_t type, unsigned char *macBuf);
   //-----------------------------------------------------
   void GenerateActiveBootloaderCmd(unsigned char type,unsigned char status,unsigned char *macBuf);// include reset,sleep,shutdown
   void GenerateGetCodePropertyCmd(unsigned char type,unsigned char *macBuf);                      // include 
   void GenerateGetBootVerCmd(unsigned char type,unsigned char verType,unsigned char *macBuf);
   void GenerateConnectBleCmd(unsigned char type,unsigned char connectIndex,unsigned char *macBuf);
   void GenerateDisconnectBleCmd(unsigned char type,unsigned char connectIndex,unsigned char *macBuf);
//-----------------------------------------------------
   void GenerateControlStatusCmd(unsigned char type, unsigned char status, unsigned char *macBuf);   // include reset,sleep,shutdown
   void GenerateGetDevMacAddrCmd(unsigned char type, unsigned char *macBuf);
   void GenerateGetDongleVerCmd(unsigned char type, unsigned char *macBuf);
   void GenerateGetDongleFixInforCmd(uint8_t type, uint8_t idIndex, uint8_t *macBuf);
   void GenerateGetDongleRunStaCmd(unsigned char type, unsigned char *macBuf);
   void GenerateGetDongleSaveStaCmd(unsigned char type, unsigned char *macBuf);
   void GenerateSetDongleSaveStaCmd(uint8_t type,uint8_t saveMode,uint8_t saveTimeCnt,uint8_t *macBuf);

   void GenerateSendTimeStampCmd(uint8_t type, time_t timeStamp,uint8_t *macBuf);
   void GenerateReadTimeStampCmd(uint8_t type, uint8_t *macBuf);
   void GenerateActiveEcgSampleCmd(uint8_t type,uint8_t *macBuf);
   void GenerateActiveSampleCmd(uint8_t type, uint8_t value, uint8_t *macBuf);
   void GenerateGetActiveSampleCmd(uint8_t type, uint8_t *macBuf);
   void GenerateDevSelfCheckCmd(uint8_t type, uint8_t cmd, uint8_t *macBuf);
   void GenerateWriteUserInforCmd(unsigned char type, int internalIndex, unsigned char *wBuf, int length, unsigned char *macBuf);
   void GenerateCommunicateCmd(unsigned char type,unsigned char *buffer,int length);


   void GenerateGetSaveBlkCnt(unsigned char type,uint8_t *macBuf);
   void GenerateEnterReadMode(unsigned char type, uint8_t *macBuf);
   void GenerateQuitReadMode(unsigned char type, uint8_t *macBuf);
   void GenerateEnterStopMode(unsigned char type, uint8_t *macBuf);
   void GenerateGetEveryBlkInforCmd(uint8_t type,uint8_t ecgBlkIndex,uint8_t internalIndex,uint8_t *macBuf );
   void GenerateGetEveryBlkContentCmd(uint8_t type, uint8_t feature, int sPageIndex,int ePageIndex, uint8_t *macBuf);

   BOOL TransformMacBuf(unsigned char type,unsigned char *sendBuf,unsigned char *macBuf);
public:
   // Constructor and Destructor
   CDataProtocolHandler();
   virtual ~CDataProtocolHandler();
private:
//-------------------------------------------------
// handle ble communicate part
//-------------------------------------------------
// analysis valid cmd head
   void AnalysisReceiveHead(unsigned char ribuf);
   void Step_FirstHandler(unsigned char ribuf);
   void Step_SecondHandler(unsigned char ribuf);
   void Step_ThirdHandler(unsigned char ribuf);
   void Step_ForthHandler(unsigned char ribuf);
//------------------------------------------------
// analysis data content
   void AnalysisReceiveDataContent(unsigned char ribuf);
   void CheckCmdHeadInDataContent(unsigned char ribuf);

   void Step_FifthHandler(unsigned char ribuf);
   void Step_SixthHandler(unsigned char ribuf);

   void CalXortmpForSendBuffer(unsigned char *target,unsigned char len);
};
//-----------------------------------------------------
#endif