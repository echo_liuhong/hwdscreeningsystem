/**
  ******************************************************************************
  * @file    CompleteCommunicateProtocol.h
  * @author  liuhong
  * @version V1.0
  * @date    12 - October - 2019
  * @brief   Complete communicate protocol.
  ******************************************************************************
  **/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMPLETE_COMMUNICATE_PROTOCOL_H_H__
#define __COMPLETE_COMMUNICATE_PROTOCOL_H_H__

//------------------------------------------------------------------
#define  UPDATE_PAGE_LENTH                                 4096     //
//------------------------------------------------------------------
#define RESERVE_MAC_BUFFER_CNT                             8
#define CMD_HEAD_PART_CNT                                  4
#define CMD_CHECKSUM_PART_CNT                              1
#define CMD_HEAD_AND_CHECKSUM_CNT                          (CMD_HEAD_PART_CNT + CMD_CHECKSUM_PART_CNT)
#define INVERT_VALUE                                       0x7F
//-----------------------------------
#define ECG_START_HIGH_TMP                                 0xA5
#define ECG_START_LOW_TMP                                  0x5A
//-------------------------------------------------
#define RECEIVE_UART_CMD_CNT                               (48)//(1)

#define CMD_SINGLE_ECG_WAVE                                0x01
#define CMD_SINGLE_ECG_WAVE_DATA_LEN                       0x65

#define CMD_ORIGINAL_SINGLE_ECG_WAVE                       0x11
#define CMD_ORIGINAL_SINGLE_ECG_WAVE_DATA_LEN              0x97

#define CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE                0x31
#define CMD_SINGLE_EMG_AND_ACCELERATOR_WAVE_DATA_LEN       0x6C

#define CMD_ONEHALF_EMG_AND_ACCELERATOR_WAVE_DATA_LEN      0x9E

#define CMD_ECG_ONEHALF_FOR_BAGINDEX                       0x52
#define CMD_ECG_ONEHALF_FOR_BAGINDEX_DATA_LEN              0xA1

#define CMD_EMG_ONEHALF_FOR_BAGINDEX                       0x66
#define CMD_EMG_ONEHALF_FOR_BAGINDEX_DATA_LEN              0xA7
//-----------------------------------------------------------------
//cmd:0x40 
#define CMD_GET_DONGLE_POWER                               0x40
// GET DONGLE POWER
#define CMD_GET_DONGLE_POWER_DATA_LEN                      0x02
#define CMD_GET_DONGLE_POWER_LEN                           ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_DONGLE_POWER_DATA_LEN )
//----------------------------
#define CMD_GET_CONNECT_STATUS                             0x41
//GET CONNECT STATUS
#define CMD_GET_CONNECT_STATUS_DATA_LEN                    0x02
#define CMD_GET_CONNECT_STATUS_LEN                         ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_CONNECT_STATUS_DATA_LEN )
//----------------------------
#define CMD_GET_BASE_COMMUNICATE_INFO                      0x42
//GET BASE COMMUNICATE
#define CMD_GET_BASE_COMMUNICATE_INFO_DATA_LEN             0x0A
#define CMD_GET_BASE_COMMUNICATE_INFO_LEN                  ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_BASE_COMMUNICATE_INFO_DATA_LEN )
//-----------------------------
#define CMD_GET_ECG_BLK_CNT                                0x43
//GET ECG BLK CNT                                   
#define CMD_GET_ECG_BLK_CNT_RECEIVE_DATA_LEN               0x00
#define CMD_GET_ECG_BLK_CNT_SEND_DATA_LEN                  0x02
#define CMD_GET_ECG_BLK_CNT_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_ECG_BLK_CNT_RECEIVE_DATA_LEN )
//-----------------------------
#define CMD_GET_OR_WRITE_ECG_BLK_INFOR                     0x44
//GET ECG BLK INDEX INFOR                                  
#define CMD_GET_ECG_BLK_INFOR_RECEIVE_DATA_LEN             0x03
#define CMD_WRITE_ECG_BLK_INFOR_RECEIVE_DATA_LEN           0x83

#define CMD_GET_ECG_BLK_INFOR_SEND_DATA_LEN                0x83
#define CMD_GET_ECG_BLK_INFOR_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_ECG_BLK_INFOR_RECEIVE_DATA_LEN )
#define CMD_WRITE_ECG_BLK_INFOR_LEN                        ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_ECG_BLK_INFOR_SEND_DATA_LEN )
#define CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG                 0x10
//-----------------------------
#define CMD_GET_ECG_BLK_CONTENT                            0x45
//GET ECG BLK INDEX INFOR                                  
#define CMD_GET_ECG_BLK_CONTENT_SEND_DATA_LEN                    0x09
#define CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN_FOR_SHORTBAG    0x95
#define CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN_FOR_LONGBAG     0xE3
#define CMD_GET_ECG_BLK_CONTENT_SEND_DATA_LEN_FOR_COMPLETE_PAGE  0x85

//#define CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN               0x95
//#define CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN               0x95
//#define CMD_GET_ECG_BLK_CONTENT_RECEIVE_DATA_LEN               0x85
#define CMD_GET_ECG_BLK_CONTENT_LEN                        ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_ECG_BLK_CONTENT_SEND_DATA_LEN )
//------------------------------------------------------------------
//cmd:0x50 
#define CMD_GET_CUR_TIMESTAMP                              0x50
#define CMD_GET_CUR_TIMESTAMP_SEND_DATA_LEN                0x00
#define CMD_GET_CUR_TIMESTAMP_RECEIVE_DATA_LEN             0x04
#define CMD_GET_CUR_TIMESTAMP_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_CUR_TIMESTAMP_SEND_DATA_LEN )
//------------------------------------
#define CMD_SET_CUR_TIMESTAMP                              0x51
#define CMD_SET_CUR_TIMESTAMP_DATA_LEN                     0x04
#define CMD_SET_CUR_TIMESTAMP_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_CUR_TIMESTAMP_DATA_LEN )
//------------------------------------
#define CMD_READ_TIMESTAMP_LOG                             0x52

#define CMD_READ_TIMESTAMP_LOG_RECEIVE_DATA_LEN            0x00
#define CMD_READ_TIMESTAMP_LOG_SEND_DATA_LEN               0x09
#define CMD_READ_TIMESTAMP_LOG_SEND_LEN                    (CMD_HEAD_AND_CHECKSUM_CNT + CMD_READ_TIMESTAMP_LOG_SEND_DATA_LEN )
//------------------------------------
#define CMD_ERASE_TIMESTAMP_LOG_AREA                       0x53

#define CMD_ERASE_TIMESTAMP_LOG_AREA_DATA_LEN              0x02
#define CMD_ERASE_TIMESTAMP_LOG_AREA_LEN                   (CMD_HEAD_AND_CHECKSUM_CNT + CMD_ERASE_TIMESTAMP_LOG_AREA_DATA_LEN )
#define ERASE_TIMESTAMP_LOG_CMD_VALUE                      0x55
//------------------------------------
#define CMD_DEV_SELF_CHECKING                              0x54

#define CMD_DEV_SELF_CHECKING_RECEIVE_DATA_LEN             0x01
#define CMD_DEV_SELF_CHECKING_SEND_DATA_LEN                0x17
#define CMD_DEV_SELF_CHECKING_LEN                          (CMD_HEAD_AND_CHECKSUM_CNT + CMD_DEV_SELF_CHECKING_RECEIVE_DATA_LEN )
#define START_SELF_CHECKING_CMD                            1
#define STOP_SELF_CHECKING_CMD                             2

#define DEV_SELF_CHECKING_PROGRESS                         1
#define DEV_SELF_CHECKING_COMPLETE                         2
//-----------------------------------
#define CMD_ERROR_LOG_HANDLER                              0x5F

#define CMD_ERROR_LOG_RECEIVE_DATA_LEN                     0x04
#define CMD_ERROR_LOG_SEND_DATA_LEN                        0x83
#define CMD_ERROR_LOG_HANDLER_LEN                          (CMD_HEAD_AND_CHECKSUM_CNT + CMD_ERROR_LOG_RECEIVE_DATA_LEN )
//------------------------------------------------------
#define CMD_CHANGE_STATUS_CMD                              0x60
#define CMD_CHANGE_STATUS_CMD_DATA_LEN                     0x02
#define CMD_CHANGE_STATUS_CMD_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_CHANGE_STATUS_CMD_DATA_LEN )
//-------------------------
#define CMD_GET_RUN_MODE                                   0x61
#define CMD_GET_RUN_MODE_RECEIVE_DATA_LEN                  0x00
#define CMD_GET_RUN_MODE_SEND_DATA_LEN                     0x02
#define CMD_GET_RUN_MODE_LEN                               ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_RUN_MODE_RECEIVE_DATA_LEN )
#define INVALID_DEV_RUNMODE                                0
#define ACTIVE_DEV_RUNMODE                                 1
#define STOP_DEV_RUNMODE                                   2
#define READ_DEV_RUNMODE                                   3
//-------------------------
#define CMD_GET_SAVE_STATUS                                0x62
#define CMD_GET_SAVE_STATUS_RECEIVE_DATA_LEN               0x09
#define CMD_GET_SAVE_STATUS_SEND_DATA_LEN                  0x00
#define CMD_GET_SAVE_STATUS_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_SAVE_STATUS_SEND_DATA_LEN )
//------------------------- 
#define CMD_SET_SAVE_STATUS                                0x63
#define CMD_SET_SAVE_STATUS_RECEIVE_DATA_LEN               0x02
#define CMD_SET_SAVE_STATUS_SEND_DATA_LEN                  0x06
#define CMD_SET_SAVE_STATUS_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_SAVE_STATUS_RECEIVE_DATA_LEN )
#define CMD_ENTER_INTO_SAVE_MODE                           1
#define CMD_QUIT_INTO_SAVE_MODE                            2
#define CMD_ENTER_INTO_AUTO_SAVE_MODE                      3
#define CMD_ERASE_BLK_CONTENT                              4
#define MAX_SAVE_HOURS_CNT                                 72

#define CMD_GET_SAVE_LEFTTIME                              0x64
#define CMD_GET_SAVE_LEFTTIME_DATA_LEN                     0x04
#define CMD_GET_SAVE_LEFTTIME_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_SAVE_LEFTTIME_DATA_LEN )
#define SAVE_ECG_IS_PROGRESS                               0x01
#define SAVE_ECG_IS_COMPLETE                               0x02  

#define CMD_SET_SAMPLERATE_VAL                             0x65
#define CMD_SET_SAMPLERATE_VAL_DATA_LEN                    0x05
#define CMD_SET_SAMPLERATE_VAL_LEN                         ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_SAMPLERATE_VAL_DATA_LEN )
#define MODIFY_CURRENT_SAMPLERATE                          1
#define READ_CURRENT_SAMPLERATE                            2
//---------------------------------------------------------
#define CMD_GET_BULK_DEVICE_VERSION                        0x70
#define CMD_GET_BULK_DEVICE_VERSION_VERSION_DATA_LEN       0x02
#define CMD_GET_BULK_DEVICE_VERSION_LEN                    ( CMD_HEAD_AND_CHECKCUM_CNT + CMD_GET_BULK_DEVICE_VERSION_VERSION_DATA_LEN )
#define CMD_ACK_GET_BULK_DEVICE_VERSION_DATA_LEN           0x22
#define BOOT_VERSION_TYPE                                  0x01
#define APP_VERSION_TYPE                                   0x02

#define CMD_GET_DONGLE_VERSION                             0x70
#define CMD_GET_DONGLE_VERSION_SEND_DATA_LEN               0x00
#define CMD_GET_DONGLE_VERSION_RECEIVE_DATA                0x02
#define CMD_GET_DONGLE_VERSION_LEN                         ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_DONGLE_VERSION_SEND_DATA_LEN )
#define CMD_ACK_GET_DONGLE_VERSION_DATA_LEN                0x0F

#define CMD_GET_DONGLE_BOOT_VERSION                        0x71
#define CMD_GET_DONGLE_BOOT_VERSION_DATA_LEN               0x0
#define CMD_GET_DONGLE_BOOT_VERSION_LEN                    ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_DONGLE_BOOT_VERSION_DATA_LEN )
#define CMD_ACK_GET_DONGLE_BOOT_VERSION_DATA_LEN           0x0E

#define CMD_SET_WIFI_INFOR                                 0x72
#define CMD_SET_WIFI_INFOR_DATA_LEN                        0x42
#define CMD_SET_WIFI_INFOR_LEN                             (CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_WIFI_INFOR_DATA_LEN)
#define WIFI_INFOR_ERROR_FEEDBACK                          0x40
#define WIFI_INFOR_ERROR_PART                              0x3F
#define WIFI_PASSWORD_LEN_ERROR                            0x00

#define CMD_GET_WIFI_INFOR                                 0x73
#define CMD_GET_WIFI_INFOR_RECEIVE_DATA_LEN                0
#define CMD_GET_WIFI_INFOR_DATA_LEN                        0x42
#define CMD_GET_WIFI_INFOR_LEN                             ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_WIFI_INFOR_DATA_LEN )

#define CMD_SET_DEVICE_INFO                                0x74
#define CMD_SET_DEVICE_INFO_DATA_LEN                       0x22
#define CMD_SET_DEVICE_INFO_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_DEVICE_INFO_DATA_LEN )

#define CMD_GET_DEVICE_INFO                                0x75
#define CMD_GET_DEVICE_INFO_RECEIVE_DATA_LEN               0x01
#define CMD_GET_DEVICE_INFO_SEND_DATA_LEN                  0x22
#define CMD_GET_DEVICE_INFO_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_DEVICE_INFO_RECEIVE_DATA_LEN )
#define CMD_GET_MANUFACTURE_NAME_INDEX                     0x01
#define CMD_GET_MODEL_NUMBER_INDEX                         0x02
#define CMD_GET_SERIAL_NUMBER_INDEX                        0x03
#define CMD_GET_FIRMWARE_REVERSION_INDEX                   0x04

#define CMD_GET_GAIN_VALUE                                 0x76
#define CMD_GET_GAIN_VALUE_RECEIVE_DATA_LEN                0x00
#define CMD_GET_GAIN_VALUE_SEND_DATA_LEN                   0x02
#define CMD_GET_GAIN_VALUE_LEN                             ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_GAIN_VALUE_SEND_DATA_LEN )

#define CMD_GET_UPLOAD_ADDR                                0x78
#define CMD_GET_UPLOAD_ADDR_RECEIVE_DATA_LEN               0x01
#define CMD_GET_UPLOAD_ADDR_SEND_DATA_LEN                  0x7A
#define CMD_GET_UPLOAD_ADDR_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_UPLOAD_ADDR_SEND_DATA_LEN )
#define GET_UPLOAD_A_URL_ADDR_INDEX                        1  
#define GET_UPLOAD_B_URL_ADDR_INDEX                        2 
//--------------------------------------------------
#define CMD_SET_UPLOAD_ADDR                                0x79
#define CMD_SET_UPLOAD_ADDR_DATA_LEN                       0x7A
#define CMD_SET_UPLOAD_ADDR_LEN                            ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_UPLOAD_ADDR_DATA_LEN )

#define CMD_GET_BASE_VERSION                               0x7A
#define CMD_GET_BASE_VERSION_RECEIVE_DATA_LEN              0x00   
#define CMD_GET_BASE_VERSION_SEND_DATA_LEN                 13
#define CMD_GET_BASE_VERSION_LEN                           ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_BASE_VERSION_SEND_DATA_LEN )
//--------------------------------------------------
#define CMD_SET_USER_INFOR                                 0x7C
#define CMD_SET_USER_INFOR_DATA_LEN                        0x22   
#define CMD_SET_USER_INFOR_LEN                             ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_USER_INFOR_DATA_LEN )

#define CMD_GET_USER_INFOR                                 0x7D
#define CMD_GET_USER_INFOR_RECEIVE_DATA_LEN                0x01   
#define CMD_GET_USER_INFOR_SEND_DATA_LEN                   0x22
#define CMD_GET_USER_INFOR_LEN                             ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_USER_INFOR_SEND_DATA_LEN )

#define CMD_GET_USER_ID_INDEX                              0x01
#define CMD_GET_SAVE_ONLY_FLAG_INDEX                       0x02
#define CMD_GET_USER_INFOR_CNT                             0x02
//--------------------------------------------------
#define CMD_GET_BLE_MAC_ADDR                               0x7E
#define CMD_GET_BLE_MAC_ADDR_RECEIVE_DATA_LEN              0x00
#define CMD_GET_BLE_MAC_INFO_SEND_DATA_LEN                 0x06
#define CMD_GET_BLE_MAC_ADDR_LEN                           ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_BLE_MAC_ADDR_RECEIVE_DATA_LEN )
//------------------------------------------------------------------
// boot cmd part
#define CMD_GET_CODE_PROPERTY                              0x68
#define CMD_GET_CODE_PROPERTY_RECEIVE_DATA_LEN             0x00
#define CMD_GET_CODE_PROPERTY_SEND_DATA_LEN                0x02
#define CMD_GET_CODE_PROPERTY_SEND_LEN                     ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_GET_CODE_PROPERTY_SEND_DATA_LEN )

#define CMD_SET_DONGLE_QUIT_STA                            0x69
#define CMD_SET_DONGLE_QUIT_STA_DATA_LEN                   0x02
#define CMD_SET_DONGLE_QUIT_STA_LEN                        ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_SET_DONGLE_QUIT_STA_DATA_LEN )
#define ENTER_INTO_POWEROFF_STA                            0x33
#define CANCEL_POWEROFF_STA                                0x55
#define ENTER_INTO_RESET_STA                               0x35
#define CANCEL_AUTO_SLEEP_STA                              0x34
#define RESUME_AUTO_SLEEP_STA                              0x36

#define CMD_ACTIVE_BOOTLOADER                              0x6A
#define CMD_ACTIVE_BOOTLOADER_DATA_LEN                     0x02
#define CMD_ACTIVE_BOOTLOADER_LEN                          ( CMD_HEAD_AND_CHECKSUM_CNT + CMD_ACTIVE_BOOTLOADER_DATA_LEN )
#define ACTIVE_BULK_FIRMWARE_BOOT                          0x01
#define ACTIVE_BULK_FIRMWARE_APP                           0x02
//------------------------------------
#define CMD_UPDATE_HEAD_BAG                                0x6B
#define CMD_UPDATE_HEAD_BAG_DATA_LEN                       36
#define CMD_UPDATE_HEAD_BAG_LEN                            (CMD_UPDATE_HEAD_BAG_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT)
#define CMD_UPDATE_HEAD_BAG_ACK_DATA_LEN                   5
#define CMD_UPDATE_HEAD_BAG_ACK_LEN                        (CMD_UPDATE_START_BAG_ACK_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT)

#define DATA_CNT_EVERY_BAG                                 32
#define CMD_UPDATE_DATA_BAG                                0x6C
#define CMD_UPDATE_DATA_BAG_DATA_LEN                       47
#define CMD_UPDATE_DATA_BAG_LEN                            (CMD_UPDATE_DATA_BAG_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT) 
#define CMD_UPDATE_DATA_BAG_ACK_DATA_LEN                   5
#define CMD_UPDATE_DATA_BAG_ACK_LEN                        (CMD_UPDATE_DATA_BAG_ACK_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT) 

#define CMD_UPDATE_FINISH_BAG                              0x6D
#define CMD_UPDATE_FINISH_BAG_DATA_LEN                     1
#define CMD_UPDATE_FINISH_BAG_LEN                          (CMD_UPDATE_FINISH_BAG_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT) 
#define CMD_UPDATE_FINISH_VAL                              0x55

#define CMD_BLE_MODULE_INTERACTIVE                         0x6F
#define CMD_BLE_MODULE_INTERACTIVE_DATA_LEN                102
#define CMD_BLE_MODULE_INTERACTIVE_LEN                     (CMD_BLE_MODULE_INTERACTIVE_DATA_LEN + CMD_HEAD_AND_CHECKSUM_CNT) 
//------------------------------------------------------------------
#define VERSION_POSITION_VAL                               (0xA000)
#define UPDATE_VERSION_LENGTH                              19
#define VERSION_PART_LEN                                   20
#define CRC_LENGTH                                         5
#define HEADBAG_CONTENT_LEN                                31

#define FIRST_OFFSET_VAL                                   CMD_UPDATE_START_BAG_LEN
//---------------------------------------------------------
#define ENCIPHER_SINGLE_ECG_VALUE             0
#define ENCIPHER_USB_BULK_VALUE               1
#define ENCIPHER_BLE_CENTER_VALUE             2
//---------------------------------------------------------
//BOOT protocol
typedef struct
{
   unsigned char BagCntHigh;
   unsigned char BagCntLow;                             //..1

   unsigned char CodeType;
   unsigned char CodeLengthHigh;
   unsigned char CodeLengthMid;
   unsigned char CodeLengthLow;                         //...5

   unsigned char VersionInfor[VERSION_PART_LEN];        //25

   unsigned char CodeCrc[CRC_LENGTH];                   //30
   unsigned char BagCrc[CRC_LENGTH];                    //35

}UPDATE_HEAD_BAG;

typedef struct
{
   unsigned short TotalBagCnt;
   unsigned int CodeLength;

   unsigned char VersionInfo[VERSION_PART_LEN];
   unsigned int CodeCrc;
   unsigned int BagCrc;

}HEAD_BAG;

typedef struct
{
   unsigned char CmdHigh;
   unsigned char CmdLow;
   unsigned char Type;
   unsigned char Len;
   UPDATE_HEAD_BAG HeadBag;
   unsigned char xortmp;

}UPDATE_COMMUNICATE_HEAD_BAG;
//------------------------------------------------
#define DATA_CONTENT_LENGTH                      32
#define COMMUNICATE_DATA_BAG_LENGTH              ((DATA_CONTENT_LENGTH/4)*5)
#define DATABAG_CONTENT_LEN                      42
typedef struct
{
   unsigned char UpdateBagIndexHigh;    
   unsigned char UpdateBagIndexLow;

   unsigned char DataContent[COMMUNICATE_DATA_BAG_LENGTH];
   unsigned char DataCrc[CRC_LENGTH];

}UPDATE_DATA_BAG;

typedef struct
{
   unsigned short UpdateBagIndex;
   unsigned char DataContent[DATA_CONTENT_LENGTH];
   unsigned int BagCrc;

}DATA_BAG;

typedef struct
{
   unsigned char CmdHigh;
   unsigned char CmdLow;
   unsigned char Type;
   unsigned char Len;
   UPDATE_DATA_BAG DataBag;
   unsigned char xortmp;
    
}UPDATE_COMMUNICATE_DATA_BAG;
typedef struct
{
   unsigned char CmdHigh;
   unsigned char CmdLow;
   unsigned char Type;
   unsigned char Len;
   unsigned char CompleteVal;
   unsigned char xortmp;
    
}UPDATE_COMMUNICATE_FINISH_BAG;
//----------------------------------------------------
#define MAX_HANDLE_BYTE_COUNT                     256
#define MAX_RECEIVE_BYTE_COUNT                    MAX_HANDLE_BYTE_COUNT
#define MAX_SEND_BYTE_COUNT                       MAX_HANDLE_BYTE_COUNT
#define COMMUNICATE_BAG_LENGTH                    ( CMD_HEAD_PART_CNT + MAX_RECEIVE_BYTE_COUNT )

typedef struct
{
   unsigned char BufferForMac[RESERVE_MAC_BUFFER_CNT];
   unsigned char HeadHigh;
   unsigned char HeadLow;
   unsigned char Type;
   unsigned char Length;

   unsigned char Buffer[MAX_HANDLE_BYTE_COUNT];

}COMMUNICATE_BAG_STRUCT;
//-----------------------------------------------------
#define MAX_INTERNAL_INDEX_EVERY_BLK_INFOR        8

#define MAX_BLE_INTERNAL_CNT_FOR_SHORTBAG         11
#define EVERY_BLE_BAG_DATA_LEN_FOR_SHORTBAG       0x90
#define MID_BLE_BAG_DATA_LEN_FOR_SHORTBAG         EVERY_BLE_BAG_DATA_LEN_FOR_SHORTBAG
#define LAST_BLE_BAG_DATA_LEN_FOR_SHORTBAG        0x60 

#define MID_BLE_BAG_COUNT_FOR_SHORTBAG            96
#define LAST_BAG_DATA_CNT_FOR_SHORTBAG            64
//------------------------------------
#define MAX_BLE_INTERNAL_CNT_FOR_LONGBAG          7
#define EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG        0xDE
#define MID_BLE_BAG_DATA_LEN_FOR_LONGBAG          EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG
#define LAST_BLE_BAG_DATA_LEN_FOR_LONGBAG         0xCC   //204

#define MID_BLE_BAG_COUNT_FOR_LONGBAG             148
#define LAST_BAG_DATA_CNT_FOR_LONGBAG             136
//-----------------------------------
#define MAX_BLE_INTERNAL_CNT_FOR_COMPLETE_DATA          16
#define EVERY_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA        0x80
#define MID_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA          EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG
#define LAST_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA         0

#define MID_BLE_BAG_COUNT_FOR_COMPLETE_DATA             128
#define LAST_BAG_DATA_CNT_FOR_COMPLETE_DATA             0
//------------------------------------
#define SAVE_HEAD_LEN                             1024
#define ORIGINAL_DATA_COUNT                       1536
#define SAVE_DATA_COUNT                           2048

enum{
   DATA_BLE_UPLOAD_WITH_ACK = 0,
   DATA_BLE_UPLOAD_WITHOUT_ACK,
};
typedef struct
{
   unsigned short ReserveHalfWord;
   unsigned short CheckTmp;
   unsigned int PageIndex;
   union
   {
      unsigned char ShortBag[MAX_BLE_INTERNAL_CNT_FOR_SHORTBAG][EVERY_BLE_BAG_DATA_LEN_FOR_SHORTBAG];
      unsigned char LongBag[MAX_BLE_INTERNAL_CNT_FOR_LONGBAG][EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG];
	  unsigned char MidBag[MAX_BLE_INTERNAL_CNT_FOR_COMPLETE_DATA][EVERY_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA];

   }DataInfor;
}PAGE_CONTENT_STRUCT;
//-----------------------------------------------------
#define EVERY_DETAILED_DATA_CNT        159
typedef union 
{   
	unsigned char dataBufer[EVERY_DETAILED_DATA_CNT];
	struct
	{
	   unsigned char timeStamp[5];
	   unsigned char bagIndex[4];
	   unsigned char dataBuf[EVERY_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA];
	}Detailed;
}EVERY_SEND_BAG_STRUCT;

#endif //__COMPLETE_COMMUNICATE_PROTOCOL_H_H__
