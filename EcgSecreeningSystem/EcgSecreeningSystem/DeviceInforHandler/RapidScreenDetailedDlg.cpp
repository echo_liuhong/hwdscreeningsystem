// RapidScreenDetailedDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "RapidScreenDetailedDlg.h"
#include "afxdialogex.h"

#include "EcgSecreeningSystemDlg.h"
// CRapidScreenDetailedDlg 对话框

//--------------------------------------------------------------
#define FEMALE_GENDER_INDEX               0
#define MALE_GENDER_INDEX                 1
#define OTHER_GENDER_INDEX                2
#define DEFAULT_GENDER_CHOOSE_INDEX       MALE_GENDER_INDEX
//--------------------------------------------------------------
IMPLEMENT_DYNAMIC(CRapidScreenDetailedDlg, CDialog)

CRapidScreenDetailedDlg::CRapidScreenDetailedDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_RAPID_SCREEN_DETAILED_DLG, pParent)
   , UserName(_T(""))
   , UserAge(_T(""))
   , UserHeight(_T(""))
   , UserWeight(_T(""))
   , UserPhoneNumber(_T(""))
{
   BleInteractiveStruct.HandleFlag = FALSE;
}

CRapidScreenDetailedDlg::~CRapidScreenDetailedDlg()
{
}

void CRapidScreenDetailedDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Text(pDX, IDC_NAME_DETAILED_VER, UserName);
   DDX_Text(pDX, IDC_AGE_DETAILED_VER, UserAge);
   DDV_MaxChars(pDX, UserAge, 3);
   DDX_Text(pDX, IDC_HEIGHT_DELTAIED_VER, UserHeight);
   DDV_MaxChars(pDX, UserHeight, 3);
   DDX_Text(pDX, IDC_WEIGHT_DETAILED_VER, UserWeight);
   DDV_MaxChars(pDX, UserWeight, 3);
   DDX_Text(pDX, IDC_PHONENO_DETAILED_VER, UserPhoneNumber);
	DDV_MaxChars(pDX, UserPhoneNumber, 20);
}

BEGIN_MESSAGE_MAP(CRapidScreenDetailedDlg, CDialog)
   ON_BN_CLICKED(IDC_W_USERNAME_DETAILED_VER, &CRapidScreenDetailedDlg::OnBnClickedWUsernameDetailedVer)
   ON_BN_CLICKED(IDC_GET_DEVICE_VERSION_INFOR, &CRapidScreenDetailedDlg::OnBnClickedGetDeviceVersionInfor)
   ON_BN_CLICKED(IDC_DETAILED_GET_RUN_STATUS, &CRapidScreenDetailedDlg::OnBnClickedDetailedGetRunStatus)
   ON_BN_CLICKED(IDC_DETAILED_GET_SAVE_STATUS, &CRapidScreenDetailedDlg::OnBnClickedDetailedGetSaveStatus)
   ON_BN_CLICKED(IDC_DETAILED_QUIT_SAVE_STATUS, &CRapidScreenDetailedDlg::OnBnClickedDetailedQuitSaveStatus)
   ON_BN_CLICKED(IDC_DETAILED_SEND_TIME_STAMP, &CRapidScreenDetailedDlg::OnBnClickedDetailedSendTimeStamp)
   ON_BN_CLICKED(IDC_DETAILED_ACTIVE_ECG_SAMPLE, &CRapidScreenDetailedDlg::OnBnClickedDetailedActiveEcgSample)
   ON_BN_CLICKED(IDC_DETAILED_ENTER_AUTO_SAVE, &CRapidScreenDetailedDlg::OnBnClickedDetailedEnterAutoSave)
END_MESSAGE_MAP()
//
// shield esc and enter key
BOOL CRapidScreenDetailedDlg::PreTranslateMessage(MSG* pMsg)
{
   if (TRUE == HandleKeyPressDown(pMsg))
      return TRUE;

   return CDialog::PreTranslateMessage(pMsg);
}
//
// shield esc and enter
//
BOOL CRapidScreenDetailedDlg::HandleKeyPressDown(MSG *pMsg)
{
   if ((WM_KEYFIRST <= pMsg->message) && (pMsg->message <= WM_KEYLAST))
   {
      if ((SHIELD_KEYBOARD_KEY1 == pMsg->wParam) || (SHIELD_KEYBOARD_KEY2 == pMsg->wParam))
         return TRUE;
   }
   return FALSE;
}
//
//
BOOL CRapidScreenDetailedDlg::OnInitDialog(void)
{
   CDialog::OnInitDialog();
   InitialAllControl();
   return TRUE;
}
//
//
void CRapidScreenDetailedDlg::InitialAllControl(void)
{ 
   ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->AddString(_T("女"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->AddString(_T("男"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->AddString(_T("其他"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(1);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_VER_INFOR)))->SetWindowText(_T(""));
   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_RUN_STATUS)))->SetWindowText(_T(""));

   InitialInputUserInfor();
}
//
//
void CRapidScreenDetailedDlg::InitialInputUserInfor()
{
   ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(DEFAULT_GENDER_CHOOSE_INDEX);
   ((CEdit *)GetDlgItem(IDC_NAME_DETAILED_VER))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_AGE_DETAILED_VER))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_HEIGHT_DELTAIED_VER))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_WEIGHT_DETAILED_VER))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_PHONENO_DETAILED_VER))->SetWindowText(_T(""));
}
//
//
void CRapidScreenDetailedDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;

   cmdtype = receivebag->Type;
   BleInteractiveStruct.HandleFlag = FALSE;
   switch (cmdtype)
   {
//---------------------------------------------------------------------
      case CMD_SET_CUR_TIMESTAMP:HandleSendTimestampCmd(receivebag);
                                 break;
      case CMD_CHANGE_STATUS_CMD:HandleActiveEcgSampleCmd(receivebag);
                                 break;
      case CMD_GET_DONGLE_POWER:HandleGetDevicePowerAndCapcityStaCmd(receivebag);
                                break;
      case CMD_GET_RUN_MODE:HandleGetDeviceRunStaCmd(receivebag);
                            break;
//---------------------------------------------------------------------
      case CMD_GET_SAVE_STATUS:HandleGetDeviceSaveStaCmd(receivebag);
                               break;
      case CMD_SET_SAVE_STATUS:HandleSetDeviceSaveStaCmd(receivebag);
                               break;
//---------------------------------------------------------------------
      case CMD_GET_DONGLE_VERSION:HandleGetDongleVersionCmd(receivebag);
                                  break;
      case CMD_GET_OR_WRITE_ECG_BLK_INFOR:HandleGetOrWriteEcgBlkInfor(receivebag);
                                          break;
//-------------------------------------------------------------------
      default:break;
   }
}
//
void CRapidScreenDetailedDlg::HandleSendTimestampCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   CString saveStr;
   unsigned int timestamp;

   timestamp = receivebag->Buffer[0];
   timestamp = timestamp + (receivebag->Buffer[1] << 8);
   timestamp = timestamp + (receivebag->Buffer[2] << 16);
   timestamp = timestamp + (receivebag->Buffer[3] << 24);
   str.Format(_T("timestamp = 0x%08X"), timestamp);
   saveStr = str;
   ((CStatic *)(GetDlgItem(IDC_DETAILED_TIMESTAMP_INFOR)))->SetWindowText(saveStr);
}
void CRapidScreenDetailedDlg::HandleActiveEcgSampleCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString saveStr;
   //------------------------------
   if (0x01 == receivebag->Buffer[0])
   {
      saveStr = _T("active success.");
   }
   else
   {
      saveStr = _T("active fail.");
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_ACTIVE_SAMPLE_INFOR)))->SetWindowText(saveStr);
}
//
// handle get device power and capcity status
void CRapidScreenDetailedDlg::HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   CString showStr;

   str.Format(_T("电量:%d"),receivebag->Buffer[0]);
   str = str + _T("%  ");
   showStr = str;
   str.Format(_T("容量:%d"), receivebag->Buffer[1]);
   str = str + _T("%");
   showStr = showStr + str;
   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_RUN_STATUS)))->SetWindowText(showStr);
}
//
// handle get device run status cmd
void CRapidScreenDetailedDlg::HandleGetDeviceRunStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int value;
   CString saveStr;
   CString showstr;

   if (0x01 == (receivebag->Buffer[0] & 0x07))
      saveStr = _T("dongle run,");
   else if (0x02 == (receivebag->Buffer[0] & 0x07))
      saveStr = _T("dongle stop,");
   else if (0x03 == (receivebag->Buffer[0] & 0x07))
      saveStr = _T("dongle read,");
   else if (0x00 == (receivebag->Buffer[0] & 0x07))
      saveStr = _T("invalid mode,");

   showstr = saveStr;
//----------------------------------------------------
   value = (receivebag->Buffer[0] >> 3) & 0x03;
   if (0x00 == value)
      saveStr = _T("sample ecg.");
   else
      saveStr = _T("sample emg.");
   showstr = showstr + saveStr;
//----------------------------------------------------
   value = (receivebag->Buffer[0] >> 5) & 0x01;
   if (0x00 == value)
      saveStr = _T("RESP DIABLE.");
   else
      saveStr = _T("RESP ENABLE.");
   showstr = showstr + saveStr;
//----------------------------------------------------
   value = (receivebag->Buffer[0] >> 6) & 0x01;
   if (0x00 == value)
      saveStr = _T("ACCE DISABLE.");
   else
      saveStr = _T("ACCE ENABLE.");
   showstr = showstr + saveStr;
//---------------------------------------------------------------------
   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_RUN_STA)))->SetWindowText(showstr);
//---------------------------------------------------------------------
}
void CRapidScreenDetailedDlg::HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;

   status = receivebag->Buffer[0];
   saveStr = _T("");
   if (0 == status)
   { 
      saveStr = _T("非保存状态");
   }
   else
   { 
      saveStr = _T("保存状态,");
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_SAVE_STA)))->SetWindowText(saveStr);
}
void CRapidScreenDetailedDlg::HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;

   status = receivebag->Buffer[0];
   if (1 == status)
   {
      saveStr = _T("进入保存模式.");
   }
   else if (2 == status)
   {
      saveStr = _T("退出保存模式.");
      ((CStatic *)(GetDlgItem(IDC_DETAILED_QUIT_DEVICE_SAVE_STA)))->SetWindowText(saveStr);
      return;
   }
   else if (3 == status)
   {
      saveStr = _T("进入自动保存模式.");
   }
   else if (4 == status)
   {
      saveStr = _T("擦除成功!");
   }
   else if (0x7F == status)
   {
      saveStr = _T("设置不成功!");
   }
   else if (0x7E == status)
   {
      saveStr = _T("没有存储空间!");
   }
   else
      return;

   ((CStatic *)(GetDlgItem(IDC_DETAILED_ENTER_SAVE_STA)))->SetWindowText(saveStr);
}
//
//
void CRapidScreenDetailedDlg::HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int i,length;
   char tmp;
   CString str,VersionStr;

   length = receivebag->Buffer[0];
   VersionStr.Empty();
   for (i = 0; i<length; i++)
   {
      tmp = receivebag->Buffer[1 + i];
      str = tmp;
      VersionStr = VersionStr + str;
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_VER_INFOR)))->SetWindowText(VersionStr);
}
void CRapidScreenDetailedDlg::HandleGetOrWriteEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int i;
   int blkIndex;
   int internalIndex;
   unsigned int crc;

   blkIndex = receivebag->Buffer[1] * 128 + receivebag->Buffer[0];  //ecg blk index
   internalIndex = receivebag->Buffer[2] & 0x0F;
   if(BleInteractiveStruct.InternalIndex  != internalIndex)
   {
      return;
   }
   if (CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG == BleInteractiveStruct.type)
   {
      if (1 == internalIndex)
      {
         for (i = 0; i < SAVE_DATA_PROPERTY_LEN; i++)
         {
            BlkDetailedInfor.Buffer[i] = receivebag->Buffer[3 + i];
         }
         crc = CalcCrc32(BlkDetailedInfor.Content.UserInfor.Data, VALID_SAVE_DATA_PROPERTY_LENGTH);
         if(crc != BlkDetailedInfor.Content.BagCrc)
         {
            return;
         }
         DisplayUserInfor();
      }
      else 
      {

      }
   }
}
//============================================================
//
void CRapidScreenDetailedDlg::OnBnClickedWUsernameDetailedVer()
{
   GetUserDetailedContent();
   DataProtocolHandler.GenerateWriteUserInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, 1,
                                                 BlkDetailedInfor.Buffer, SAVE_DATA_PROPERTY_LEN,
                                                 NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
//------------------------------------------
   BleInteractiveStruct.InternalIndex = 1;
   BleInteractiveStruct.type = CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG;

   InitialInputUserInfor();
//------------------------------------------
}
//
//
void CRapidScreenDetailedDlg::OnBnClickedGetDeviceVersionInfor(void)
{
   DataProtocolHandler.GenerateGetDongleVerCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_VER_INFOR)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedGetRunStatus(void)
{
   DataProtocolHandler.GenerateGetDongleRunStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_RUN_STA)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedGetSaveStatus()
{
   DataProtocolHandler.GenerateGetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_DEVICE_SAVE_STA)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedQuitSaveStatus()
{
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, 
                                                   CMD_QUIT_INTO_SAVE_MODE,0,NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_QUIT_DEVICE_SAVE_STA)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedSendTimeStamp()
{
   time_t timevalue;
   CTime time = CTime::GetCurrentTime();

   timevalue = time.GetTime();
   DataProtocolHandler.GenerateSendTimeStampCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,timevalue,NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_TIMESTAMP_INFOR)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedActiveEcgSample()
{
   DataProtocolHandler.GenerateActiveEcgSampleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_ACTIVE_SAMPLE_INFOR)))->SetWindowText(_T(""));
}
void CRapidScreenDetailedDlg::OnBnClickedDetailedEnterAutoSave()
{
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                   CMD_ENTER_INTO_AUTO_SAVE_MODE,0, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_ENTER_SAVE_STA)))->SetWindowText(_T(""));
}
//
//============================================================
void CRapidScreenDetailedDlg::GetUserDetailedContent(void)
{
   UpdateData();

   CDataHandle dataHandle;
   int i;
   int strLen;
   char *nameBuf,*name_To_Utf8Buf;
   CString str;
   int tmp;
// GB2312 TO UTF-8
//---------------------------------------------------------
// initial
   for (i = 0; i <VALID_SAVE_DATA_PROPERTY_LENGTH; i++)
   {
      BlkDetailedInfor.Buffer[i] = 0;
   }
//---------------------------------------------------------
// get name
   strLen = UserName.GetLength();

   nameBuf = new char[strLen+1];
   for (i = 0; i < strLen; i++)
   {
      nameBuf[i] = UserName[i];
   }
   nameBuf[i] = '\0';
   name_To_Utf8Buf = dataHandle.GB2312_To_Utf8(nameBuf);

   i = 0;
   while (1)
   {
      if (i > USER_NAME_STR_LEN)
         break;

      if ('\0' == name_To_Utf8Buf[i])
         break;
      BlkDetailedInfor.Content.UserInfor.Detailed.Name[i] = name_To_Utf8Buf[i];
      i++;
   }
   delete[] nameBuf;
   delete[] name_To_Utf8Buf;
//---------------------------------------------------------
// get gender
   tmp = ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->GetCurSel() + 0x30;
   BlkDetailedInfor.Content.UserInfor.Detailed.Gender[0] = tmp;
//---------------------------------------------------------
// get phonenumber
   for (i = 0; i < UserPhoneNumber.GetLength(); i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.PhoneNo[i] = (unsigned char)(UserPhoneNumber[i]);
   }
//---------------------------------------------------------
// get age
   for (i = 0; i < UserAge.GetLength(); i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Age[i] = (unsigned char)(UserAge[i]);
   }
//---------------------------------------------------------
// get height 
   for (i = 0; i < UserHeight.GetLength(); i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Height[i] = (unsigned char)(UserHeight[i]);
   }
//---------------------------------------------------------
// get weight
   for (i = 0; i < UserWeight.GetLength(); i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Weight[i] = (unsigned char)(UserWeight[i]);
   }
   BlkDetailedInfor.Content.BagCrc = CalcCrc32(BlkDetailedInfor.Content.UserInfor.Data,VALID_SAVE_DATA_PROPERTY_LENGTH);
}
void CRapidScreenDetailedDlg::DisplayUserInfor(void)
{
   CString nameStr,ageStr,genderStr,weightStr,heightStr,phoneNoStr;
   char *nameBuf,*showNameBuf;
   char tmp;
   int i;
   CString str;
   CDataHandle dataHandle;
//---------------------------
   nameStr.Empty();
   nameBuf = new char[USER_NAME_STR_LEN];
   memset(nameBuf, 0, USER_NAME_STR_LEN);
   for (i = 0; i < USER_NAME_STR_LEN; i++)
   {
      nameBuf[i] = BlkDetailedInfor.Content.UserInfor.Detailed.Name[i];
   }
   showNameBuf = dataHandle.Utf8_To_GB2312(nameBuf);
   i = 0;
   nameStr.Empty();
   while (1)
   {
      if (i > USER_NAME_STR_LEN)
         break;

      if ('\0' == showNameBuf[i])
         break;

      str = showNameBuf[i];
      nameStr = nameStr + str;
      i++;
   }
   delete [] showNameBuf;
   delete [] nameBuf;
   ((CEdit *)GetDlgItem(IDC_NAME_DETAILED_VER))->SetWindowText(nameStr);
//---------------------------------------------------------------------------
   phoneNoStr.Empty();
   for (i = 0; i < USER_PHONE_NUMBER_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.PhoneNo[i];
      str = tmp;
      phoneNoStr = phoneNoStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_PHONENO_DETAILED_VER))->SetWindowText(phoneNoStr);
//---------------------------------------------------------------------------
   ageStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Age[i];
      str = tmp;
      ageStr = ageStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_AGE_DETAILED_VER))->SetWindowText(ageStr);
//---------------------------------------------------------------------------
   heightStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Height[i];
      str = tmp;
      heightStr = heightStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_HEIGHT_DELTAIED_VER))->SetWindowText(heightStr);
//---------------------------------------------------------------------------
   weightStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Weight[i];
      str = tmp;
      weightStr = weightStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_WEIGHT_DETAILED_VER))->SetWindowText(weightStr);
//---------------------------------------------------------------------------
   unsigned char gender;
   gender = BlkDetailedInfor.Content.UserInfor.Detailed.Gender[0];
   if ('0' == gender )
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(FEMALE_GENDER_INDEX);
   else if ('1' == gender )
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(MALE_GENDER_INDEX);
   else if ('2' == gender )
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(OTHER_GENDER_INDEX);
}
//============================================================
//
//
void CRapidScreenDetailedDlg::SendDataToLowerMachine(unsigned char *buffer, unsigned short length)
{
   if (NULL == ParentPt)
      return;

   unsigned type;
   CEcgSecreeningSystemDlg *dlgPt;
   dlgPt = (CEcgSecreeningSystemDlg *)(ParentPt);

   type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
   type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
   type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
   dlgPt->SendBuffer(type, buffer, length);
}
















