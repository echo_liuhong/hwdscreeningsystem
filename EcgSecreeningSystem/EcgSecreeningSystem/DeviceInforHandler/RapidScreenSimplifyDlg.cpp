// RapidScreenSimplifyDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "RapidScreenSimplifyDlg.h"
#include "afxdialogex.h"

#include "EcgSecreeningSystemDlg.h"
// CRapidScreenSimplifyDlg 对话框

#define USER_INFOR_TIP   _T("输入不完整信息，仍然激活设备?")
IMPLEMENT_DYNAMIC(CRapidScreenSimplifyDlg, CDialog)

CRapidScreenSimplifyDlg::CRapidScreenSimplifyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_RAPID_SCREEN_SIMPLIFY_DLG, pParent)
   , UserName(_T(""))
   , UserAge(_T(""))
   , UserHeight(_T(""))
   , UserWeight(_T(""))
   , UserPhoneNo(_T(""))
{
   ActiveDevProgressStruct.SaveActiveFileFlag = FALSE;
   ActiveDevProgressStruct.ActiveDevFlag = FALSE;
   ActiveDevProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   ActiveDevProgressStruct.RepeatSendCnt = 0;
   ActiveDevProgressStruct.ActiveDevStep = INVALID_READ_STEP;
   ClearDeviceUserRelationInfor();
}
void CRapidScreenSimplifyDlg::ClearDeviceUserRelationInfor(void)
{
   DeviceUserRelationInfor.Age.Empty();
   DeviceUserRelationInfor.BlkCnt.Empty();
   DeviceUserRelationInfor.DeviceId.Empty();
   DeviceUserRelationInfor.EndTime.Empty();
   DeviceUserRelationInfor.FileRoute.Empty();
   DeviceUserRelationInfor.Gender.Empty();
   DeviceUserRelationInfor.Height.Empty();
   DeviceUserRelationInfor.NameStr.Empty();
   DeviceUserRelationInfor.PhoneNumber.Empty();
   DeviceUserRelationInfor.StartTime.Empty();
   DeviceUserRelationInfor.Weight.Empty();
}
CRapidScreenSimplifyDlg::~CRapidScreenSimplifyDlg()
{
}
void CRapidScreenSimplifyDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Text(pDX, IDC_NAME_SIMPLIFY, UserName);
   DDV_MaxChars(pDX, UserName, 20);
   DDX_Text(pDX, IDC_AGE_SIMPLIFY, UserAge);
   DDV_MaxChars(pDX, UserAge, 3);
   DDX_Text(pDX, IDC_HEIGHT_SIMPLIFY, UserHeight);
   DDV_MaxChars(pDX, UserHeight, 3);
   DDX_Text(pDX, IDC_WEIGHT_SIMPLIFY, UserWeight);
   DDV_MaxChars(pDX, UserWeight, 3);
   DDX_Text(pDX, IDC_PHONENO_SIMPLIFY, UserPhoneNo);
   DDV_MaxChars(pDX, UserPhoneNo, 20);
}


BEGIN_MESSAGE_MAP(CRapidScreenSimplifyDlg, CDialog)
   ON_BN_CLICKED(IDC_SIMPLIFY_ACTIVE_DEVICE, &CRapidScreenSimplifyDlg::OnBnClickedSimplifyActiveDevice)
   ON_BN_CLICKED(IDC_SIMPLIFY_SECREEN_GET_ID_BTN, &CRapidScreenSimplifyDlg::OnBnClickedSimplifySecreenGetIdBtn)
   ON_WM_TIMER()
END_MESSAGE_MAP()

//
// shield esc and enter key
BOOL CRapidScreenSimplifyDlg::PreTranslateMessage(MSG* pMsg)
{
   if (TRUE == HandleKeyPressDown(pMsg))
      return TRUE;

   return CDialog::PreTranslateMessage(pMsg);
}
//
// shield esc and enter
//
BOOL CRapidScreenSimplifyDlg::HandleKeyPressDown(MSG *pMsg)
{
   if ((WM_KEYFIRST <= pMsg->message) && (pMsg->message <= WM_KEYLAST))
   {
      if ((SHIELD_KEYBOARD_KEY1 == pMsg->wParam) || (SHIELD_KEYBOARD_KEY2 == pMsg->wParam))
         return TRUE;
   }
   return FALSE;
}
//
//
BOOL CRapidScreenSimplifyDlg::OnInitDialog(void)
{
   CDialog::OnInitDialog();
   InitialAllControl();
   return TRUE;
}
//
//
void CRapidScreenSimplifyDlg::InitialAllControl(void)
{
   ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->AddString(_T("女"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->AddString(_T("男"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->AddString(_T("其他"));
   ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->SetCurSel(1);

   SetTimer(ACTIVE_DEV_TIMER_INDEX, ACTIVE_DEV_PROGRESS_TIME_INTERVAL, NULL);

   InitialInputUserInfor();
}
void CRapidScreenSimplifyDlg::InitialInputUserInfor(void)
{
   ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->SetCurSel(DEFAULT_GENDER_CHOOSE_INDEX);
   ((CEdit *)GetDlgItem(IDC_NAME_SIMPLIFY))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_AGE_SIMPLIFY))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_HEIGHT_SIMPLIFY))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_WEIGHT_SIMPLIFY))->SetWindowText(_T(""));
   ((CEdit *)GetDlgItem(IDC_PHONENO_SIMPLIFY))->SetWindowText(_T(""));
}
//
//
void CRapidScreenSimplifyDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;

   cmdtype = receivebag->Type;
   switch (cmdtype)
   {
      case CMD_GET_DONGLE_POWER:HandleGetDevicePowerAndCapcityStaCmd(receivebag);
                                break;
      case CMD_GET_DEVICE_INFO:HandleGetDeviceInfo(receivebag);
                               break;
//---------------------------------------------------------------------
      case CMD_SET_CUR_TIMESTAMP:HandleSendTimestampCmd(receivebag);
                                 break;
      case CMD_CHANGE_STATUS_CMD:HandleChangeDeviceStatus(receivebag);
                                 break;
      case CMD_GET_RUN_MODE:HandleGetDeviceRunStaCmd(receivebag);
                            break;
//---------------------------------------------------------------------
      case CMD_GET_SAVE_STATUS:HandleGetDeviceSaveStaCmd(receivebag);
                               break;
      case CMD_SET_SAVE_STATUS:HandleSetDeviceSaveStaCmd(receivebag);
                               break;
//---------------------------------------------------------------------
      case CMD_GET_OR_WRITE_ECG_BLK_INFOR:HandleGetOrWriteEcgBlkInfor(receivebag);
                                          break;
//-------------------------------------------------------------------
      default:break;
   }
}
void CRapidScreenSimplifyDlg::HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   CString showStr;

   str.Format(_T("电量:%d"), receivebag->Buffer[0]);
   str = str + _T("%  ");
   showStr = str;
   str.Format(_T("容量:%d"), receivebag->Buffer[1]);
   str = str + _T("%");
   showStr = showStr + str;
   ((CStatic *)(GetDlgItem(IDC_SIMPLIFY_DEVICE_RUN_STATUS)))->SetWindowText(showStr);
}
void CRapidScreenSimplifyDlg::HandleGetDeviceInfo(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int len, i;
   char memoryStr;
   CString str, showStr;

   len = receivebag->Buffer[1];
   showStr.Empty();
   for (i = 0; i<len; i++)
   {
      memoryStr = receivebag->Buffer[i + 2];
      str = memoryStr;
      showStr = showStr + str;
   }
   ((CStatic *)(GetDlgItem(IDC_SIMPLIFY_DEV_ID_INFOR)))->SetWindowText(showStr);
   DeviceUserRelationInfor.DeviceId = showStr;
   CheckEnterIntoNextStep(FIFTH_GET_DEV_ID);
}
void CRapidScreenSimplifyDlg::HandleGetDeviceRunStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   if (0x01 == (receivebag->Buffer[0] & 0x07))
   {
      ActiveDevProgressStruct.RunMode = ACTIVE_DEV_RUNMODE;
   }
   else if (0x02 == (receivebag->Buffer[0] & 0x07))
   {
      ActiveDevProgressStruct.RunMode = STOP_DEV_RUNMODE;
   }
   else if (0x03 == (receivebag->Buffer[0] & 0x07))
   {
      ActiveDevProgressStruct.RunMode = READ_DEV_RUNMODE;
   }
   else 
   {
      ActiveDevProgressStruct.RunMode = INVALID_DEV_RUNMODE;
   }
   CheckEnterIntoNextStep(FIRST_GET_DEV_RUN_STA);
}
void CRapidScreenSimplifyDlg::HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   if ((receivebag->Buffer[0] != (0x7F - receivebag->Buffer[1])))
   {
      return;
   }
   ActiveDevProgressStruct.RunMode = receivebag->Buffer[0];
   if (0x03 == ActiveDevProgressStruct.RunMode)
   {
      ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("进入读取模式"));
   }
   else if (0x01 == ActiveDevProgressStruct.RunMode)
   {
      if (STEP_QUIT_READ_MODE == ActiveDevProgressStruct.ChangeDevStaStep)
      {
         ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("退出读取模式"));
      }
      else if (STEP_ENTER_INTO_RUN_MODE == ActiveDevProgressStruct.ChangeDevStaStep)
      {
         ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("设备正常运行"));
      }
   }

   if( STEP_QUIT_READ_MODE == ActiveDevProgressStruct.ChangeDevStaStep )
   {
      CheckEnterIntoNextStep(SECOND_QUIT_DEV_READ_MODE);
   }
   else if(STEP_ENTER_INTO_RUN_MODE == ActiveDevProgressStruct.ChangeDevStaStep)
   {
      CheckEnterIntoNextStep(SEVENTH_ACTIVE_DEV);
   }
   else
   {

   }
}
//
//
void CRapidScreenSimplifyDlg::HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;

   status = receivebag->Buffer[0];
   if (0 == status)
   {
      ActiveDevProgressStruct.SaveStatusFlag = TRUE;
   }
   else
   {
      ActiveDevProgressStruct.SaveStatusFlag = FALSE;
   }
   CheckEnterIntoNextStep(THIRD_GET_DEV_SAVE_STA);
}
void CRapidScreenSimplifyDlg::HandleGetOrWriteEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int i;
   int blkIndex;
   int internalIndex;
   unsigned int crc;

   blkIndex = receivebag->Buffer[1] * 128 + receivebag->Buffer[0];  //ecg blk index
   internalIndex = receivebag->Buffer[2] & 0x0F;
   if (ActiveDevProgressStruct.InternalIndex != internalIndex)
   {
      return;
   }
   if (CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG == ActiveDevProgressStruct.Type)
   {
      if (1 == internalIndex)
      {
         for (i = 0; i < SAVE_DATA_PROPERTY_LEN; i++)
         {
            BlkDetailedInfor.Buffer[i] = receivebag->Buffer[3 + i];
         }
         crc = CalcCrc32(BlkDetailedInfor.Content.UserInfor.Data, VALID_SAVE_DATA_PROPERTY_LENGTH);
         if (crc != BlkDetailedInfor.Content.BagCrc)
         {
            return;
         }
         DisplayUserInfor();   //20210106
      }
   }
   CheckEnterIntoNextStep(FOUTH_WRITE_USER_INFOR);
}
void CRapidScreenSimplifyDlg::DisplayUserInfor(void)
{
/*
   CString nameStr, ageStr, genderStr, weightStr, heightStr, phoneNoStr;
   char *nameBuf, *showNameBuf;
   char tmp;
   int i;
   CString str;
   CDataHandle dataHandle;
   //---------------------------
   nameStr.Empty();
   nameBuf = new char[USER_NAME_STR_LEN];
   memset(nameBuf, 0, USER_NAME_STR_LEN);
   for (i = 0; i < USER_NAME_STR_LEN; i++)
   {
      nameBuf[i] = BlkDetailedInfor.Content.UserInfor.Detailed.Name[i];
   }
   showNameBuf = dataHandle.Utf8_To_GB2312(nameBuf);
   i = 0;
   nameStr.Empty();
   while (1)
   {
      if (i > USER_NAME_STR_LEN)
         break;

      if ('\0' == showNameBuf[i])
         break;

      str = showNameBuf[i];
      nameStr = nameStr + str;
      i++;
   }
   delete[] showNameBuf;
   delete[] nameBuf;
   ((CEdit *)GetDlgItem(IDC_NAME_DETAILED_VER))->SetWindowText(nameStr);
   //---------------------------------------------------------------------------
   phoneNoStr.Empty();
   for (i = 0; i < USER_PHONE_NUMBER_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.PhoneNo[i];
      str = tmp;
      phoneNoStr = phoneNoStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_PHONENO_DETAILED_VER))->SetWindowText(phoneNoStr);
   //---------------------------------------------------------------------------
   ageStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Age[i];
      str = tmp;
      ageStr = ageStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_AGE_DETAILED_VER))->SetWindowText(ageStr);
   //---------------------------------------------------------------------------
   heightStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Height[i];
      str = tmp;
      heightStr = heightStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_HEIGHT_DELTAIED_VER))->SetWindowText(heightStr);
   //---------------------------------------------------------------------------
   weightStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = BlkDetailedInfor.Content.UserInfor.Detailed.Weight[i];
      str = tmp;
      weightStr = weightStr + str;
   }
   ((CEdit *)GetDlgItem(IDC_WEIGHT_DETAILED_VER))->SetWindowText(weightStr);
   //---------------------------------------------------------------------------
   unsigned char gender;
   gender = BlkDetailedInfor.Content.UserInfor.Detailed.Gender[0];
   if ('0' == gender)
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(FEMALE_GENDER_INDEX);
   else if ('1' == gender)
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(MALE_GENDER_INDEX);
   else if ('2' == gender)
      ((CComboBox *)GetDlgItem(IDC_GENDER_DETAILED_VER))->SetCurSel(OTHER_GENDER_INDEX);
      */

}
//
void CRapidScreenSimplifyDlg::HandleSendTimestampCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   CString saveStr;
   unsigned int timestamp;

   timestamp = receivebag->Buffer[0];
   timestamp = timestamp + (receivebag->Buffer[1] << 8);
   timestamp = timestamp + (receivebag->Buffer[2] << 16);
   timestamp = timestamp + (receivebag->Buffer[3] << 24);
   str.Format(_T("timestamp = 0x%08X"), timestamp);
   saveStr = str;
   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(saveStr);

   CheckEnterIntoNextStep(SIXTH_SEND_TIMSTAMP);
}
void CRapidScreenSimplifyDlg::HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;

   status = receivebag->Buffer[0];
   ActiveDevProgressStruct.SaveMode = status;
   if (1 == status)
   {
      saveStr = _T("进入保存模式.");
   }
   else if (2 == status)
   {
      saveStr = _T("退出保存模式.");
      ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(saveStr);
      return;
   }
   else if (3 == status)
   {
      saveStr = _T("进入保存模式.");
   }
   else if (4 == status)
   {
      saveStr = _T("擦除成功!");
   }
   else if (0x7F == status)
   {
      saveStr = _T("设置不成功!");
   }
   else if (0x7E == status)
   {
      saveStr = _T("没有存储空间!");
   }
   else
      return;

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(saveStr);
   CheckEnterIntoNextStep(EIGHTH_ACTIVE_DEV_SAVE);
}
//
//
void CRapidScreenSimplifyDlg::CheckEnterIntoNextStep(int step)
{
   if (FALSE == ActiveDevProgressStruct.ActiveDevFlag)
      return;

   if (step == ActiveDevProgressStruct.ActiveDevStep)
   {
      ActiveDevProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
      ActiveDevProgressStruct.RepeatSendCnt = 0;
   }
   else
   {
      ActiveDevProgressStruct.ActiveDevFlag = FALSE;
      ActiveDevProgressStruct.ActiveDevStep = INVALID_ACTIVE_DEV_STEP;
   }
//------------------------------------------------------------------------
   switch (step)
   {
       case INVALID_ACTIVE_DEV_STEP:break;
//--------------------------------------------------------------------
       case FIRST_GET_DEV_RUN_STA:if( READ_DEV_RUNMODE  == ActiveDevProgressStruct.RunMode )
                                     ActiveDevProgressStruct.ActiveDevStep++;
                                  else
                                     ActiveDevProgressStruct.ActiveDevStep += 2;
                                  break;
       case SECOND_QUIT_DEV_READ_MODE:ActiveDevProgressStruct.ActiveDevStep++;
                                      break;
//---------------------------------------------------------------------------
       case THIRD_GET_DEV_SAVE_STA:if(TRUE == ActiveDevProgressStruct.SaveStatusFlag)
                                      ActiveDevProgressStruct.ActiveDevStep++;
                                   else
                                      ActiveDevProgressStruct.ActiveDevStep = INVALID_ACTIVE_DEV_STEP;
                                   break;
//--------------------------------------------------------------------
       case FOUTH_WRITE_USER_INFOR:ActiveDevProgressStruct.ActiveDevStep++;
                                   break;
       case FIFTH_GET_DEV_ID:ActiveDevProgressStruct.ActiveDevStep++;
                             break;
       case SIXTH_SEND_TIMSTAMP:ActiveDevProgressStruct.ActiveDevStep++;
                                break;
       case SEVENTH_ACTIVE_DEV:ActiveDevProgressStruct.ActiveDevStep++;
                               break;
       case EIGHTH_ACTIVE_DEV_SAVE:if (0x03 == ActiveDevProgressStruct.SaveMode)
                                   {
                                      //AddNewItemForActiveDevice();
                                      ActiveDevProgressStruct.SaveActiveFileFlag = TRUE;
                                      ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("激活成功"));
                                   }
                                   else
                                   {
                                      ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("激活失败"));
                                   }
                                   ActiveDevProgressStruct.ActiveDevStep = INVALID_ACTIVE_DEV_STEP;
                                   ActiveDevProgressStruct.ActiveDevFlag = FALSE;
                                   break; 
       default:break;
   }
}

void CRapidScreenSimplifyDlg::OnTimer(UINT_PTR nIDEvent)
{
   if (ACTIVE_DEV_TIMER_INDEX == nIDEvent)
   {
      ActiveDevProgressHandler();

      if (TRUE == ActiveDevProgressStruct.SaveActiveFileFlag)
      {
         ActiveDevProgressStruct.SaveActiveFileFlag = FALSE;
         AddNewItemForActiveDevice();
      }
   }
   CDialog::OnTimer(nIDEvent);
}
void CRapidScreenSimplifyDlg::ActiveDevProgressHandler(void)
{
   if (FALSE == ActiveDevProgressStruct.ActiveDevFlag)
      return;

   if ((INVALID_ACTIVE_DEV_STEP == ActiveDevProgressStruct.ActiveDevStep)
      || (ActiveDevProgressStruct.ActiveDevStep > EIGHTH_ACTIVE_DEV_SAVE))
   {
      InvalidActiveDevStepHandler();
   }
   //------------------------------
   if (SEND_BLE_CMD_STEP == ActiveDevProgressStruct.DirectStatus)
   {
      DevDiffActiveStepHandler(ActiveDevProgressStruct.ActiveDevStep);
      CheckRepeatSendCmdHandler();
   }
   else if (WAIT_ACK_CMD_STEP == ActiveDevProgressStruct.DirectStatus)
   {
      CheckWaitAckCmdHandler(WAIT_ACK_TIME_CNT);
   }
   else
   {
      InvalidActiveDevStepHandler();
   }
}
void CRapidScreenSimplifyDlg::DevDiffActiveStepHandler(int activeDevStep)
{
   switch (activeDevStep)
   {
      case FIRST_GET_DEV_RUN_STA:GetDevRunStatusHandler();
                                 break;
      case SECOND_QUIT_DEV_READ_MODE:ActiveDevProgressStruct.ChangeDevStaStep = STEP_QUIT_READ_MODE;
                                     QuitDeviceReadModeHandler();
                                     break;
      case THIRD_GET_DEV_SAVE_STA:GetDeviceSaveStatusHandler();
                                  break;
      case FOUTH_WRITE_USER_INFOR:WriteUserInforHandler();
                                  break;
      case FIFTH_GET_DEV_ID:ReadDeviceIdHandler();
                            break;
      case SIXTH_SEND_TIMSTAMP:SendTimeStampHandler();
                               break;
      case SEVENTH_ACTIVE_DEV:ActiveDevProgressStruct.ChangeDevStaStep = STEP_ENTER_INTO_RUN_MODE;
                              ActiveEcgSampleHandler();
                              break;
      case EIGHTH_ACTIVE_DEV_SAVE:ActiveDeviceSaveHandler();
                                  break;
      default:break;
   }
}
void CRapidScreenSimplifyDlg::GetDevRunStatusHandler(void)
{
   DataProtocolHandler.GenerateGetDongleRunStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在获取运行状态..."));
}
void CRapidScreenSimplifyDlg::QuitDeviceReadModeHandler(void)
{
   DataProtocolHandler.GenerateQuitReadMode(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在退出读取状态..."));
}
void CRapidScreenSimplifyDlg::GetDeviceSaveStatusHandler(void)
{
   DataProtocolHandler.GenerateGetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在获取设备的保存状态..."));
}
void CRapidScreenSimplifyDlg::WriteUserInforHandler(void)
{
   DataProtocolHandler.GenerateWriteUserInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, 1,
                                                 BlkDetailedInfor.Buffer, SAVE_DATA_PROPERTY_LEN,
                                                 NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
//----------------------------------------------------------------------
   ActiveDevProgressStruct.InternalIndex = 1;
   ActiveDevProgressStruct.Type = CMD_WRITE_ECG_BLK_INFOR_WRITE_FLAG;

   InitialInputUserInfor();
//-----------------------------------------------------------------------
   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在写入用户信息..."));
}
void CRapidScreenSimplifyDlg::ReadDeviceIdHandler(void)
{
   DataProtocolHandler.GenerateGetDongleFixInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                    CMD_GET_SERIAL_NUMBER_INDEX,
                                                    NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在获取设备的ID..."));
   ((CStatic *)(GetDlgItem(IDC_SIMPLIFY_DEV_ID_INFOR)))->SetWindowText(_T(""));
}
void CRapidScreenSimplifyDlg::SendTimeStampHandler(void)
{
   time_t timevalue;
   CTime time = CTime::GetCurrentTime();

   timevalue = time.GetTime();
   DataProtocolHandler.GenerateSendTimeStampCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, timevalue, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在发送起始时间..."));
}
void CRapidScreenSimplifyDlg::ActiveEcgSampleHandler(void)
{
   DataProtocolHandler.GenerateActiveEcgSampleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在激活设备采样..."));
}
void CRapidScreenSimplifyDlg::ActiveDeviceSaveHandler(void)
{
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                       CMD_ENTER_INTO_AUTO_SAVE_MODE, 0, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_ACTIVE_DEV_STA)))->SetWindowText(_T("正在激活设备保存..."));
}

void CRapidScreenSimplifyDlg::InvalidActiveDevStepHandler(void)
{
   ActiveDevProgressStruct.ActiveDevFlag = FALSE;
   ActiveDevProgressStruct.ActiveDevStep = INVALID_ACTIVE_DEV_STEP;
}
//
// 
void CRapidScreenSimplifyDlg::CheckRepeatSendCmdHandler(void)
{
   ActiveDevProgressStruct.DirectStatus = WAIT_ACK_CMD_STEP;
   ActiveDevProgressStruct.WaitAckTimeCnt = 0;
   ActiveDevProgressStruct.RepeatSendCnt++;
   if (ActiveDevProgressStruct.RepeatSendCnt > REPEART_SEND_CNT)
   {
      ActiveDevProgressStruct.ActiveDevFlag = FALSE;
      ActiveDevProgressStruct.ActiveDevStep = INVALID_ACTIVE_DEV_STEP;
      ((CStatic *)(GetDlgItem(IDC_ACTIVE_PROGRESS_TIP)))->SetWindowText(_T("超时退出,读取状态"));
   }
   else
   {
      CString str;
      str.Format(_T("%d:repeat cnt = %d"), ActiveDevProgressStruct.ActiveDevStep, ActiveDevProgressStruct.RepeatSendCnt);
      ((CStatic *)(GetDlgItem(IDC_ACTIVE_PROGRESS_TIP)))->SetWindowText(str);
   }
}
//
//
void CRapidScreenSimplifyDlg::CheckWaitAckCmdHandler(int waitAckTimeCnt)
{
   ActiveDevProgressStruct.WaitAckTimeCnt++;
   if (ActiveDevProgressStruct.WaitAckTimeCnt > waitAckTimeCnt)
   {
      ActiveDevProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   }
}
//
//
void CRapidScreenSimplifyDlg::OnBnClickedSimplifySecreenGetIdBtn()
{
   DataProtocolHandler.GenerateGetDongleFixInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                    CMD_GET_SERIAL_NUMBER_INDEX,
                                                    NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_SIMPLIFY_DEV_ID_INFOR)))->SetWindowText(_T(""));
}
//
//
void CRapidScreenSimplifyDlg::OnBnClickedSimplifyActiveDevice()
{
   ClearDeviceUserRelationInfor();
   if (FALSE == GetUserDetailedContent())
   {
      if (IDCANCEL == MessageBox(USER_INFOR_TIP, NULL, MB_ICONWARNING | MB_OKCANCEL))
         return;
   }
   ActiveDevProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   ActiveDevProgressStruct.RepeatSendCnt = 0;

   ActiveDevProgressStruct.ActiveDevFlag = TRUE;
   ActiveDevProgressStruct.ActiveDevStep = FIRST_GET_DEV_RUN_STA;
}
//
//============================================================
BOOL CRapidScreenSimplifyDlg::GetUserDetailedContent(void)
{
   BOOL flag;
   UpdateData();
   CDataHandle dataHandle;
   int i;
   int strLen;
   char *nameBuf, *name_To_Utf8Buf;
   CString str;
   int tmp;
   flag = TRUE;
   // GB2312 TO UTF-8
   //---------------------------------------------------------
   // initial
   for (i = 0; i <VALID_SAVE_DATA_PROPERTY_LENGTH; i++)
   {
      BlkDetailedInfor.Buffer[i] = 0;
   }
   //---------------------------------------------------------
   // get name
   strLen = UserName.GetLength();
   if (0 == strLen)
      flag = FALSE;

   nameBuf = new char[strLen + 1];
   for (i = 0; i < strLen; i++)
   {
      nameBuf[i] = UserName[i];
   }
   nameBuf[i] = '\0';
   name_To_Utf8Buf = dataHandle.GB2312_To_Utf8(nameBuf);

   i = 0;
   while (1)
   {
      if (i > USER_NAME_STR_LEN)
         break;

      if ('\0' == name_To_Utf8Buf[i])
         break;
      BlkDetailedInfor.Content.UserInfor.Detailed.Name[i] = name_To_Utf8Buf[i];
      i++;
   }
   delete[] nameBuf;
   delete[] name_To_Utf8Buf;
   //---------------------------------------------------------
   // get gender
   tmp = ((CComboBox *)GetDlgItem(IDC_GENDER_SIMPLIFY_TAB))->GetCurSel() + 0x30;
   BlkDetailedInfor.Content.UserInfor.Detailed.Gender[0] = tmp;
   if ('0' == tmp)
      DeviceUserRelationInfor.Gender = _T("女");
   else if ('1' == tmp)
      DeviceUserRelationInfor.Gender = _T("男");
   else 
      DeviceUserRelationInfor.Gender = _T("其他");
   //---------------------------------------------------------
   // get phonenumber
   strLen = UserPhoneNo.GetLength();
   if (0 == strLen)
      flag = FALSE;

   for (i = 0; i < strLen; i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.PhoneNo[i] = (unsigned char)(UserPhoneNo[i]);
   }
   //---------------------------------------------------------
   // get age
   strLen = UserAge.GetLength();
   if (0 == strLen)
      flag = FALSE;

   for (i = 0; i < strLen; i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Age[i] = (unsigned char)(UserAge[i]);
   }
   //---------------------------------------------------------
   // get height 
   strLen = UserHeight.GetLength();
   if (0 == strLen)
      flag = FALSE;

   for (i = 0; i < strLen; i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Height[i] = (unsigned char)(UserHeight[i]);
   }
   //---------------------------------------------------------
   // get weight
   strLen = UserWeight.GetLength();
   if (0 == strLen)
      flag = FALSE;

   for (i = 0; i < UserWeight.GetLength(); i++)
   {
      BlkDetailedInfor.Content.UserInfor.Detailed.Weight[i] = (unsigned char)(UserWeight[i]);
   }
   BlkDetailedInfor.Content.BagCrc = CalcCrc32(BlkDetailedInfor.Content.UserInfor.Data, VALID_SAVE_DATA_PROPERTY_LENGTH);

   DeviceUserRelationInfor.NameStr = UserName;
   DeviceUserRelationInfor.Age = UserAge;
   DeviceUserRelationInfor.Height = UserHeight;
   DeviceUserRelationInfor.Weight = UserWeight;
   DeviceUserRelationInfor.PhoneNumber = UserPhoneNo;
   return flag;
}
//
//
void CRapidScreenSimplifyDlg::SendDataToLowerMachine(unsigned char *buffer, unsigned short length)
{
   if (NULL == ParentPt)
      return;

   unsigned type;
   CEcgSecreeningSystemDlg *dlgPt;
   dlgPt = (CEcgSecreeningSystemDlg *)(ParentPt);

   type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
   type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
   type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
   dlgPt->SendBuffer(type, buffer, length);
}

//============================================================================
//----------------------------------------------------------------------------
//============================================================================
void CRapidScreenSimplifyDlg::AddNewItemForActiveDevice(void)
{
   char path[MAX_PATH];
   GetCurrentDirectory(MAX_PATH, (TCHAR*)path);//获取当前路径
   CString strExcelFile = (TCHAR*)path;
   CString str;
   CTime curt = CTime::GetCurrentTime();
   str.Format(_T("\\%4d-%02d-%02d开始列表.xlsx"), (curt.GetYear()), (curt.GetMonth()), (curt.GetDay()));
   strExcelFile = strExcelFile + str;

   CFileStatus Status;
   if (!CFile::GetStatus(strExcelFile, Status))
   {
      CreateNewActiveList(strExcelFile);
   }
   else
   {
      AddNewActiveInfor(strExcelFile);
   }
}
//
//
void CRapidScreenSimplifyDlg::CreateNewActiveList(CString fileName)
{
   if (FALSE == ExcelHandle.InitExcel())
      return;

   CString str;
   CTime curt = CTime::GetCurrentTime();
   str.Format(_T("%4d-%02d-%02d开始列表"), (curt.GetYear()), (curt.GetMonth()), (curt.GetDay()));
   ExcelHandle.GetWorksContainer();

   ExcelHandle.CreateExcelListHead(INDEX_ACTIVE_DEVICE_LIST_NAME, str);
   WriteUserInforToExcelFile(1);

   ExcelHandle.SaveExcelFile(fileName);
   ExcelHandle.release();
}
//
//
void CRapidScreenSimplifyDlg::AddNewActiveInfor(CString fileName)
{
   if (FALSE == ExcelHandle.InitExcel())
      return;

   int nRowCnt;
   ExcelHandle.LoadAllCells(fileName);
   nRowCnt = ExcelHandle.GetRowCnt();
   WriteUserInforToExcelFile(nRowCnt);

   ExcelHandle.SaveExcelFile(fileName);
   ExcelHandle.release();
}
//
//
void CRapidScreenSimplifyDlg::WriteUserInforToExcelFile(int nRowCnt)
{
   CString str;
   CTime curt = CTime::GetCurrentTime();
   str.Format("%d", nRowCnt);
   ExcelHandle.SetSheetTableContent(nRowCnt+1, 1,str);        // NUMBER
   nRowCnt++;
   ExcelHandle.SetSheetTableContent(nRowCnt, 2,DeviceUserRelationInfor.DeviceId);   // DEVICE ID

   str.Format(_T("%4d-%02d-%02d,%2d:%02d:%02d"), (curt.GetYear()), 
                                                 (curt.GetMonth()), 
                                                 (curt.GetDay()),
                                                 (curt.GetHour()),
                                                 (curt.GetMinute()),
                                                 (curt.GetSecond()));
   DeviceUserRelationInfor.StartTime = str;
   ExcelHandle.SetSheetTableContent(nRowCnt, 3, DeviceUserRelationInfor.StartTime);   // START TIME
   ExcelHandle.SetSheetTableContent(nRowCnt, 4, _T(""));    // END TIME 
   ExcelHandle.SetSheetTableContent(nRowCnt, 5, _T(""));    // BLOCK CNT
   ExcelHandle.SetSheetTableContent(nRowCnt, 6, DeviceUserRelationInfor.NameStr);   // NAME
   ExcelHandle.SetSheetTableContent(nRowCnt, 7, DeviceUserRelationInfor.Gender);   // GENDER
   ExcelHandle.SetSheetTableContent(nRowCnt, 8, DeviceUserRelationInfor.Age);   // AGE
   ExcelHandle.SetSheetTableContent(nRowCnt, 9, DeviceUserRelationInfor.Height);   // HEIGHT
   ExcelHandle.SetSheetTableContent(nRowCnt, 10, DeviceUserRelationInfor.Weight); // WEIGHT
   ExcelHandle.SetSheetTableContent(nRowCnt, 11, DeviceUserRelationInfor.PhoneNumber); // PHONE_NO
}
