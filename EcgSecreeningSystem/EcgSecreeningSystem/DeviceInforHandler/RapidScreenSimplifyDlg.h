#pragma once
#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "ProjDefine.h"
#include "ExcelHandler.h"

// CRapidScreenSimplifyDlg 对话框

class CRapidScreenSimplifyDlg : public CDialog
{
	DECLARE_DYNAMIC(CRapidScreenSimplifyDlg)

public:
   void *ParentPt;
   SAVE_BLK_DETAILED_INFOR BlkDetailedInfor;
   CDataProtocolHandler DataProtocolHandler;
//----------------------------------------------------------
   DEVICE_USER_RELATION_INFOR DeviceUserRelationInfor;
   CExcelHandler ExcelHandle;

   void ClearDeviceUserRelationInfor();
   void AddNewItemForActiveDevice();
   void CreateNewActiveList(CString fileName);
   void AddNewActiveInfor(CString fileName);
   void WriteUserInforToExcelFile(int nRowCnt);
//----------------------------------------------------------
   ACTIVE_DEVICE_PROGRESS_STRUCT ActiveDevProgressStruct;
   void ActiveDevProgressHandler(void);
   void DevDiffActiveStepHandler(int activeDevStep);
   void GetDevRunStatusHandler(void);
   void QuitDeviceReadModeHandler(void);
   void GetDeviceSaveStatusHandler(void);
   void WriteUserInforHandler(void);
   void ReadDeviceIdHandler(void);
   void SendTimeStampHandler(void);
   void ActiveEcgSampleHandler(void);
   void ActiveDeviceSaveHandler(void);
//----------------------------------------------------------
   void InvalidActiveDevStepHandler(void);
   void CheckRepeatSendCmdHandler(void);
   void CheckWaitAckCmdHandler(int waitAckTimeCnt);
//----------------------------------------------------
   BOOL GetUserDetailedContent(void);

   void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceInfo(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceRunStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetOrWriteEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag);
   void DisplayUserInfor(void);
   void HandleSendTimestampCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);

   void HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void CheckEnterIntoNextStep(int step);
//-----------------------------------------------------------------------
   void SendDataToLowerMachine(unsigned char *buffer, unsigned short length);

   void InitialAllControl(void);
   void InitialInputUserInfor(void);
//-----------------------------------------------------------------------
   CRapidScreenSimplifyDlg(CWnd* pParent = NULL);   // 标准构造函数
   virtual ~CRapidScreenSimplifyDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_RAPID_SCREEN_SIMPLIFY_DLG };
#endif
public:
   BOOL PreTranslateMessage(MSG* pMsg);
   BOOL HandleKeyPressDown(MSG *pMsg);
protected:
   virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnBnClickedSimplifyActiveDevice();
   CString UserName;
   CString UserAge;
   CString UserHeight;
   CString UserWeight;
   CString UserPhoneNo;
   afx_msg void OnBnClickedSimplifySecreenGetIdBtn();
   afx_msg void OnTimer(UINT_PTR nIDEvent);
};
