#pragma once

#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "ProjDefine.h"

// CRapidScreenDetailedDlg 对话框
class CRapidScreenDetailedDlg : public CDialog
{
	DECLARE_DYNAMIC(CRapidScreenDetailedDlg)

public:
   void *ParentPt;
   SAVE_BLK_DETAILED_INFOR BlkDetailedInfor;
   CDataProtocolHandler DataProtocolHandler;
   BLE_INTERACTIVE_STRUCT BleInteractiveStruct;
//------------------------------------
   void GetUserDetailedContent(void);
   void DisplayUserInfor(void);
//------------------------------------
   void InitialAllControl(void);
   void InitialInputUserInfor(void);
//----------------------------------------------------------------------------
   void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);

   void HandleSendTimestampCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleActiveEcgSampleCmd(COMMUNICATE_BAG_STRUCT *receivebag);

   void HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceRunStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);

   void HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetOrWriteEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag);
//----------------------------------------------------------------------------
   void SendDataToLowerMachine(unsigned char *buffer, unsigned short length);
//-----------------------------------
	CRapidScreenDetailedDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRapidScreenDetailedDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_RAPID_SCREEN_DETAILED_DLG };
#endif
public:
   BOOL PreTranslateMessage(MSG* pMsg);
   BOOL HandleKeyPressDown(MSG *pMsg);

protected:
   virtual BOOL OnInitDialog();
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnBnClickedWUsernameDetailedVer();
   CString UserName;
   CString UserAge;
   CString UserHeight;
   CString UserWeight;
   CString UserPhoneNumber;
   afx_msg void OnBnClickedGetDeviceVersionInfor();
   afx_msg void OnBnClickedDetailedGetRunStatus();
   afx_msg void OnBnClickedDetailedGetSaveStatus();
   afx_msg void OnBnClickedDetailedQuitSaveStatus();
   afx_msg void OnBnClickedDetailedSendTimeStamp();
   afx_msg void OnBnClickedDetailedActiveEcgSample();
   afx_msg void OnBnClickedDetailedEnterAutoSave();
};
