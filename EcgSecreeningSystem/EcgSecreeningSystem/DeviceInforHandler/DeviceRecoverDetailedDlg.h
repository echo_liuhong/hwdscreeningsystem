#pragma once

#include "ProjDefine.h"

#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "FileHandler.h"

// CDeviceRecoverDetailedDlg 对话框

class CDeviceRecoverDetailedDlg : public CDialog
{
	DECLARE_DYNAMIC(CDeviceRecoverDetailedDlg)

public:
   void *ParentPt;
   int UploadRange;
   CDataProtocolHandler DataProtocolHandler;
   BLE_INTERACTIVE_STRUCT BleInteractiveStruct;
   BLE_READ_PROGRESS_STRUCT BleReadProgressStruct;

   BLE_DEVICE_STATUS_STRUCT DeviceStatusStruct;
   SIGNAL_SAVE_CONTENT_STRUCT SignalSaveContentStruct;       //Include ecg,emg

   DATA_SAVE_DIRECTORY_STRUCT DataSaveDirectoryStruct;
   CFileHandler FileHandler;
   MERGE_DATA_STRUCT MergeDataStruct;

   void ConfigureDiffBagLenCommunicate(unsigned char bagLenFlag);
//----------------------------------------------------------------------
   void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
//--------------------
   void HandleGetEcgBlkCnt(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag);
   void ApplyMemoryForSaveContent(int blkIndex);
   void DisplaySignalSaveBlkInfo(int blkIndex);

   void GetHeadStrInfor(int blkIndex);
   void GetEveryBlkInfor(int blkIndex);
   void HandleGetEcgContentInfor(COMMUNICATE_BAG_STRUCT *receivebag);
   void ReadDataProgressHandler(void);
   void BleDiffReadStepHandler(int readStep);

   void CheckEnterIntoNextStep(int step);
   void InvalidReadStepHandler(void);
   void ReadDeviceSaveStatusHandler(void);
   void QuitDeviceSaveStatusHandler(void);
   void ReadDeviceSaveBlkCntHandler(void);
   void ReadEverySaveBlkInforHandler(void);
   void EnterIntoReadmodeHandler(void);
   void BeginReadBlkContentHandler(void);
   void EraseDeviceBlkContentHandler(void);
   void QuitReadmodeHandler(void);

   void CheckRepeatSendCmdHandler(void);
   void CheckWaitAckCmdHandler(int waitAckTimeCnt);

   void UpdateUploadProgressAndCheck(void);
//-----------------------
   void UpdateProgress_ReadAllBlkData(void);
   void MergeAllReadBlk(void);
   void ReadBlkSourceDataFile_DataContent(int blkIndex);
   void SaveMergeDataFile(unsigned char createFileFlag, int index, int mergeIndex);
   CString GetMergeDataFileName(int beginIndex, int mergeIndex, int suffixIndex, int typeIndex);
   void SaveSingleBlkContent_ForBaihui(CString fileName, HALFWORD_TYPE *buf, int len);
   void SaveMergeDataHeadInfor(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt);
   void ChangeData_DiffType(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt);

   void UpdateProgress_ReadSingleBlkData(void);
   void DisplayUpdateSpendTimeCnt(void);
   void UpdateReadProgressControlBar(void);
   void SaveAndChangeSignalFile(int rBlkIndex);
   void GetAllSignalContentBuf(int rBlkIndex);
   void GetSinglePageContent(unsigned char *resultPt, PAGE_CONTENT_STRUCT *pagePt);
   void GetOriginalBuffer(unsigned char *originalBuffer, PAGE_CONTENT_STRUCT *pagePt);
   void GetResultBuffer(unsigned char *originalBuffer, unsigned char *resultPt);
   void SaveSingleBlkForBinFile(int blkIndex);
   void SaveSingleBlkForTxtFile(int blkIndex);
   //
   //
   CString CreateNewSaveRoute(CString route, CString newFileFolder);
   CString GetSaveInforFileName_OnSourceData(int blkIndex);
   CString GetSaveTxtFileName_OnSourceData(int blkIndex);
   CString GetSaveBinFileName_OnSourceData(int blkIndex);
//-----------------------
   void HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag);

   void ActiveSingleReadBlkCmd(int blkIndex);
   void SendDataToLowerMachine(unsigned char *buffer, unsigned short length);
//---------------------------------------------------------
   void InitialAllControl(void);
   void InitialBleReadProgressStruct();
   void InitialDataSaveDirectoryStruct();

   BOOL GetHandleBlkIndex(unsigned char *blkIndex);
//---------------------------------------------------------
   CDeviceRecoverDetailedDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDeviceRecoverDetailedDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEVICE_RECOVER_DETAILED_DLG };
#endif
public:
   BOOL PreTranslateMessage(MSG* pMsg);
   BOOL HandleKeyPressDown(MSG *pMsg);

protected:
   virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnBnClickedDetailedRecoverGetDevVerBtn();
   afx_msg void OnBnClickedDetailedRecoverGetSaveSta();
   afx_msg void OnBnClickedDetailedRecoverQuitSaveStatus();
   afx_msg void OnBnClickedDetailedRecoverGetSaveblkCnt();
   afx_msg void OnBnClickedDetailedRecoverEnterReadMode();
   afx_msg void OnBnClickedDetailedRecoverQuitReadMode();
   afx_msg void OnBnClickedDetailedRecoverGetBlkInfor();
   afx_msg void OnTimer(UINT_PTR nIDEvent);
   afx_msg void OnBnClickedDetailedRecoverGetBlkContent();
   afx_msg void OnBnClickedDetailedRecoverReadAllContent();
   afx_msg void OnBnClickedAutoEraseFlag();
};
