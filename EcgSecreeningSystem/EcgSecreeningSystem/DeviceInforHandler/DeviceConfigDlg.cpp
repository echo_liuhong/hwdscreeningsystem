// DeviceConfigDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "DeviceConfigDlg.h"
#include "afxdialogex.h"

#include "EcgSecreeningSystemDlg.h"
// CDeviceConfigDlg 对话框

#pragma warning(disable:4996)      // for localtime warning

#define  MAX_SAVE_TIME_CNT                    (72+1)
IMPLEMENT_DYNAMIC(CDeviceConfigDlg,CDialog)

CDeviceConfigDlg::CDeviceConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_DEVICE_CONFIG_DLG, pParent)
{
	int i;
	CompletePageFlag = TRUE;

	ConfigureDiffBagLenCommunicate(LONG_BAG_COMMUNICATE);
	SignalSaveContentStruct.BlkCnt = 0;
	SignalSaveContentStruct.GetBufferFlag = FALSE;
	for (i = 0; i<MAX_SAVE_BLK_CNT; i++)
	{
		SignalSaveContentStruct.BlkContent[i].InitialFlag = FALSE;
	}
	InitialBleReadProgressStruct();
	InitialDataSaveDirectoryStruct();
}
//
//
void CDeviceConfigDlg::InitialDataSaveDirectoryStruct(void)
{
	int i;
	DataSaveDirectoryStruct.IdDirectoryStr.Empty();
	for (i = 0; i<MAX_SAVE_BLK_CNT; i++)
	{
		DataSaveDirectoryStruct.Date_DirectoryStr[i].Empty();
	}
}
//
//
void CDeviceConfigDlg::InitialBleReadProgressStruct(void)
{
	BleReadProgressStruct.ChangeDevStaStep = STEP_INVALID;
	BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;

	BleReadProgressStruct.AutoEraseDataFlag = RESERVE_DATA_IN_READ_ALL_BLK;
	BleReadProgressStruct.UpdateUploadProgressFlag = FALSE;
	BleReadProgressStruct.StartPageIndex = 0;
	BleReadProgressStruct.ReadPageIndex = 0;
	BleReadProgressStruct.EndPageIndex = 0;
	BleReadProgressStruct.ReadAllPageCnt = 0;
}
//
//
void CDeviceConfigDlg::ConfigureDiffBagLenCommunicate(unsigned char bagLenFlag)
{
	DeviceStatusStruct.UploadBagLenFlag = LONG_BAG_COMMUNICATE;

	DeviceStatusStruct.UploadBagDataLen = EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG;           // 2021 1112
	DeviceStatusStruct.MaxUploadBagInternalCnt = MAX_BLE_INTERNAL_CNT_FOR_LONGBAG;                                      // 2021 1112 

	DeviceStatusStruct.MidUploadBagDataLen = MID_BLE_BAG_DATA_LEN_FOR_LONGBAG;          // 2021 1112
	DeviceStatusStruct.LastUploadBagDataLen = LAST_BLE_BAG_DATA_LEN_FOR_LONGBAG;        // 2021 1112

	DeviceStatusStruct.CheckTmp = VALID_CHECKTMP_FOR_7_PART;
}
//
//
CDeviceConfigDlg::~CDeviceConfigDlg()
{
}

void CDeviceConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDeviceConfigDlg, CDialog)
	ON_BN_CLICKED(IDC_SEND_TIME_STAMP, &CDeviceConfigDlg::OnBnClickedSendTimeStamp)
	ON_BN_CLICKED(IDC_GET_TIME_STAMP, &CDeviceConfigDlg::OnBnClickedGetTimeStamp)
	ON_BN_CLICKED(IDC_SET_RUN_STATUS, &CDeviceConfigDlg::OnBnClickedSetRunStatus)
	ON_BN_CLICKED(IDC_GET_RUN_STATUS, &CDeviceConfigDlg::OnBnClickedGetRunStatus)
	ON_BN_CLICKED(IDC_GET_DEV_VERSION, &CDeviceConfigDlg::OnBnClickedGetDevVersion)
	ON_BN_CLICKED(IDC_GET_DEV_MACADDR, &CDeviceConfigDlg::OnBnClickedGetDevMacaddr)
	ON_BN_CLICKED(IDC_GET_DEV_IDNUM, &CDeviceConfigDlg::OnBnClickedGetDevIdnum)
	ON_BN_CLICKED(IDC_SET_SAVE_STATUS, &CDeviceConfigDlg::OnBnClickedSetSaveStatus)
	ON_BN_CLICKED(IDC_GET_SAVE_STATUS, &CDeviceConfigDlg::OnBnClickedGetSaveStatus)
	ON_BN_CLICKED(IDC_GET_DEV_SAVE_BLK_CNT, &CDeviceConfigDlg::OnBnClickedGetDevSaveBlkCnt)
	ON_BN_CLICKED(IDC_GET_SAVEBLK_INFOR, &CDeviceConfigDlg::OnBnClickedGetSaveblkInfor)
	ON_BN_CLICKED(IDC_GET_SAVEBLK_CONTENT, &CDeviceConfigDlg::OnBnClickedGetSaveblkContent)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_DEV_SELF_CHECK, &CDeviceConfigDlg::OnBnClickedDevSelfCheck)
	ON_BN_CLICKED(IDC_COMPLETE_PAGE_CONTENT, &CDeviceConfigDlg::OnBnClickedCompletePageContent)
END_MESSAGE_MAP()
//
//
BOOL CDeviceConfigDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();
    InitialAllControl();
	return TRUE;
}
//
//
void CDeviceConfigDlg::InitialAllControl(void)
{
	CString str;
	CComboBox *comchoose;
	int i;

//----------------------------------------------
	FileHandler.GetCurrentProgramPath();
//----------------------------------------------
	comchoose = (CComboBox *)(GetDlgItem(IDC_SET_RUN_STATUS_CHOOSE));
	comchoose->AddString(_T("Invalid"));
	comchoose->AddString(_T("启动"));
	comchoose->AddString(_T("停止"));
	comchoose->AddString(_T("读取模式"));
	comchoose->AddString(_T("ECG_ORIGINAL"));
	comchoose->AddString(_T("RESP_ORIGINAL"));
	comchoose->AddString(_T("OneHalf"));
	comchoose->AddString(_T("激活"));
	comchoose->SetCurSel(0);

	comchoose = (CComboBox *)(GetDlgItem(IDC_CHOOSE_BIOELECTRICITY));
	comchoose->AddString(_T("ECG"));
	comchoose->AddString(_T("EMG"));
	comchoose->AddString(_T("EEG"));
	comchoose->AddString(_T("OTHER"));
	comchoose->SetCurSel(0);

	comchoose = (CComboBox *)(GetDlgItem(IDC_RESP_STATUS));
	comchoose->AddString(_T("呼吸禁用"));
	comchoose->AddString(_T("呼吸使能"));
	comchoose->SetCurSel(0);

	comchoose = (CComboBox *)(GetDlgItem(IDC_ACCELERATION_STATUS));
	comchoose->AddString(_T("加速度禁用"));
	comchoose->AddString(_T("加速度使能"));
	comchoose->SetCurSel(0);

	comchoose = (CComboBox *)(GetDlgItem(IDC_CHOOSE_SAVE_STATUS));
	comchoose->AddString(_T("进入"));
	comchoose->AddString(_T("退出"));
	comchoose->AddString(_T("自动保存"));
	comchoose->AddString(_T("擦除保存数据"));
	comchoose->AddString(_T("自动保存带索引"));

	comchoose->SetCurSel(0);
//-----------------------------------------
	comchoose = (CComboBox *)(GetDlgItem(IDC_CHOOSE_SAVE_TIME_CNT));
	for (i = 0; i<MAX_SAVE_TIME_CNT; i++)
	{
		if (0 == i)
			str.Format(_T("Invalid"), i);
		else
			str.Format(_T("时长:%d小时"), i);

		comchoose->AddString(str);
	}
	comchoose->SetCurSel(0);
//------------------------------------------------------
	for (i = 0; i < MAX_INTERNAL_INDEX_EVERY_BLK_INFOR; i++)
	{
		str.Format(_T("%d"), i);
		((CComboBox *)GetDlgItem(IDC_SAVE_BLK_INTERNAL_INDEX))->AddString(str);
	}
	((CComboBox *)GetDlgItem(IDC_SAVE_BLK_INTERNAL_INDEX))->SetCurSel(0);
//-------------------------------------------
	SetTimer(OVERFLOW_TIMER_INDEX, UPLOAD_OVERFLOW_TIME_INTERVAL, NULL);
	SetTimer(READ_PROGRESS_TIMER_INDEX, READ_PROGRESS_TIME_INTERVAL, NULL);

	int nLower, nUpper;
	((CProgressCtrl *)(GetDlgItem(IDC_READ_CONTENT_PROGRESS)))->GetRange(nLower, nUpper);
	UploadRange = nUpper - nLower;
//--------------------------------------------------
	if (TRUE == CompletePageFlag)
       ((CButton *)(GetDlgItem(IDC_COMPLETE_PAGE_CONTENT)))->SetCheck(1);
	else
       ((CButton *)(GetDlgItem(IDC_COMPLETE_PAGE_CONTENT)))->SetCheck(0);
}
//
//
void CDeviceConfigDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (READ_PROGRESS_TIMER_INDEX == nIDEvent)
	{
       //ReadDataProgressHandler();
	}
	if (OVERFLOW_TIMER_INDEX == nIDEvent)
	{
       UpdateUploadProgressAndCheck();
	}
	CDialog::OnTimer(nIDEvent);
}
//
//
void CDeviceConfigDlg::UpdateUploadProgressAndCheck()
{
	if (READ_DEVICE_DATA_IDLE == BleReadProgressStruct.CmdIndex)
		return;

	if (FALSE == BleReadProgressStruct.UpdateUploadProgressFlag)
		return;

	BleReadProgressStruct.UpdateUploadProgressFlag = FALSE;
	if (READ_SINGLE_BLK_DATA == BleReadProgressStruct.CmdIndex)
	{
        UpdateProgress_ReadSingleBlkData();
	}
	else
	{
//		UpdateProgress_ReadAllBlkData();
	}
}
void CDeviceConfigDlg::UpdateProgress_ReadSingleBlkData(void)
{
	CString str;
	int i;
	int rBlkIndex;
	BOOL errorFlag;
	unsigned int checkTmp;
//--------------------------------------------------
	rBlkIndex = BleReadProgressStruct.ReadBlkIndex;
	str.Format(_T("page: %d / %d"), BleReadProgressStruct.ReadPageIndex, BleReadProgressStruct.EndPageIndex);
	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(str);
//--------------------------------------------------
	if (BleReadProgressStruct.ReadPageIndex == BleReadProgressStruct.EndPageIndex)
	{
		DisplayUpdateSpendTimeCnt();

		errorFlag = FALSE;
		for (i = 0; i<BleReadProgressStruct.ReadAllPageCnt; i++)
		{
			if ( TRUE == CompletePageFlag )
				checkTmp = 0xFFFF;
			else
				checkTmp = DeviceStatusStruct.CheckTmp;

			if (checkTmp != SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[i].CheckTmp)
			{
				errorFlag = TRUE;
				//break;             //20220309
			}
		}
		if (TRUE == errorFlag)
		{
			AfxMessageBox(_T("invalid"));
		}
		else
		{
			if(TRUE == CompletePageFlag)
			{
			   SaveEverySendBagInfor(rBlkIndex);
			}
			else
			{
               SaveAndChangeSignalFile(rBlkIndex);
			}
		}
		BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
	}
	UpdateReadProgressControlBar();
}
//
void CDeviceConfigDlg::DisplayUpdateSpendTimeCnt(void)
{
	CString str;
	int timeTick;

	timeTick = GetTickCount();
	timeTick = timeTick - BleReadProgressStruct.BeginTimeTick;
	str.Format(_T("传输完成,耗时:%d分钟%d秒"), timeTick / 60000, (timeTick % 60000 / 1000));
	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(str);
}
//
//
void CDeviceConfigDlg::UpdateReadProgressControlBar(void)
{
	int progressValue;

	progressValue = (BleReadProgressStruct.ReadPageIndex - BleReadProgressStruct.StartPageIndex) * UploadRange;
	progressValue = progressValue / BleReadProgressStruct.ReadAllPageCnt;
	((CProgressCtrl *)(GetDlgItem(IDC_READ_CONTENT_PROGRESS)))->SetPos(progressValue);
}
//
//
void CDeviceConfigDlg::SaveEverySendBagInfor(int rBlkIndex)
{
	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(_T("正在保存每个块..."));

	CString fileName;
	fileName = GetSaveTxtFileName_OnSourceData(rBlkIndex);

	EVERY_SEND_BAG_STRUCT everySendBag;

	int readPageCnt;
	int i, index,pageIndex;
	unsigned char *readPt;
	int cnt;

	readPageCnt = SignalSaveContentStruct.BlkContent[rBlkIndex].readPageCnt;

	index = 0;
	cnt = 0 ;
	for (pageIndex = 0; pageIndex < readPageCnt; pageIndex++)
	{
		readPt = &SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[pageIndex].DataInfor.MidBag[0][0];
		for (i = 0; i < SAVE_DATA_COUNT; i++)
		{
			everySendBag.dataBufer[index++] = readPt[i];
			if (EVERY_DETAILED_DATA_CNT == index)
			{
				index = 0;
				cnt++;
				if (cnt > BleReadProgressStruct.ReadBagCnt)
				{
					break;
				}
				//Save
				SaveDetailedInfor(fileName,cnt,&everySendBag);
			}
		}
	}
	//SaveSingleBlkContent_ForChangeSamplerate(blkIndex);

	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(_T("每个块完成..."));
}
//
//
void CDeviceConfigDlg::SaveDetailedInfor(CString fileName,int bagSaveIndex,EVERY_SEND_BAG_STRUCT *detailInfor)
{
	CString showStr,str,saveStr;
	time_t timevalue;
	struct tm *mytm;
	unsigned int timeStamp, bagIndex;
	int i;
//-----------------------------------------------------------
	timeStamp = ((detailInfor->Detailed.timeStamp[0] & 0x0F) << 28);
	timeStamp = timeStamp + ((detailInfor->Detailed.timeStamp[1] & 0x7F) << 21);
	timeStamp = timeStamp + ((detailInfor->Detailed.timeStamp[2] & 0x7F) << 14);
	timeStamp = timeStamp + ((detailInfor->Detailed.timeStamp[3] & 0x7F) << 7);
	timeStamp = timeStamp + ((detailInfor->Detailed.timeStamp[4] & 0x7F));
	//------------------------------------------
	bagIndex = ((detailInfor->Detailed.bagIndex[0] & 0x7F) << 21);
	bagIndex = bagIndex + ((detailInfor->Detailed.bagIndex[1] & 0x7F) << 14);
	bagIndex = bagIndex + ((detailInfor->Detailed.bagIndex[2] & 0x7F) << 7);
	bagIndex = bagIndex + (detailInfor->Detailed.bagIndex[3] & 0x7F);
//-----------------------------------------------------------
	timevalue = timeStamp;
	mytm = localtime(&timevalue);

	str.Format(_T("%04d-%02d-%02d"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday);
	showStr = str;
	str.Format(_T(",%02d:%02d:%02d "), mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
	showStr = showStr + str;
	str.Format(_T("bagIndex = %08d "), bagIndex);
	showStr = showStr + str;

	saveStr = showStr + saveStr;
	for (i = 0; i < 150; i++)
	{
		str.Format(_T("%02X"), detailInfor->Detailed.dataBuf[i]);
		saveStr = saveStr + str;
	}
	saveStr = saveStr + _T("\r\n");

	if( 0 == bagSaveIndex)
	{
	   FileHandler.SaveDataOverwriteWrite(fileName, saveStr);
	}
	else
	{
		FileHandler.SaveDataAppendWrite(fileName, saveStr);
	}
}
//
void CDeviceConfigDlg::SaveAndChangeSignalFile(int rBlkIndex)
{
	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(_T("正在保存单块数据..."));
	GetAllSignalContentBuf(rBlkIndex);
	SaveSingleBlkForBinFile(rBlkIndex);
	SaveSingleBlkForTxtFile(rBlkIndex);

	//SaveSingleBlkContent_ForChangeSamplerate(blkIndex);

	((CStatic *)(GetDlgItem(IDC_READ_PROGRESS_INFOR)))->SetWindowText(_T("保存数据完成..."));
}
void CDeviceConfigDlg::GetAllSignalContentBuf(int blkIndex)
{
	int saveDataLen;
	int pageIndex, readPageCnt;
	PAGE_CONTENT_STRUCT *pagePt;
	unsigned char *resultBuffer;

	unsigned char *saveBuffer;
	int i, index;

	resultBuffer = new unsigned char[SAVE_DATA_COUNT];
	readPageCnt = SignalSaveContentStruct.BlkContent[blkIndex].readPageCnt;
	saveDataLen = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen;
	//----------------------------------
	// GET 2048 BYTE
	index = 0;
	saveBuffer = SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataByte;
	for (pageIndex = 0; pageIndex<readPageCnt; pageIndex++)
	{
		pagePt = &SignalSaveContentStruct.BlkContent[blkIndex].PageBuf[pageIndex];
		GetSinglePageContent(resultBuffer, pagePt);

		for (i = 0; i<SAVE_DATA_COUNT; i++)
		{
			saveBuffer[index++] = resultBuffer[i];
			if (index >= saveDataLen)
			{
				break;
			}
		}
	}
	delete[] resultBuffer;
}
void CDeviceConfigDlg::GetSinglePageContent(unsigned char *resultPt, PAGE_CONTENT_STRUCT *pagePt)
{
	unsigned char *originalBuffer;
	//---------------------------------------------------------------
	// first
	originalBuffer = new unsigned char[ORIGINAL_DATA_COUNT];

	GetOriginalBuffer(originalBuffer, pagePt);
	//-------------------------------------------------
	// second 1.5 byte to 2 byte
	GetResultBuffer(originalBuffer, resultPt);
	//-------------------------------------------------
	delete[]originalBuffer;
}
void CDeviceConfigDlg::GetOriginalBuffer(unsigned char *originalBuffer, PAGE_CONTENT_STRUCT *pagePt)
{
	int i, index, cnt, j;
	unsigned char *originalPt;
	unsigned char maxUploadBagInternalCnt;

	maxUploadBagInternalCnt = DeviceStatusStruct.MaxUploadBagInternalCnt;
	index = 0;
	for (i = 0; i<DeviceStatusStruct.MaxUploadBagInternalCnt; i++)   //20211102 for slow
	{
		originalPt = pagePt->DataInfor.LongBag[i];
		//-----------------
		if (i != (maxUploadBagInternalCnt - 1))
			cnt = DeviceStatusStruct.MidUploadBagDataLen;
		else
			cnt = DeviceStatusStruct.LastUploadBagDataLen;
		//---------------------
		for (j = 0; j<cnt; j++)
		{
			originalBuffer[index++] = originalPt[j];
		}
	}
}
void CDeviceConfigDlg::GetResultBuffer(unsigned char *originalBuffer, unsigned char *resultPt)
{
	int i, index;
	unsigned char val1, val2, val3;
	WORD_TYPE value1, value2;

	index = 0;
	i = 0;
	do
	{
		val1 = originalBuffer[i];
		val2 = originalBuffer[i + 1];
		val3 = originalBuffer[i + 2];

		value1.DataUint = (val2 & 0xf0) * 16 + val1;
		value2.DataUint = (val2 & 0x0f) * 256 + val3;

		value1.DataInt = value1.DataInt - 0x800;
		//value1.DataUint = (value1.DataUint + 0x8000) & 0xFFFF;
		value2.DataInt = value2.DataInt - 0x800;
		//value2.DataUint = (value2.DataUint + 0x8000) & 0xFFFF;

		resultPt[index++] = (unsigned char)(value1.DataUint & 0xff);
		resultPt[index++] = (unsigned char)((value1.DataUint >> 8) & 0xff);
		resultPt[index++] = (unsigned char)(value2.DataUint & 0xff);
		resultPt[index++] = (unsigned char)((value2.DataUint >> 8) & 0xff);
		i = i + 3;

	} while (i<ORIGINAL_DATA_COUNT);
}
void CDeviceConfigDlg::SaveSingleBlkForBinFile(int blkIndex)
{
	CString fileName;

	fileName = GetSaveBinFileName_OnSourceData(blkIndex);
	FileHandler.SaveDataOverwriteWrite(fileName,
                                       SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataByte,
                                       SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen);
}
//
//
void CDeviceConfigDlg::SaveSingleBlkForTxtFile(int blkIndex)
{
	CString fileName;

	fileName = GetSaveTxtFileName_OnSourceData(blkIndex);
	FileHandler.SaveShortBufToTxt(fileName,
                                  &SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataShort,
                                  SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen / 2);
}
//
//
CString CDeviceConfigDlg::GetSaveTxtFileName_OnSourceData(int blkIndex)
{
	CString fileName;

	fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
	fileName = fileName + _T("\\");
	fileName = fileName + SOURCE_DATA_FOLDER_STR;
	fileName = fileName + _T("\\");
	fileName = fileName + SIGNAL_PREFIX_VAL;
	fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
	fileName = fileName + TXT_SUFFIX_FILE_NAME;

	return fileName;
}
//
//
CString CDeviceConfigDlg::GetSaveBinFileName_OnSourceData(int blkIndex)
{
	CString fileName;

	fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
	fileName = fileName + _T("\\");
	fileName = fileName + SOURCE_DATA_FOLDER_STR;
	fileName = fileName + _T("\\");
	fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
	fileName = fileName + BIN_SUFFIX_FILE_NAME;

	return fileName;
}

//
// CDeviceConfigDlg 消息处理程序
void CDeviceConfigDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char cmdtype;

	cmdtype = receivebag->Type;
	switch (cmdtype)
	{
	    case CMD_GET_CUR_TIMESTAMP:
	    case CMD_SET_CUR_TIMESTAMP:HandleSendTimeStampCmd(receivebag);
			                       break;
//----------------------------------------------------------------------
		case CMD_GET_RUN_MODE:
		case CMD_CHANGE_STATUS_CMD:HandleChangeDeviceStatus(receivebag);
                                   break;
//---------------------------------------------------------------------
		case CMD_GET_SAVE_STATUS:HandleGetDeviceSaveStaCmd(receivebag);
                                 break;
		case CMD_SET_SAVE_STATUS:HandleSetDeviceSaveStaCmd(receivebag);
                                 break;
		case CMD_DEV_SELF_CHECKING:HandleDevSelfCheckingCmd(receivebag);
			                       break;
//---------------------------------------------------------------------
		case CMD_GET_DONGLE_POWER:HandleGetDevicePowerAndCapcityStaCmd(receivebag);
		                          break;
	    case CMD_GET_DONGLE_VERSION:HandleGetDongleVersionCmd(receivebag);
                                    break;
		case CMD_GET_BLE_MAC_ADDR:HandleGetDongleMacAddrCmd(receivebag);
			                      break;
		case CMD_GET_DEVICE_INFO:HandleGetDeviceInfo(receivebag);
			                     break;
//---------------------------------------------------------------------
	    case CMD_GET_ECG_BLK_CNT:HandleGetEcgBlkCnt(receivebag);
		                         break;
        case CMD_GET_OR_WRITE_ECG_BLK_INFOR:HandleGetEcgBlkInfor(receivebag);
		                                    break;
	    case CMD_GET_ECG_BLK_CONTENT:if (TRUE == SignalSaveContentStruct.GetBufferFlag)
	                                 {
                                         HandleGetEcgContentInfor(receivebag);
	                                 }
								     break;
//---------------------------------------------------------------------
        default:break;
	}
}
//
void CDeviceConfigDlg::HandleSendTimeStampCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	CString str;
	CString saveStr;
	unsigned int timestamp;

	timestamp = receivebag->Buffer[0];
	timestamp = timestamp + (receivebag->Buffer[1] << 8);
	timestamp = timestamp + (receivebag->Buffer[2] << 16);
	timestamp = timestamp + (receivebag->Buffer[3] << 24);
	str.Format(_T("timestamp = 0x%08X"), timestamp);
	saveStr = str;
	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(saveStr);
}
//
void CDeviceConfigDlg::HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag)
{
	CString str,showStr;
	unsigned char runMode,bioIndex,respFlag,acceFlag;

	if ((receivebag->Buffer[0] != (0x7F - receivebag->Buffer[1])))
		return;
//------------------------------------------------------
	runMode = receivebag->Buffer[0] & 0x07;
	str.Format(_T("runMode:%d,"), runMode);
	showStr = str;

	bioIndex = (receivebag->Buffer[0] & 0x18) >> 3;
	if ( 0 == bioIndex )
		showStr = showStr + _T("ECG,");
	else if( 1 == bioIndex )
		showStr = showStr + _T("EMG,");
	else if (2 == bioIndex)
		showStr = showStr + _T("EEG,");
	else
		return;

	respFlag = (receivebag->Buffer[0] & 0x20) >> 5;
	if( 0 == respFlag )
		showStr = showStr + _T("resp disable,");
	else
		showStr = showStr + _T("resp enable,");

	acceFlag = (receivebag->Buffer[0] & 0x40) >> 5;
	if (0 == acceFlag)
		showStr = showStr + _T("acce disable.");
	else
		showStr = showStr + _T("acce enable.");
//--------------------------------------------------------------------------
	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(showStr);
}
void CDeviceConfigDlg::HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	CString str;
	CString showStr;

	str.Format(_T("电量:%d"), receivebag->Buffer[0]);
	str = str + _T("%  ");
	showStr = str;
	str.Format(_T("已存:%d"), receivebag->Buffer[1]);
	str = str + _T("%");
	showStr = showStr + str;
	((CStatic *)(GetDlgItem(IDC_DEVICE_RUN_STATUS)))->SetWindowText(showStr);
}
void CDeviceConfigDlg::HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
    int length, i;
    char mory;
    CString strmory, versionStr;
    length = receivebag->Buffer[0];
    versionStr.Empty();
    for (i = 0; i < length; i++)
    {
	   mory = receivebag->Buffer[1 + i];
	   strmory = mory;
	   versionStr = versionStr + strmory;
    }
	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(versionStr);
}
//
//
void CDeviceConfigDlg::HandleGetDongleMacAddrCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int i;
	CString showStr, str;

	showStr.Empty();
	for (i = 0; i<5; i++)
	{
		str.Format(_T("%02X:"), receivebag->Buffer[i]);
		showStr = showStr + str;
	}
	str.Format(_T("%02X"), receivebag->Buffer[5]);
	showStr = showStr + str;
	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(showStr);
}
void CDeviceConfigDlg::HandleGetDeviceInfo(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int len, i;
	char memoryStr;
	CString str, showStr;

	len = receivebag->Buffer[1];
	showStr.Empty();
	showStr = _T("ID:");
	for (i = 0; i<len; i++)
	{
		memoryStr = receivebag->Buffer[i + 2];
		str = memoryStr;
		showStr = showStr + str;
	}
	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(showStr);
}
void CDeviceConfigDlg::HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char status;
	CString str;
	CString saveStr;
	unsigned int timestamp;

	status = receivebag->Buffer[0];
	if (1 == status)
	{
		str = _T("进入保存模式成功.");
	}
	else if (2 == status)
	{
		str = _T("退出保存模式成功.");
	}
	else if (3 == status)
	{
		str = _T("进入自动保存模式成功.");
	}
	else if (4 == status)
	{
		str = _T("擦除成功!");
	}
	else if (0x7F == status)
	{
		str = _T("设置不成功!");
		((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(str);
		return;
	}
	else if (0x7E == status)
	{
		str = _T("dongle没有足够的存储空间,设置不成功!");
		((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(str);
		return;
	}
	else
		return;

	saveStr = str;
	timestamp = receivebag->Buffer[1];
	timestamp = timestamp + (receivebag->Buffer[2] << 8);
	timestamp = timestamp + (receivebag->Buffer[3] << 16);
	timestamp = timestamp + (receivebag->Buffer[4] << 24);
	str.Format(_T("timestamp = 0x%08X,"), timestamp);
	saveStr = saveStr + str;

	if (3 != status)
	{
		str.Format(_T("save hours = %d"), receivebag->Buffer[5]);
		saveStr = saveStr + str;
	}
	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(saveStr);
}
//
//
void CDeviceConfigDlg::HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char status;
	CString str;
	CString saveStr;
	unsigned int timestamp, leftsavetime;

	status = receivebag->Buffer[0];
	saveStr = _T("");
	if (0 == status)
		saveStr = _T("非保存状态,");
	else
		saveStr = _T("保存状态,");

	timestamp = receivebag->Buffer[1];
	timestamp = timestamp + (receivebag->Buffer[2] << 8);
	timestamp = timestamp + (receivebag->Buffer[3] << 16);
	timestamp = timestamp + (receivebag->Buffer[4] << 24);
	str.Format(_T("timestamp = 0x%08X,"), timestamp);
	saveStr = saveStr + str;

	leftsavetime = (receivebag->Buffer[5]);
	leftsavetime = leftsavetime + (receivebag->Buffer[6] << 8);
	leftsavetime = leftsavetime + (receivebag->Buffer[7] << 16);
	leftsavetime = leftsavetime + (receivebag->Buffer[8] << 24);
	str.Format(_T("lefttime = %d,"), leftsavetime);
	saveStr = saveStr + str;
	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(saveStr);
}
//
//
void CDeviceConfigDlg::HandleDevSelfCheckingCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char value1, value2;
	int sampleRate;
	CString str, showStr;
	if (DEV_SELF_CHECKING_PROGRESS == receivebag->Buffer[0])
	{
		((CStatic *)(GetDlgItem(IDC_DEV_SELF_CHECKING_INFOR)))->SetWindowText(_T("设备自检中"));  //20191217
		return;
	}
	value1 = receivebag->Buffer[1];
	value2 = receivebag->Buffer[2];
//-----------------------------------
	showStr.Empty();
	str.Format(_T("bat:%d"), receivebag->Buffer[3]);
	str = str + _T("%,");
	showStr = showStr + str;
//-----------------------------------
	str.Format(_T("capacity:%d"), receivebag->Buffer[4]);
	str = str + _T("%,");
	showStr = showStr + str;
//-----------------------------------
	sampleRate = receivebag->Buffer[12] * 128 + receivebag->Buffer[13];
	str.Format(_T("sampleRate:%d"), sampleRate);
	showStr = showStr + str;
//-----------------------------------
	((CStatic *)(GetDlgItem(IDC_DEV_SELF_CHECKING_INFOR)))->SetWindowText(showStr);  //20191217
}
//
//
void CDeviceConfigDlg::HandleGetEcgBlkCnt(COMMUNICATE_BAG_STRUCT *receivebag)
{
	CString str;
	unsigned short ecgblkCnt;

	ecgblkCnt = receivebag->Buffer[1];
	ecgblkCnt = (ecgblkCnt * 128) + (receivebag->Buffer[0] & 0x7F);

	str.Format(_T("BlkCnt = %d"), ecgblkCnt);
	SignalSaveContentStruct.BlkCnt = ecgblkCnt;
	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_BLKCNT)))->SetWindowText(str);
}
//
void CDeviceConfigDlg::HandleGetEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int i;
	int blkIndex;
	int internalIndex;
	unsigned char *ecgBlkHeadPt;
	unsigned char *ecgBlkInforPt;

	CString blkInforStr, str;

	blkIndex = receivebag->Buffer[1] * 128 + receivebag->Buffer[0];  //ecg blk index
	internalIndex = receivebag->Buffer[2] & 0x0F;

	if (0 == internalIndex)
	{
		ecgBlkHeadPt = SignalSaveContentStruct.BlkHead[blkIndex].HeadBuffer;
		for (i = 0; i<EVERY_BAG_LEN; i++)
		{
			ecgBlkHeadPt[i] = receivebag->Buffer[3 + i];
		}
        ApplyMemoryForSaveContent(blkIndex);
		GetHeadStrInfor(blkIndex);
	}
	else if (1 == internalIndex)
	{
		ecgBlkInforPt = SignalSaveContentStruct.BlkInfor[blkIndex].Buffer;
		for (i = 0; i<EVERY_BAG_LEN; i++)
		{
			ecgBlkInforPt[i] = receivebag->Buffer[3 + i];
		}
//		GetEveryBlkInfor(blkIndex);
	}
	DisplaySignalSaveBlkInfo(blkIndex);
	AfxMessageBox(_T("成功获取块信息!"));
}
//
//
void CDeviceConfigDlg::ApplyMemoryForSaveContent(int blkIndex)
{
	unsigned int saveDataLen;
	unsigned int readPageCnt;

	SignalSaveContentStruct.GetBufferFlag = TRUE;
	saveDataLen = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen;

	readPageCnt = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.EndPageIndex;
	readPageCnt = readPageCnt - SignalSaveContentStruct.BlkHead[blkIndex].DataHead.BeginPageIndex + 1;
	SignalSaveContentStruct.BlkContent[blkIndex].readPageCnt = readPageCnt;
//---------------------------------
	if (FALSE == SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag)
	{
		SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag = TRUE;

		SignalSaveContentStruct.BlkContent[blkIndex].Buffer = new HALFWORD_TYPE[saveDataLen / 2];
		SignalSaveContentStruct.BlkContent[blkIndex].PageBuf = new PAGE_CONTENT_STRUCT[readPageCnt];
	}
	else
	{
		delete[] SignalSaveContentStruct.BlkContent[blkIndex].PageBuf;
		delete[] SignalSaveContentStruct.BlkContent[blkIndex].Buffer;

		SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag = TRUE;
		SignalSaveContentStruct.BlkContent[blkIndex].Buffer = new HALFWORD_TYPE[saveDataLen / 2];
		SignalSaveContentStruct.BlkContent[blkIndex].PageBuf = new PAGE_CONTENT_STRUCT[readPageCnt];
	}
}
//
//
void CDeviceConfigDlg::GetHeadStrInfor(int blkIndex)
{
	int i;
	SAVE_DATA_HEAD *saveDataHeadPt;
	unsigned short actualSampleRate;
	CString blkInforStr, str;

	saveDataHeadPt = &SignalSaveContentStruct.BlkHead[blkIndex];
	blkInforStr.Empty();
	if (0 == saveDataHeadPt->DataHead.DivideVal)
	{
		actualSampleRate = saveDataHeadPt->DataHead.SampleRate;
	}
	else if (0xFF == saveDataHeadPt->DataHead.DivideVal)
	{
		actualSampleRate = saveDataHeadPt->DataHead.SampleRate;
	}
	else
	{
		actualSampleRate = saveDataHeadPt->DataHead.SampleRate / saveDataHeadPt->DataHead.DivideVal;
	}
	str.Format(_T("samplerate = %d,divideVal = %d,actualSamplerate = %d,LeadCnt = %d,"), saveDataHeadPt->DataHead.SampleRate,
		saveDataHeadPt->DataHead.DivideVal,
		actualSampleRate,
		saveDataHeadPt->DataHead.LeadCnt);
	blkInforStr = blkInforStr + str;

	if (DONGLE_CHOOSE_ECG == saveDataHeadPt->DataHead.LeadType)
		str = _T("ECG\n");
	else if (DONGLE_CHOOSE_EMG == saveDataHeadPt->DataHead.LeadType)
		str = _T("EMG\n");
	else if (DONGLE_CHOOSE_EEG == saveDataHeadPt->DataHead.LeadType)
		str = _T("EEG\n");
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	str.Format(_T("Mac ADDR = %02X,%02X,%02X,%02X,%02X,%02X,%02X,%02X\r\n"), saveDataHeadPt->DataHead.MacAddr[0],
	                                                                         saveDataHeadPt->DataHead.MacAddr[1],
                                                                             saveDataHeadPt->DataHead.MacAddr[2],
                                                                             saveDataHeadPt->DataHead.MacAddr[3],
                                                                             saveDataHeadPt->DataHead.MacAddr[4],
                                                                             saveDataHeadPt->DataHead.MacAddr[5],
                                                                             saveDataHeadPt->DataHead.MacAddr[6],
                                                                             saveDataHeadPt->DataHead.MacAddr[7]);
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	time_t timevalue;
	struct tm *mytm;
	timevalue = saveDataHeadPt->DataHead.StartTimeStamp;
	mytm = localtime(&timevalue);
	blkInforStr = blkInforStr + _T("start time stamp = 0x");
	SignalSaveContentStruct.StartTimeStampStr[blkIndex].Empty();
	str.Format(_T("%08X"), saveDataHeadPt->DataHead.StartTimeStamp);   //20211102
	SignalSaveContentStruct.StartTimeStampStr[blkIndex] = str;
	blkInforStr = blkInforStr + str + _T("\r\n");
	blkInforStr = blkInforStr + _T("开始保存的时间 ");

	str.Format(_T("%04d-%02d-%02d"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday);
	SignalSaveContentStruct.RecordedDateStr[blkIndex] = str;
	blkInforStr = blkInforStr + str;

	str.Format(_T(",%02d:%02d:%02d\r\n"), mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	timevalue = saveDataHeadPt->DataHead.EndTimeStamp;
	mytm = localtime(&timevalue);
	str.Format(_T("end time stamp = 0x%08X\r\n"), saveDataHeadPt->DataHead.EndTimeStamp);
	blkInforStr = blkInforStr + str;
	str.Format(_T("结束保存的时间 %04d-%02d-%02d,%02d:%02d:%02d\r\n"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday, mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
	blkInforStr = blkInforStr + str;

	str.Format(_T("beginPageIndex = %d,"),saveDataHeadPt->DataHead.BeginPageIndex);
	blkInforStr = blkInforStr + str;
	str.Format(_T("%d"), saveDataHeadPt->DataHead.BeginPageIndex);
	((CStatic *)(GetDlgItem(IDC_BLK_PAGE_MIN)))->SetWindowText(str);

	str.Format(_T("endPageIndex = %d\r\n"),saveDataHeadPt->DataHead.EndPageIndex);
	blkInforStr = blkInforStr + str;
	str.Format(_T("%d"), saveDataHeadPt->DataHead.EndPageIndex);
	((CStatic *)(GetDlgItem(IDC_BLK_PAGE_MAX)))->SetWindowText(str);
	//--------------------------------------------------
	str.Format(_T("save data len = %d,data len = %d\r\n"), saveDataHeadPt->DataHead.SaveDataLen, saveDataHeadPt->DataHead.SaveDataLen / 2);
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	str.Format(_T("ID num = "));
	blkInforStr = blkInforStr + str;
	SignalSaveContentStruct.IdStr[blkIndex].Empty();
	for (i = 0; i < saveDataHeadPt->DataHead.IDNumberLen; i++)
	{
		str = (char)(saveDataHeadPt->DataHead.IdNumberBuf[i]);
		blkInforStr = blkInforStr + str;
		SignalSaveContentStruct.IdStr[blkIndex] = SignalSaveContentStruct.IdStr[blkIndex] + str;
	}
	str = _T(",");
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	str.Format(_T("user ID = "));
	blkInforStr = blkInforStr + str;
	for (i = 0; i < saveDataHeadPt->DataHead.UserIdLen; i++)
	{
		str = (char)(saveDataHeadPt->DataHead.UserIDStrBuf[i]);
		blkInforStr = blkInforStr + str;
	}
	str.Format(_T("\n"));
	blkInforStr = blkInforStr + str;
	//--------------------------------------------------
	str.Format(_T("保存唯一标识 = "));
	blkInforStr = blkInforStr + str;
	for (i = 0; i < saveDataHeadPt->DataHead.SaveDataOnlyFlagLen; i++)
	{
		str = (char)(saveDataHeadPt->DataHead.SaveDataOnlyFlagBuf[i]);
		blkInforStr = blkInforStr + str;
	}
	str.Format(_T("\n"));
	blkInforStr = blkInforStr + str;
	//------------------------------------------------
	str.Format(_T("数据属性 = 0x%02X:"), saveDataHeadPt->DataHead.EndBlkValue);
	blkInforStr = blkInforStr + str;
	if (0x00 == saveDataHeadPt->DataHead.EndBlkValue)
	{
		str.Format(_T("中间数据\n"));
	}
	else if (0x55 == saveDataHeadPt->DataHead.EndBlkValue)
	{
		str.Format(_T("结尾数据\n"));
	}
	blkInforStr = blkInforStr + str;
//------------------------------------------------------
	int startBagIndex, endBagIndex;
	if (0 != saveDataHeadPt->DataHead.AuxType0)
	{
		if( CMD_ECG_ONEHALF_FOR_BAGINDEX == saveDataHeadPt->DataHead.AuxType0 )
		{
			startBagIndex = (saveDataHeadPt->DataHead.StartBagIndex[0] << 21);
			startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[1] << 14);
			startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[2] << 7);
			startBagIndex = startBagIndex + saveDataHeadPt->DataHead.StartBagIndex[3];

            endBagIndex = (saveDataHeadPt->DataHead.EndBagIndex[0] << 21);
            endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[1] << 14);
            endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[2] << 7);
			endBagIndex = endBagIndex + saveDataHeadPt->DataHead.EndBagIndex[3];
			str.Format("%d", startBagIndex);
			((CStatic *)(GetDlgItem(IDC_BLK_BAG_MIN)))->SetWindowText(str);
			str.Format("%d", endBagIndex);
			((CStatic *)(GetDlgItem(IDC_BLK_BAG_MAX)))->SetWindowText(str);

			str.Format("startBagIndex = %d,endBagIndex = %d\r\n", startBagIndex, endBagIndex);
		}
		else if( CMD_EMG_ONEHALF_FOR_BAGINDEX == saveDataHeadPt->DataHead.AuxType0 )
		{
			startBagIndex = (saveDataHeadPt->DataHead.StartBagIndex[0] << 21);
			startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[1] << 14);
			startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[2] << 7);
			startBagIndex = startBagIndex + saveDataHeadPt->DataHead.StartBagIndex[3];

			endBagIndex = (saveDataHeadPt->DataHead.EndBagIndex[0] << 21);
			endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[1] << 14);
			endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[2] << 7);
			endBagIndex = endBagIndex + saveDataHeadPt->DataHead.EndBagIndex[3];

			str.Format("%d", startBagIndex);
			((CStatic *)(GetDlgItem(IDC_BLK_BAG_MIN)))->SetWindowText(str);
			str.Format("%d", endBagIndex);
			((CStatic *)(GetDlgItem(IDC_BLK_BAG_MAX)))->SetWindowText(str);

			str.Format("startBagIndex = %d,endBagIndex = %d\r\n", startBagIndex, endBagIndex);
		}
	}
	blkInforStr = blkInforStr + str;
	SignalSaveContentStruct.BlkHeadStr[blkIndex] = blkInforStr;
//------------------------------------------------------
	/*
	if (READ_SINGLE_BLK_DATA == BleReadProgressStruct.CmdIndex)
	{
	AfxMessageBox(SignalSaveContentStruct.BlkHeadStr[blkIndex]);
	}
	*/
	//--------------------------------------------------------------------------------------
}
void CDeviceConfigDlg::DisplaySignalSaveBlkInfo(int blkIndex)
{
	CString str;
	//------------------------------------------------------------------------------------
	// first create id file folder
	str = CreateNewSaveRoute(FileHandler.CurModuleRoute, SignalSaveContentStruct.IdStr[blkIndex]);
	if (0 == str.IsEmpty())
	{
		DataSaveDirectoryStruct.IdDirectoryStr = str;
	}
	else
	{
		AfxMessageBox(_T("创建文件夹失败!"));
		return;
	}
	//------------------------------------------------------------------------------------
	// second create date file folder
	str = CreateNewSaveRoute(DataSaveDirectoryStruct.IdDirectoryStr, SignalSaveContentStruct.RecordedDateStr[blkIndex]);
	if (0 == str.IsEmpty())
	{
		DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex] = str;
	}
	else
	{
		AfxMessageBox(_T("创建文件夹失败!"));
		return;
	}
	//--------------------------------------------------
	// fourth create source data file folder
	str = CreateNewSaveRoute(DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex], SOURCE_DATA_FOLDER_STR);
	if (0 != str.IsEmpty())
	{
		AfxMessageBox(_T("创建文件夹失败!"));
		return;
	}

	//-----------------------------------------------
	// fifth eighth merge data file folder
	if (READ_ALL_BLK_DATA == BleReadProgressStruct.CmdIndex)
	{
		str = CreateNewSaveRoute(DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex], MERGE_DATA_FOLDER_STR);
		if (0 != str.IsEmpty())
		{
			AfxMessageBox(_T("创建文件夹失败!"));
			return;
		}
	}
	//-------------------------------------------
	CString fileName;
	fileName = GetSaveInforFileName_OnSourceData(blkIndex);
	FileHandler.SaveDataOverwriteWrite(fileName, SignalSaveContentStruct.BlkHeadStr[blkIndex]);
	//--------------------------------------------------
}
//
//
CString CDeviceConfigDlg::CreateNewSaveRoute(CString route, CString newFileFolder)
{
	BOOL createDirectorySuccessFlag;
	CString str;

	createDirectorySuccessFlag = FileHandler.CreateNewFileFolder(route, newFileFolder);
	str.Empty();
	if (TRUE == createDirectorySuccessFlag)
	{
		str = route + _T("\\");
		str = str + newFileFolder;
	}
	return str;
}
//
//
CString CDeviceConfigDlg::GetSaveInforFileName_OnSourceData(int blkIndex)
{
	CString fileName;
	CString str;

	fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
	fileName = fileName + _T("\\");
	fileName = fileName + SOURCE_DATA_FOLDER_STR;
	fileName = fileName + _T("\\");
	str.Format(_T("Blk%02d_"), blkIndex);
	fileName = fileName + str;

	fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
	fileName = fileName + FOLDER_NAMER_STR;
	fileName = fileName + TXT_SUFFIX_FILE_NAME;

	return fileName;
}
//
//
void CDeviceConfigDlg::HandleGetEcgContentInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
	if (TRUE == CompletePageFlag)
	{
		HandleGetCompleteSaveContent(receivebag);
	}
	else
	{
		HandleGetCompressSaveContent(receivebag);
	}
	
}
//
//
void CDeviceConfigDlg::HandleGetCompleteSaveContent(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int pageIndex, savePageIndex;
	int internalIndex;
	int i;
	unsigned short chckTmp;
	int rBlkIndex;

	pageIndex = receivebag->Buffer[0];
	pageIndex = pageIndex + (receivebag->Buffer[1] << 7);
	pageIndex = pageIndex + (receivebag->Buffer[2] << 14);

	rBlkIndex = SignalSaveContentStruct.rBlkIndex;

	SignalSaveContentStruct.readPageIndex = pageIndex;
	savePageIndex = pageIndex - SignalSaveContentStruct.BlkHead[rBlkIndex].DataHead.BeginPageIndex;
	SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex = pageIndex;

	internalIndex = receivebag->Buffer[3];

	chckTmp = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp;
	chckTmp = chckTmp | (1 << internalIndex);
	SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp = chckTmp;
	//-------------------------------------------
	unsigned char *targetPt;
	targetPt = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].DataInfor.MidBag[internalIndex];
	for (i = 0; i<EVERY_BLE_BAG_DATA_LEN_FOR_COMPLETE_DATA; i++)
	{
		targetPt[i] = receivebag->Buffer[5 + i];
	}
	BleReadProgressStruct.ReadPageIndex = SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex;
	if ((MAX_BLE_INTERNAL_CNT_FOR_COMPLETE_DATA - 1) != internalIndex)
	{
		return;
	}
	BleReadProgressStruct.UpdateUploadProgressFlag = TRUE;
}
//
//
void CDeviceConfigDlg::HandleGetCompressSaveContent(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int pageIndex, savePageIndex;
	int internalIndex;
	int i;
	unsigned short chckTmp;
	int rBlkIndex;

	pageIndex = receivebag->Buffer[0];
	pageIndex = pageIndex + (receivebag->Buffer[1] << 7);
	pageIndex = pageIndex + (receivebag->Buffer[2] << 14);

	rBlkIndex = SignalSaveContentStruct.rBlkIndex;

	SignalSaveContentStruct.readPageIndex = pageIndex;
	savePageIndex = pageIndex - SignalSaveContentStruct.BlkHead[rBlkIndex].DataHead.BeginPageIndex;
	SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex = pageIndex;

	internalIndex = receivebag->Buffer[3];

	chckTmp = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp;
	chckTmp = chckTmp | (1 << internalIndex);
	SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp = chckTmp;
	//-------------------------------------------
	unsigned char *targetPt;
	targetPt = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].DataInfor.LongBag[internalIndex];
	for (i = 0; i<DeviceStatusStruct.MidUploadBagDataLen; i++)
	{
		targetPt[i] = receivebag->Buffer[5 + i];
	}
	BleReadProgressStruct.ReadPageIndex = SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex;
	if ((DeviceStatusStruct.MaxUploadBagInternalCnt - 1) != internalIndex)
	{
		return;
	}
	BleReadProgressStruct.UpdateUploadProgressFlag = TRUE;
}
//======================================================
//
//
void CDeviceConfigDlg::OnBnClickedSendTimeStamp()
{
	time_t timevalue;
	CTime time = CTime::GetCurrentTime();

	timevalue = time.GetTime();
	DataProtocolHandler.GenerateSendTimeStampCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, timevalue, NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                            DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(_T(""));
}
void CDeviceConfigDlg::OnBnClickedGetTimeStamp()
{
	DataProtocolHandler.GenerateReadTimeStampCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(_T(""));
}

void CDeviceConfigDlg::OnBnClickedSetRunStatus()
{
	unsigned char temp,value;

	temp = ((CComboBox *)(GetDlgItem(IDC_SET_RUN_STATUS_CHOOSE)))->GetCurSel();
	value = temp;
	temp = ((CComboBox *)(GetDlgItem(IDC_CHOOSE_BIOELECTRICITY)))->GetCurSel();
	value = value | (temp << 3);
	temp = ((CComboBox *)(GetDlgItem(IDC_RESP_STATUS)))->GetCurSel();
	value = value | (temp << 5);
	temp = ((CComboBox *)(GetDlgItem(IDC_ACCELERATION_STATUS)))->GetCurSel();
	value = value | (temp << 6);

	DataProtocolHandler.GenerateActiveSampleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,value,NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
		                   DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(_T(""));
}

void CDeviceConfigDlg::OnBnClickedGetRunStatus()
{
	DataProtocolHandler.GenerateGetActiveSampleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
	((CStatic *)(GetDlgItem(IDC_ACK_STATUS)))->SetWindowText(_T(""));
}
//
void CDeviceConfigDlg::OnBnClickedGetDevVersion()
{
	DataProtocolHandler.GenerateGetDongleVerCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(_T(""));
}
//
void CDeviceConfigDlg::OnBnClickedGetDevMacaddr()
{
	DataProtocolHandler.GenerateGetDevMacAddrCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(_T(""));
}
void CDeviceConfigDlg::OnBnClickedGetDevIdnum()
{
	DataProtocolHandler.GenerateGetDongleFixInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
		                                             CMD_GET_SERIAL_NUMBER_INDEX,
                                                     NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
		                    DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_DEV_FIX_INFOR)))->SetWindowText(_T(""));
}
//
//
void CDeviceConfigDlg::OnBnClickedSetSaveStatus()
{
	unsigned char saveMode, saveTimeCnt;
	saveMode = ((CComboBox *)(GetDlgItem(IDC_CHOOSE_SAVE_STATUS)))->GetCurSel();
	saveMode = saveMode + 1;

	saveTimeCnt = ((CComboBox *)(GetDlgItem(IDC_CHOOSE_SAVE_TIME_CNT)))->GetCurSel();

	DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
		                                            saveMode, saveTimeCnt, NULL);
 	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(_T(""));
}
//
void CDeviceConfigDlg::OnBnClickedGetSaveStatus()
{
	DataProtocolHandler.GenerateGetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                            DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_STA)))->SetWindowText(_T(""));
}
//
//
void CDeviceConfigDlg::OnBnClickedGetDevSaveBlkCnt()
{
	DataProtocolHandler.GenerateGetSaveBlkCnt(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                            DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_DEV_SAVE_BLKCNT)))->SetWindowText(_T(""));
}

void CDeviceConfigDlg::OnBnClickedGetSaveblkInfor()
{
	unsigned char ecgBlkIndex;
	unsigned char internalIndex;
	internalIndex = ((CComboBox *)(GetDlgItem(IDC_SAVE_BLK_INTERNAL_INDEX)))->GetCurSel();
	if ((FALSE == GetHandleBlkIndex(&ecgBlkIndex)) || (internalIndex >= MAX_BLK_INTERNAL_INDEX))
	{
		AfxMessageBox(_T("未获取到有效的块索引"));
		return;
	}
	DataProtocolHandler.GenerateGetEveryBlkInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, ecgBlkIndex, internalIndex, NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
		                    DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

}
//
//
void CDeviceConfigDlg::OnBnClickedGetSaveblkContent()
{
	unsigned char blkIndex;
	if ( FALSE == GetHandleBlkIndex(&blkIndex))
	{
		AfxMessageBox(_T("未获取到有效的块索引!"));
		return;
	}
	if ( FALSE == SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag)
	{
		AfxMessageBox(_T("未获取到该块的保存信息!"));
		return;
	}
	BleReadProgressStruct.BeginTimeTick = GetTickCount();
	BleReadProgressStruct.CmdIndex = READ_SINGLE_BLK_DATA;
	ActiveSingleReadBlkCmd(blkIndex);
}
void CDeviceConfigDlg::ActiveSingleReadBlkCmd(int blkIndex)
{
	SAVE_DATA_HEAD *saveDataHeadPt;
	int i;
	unsigned char startCmd;
	unsigned int startBagIndex, endBagIndex;
//-------------------------------
	startCmd = START_TRANSMIT_CMD | ACK_LONG_BAG_FLAG;
	if (TRUE == CompletePageFlag)
	{
		startCmd = startCmd | ACK_COMPLETE_PAGE_CONTENT_FLAG;
	}
//-------------------------------
	SignalSaveContentStruct.rBlkIndex = blkIndex;
	BleReadProgressStruct.ReadBlkIndex = blkIndex;
	//---------------------------------
	saveDataHeadPt = &SignalSaveContentStruct.BlkHead[blkIndex];
	BleReadProgressStruct.StartPageIndex = saveDataHeadPt->DataHead.BeginPageIndex;
	BleReadProgressStruct.EndPageIndex = saveDataHeadPt->DataHead.EndPageIndex;
	BleReadProgressStruct.ReadPageIndex = BleReadProgressStruct.StartPageIndex;
	BleReadProgressStruct.ReadAllPageCnt = BleReadProgressStruct.EndPageIndex - BleReadProgressStruct.StartPageIndex + 1;

	startBagIndex = (saveDataHeadPt->DataHead.StartBagIndex[0] << 21);
	startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[1] << 14);
	startBagIndex = startBagIndex + (saveDataHeadPt->DataHead.StartBagIndex[2] << 7);
	startBagIndex = startBagIndex + saveDataHeadPt->DataHead.StartBagIndex[3];

	endBagIndex = (saveDataHeadPt->DataHead.EndBagIndex[0] << 21);
	endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[1] << 14);
	endBagIndex = endBagIndex + (saveDataHeadPt->DataHead.EndBagIndex[2] << 7);
	endBagIndex = endBagIndex + saveDataHeadPt->DataHead.EndBagIndex[3];

	BleReadProgressStruct.StartBagIndex = startBagIndex;
	BleReadProgressStruct.EndBagIndex = endBagIndex;
	BleReadProgressStruct.ReadBagCnt = endBagIndex - startBagIndex + 1;

	for (i = 0; i<BleReadProgressStruct.ReadAllPageCnt; i++)
	{
		SignalSaveContentStruct.BlkContent[blkIndex].PageBuf[i].CheckTmp = 0;
	}
	DataProtocolHandler.GenerateGetEveryBlkContentCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                	  startCmd,
                                                      BleReadProgressStruct.StartPageIndex,
		                                              BleReadProgressStruct.EndPageIndex,
		                                              NULL);
	SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                           DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}

//
//
BOOL CDeviceConfigDlg::GetHandleBlkIndex(unsigned char *blkIndex)
{
	CString ecgBlkIndexStr;
	int length;
	int ecgBlkIndex;
	int ecgBlkCnt;
	int i;
	unsigned char tmp;

	((CEdit *)(GetDlgItem(IDC_READ_SAVE_BLK_INDEX)))->GetWindowText(ecgBlkIndexStr);  //20191217
	ecgBlkIndex = 0;

	length = ecgBlkIndexStr.GetLength();

	if ((length>2) || (0 == length))
	{
		((CEdit *)(GetDlgItem(IDC_READ_SAVE_BLK_INDEX)))->SetWindowText(_T(""));  //20191217
		return FALSE;
	}
	for (i = 0; i<ecgBlkIndexStr.GetLength(); i++)
	{
		tmp = ecgBlkIndexStr[i] - '0';
		ecgBlkIndex = ecgBlkIndex * 10 + tmp;
	}
	ecgBlkCnt = SignalSaveContentStruct.BlkCnt;
	if ((0 == ecgBlkCnt) || (ecgBlkIndex >= ecgBlkCnt))
	{
		((CEdit *)(GetDlgItem(IDC_READ_SAVE_BLK_INDEX)))->SetWindowText(_T(""));  //20191217
		return FALSE;
	}
	*blkIndex = ecgBlkIndex;
	return TRUE;
}
void CDeviceConfigDlg::OnBnClickedDevSelfCheck()
{
	DataProtocolHandler.GenerateDevSelfCheckCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, START_SELF_CHECKING_CMD, NULL);
	SendDataToLowerMachine( DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                            DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

	((CStatic *)(GetDlgItem(IDC_DEV_SELF_CHECKING_INFOR)))->SetWindowText(_T(""));  //20191217
}
//
//
void CDeviceConfigDlg::SendDataToLowerMachine(unsigned char *buffer, unsigned short length)
{
	if (NULL == ParentPt)
		return;

	unsigned type;
	CEcgSecreeningSystemDlg *dlgPt;
	dlgPt = (CEcgSecreeningSystemDlg *)(ParentPt);

	type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
	type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
	type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
	dlgPt->SendBuffer(type, buffer, length);
}

void CDeviceConfigDlg::OnBnClickedCompletePageContent()
{
//---------------------
	if (TRUE == CompletePageFlag)
        CompletePageFlag = FALSE;
	else
		CompletePageFlag = TRUE;
//---------------------
	if (TRUE == CompletePageFlag)
		((CButton *)(GetDlgItem(IDC_COMPLETE_PAGE_CONTENT)))->SetCheck(1);
	else
		((CButton *)(GetDlgItem(IDC_COMPLETE_PAGE_CONTENT)))->SetCheck(0);
}
