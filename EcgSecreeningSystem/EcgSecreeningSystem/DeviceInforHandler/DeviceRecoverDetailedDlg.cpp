// DeviceRecoverDetailedDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "DeviceRecoverDetailedDlg.h"
#include "afxdialogex.h"

#include "EcgSecreeningSystemDlg.h"
// CDeviceRecoverDetailedDlg 对话框

#pragma warning(disable:4996)      // for localtime warning
//----------------------------------------------------------
//#define OVERFLOW_TIMER_INDEX                    2
//#define UPLOAD_OVERFLOW_TIME_INTERVAL           1000

//#define READ_PROGRESS_TIMER_INDEX               3
//#define READ_PROGRESS_TIME_INTERVAL             400

IMPLEMENT_DYNAMIC(CDeviceRecoverDetailedDlg, CDialog)

CDeviceRecoverDetailedDlg::CDeviceRecoverDetailedDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_DEVICE_RECOVER_DETAILED_DLG, pParent)
{
   int i;

//-------------------------------------------
   ConfigureDiffBagLenCommunicate(LONG_BAG_COMMUNICATE);
   SignalSaveContentStruct.BlkCnt = 0;
   SignalSaveContentStruct.GetBufferFlag = FALSE;
   for (i = 0; i<MAX_SAVE_BLK_CNT; i++)
   {
      SignalSaveContentStruct.BlkContent[i].InitialFlag = FALSE;
   }
   InitialBleReadProgressStruct();
   InitialDataSaveDirectoryStruct();
}
//
//
void CDeviceRecoverDetailedDlg::InitialDataSaveDirectoryStruct(void)
{
   int i;
   DataSaveDirectoryStruct.IdDirectoryStr.Empty();
   for (i = 0; i<MAX_SAVE_BLK_CNT; i++)
   {
      DataSaveDirectoryStruct.Date_DirectoryStr[i].Empty();
   }
}
//
//
void CDeviceRecoverDetailedDlg::InitialBleReadProgressStruct(void)
{
   BleReadProgressStruct.ChangeDevStaStep = STEP_INVALID;
   BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;

   BleReadProgressStruct.AutoEraseDataFlag = RESERVE_DATA_IN_READ_ALL_BLK;
   BleReadProgressStruct.UpdateUploadProgressFlag = FALSE;
   BleReadProgressStruct.StartPageIndex = 0;
   BleReadProgressStruct.ReadPageIndex = 0;
   BleReadProgressStruct.EndPageIndex = 0;
   BleReadProgressStruct.ReadAllPageCnt = 0;
}
//
//
void CDeviceRecoverDetailedDlg::ConfigureDiffBagLenCommunicate(unsigned char bagLenFlag)
{
   DeviceStatusStruct.UploadBagLenFlag = LONG_BAG_COMMUNICATE;

   DeviceStatusStruct.UploadBagDataLen = EVERY_BLE_BAG_DATA_LEN_FOR_LONGBAG;           // 2021 1112
   DeviceStatusStruct.MaxUploadBagInternalCnt = MAX_BLE_INTERNAL_CNT_FOR_LONGBAG;                                      // 2021 1112 

   DeviceStatusStruct.MidUploadBagDataLen = MID_BLE_BAG_DATA_LEN_FOR_LONGBAG;          // 2021 1112
   DeviceStatusStruct.LastUploadBagDataLen = LAST_BLE_BAG_DATA_LEN_FOR_LONGBAG;        // 2021 1112

   DeviceStatusStruct.CheckTmp = VALID_CHECKTMP_FOR_7_PART;
}
CDeviceRecoverDetailedDlg::~CDeviceRecoverDetailedDlg()
{
}
void CDeviceRecoverDetailedDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDeviceRecoverDetailedDlg, CDialog)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_GET_DEV_VER_BTN, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetDevVerBtn)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_GET_SAVE_STA, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetSaveSta)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_QUIT_SAVE_STATUS, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverQuitSaveStatus)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_GET_SAVEBLK_CNT, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetSaveblkCnt)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_ENTER_READ_MODE, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverEnterReadMode)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_QUIT_READ_MODE, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverQuitReadMode)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_GET_BLK_INFOR, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetBlkInfor)
   ON_WM_TIMER()
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_GET_BLK_CONTENT, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetBlkContent)
   ON_BN_CLICKED(IDC_DETAILED_RECOVER_READ_ALL_CONTENT, &CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverReadAllContent)
   ON_BN_CLICKED(IDC_AUTO_ERASE_FLAG, &CDeviceRecoverDetailedDlg::OnBnClickedAutoEraseFlag)
END_MESSAGE_MAP()

//
// shield esc and enter key
BOOL CDeviceRecoverDetailedDlg::PreTranslateMessage(MSG* pMsg)
{
   if (TRUE == HandleKeyPressDown(pMsg))
      return TRUE;

   return CDialog::PreTranslateMessage(pMsg);
}
//
// shield esc and enter
//
BOOL CDeviceRecoverDetailedDlg::HandleKeyPressDown(MSG *pMsg)
{
   if ((WM_KEYFIRST <= pMsg->message) && (pMsg->message <= WM_KEYLAST))
   {
      if ((SHIELD_KEYBOARD_KEY1 == pMsg->wParam) || (SHIELD_KEYBOARD_KEY2 == pMsg->wParam))
         return TRUE;
   }
   return FALSE;
}
//
//
BOOL CDeviceRecoverDetailedDlg::OnInitDialog(void)
{
   CDialog::OnInitDialog();
   InitialAllControl();
   return TRUE;
}
void CDeviceRecoverDetailedDlg::InitialAllControl(void)
{
   int i;
   CString str;

   FileHandler.GetCurrentProgramPath();
//-------------------------------------
   for (i = 0; i < MAX_INTERNAL_INDEX_EVERY_BLK_INFOR;i++)
   {
      str.Format(_T("%d"), i);
      ((CComboBox *)GetDlgItem(IDC_DETAILED_RECOVER_BLK_INTERNAL_INDEX))->AddString(str);
   }
   ((CComboBox *)GetDlgItem(IDC_DETAILED_RECOVER_BLK_INTERNAL_INDEX))->SetCurSel(0);
//---------------
   SetTimer(OVERFLOW_TIMER_INDEX, UPLOAD_OVERFLOW_TIME_INTERVAL, NULL);
   SetTimer(READ_PROGRESS_TIMER_INDEX, READ_PROGRESS_TIME_INTERVAL, NULL);

   int nLower, nUpper;
   ((CProgressCtrl *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_PROGRESS)))->GetRange(nLower, nUpper);
   UploadRange = nUpper - nLower;

}
void CDeviceRecoverDetailedDlg::OnTimer(UINT_PTR nIDEvent)
{
   if (READ_PROGRESS_TIMER_INDEX == nIDEvent)
   {
      ReadDataProgressHandler();
   }
   if (OVERFLOW_TIMER_INDEX == nIDEvent)
   {
      UpdateUploadProgressAndCheck();
   }
   CDialog::OnTimer(nIDEvent);
}
//
//
void CDeviceRecoverDetailedDlg::ReadDataProgressHandler(void)
{
   if (READ_ALL_BLK_DATA != BleReadProgressStruct.CmdIndex)
      return;

   if ((INVALID_READ_STEP == BleReadProgressStruct.ReadStep) || (BleReadProgressStruct.ReadStep > QUIT_READ_MODE))
   {
      InvalidReadStepHandler();
   }
//------------------------------
   if (SEND_BLE_CMD_STEP == BleReadProgressStruct.DirectStatus)
   {
      BleDiffReadStepHandler(BleReadProgressStruct.ReadStep);
      CheckRepeatSendCmdHandler();
   }
   else if (WAIT_ACK_CMD_STEP == BleReadProgressStruct.DirectStatus)
   {
      CheckWaitAckCmdHandler(WAIT_ACK_TIME_CNT);
   }
}

void CDeviceRecoverDetailedDlg::BleDiffReadStepHandler(int readStep)
{
   switch (readStep)
   {
      case FIRST_QUIT_READ_MODE:BleReadProgressStruct.ChangeDevStaStep = STEP_FIRST_QUIT_READMODE;
                                QuitReadmodeHandler();
                                break;
      case READ_DEVICE_SAVE_STATUS:ReadDeviceSaveStatusHandler();
                                   break;
      case QUIT_DEVICE_SAVE_STATUS:BleReadProgressStruct.ChangeDevStaStep = STEP_QUIT_SAVE_MODE;
                                   QuitDeviceSaveStatusHandler();
                                   break;
      case READ_DEVICE_SAVE_BLK_CNT:ReadDeviceSaveBlkCntHandler();
                                    break;
      case READ_EVERY_SAVE_BLK_INFO:ReadEverySaveBlkInforHandler();
                                    break;
      case ENTER_INTO_READ_MODE:BleReadProgressStruct.ChangeDevStaStep = STEP_ENTER_INTO_READMODE;
                                EnterIntoReadmodeHandler();
                                break;
      case READ_EVERY_BLK_CONTENT:BeginReadBlkContentHandler();
                                  break;
      case ERASE_DEVICE_BLK_CONTENT:BleReadProgressStruct.ChangeDevStaStep = STEP_ERASE_BLK_CONTENT;
                                    EraseDeviceBlkContentHandler();
                                    break;
      case QUIT_READ_MODE:BleReadProgressStruct.ChangeDevStaStep = STEP_QUIT_READ_MODE;
                          QuitReadmodeHandler();
                          break;
      default:break;
   }
}

void CDeviceRecoverDetailedDlg::InvalidReadStepHandler(void)
{
   BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
   BleReadProgressStruct.ReadStep = INVALID_READ_STEP;

   //((CStatic *)GetDlgItem(IDC_USB_BULK_RECEIVE_STATUS))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::ReadDeviceSaveStatusHandler(void)
{
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVEBLK_CNT)))->SetWindowText(_T(""));
   DataProtocolHandler.GenerateGetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_STA)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::QuitDeviceSaveStatusHandler(void)
{
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                   CMD_QUIT_INTO_SAVE_MODE, 0, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_CUR_SAVE_STA)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::ReadDeviceSaveBlkCntHandler(void)
{
   DataProtocolHandler.GenerateGetSaveBlkCnt(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVEBLK_CNT)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::ReadEverySaveBlkInforHandler(void)
{
   if ( 0 == SignalSaveContentStruct.BlkCnt )
   {
      ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_UPLOAD_INFOR)))->SetWindowText(_T("no blk cnt!"));
      BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
      return;
   }
   CString str;
   str.Format(_T("Read infor:%d,%d"), BleReadProgressStruct.ReadBlkIndex + 1, SignalSaveContentStruct.BlkCnt);
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_FILE_STA)))->SetWindowText(str);
//----------------------------------
   DataProtocolHandler.GenerateGetEveryBlkInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                   BleReadProgressStruct.ReadBlkIndex,
                                                   BleReadProgressStruct.InternalIndex,
                                                   NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

}
void CDeviceRecoverDetailedDlg::EnterIntoReadmodeHandler(void)
{
   DataProtocolHandler.GenerateEnterReadMode(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::BeginReadBlkContentHandler(void)
{
   ActiveSingleReadBlkCmd(BleReadProgressStruct.ReadBlkIndex);
}
void CDeviceRecoverDetailedDlg::EraseDeviceBlkContentHandler(void)
{
   if (RESERVE_DATA_IN_READ_ALL_BLK == BleReadProgressStruct.AutoEraseDataFlag)
   {
      BleReadProgressStruct.ReadStep++;
      return;
   }
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                   CMD_ERASE_BLK_CONTENT,
                                                   0,
                                                   NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
void CDeviceRecoverDetailedDlg::QuitReadmodeHandler(void)
{
   DataProtocolHandler.GenerateQuitReadMode(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T(""));
}
//
//
void CDeviceRecoverDetailedDlg::UpdateUploadProgressAndCheck(void)
{
   if (READ_DEVICE_DATA_IDLE == BleReadProgressStruct.CmdIndex)
      return;

   if (FALSE == BleReadProgressStruct.UpdateUploadProgressFlag)
      return;

   BleReadProgressStruct.UpdateUploadProgressFlag = FALSE;
   if (READ_SINGLE_BLK_DATA == BleReadProgressStruct.CmdIndex)
   {
      UpdateProgress_ReadSingleBlkData();
   }
   else
   {
      UpdateProgress_ReadAllBlkData();
   }
}
void CDeviceRecoverDetailedDlg::UpdateProgress_ReadAllBlkData(void)
{
   CString str;
   int i;
   int rBlkIndex;

   rBlkIndex = BleReadProgressStruct.ReadBlkIndex;
   str.Format(_T("blk:%d:%d,page: %d / %d"), rBlkIndex,SignalSaveContentStruct.BlkCnt,
                                             BleReadProgressStruct.ReadPageIndex, BleReadProgressStruct.EndPageIndex);
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_PROGRESS_INFOR)))->SetWindowText(str);
   UpdateReadProgressControlBar();

   if (BleReadProgressStruct.ReadPageIndex != BleReadProgressStruct.EndPageIndex)
      return;

   BleReadProgressStruct.DataErrorFlag = 0;
   for (i = 0; i<BleReadProgressStruct.ReadAllPageCnt; i++)
   {
      if (DeviceStatusStruct.CheckTmp != SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[i].CheckTmp)
      {
         BleReadProgressStruct.DataErrorFlag = 1;
         break;
      }
   }
   if (1 == BleReadProgressStruct.DataErrorFlag)
   {
      //data receive error
      BleReadProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
      BleReadProgressStruct.RepeatSendCnt = 0;
      return;
   }
   //-------------------------------------------
   SaveAndChangeSignalFile(BleReadProgressStruct.ReadBlkIndex);
   BleReadProgressStruct.ReadBlkIndex++;
   BleReadProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   BleReadProgressStruct.RepeatSendCnt = 0;
   if (BleReadProgressStruct.ReadBlkIndex == SignalSaveContentStruct.BlkCnt)
   {
      MergeAllReadBlk();

      DisplayUpdateSpendTimeCnt();
      BleReadProgressStruct.ReadStep++;
   }
}
void CDeviceRecoverDetailedDlg::MergeAllReadBlk(void)
{
   if (0 == SignalSaveContentStruct.BlkCnt || 1 == SignalSaveContentStruct.BlkCnt)
      return;

   int mergeIndex;
   int index;
   unsigned char createFileFlag;
   unsigned int preStartTimeStamp, preEndTimeStamp;
   unsigned int curStartTimeStamp, curEndTimeStamp;
   SAVE_DATA_HEAD *headPt;

   CDataHandle dataHandle;

   createFileFlag = 0;
   //--------------------------------------------------------
   index = 0;
   mergeIndex = 0;
   for (index = 0; index<SignalSaveContentStruct.BlkCnt; index++)
   {
      ReadBlkSourceDataFile_DataContent(index);
      if (0 == MergeDataStruct.SourceTxtContent.DataLen)
      {
         continue;
      }
      //-----------------------------------------------------
      headPt = &SignalSaveContentStruct.BlkHead[index];
      if (0 == index)
      {
         MergeDataStruct.beginIndex = index;
         dataHandle.CopyDataForByte(MergeDataStruct.SaveHead.HeadBuffer, headPt->HeadBuffer, HEAD_BUFFER_LEN);
         preEndTimeStamp = headPt->DataHead.EndTimeStamp;
         createFileFlag = 1;
      }
      else
      {
         curStartTimeStamp = headPt->DataHead.StartTimeStamp;
         curEndTimeStamp = headPt->DataHead.EndTimeStamp;

         if ((curStartTimeStamp - preEndTimeStamp) < 2)
         {
            createFileFlag = 0;
            MergeDataStruct.SaveHead.DataHead.SaveDataLen += headPt->DataHead.SaveDataLen;  //20211110
         }
         else
         {
            MergeDataStruct.SaveHead.DataHead.EndTimeStamp = preEndTimeStamp;               //20211110
            SaveMergeDataHeadInfor(MergeDataStruct.beginIndex, mergeIndex, &MergeDataStruct.SaveHead);

            MergeDataStruct.beginIndex = index;
            dataHandle.CopyDataForByte(MergeDataStruct.SaveHead.HeadBuffer, headPt->HeadBuffer, HEAD_BUFFER_LEN);
            MergeDataStruct.SaveHead.DataHead.SaveDataLen = headPt->DataHead.SaveDataLen;
            createFileFlag = 1;
            mergeIndex++;
         }
         preStartTimeStamp = curStartTimeStamp;
         preEndTimeStamp = curEndTimeStamp;
      }
      SaveMergeDataFile(createFileFlag, index, mergeIndex);
      if ((SignalSaveContentStruct.BlkCnt - 1) == index)
      {
         MergeDataStruct.SaveHead.DataHead.EndTimeStamp = preEndTimeStamp;               //20211110
         SaveMergeDataHeadInfor(MergeDataStruct.beginIndex, mergeIndex, &MergeDataStruct.SaveHead);
         mergeIndex++;
      }
   }
}
void CDeviceRecoverDetailedDlg::ReadBlkSourceDataFile_DataContent(int blkIndex)
{
   CString sfileName;
   CFile rFile;
   int fileLen;

   //-----------------------------------------------------------
   // source bin
   sfileName = GetSaveBinFileName_OnSourceData(blkIndex);
   rFile.Open(sfileName, CFile::modeRead);

   fileLen = (int)(rFile.GetLength());
   MergeDataStruct.SourceBinContent.DataLen = fileLen;
   if (0 == fileLen)
      return;

   MergeDataStruct.SourceBinContent.DataContent = new unsigned char[fileLen];

   rFile.Read(MergeDataStruct.SourceBinContent.DataContent, fileLen);
   rFile.Close();
   //-----------------------------------------------------------
   // source txt
   sfileName = GetSaveTxtFileName_OnSourceData(blkIndex);
   rFile.Open(sfileName, CFile::modeRead);

   fileLen = (int)(rFile.GetLength());
   MergeDataStruct.SourceTxtContent.DataLen = fileLen;
   if (0 == fileLen)
      return;

   MergeDataStruct.SourceTxtContent.DataContent = new unsigned char[fileLen];

   rFile.Read(MergeDataStruct.SourceTxtContent.DataContent, fileLen);
   rFile.Close();
}
void CDeviceRecoverDetailedDlg::SaveMergeDataFile(unsigned char createFileFlag, int index, int mergeIndex)
{
   CString mergeFileName;
   CString str;

   if (1 == createFileFlag)
   {
      mergeFileName = DataSaveDirectoryStruct.Date_DirectoryStr[index];
      mergeFileName = mergeFileName + _T("\\");
      mergeFileName = mergeFileName + MERGE_DATA_FOLDER_STR;
      mergeFileName = mergeFileName + _T("\\");
      str.Format("_%02d", mergeIndex);

      MergeDataStruct.SourceBinFileName = mergeFileName + SIGNAL_PREFIX_VAL;
      MergeDataStruct.SourceBinFileName = MergeDataStruct.SourceBinFileName + MERGE_DATA_FOLDER_STR;
      MergeDataStruct.SourceBinFileName = MergeDataStruct.SourceBinFileName + str;
      MergeDataStruct.SourceBinFileName = MergeDataStruct.SourceBinFileName + BIN_SUFFIX_FILE_NAME;

      MergeDataStruct.SourceTxtFileName = mergeFileName + SIGNAL_PREFIX_VAL;
      MergeDataStruct.SourceTxtFileName = MergeDataStruct.SourceTxtFileName + MERGE_DATA_FOLDER_STR;
      MergeDataStruct.SourceTxtFileName = MergeDataStruct.SourceTxtFileName + str;
      MergeDataStruct.SourceTxtFileName = MergeDataStruct.SourceTxtFileName + TXT_SUFFIX_FILE_NAME;

      FileHandler.SaveDataOverwriteWrite(MergeDataStruct.SourceBinFileName,
      MergeDataStruct.SourceBinContent.DataContent,
      MergeDataStruct.SourceBinContent.DataLen);
      FileHandler.SaveDataOverwriteWrite(MergeDataStruct.SourceTxtFileName,
      MergeDataStruct.SourceTxtContent.DataContent,
      MergeDataStruct.SourceTxtContent.DataLen);
   }
   else
   {
      FileHandler.SaveDataAppendWrite(MergeDataStruct.SourceBinFileName,
      MergeDataStruct.SourceBinContent.DataContent,
      MergeDataStruct.SourceBinContent.DataLen);
      FileHandler.SaveDataAppendWrite(MergeDataStruct.SourceTxtFileName,
      MergeDataStruct.SourceTxtContent.DataContent,
      MergeDataStruct.SourceTxtContent.DataLen);
   }
   delete[] MergeDataStruct.SourceBinContent.DataContent;
   delete[] MergeDataStruct.SourceTxtContent.DataContent;
}
//
//
void CDeviceRecoverDetailedDlg::SaveMergeDataHeadInfor(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt)
{
   CString mergeFileName;
   CString fileName;
   CString str;
   mergeFileName = DataSaveDirectoryStruct.Date_DirectoryStr[beginIndex];
   mergeFileName = mergeFileName + _T("\\");
   mergeFileName = mergeFileName + MERGE_DATA_FOLDER_STR;
   mergeFileName = mergeFileName + _T("\\");

   mergeFileName = mergeFileName + SIGNAL_PREFIX_VAL;
   mergeFileName = mergeFileName + MERGE_DATA_FOLDER_STR;
   str.Format("_%02d", mergeIndex);
   mergeFileName = mergeFileName + str;
   mergeFileName = mergeFileName + FOLDER_NAMER_STR;

   fileName = mergeFileName + BIN_SUFFIX_FILE_NAME;
   FileHandler.SaveDataOverwriteWrite(fileName, headPt->HeadBuffer, HEAD_BUFFER_LEN);

   CString mergeDataInfor;

   fileName = mergeFileName + TXT_SUFFIX_FILE_NAME;

   mergeDataInfor.Empty();
   str.Format(_T("samplerate = %d,divideVal = %d"),
      headPt->DataHead.SampleRate,
      headPt->DataHead.DivideVal);

   mergeDataInfor = str;
   mergeDataInfor = mergeDataInfor + _T("\r\n");
   //----------------------------------------------   
   time_t timevalue;
   struct tm *mytm;

   timevalue = headPt->DataHead.StartTimeStamp;
   mytm = localtime(&timevalue);
   str.Format(_T("开始保存的时间 %04d-%02d-%02d,%02d:%02d:%02d\r\n"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday, mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
   mergeDataInfor = mergeDataInfor + str;

   timevalue = headPt->DataHead.EndTimeStamp;
   mytm = localtime(&timevalue);
   str.Format(_T("结束保存的时间 %04d-%02d-%02d,%02d:%02d:%02d\r\n"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday, mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
   mergeDataInfor = mergeDataInfor + str;

   str.Format(_T("data len = %d\r\n"), headPt->DataHead.SaveDataLen / 2);
   mergeDataInfor = mergeDataInfor + str;

   FileHandler.SaveDataOverwriteWrite(fileName, mergeDataInfor);
   //-----------------------------------------------------------------
   // change sample data,generate to baihui and bosheng
   ChangeData_DiffType(beginIndex, mergeIndex, headPt);
   //-----------------------------------------------------------------
}
void CDeviceRecoverDetailedDlg::ChangeData_DiffType(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt)
{
   CString sourceFileName;
   CString str;

   HALFWORD_TYPE *sourceData;

   sourceFileName = DataSaveDirectoryStruct.Date_DirectoryStr[beginIndex];
   sourceFileName = sourceFileName + _T("\\");
   sourceFileName = sourceFileName + MERGE_DATA_FOLDER_STR;
   sourceFileName = sourceFileName + _T("\\");
   str.Format("_%02d", mergeIndex);

   sourceFileName = sourceFileName + SIGNAL_PREFIX_VAL;
   sourceFileName = sourceFileName + MERGE_DATA_FOLDER_STR;
   sourceFileName = sourceFileName + str;
   sourceFileName = sourceFileName + BIN_SUFFIX_FILE_NAME;

   sourceData = new HALFWORD_TYPE[headPt->DataHead.SaveDataLen / 2];
   //-----------------------------------------------------------
   // source bin
   CFile rFile;
   int fileLen;
   rFile.Open(sourceFileName, CFile::modeRead);

   fileLen = (int)(rFile.GetLength());
   if (0 == fileLen)
      return;

   rFile.Read(&sourceData->DataByte[0], fileLen);
   rFile.Close();
   //---------------------------------------------------
   // 
   CDataHandle dataHandle;
   int sampleRate;
   int minValue;
   int interpolationCnt, outSampleCnt;
   int saveDataLen;
   CString fileName;

   SIGNAL_SAMPLERATE_CHANGE_STRUCT  singleSampleChangeStruct;

   saveDataLen = headPt->DataHead.SaveDataLen / 2;
   sampleRate = headPt->DataHead.SampleRate / headPt->DataHead.DivideVal;
   singleSampleChangeStruct.sourceBuf = sourceData;

   minValue = dataHandle.Lcm(sampleRate, BAIHUI_ECG_SIGNAL_SAMPLERATE);

   singleSampleChangeStruct.insertCnt = minValue / sampleRate;
   singleSampleChangeStruct.sampleCnt = minValue / BAIHUI_ECG_SIGNAL_SAMPLERATE;
   //-------------------------------------------------------------------------------
   // second: interpalation
   interpolationCnt = ((saveDataLen - 1) * singleSampleChangeStruct.insertCnt) + 1;
   singleSampleChangeStruct.MidBuf = new HALFWORD_TYPE[interpolationCnt];
   dataHandle.SignalInterpalationHandler(singleSampleChangeStruct.insertCnt,
                                         &singleSampleChangeStruct.MidBuf[0].DataShort,
                                         &singleSampleChangeStruct.sourceBuf[0].DataShort,
                                         saveDataLen);

   outSampleCnt = interpolationCnt / singleSampleChangeStruct.sampleCnt + 1;
   singleSampleChangeStruct.TargetBuf = new HALFWORD_TYPE[outSampleCnt];
   dataHandle.SignalSampleHandler(singleSampleChangeStruct.sampleCnt,
                                 &singleSampleChangeStruct.TargetBuf[0].DataShort,
                                 &singleSampleChangeStruct.MidBuf[0].DataShort,
                                  interpolationCnt);

   //-------------------------------------------------------------
   // save change date to baihui
   fileName = GetMergeDataFileName(beginIndex, mergeIndex, NO_TXT_FILE_SUFFIX_TIP, DATA_FILE_TYPE_BAIHUI);
   SaveSingleBlkContent_ForBaihui(fileName,&singleSampleChangeStruct.TargetBuf[0],outSampleCnt);
   //-------------------------------------------------------------
   delete[] sourceData;
   delete[] singleSampleChangeStruct.MidBuf;
   delete[] singleSampleChangeStruct.TargetBuf;
}
//
//
void CDeviceRecoverDetailedDlg::SaveSingleBlkContent_ForBaihui(CString fileName, HALFWORD_TYPE *buf, int len)
{
   int i;
   HALFWORD_TYPE *tmpBuf;
   float tmp;

   tmpBuf = new HALFWORD_TYPE[len];
   for (i = 0; i<len; i++)
   {
      tmp = (float)(buf[i].DataShort);
      tmp = tmp * BAIHUI_SCALE_VAL;
      tmp = tmp + BAIHUI_DC_OFFSET;
      tmpBuf[i].DataShort = (signed short)tmp;
   }
   FileHandler.SaveDataOverwriteWrite(fileName,tmpBuf[0].DataByte,len * 2);
   delete[] tmpBuf;
}
//
//
CString CDeviceRecoverDetailedDlg::GetMergeDataFileName(int beginIndex, int mergeIndex, int suffixIndex, int typeIndex)
{
   CString mergeFileName;
   CString fileName;
   CString str;
   mergeFileName = DataSaveDirectoryStruct.Date_DirectoryStr[beginIndex];
   mergeFileName = mergeFileName + _T("\\");
   mergeFileName = mergeFileName + MERGE_DATA_FOLDER_STR;
   mergeFileName = mergeFileName + _T("\\");

   if (0 == typeIndex)
   {
      mergeFileName = mergeFileName + BAIHUI_SIGNAL_PREDIX;
   }
   else
   {
      mergeFileName = mergeFileName + BOSHENG_SIGNAL_PREDIX;
   }

   mergeFileName = mergeFileName + MERGE_DATA_FOLDER_STR;
   str.Format("_%02d", mergeIndex);
   mergeFileName = mergeFileName + str;

   if (0 == suffixIndex)
   {
      fileName = mergeFileName + TXT_SUFFIX_FILE_NAME;
   }
   else
   {
      if (0 == typeIndex)
         fileName = mergeFileName + DAT_SUFFIX_FILE_NAME;
      else
         fileName = mergeFileName + BIN_SUFFIX_FILE_NAME;
   }
   return fileName;
}

void CDeviceRecoverDetailedDlg::UpdateProgress_ReadSingleBlkData(void)
{
   CString str;
   int i;
   int rBlkIndex;
   BOOL errorFlag;

   rBlkIndex = BleReadProgressStruct.ReadBlkIndex;
   str.Format(_T("page: %d / %d"), BleReadProgressStruct.ReadPageIndex, BleReadProgressStruct.EndPageIndex);
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_PROGRESS_INFOR)))->SetWindowText(str);

   if (BleReadProgressStruct.ReadPageIndex == BleReadProgressStruct.EndPageIndex)
   {
      DisplayUpdateSpendTimeCnt();

      errorFlag = FALSE;
      for (i = 0; i<BleReadProgressStruct.ReadAllPageCnt; i++)
      {
         if (DeviceStatusStruct.CheckTmp != SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[i].CheckTmp)
         {
            errorFlag = TRUE;
            //break;             //20220309
         }
      }
      if (TRUE == errorFlag)
      {
         AfxMessageBox(_T("invalid"));
      }
      else
      {
         SaveAndChangeSignalFile(rBlkIndex);
      }
      BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
   }
   UpdateReadProgressControlBar();
}
void CDeviceRecoverDetailedDlg::SaveAndChangeSignalFile(int rBlkIndex)
{
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_FILE_STA)))->SetWindowText(_T("正在保存单块数据..."));
   GetAllSignalContentBuf(rBlkIndex);
   SaveSingleBlkForBinFile(rBlkIndex);
   SaveSingleBlkForTxtFile(rBlkIndex);

   //SaveSingleBlkContent_ForChangeSamplerate(blkIndex);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_FILE_STA)))->SetWindowText(_T("保存数据完成..."));
}
void CDeviceRecoverDetailedDlg::GetAllSignalContentBuf(int blkIndex)
{
   int saveDataLen;
   int pageIndex, readPageCnt;
   PAGE_CONTENT_STRUCT *pagePt;
   unsigned char *resultBuffer;

   unsigned char *saveBuffer;
   int i, index;

   resultBuffer = new unsigned char[SAVE_DATA_COUNT];
   readPageCnt = SignalSaveContentStruct.BlkContent[blkIndex].readPageCnt;
   saveDataLen = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen;
   //----------------------------------
   // GET 2048 BYTE
   index = 0;
   saveBuffer = SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataByte;
   for (pageIndex = 0; pageIndex<readPageCnt; pageIndex++)
   {
      pagePt = &SignalSaveContentStruct.BlkContent[blkIndex].PageBuf[pageIndex];
      GetSinglePageContent(resultBuffer, pagePt);

      for (i = 0; i<SAVE_DATA_COUNT; i++)
      {
         saveBuffer[index++] = resultBuffer[i];
         if (index >= saveDataLen)
         {
            break;
         }
      }
   }
   delete[] resultBuffer;
}
void CDeviceRecoverDetailedDlg::GetSinglePageContent(unsigned char *resultPt, PAGE_CONTENT_STRUCT *pagePt)
{
   unsigned char *originalBuffer;
   //---------------------------------------------------------------
   // first
   originalBuffer = new unsigned char[ORIGINAL_DATA_COUNT];

   GetOriginalBuffer(originalBuffer, pagePt);
   //-------------------------------------------------
   // second 1.5 byte to 2 byte
   GetResultBuffer(originalBuffer, resultPt);
   //-------------------------------------------------
   delete[]originalBuffer;
}
void CDeviceRecoverDetailedDlg::GetOriginalBuffer(unsigned char *originalBuffer, PAGE_CONTENT_STRUCT *pagePt)
{
   int i, index, cnt, j;
   unsigned char *originalPt;
   unsigned char maxUploadBagInternalCnt;

   maxUploadBagInternalCnt = DeviceStatusStruct.MaxUploadBagInternalCnt;
   index = 0;
   for (i = 0; i<DeviceStatusStruct.MaxUploadBagInternalCnt; i++)   //20211102 for slow
   {
      originalPt = pagePt->DataInfor.LongBag[i];
      //-----------------
      if (i != (maxUploadBagInternalCnt - 1))
         cnt = DeviceStatusStruct.MidUploadBagDataLen;
      else
         cnt = DeviceStatusStruct.LastUploadBagDataLen;
      //---------------------
      for (j = 0; j<cnt; j++)
      {
         originalBuffer[index++] = originalPt[j];
      }
   }
}
void CDeviceRecoverDetailedDlg::GetResultBuffer(unsigned char *originalBuffer, unsigned char *resultPt)
{
   int i, index;
   unsigned char val1, val2, val3;
   WORD_TYPE value1, value2;

   index = 0;
   i = 0;
   do
   {
      val1 = originalBuffer[i];
      val2 = originalBuffer[i + 1];
      val3 = originalBuffer[i + 2];

      value1.DataUint = (val2 & 0xf0) * 16 + val1;
      value2.DataUint = (val2 & 0x0f) * 256 + val3;

      value1.DataInt = value1.DataInt - 0x800;
      //value1.DataUint = (value1.DataUint + 0x8000) & 0xFFFF;
      value2.DataInt = value2.DataInt - 0x800;
      //value2.DataUint = (value2.DataUint + 0x8000) & 0xFFFF;

      resultPt[index++] = (unsigned char)(value1.DataUint & 0xff);
      resultPt[index++] = (unsigned char)((value1.DataUint >> 8) & 0xff);
      resultPt[index++] = (unsigned char)(value2.DataUint & 0xff);
      resultPt[index++] = (unsigned char)((value2.DataUint >> 8) & 0xff);
      i = i + 3;

   } while (i<ORIGINAL_DATA_COUNT);
}
void CDeviceRecoverDetailedDlg::SaveSingleBlkForBinFile(int blkIndex)
{
   CString fileName;

   fileName = GetSaveBinFileName_OnSourceData(blkIndex);
   FileHandler.SaveDataOverwriteWrite(fileName,
                                      SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataByte,
                                      SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen);
}
//
//
void CDeviceRecoverDetailedDlg::SaveSingleBlkForTxtFile(int blkIndex)
{
   CString fileName;

   fileName = GetSaveTxtFileName_OnSourceData(blkIndex);
   FileHandler.SaveShortBufToTxt(fileName,
                                 &SignalSaveContentStruct.BlkContent[blkIndex].Buffer[0].DataShort,
                                  SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen / 2);
}
//
//
CString CDeviceRecoverDetailedDlg::CreateNewSaveRoute(CString route, CString newFileFolder)
{
   BOOL createDirectorySuccessFlag;
   CString str;

   createDirectorySuccessFlag = FileHandler.CreateNewFileFolder(route, newFileFolder);
   str.Empty();
   if (TRUE == createDirectorySuccessFlag)
   {
      str = route + _T("\\");
      str = str + newFileFolder;
   }
   return str;
}
//
//
CString CDeviceRecoverDetailedDlg::GetSaveInforFileName_OnSourceData(int blkIndex)
{
   CString fileName;

   fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
   fileName = fileName + _T("\\");
   fileName = fileName + SOURCE_DATA_FOLDER_STR;
   fileName = fileName + _T("\\");
   fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
   fileName = fileName + FOLDER_NAMER_STR;
   fileName = fileName + TXT_SUFFIX_FILE_NAME;

   return fileName;
}
//
//
CString CDeviceRecoverDetailedDlg::GetSaveTxtFileName_OnSourceData(int blkIndex)
{
   CString fileName;

   fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
   fileName = fileName + _T("\\");
   fileName = fileName + SOURCE_DATA_FOLDER_STR;
   fileName = fileName + _T("\\");
   fileName = fileName + SIGNAL_PREFIX_VAL;
   fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
   fileName = fileName + TXT_SUFFIX_FILE_NAME;

   return fileName;
}
//
//
CString CDeviceRecoverDetailedDlg::GetSaveBinFileName_OnSourceData(int blkIndex)
{
   CString fileName;

   fileName = DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex];
   fileName = fileName + _T("\\");
   fileName = fileName + SOURCE_DATA_FOLDER_STR;
   fileName = fileName + _T("\\");
   fileName = fileName + SignalSaveContentStruct.StartTimeStampStr[blkIndex];
   fileName = fileName + BIN_SUFFIX_FILE_NAME;

   return fileName;
}
//
//
void CDeviceRecoverDetailedDlg::DisplayUpdateSpendTimeCnt(void)
{
   CString str;
   int timeTick;

   timeTick = GetTickCount();
   timeTick = timeTick - BleReadProgressStruct.BeginTimeTick;
   str.Format(_T("传输完成,耗时:%d分钟%d秒"),timeTick / 60000, (timeTick % 60000 / 1000));
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_PROGRESS_INFOR)))->SetWindowText(str);
}
//
//
void CDeviceRecoverDetailedDlg::UpdateReadProgressControlBar(void)
{
   int progressValue;

   progressValue = (BleReadProgressStruct.ReadPageIndex - BleReadProgressStruct.StartPageIndex) * UploadRange;
   progressValue = progressValue / BleReadProgressStruct.ReadAllPageCnt;
   ((CProgressCtrl *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_PROGRESS)))->SetPos(progressValue);
}
//
//
void CDeviceRecoverDetailedDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;

   cmdtype = receivebag->Type;
   switch (cmdtype)
   {
//----------------------------------------------------------------------
      case CMD_CHANGE_STATUS_CMD:HandleChangeDeviceStatus(receivebag);
                                 break;
//---------------------------------------------------------------------
      case CMD_GET_DONGLE_POWER:HandleGetDevicePowerAndCapcityStaCmd(receivebag);
                                break;
      case CMD_GET_DONGLE_VERSION:HandleGetDongleVersionCmd(receivebag);
                                  break;
//---------------------------------------------------------------------
      case CMD_GET_SAVE_STATUS:HandleGetDeviceSaveStaCmd(receivebag);
                               break;
      case CMD_SET_SAVE_STATUS:HandleSetDeviceSaveStaCmd(receivebag);
                               break;
//---------------------------------------------------------------------
      case CMD_GET_ECG_BLK_CNT:HandleGetEcgBlkCnt(receivebag);
                               break;
      case CMD_GET_OR_WRITE_ECG_BLK_INFOR:HandleGetEcgBlkInfor(receivebag);
                                          break;
      case CMD_GET_ECG_BLK_CONTENT:if (TRUE == SignalSaveContentStruct.GetBufferFlag)
                                   {
                                      HandleGetEcgContentInfor(receivebag);
                                   }
                                   break;
//---------------------------------------------------------------------
      default:break;
   }
}
void CDeviceRecoverDetailedDlg::HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   if ((receivebag->Buffer[0] != (0x7F - receivebag->Buffer[1])))
   {
      return;
   }
   BleInteractiveStruct.RumMode = receivebag->Buffer[0];
   if ( 0x03 == BleInteractiveStruct.RumMode)
   {
      ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T("进入读取模式"));
   }
   else if (0x01 == BleInteractiveStruct.RumMode)
   {
      ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T("退出读取模式"));
   }
//---------------------------------------------
   if (STEP_FIRST_QUIT_READMODE == BleReadProgressStruct.ChangeDevStaStep)
   {
      CheckEnterIntoNextStep(FIRST_QUIT_READ_MODE);
   }
   else if (STEP_ENTER_INTO_READMODE  == BleReadProgressStruct.ChangeDevStaStep)
   {
      CheckEnterIntoNextStep(ENTER_INTO_READ_MODE);
   }
   else if (STEP_QUIT_READ_MODE  == BleReadProgressStruct.ChangeDevStaStep)
   {
      CheckEnterIntoNextStep(QUIT_READ_MODE);
   }
//---------------------------------------------
}
void CDeviceRecoverDetailedDlg::HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   CString showStr;

   str.Format(_T("电量:%d"), receivebag->Buffer[0]);
   str = str + _T("%  ");
   showStr = str;
   str.Format(_T("容量:%d"), receivebag->Buffer[1]);
   str = str + _T("%");
   showStr = showStr + str;
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_DEV_STATUS)))->SetWindowText(showStr);
}
//
//
void CDeviceRecoverDetailedDlg::HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int i, length;
   char tmp;
   CString str, VersionStr;

   length = receivebag->Buffer[0];
   VersionStr.Empty();
   for (i = 0; i<length; i++)
   {
      tmp = receivebag->Buffer[1 + i];
      str = tmp;
      VersionStr = VersionStr + str;
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_DEVICE_VER)))->SetWindowText(VersionStr);
}
void CDeviceRecoverDetailedDlg::HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;

   status = receivebag->Buffer[0];
   saveStr = _T("");
   if (0 == status)
   {
      saveStr = _T("非保存状态");
      BleReadProgressStruct.DeviceSaveSta = DEVICE_NOT_IN_SAVE_STATUS;
   }
   else
   {
      saveStr = _T("保存状态,");
      BleReadProgressStruct.DeviceSaveSta = DEVICE_IN_SAVE_STATUS;
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_STA)))->SetWindowText(saveStr);

   CheckEnterIntoNextStep(READ_DEVICE_SAVE_STATUS);
}
void CDeviceRecoverDetailedDlg::HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char status;
   CString saveStr;
   status = receivebag->Buffer[0];
   if (2 == status)
   {
      saveStr = _T("退出保存模式.");
      return;
   }
   else 
   {
      saveStr = _T("退出保存模式失败.");
   }
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_CUR_SAVE_STA)))->SetWindowText(saveStr);

   if(STEP_QUIT_SAVE_MODE == BleReadProgressStruct.ChangeDevStaStep )
   { 
      CheckEnterIntoNextStep(QUIT_DEVICE_SAVE_STATUS);
   }
   else if(STEP_ERASE_BLK_CONTENT  == BleReadProgressStruct.ChangeDevStaStep)
   {
      CheckEnterIntoNextStep(ERASE_DEVICE_BLK_CONTENT);
   }
}
void CDeviceRecoverDetailedDlg::HandleGetEcgBlkCnt(COMMUNICATE_BAG_STRUCT *receivebag)
{
   CString str;
   unsigned short ecgblkCnt;

   ecgblkCnt = receivebag->Buffer[1];
   ecgblkCnt = (ecgblkCnt * 128) + (receivebag->Buffer[0] & 0x7F);

   str.Format(_T("BlkCnt = %d"),ecgblkCnt);
   SignalSaveContentStruct.BlkCnt = ecgblkCnt;
   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVEBLK_CNT)))->SetWindowText(str);

   CheckEnterIntoNextStep(READ_DEVICE_SAVE_BLK_CNT);
}
void CDeviceRecoverDetailedDlg::HandleGetEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int i;
   int blkIndex;
   int internalIndex;
   unsigned char *ecgBlkHeadPt;
   unsigned char *ecgBlkInforPt;

   CString blkInforStr, str;

   blkIndex = receivebag->Buffer[1] * 128 + receivebag->Buffer[0];  //ecg blk index
   internalIndex = receivebag->Buffer[2] & 0x0F;

   if (0 == internalIndex)
   {
      ecgBlkHeadPt = SignalSaveContentStruct.BlkHead[blkIndex].HeadBuffer;
      for (i = 0; i<EVERY_BAG_LEN; i++)
      {
         ecgBlkHeadPt[i] = receivebag->Buffer[3 + i];
      }
      ApplyMemoryForSaveContent(blkIndex);
      GetHeadStrInfor(blkIndex);
   }
   else if (1 == internalIndex)
   {
      ecgBlkInforPt = SignalSaveContentStruct.BlkInfor[blkIndex].Buffer;
      for (i = 0; i<EVERY_BAG_LEN; i++)
      {
         ecgBlkInforPt[i] = receivebag->Buffer[3 + i];
      }
      GetEveryBlkInfor(blkIndex);
   }
   DisplaySignalSaveBlkInfo(blkIndex);

   CheckEnterIntoNextStep(READ_EVERY_SAVE_BLK_INFO);
}
//
//
void CDeviceRecoverDetailedDlg::ApplyMemoryForSaveContent(int blkIndex)
{
   unsigned int saveDataLen;
   unsigned int readPageCnt;

   SignalSaveContentStruct.GetBufferFlag = TRUE;
   saveDataLen = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.SaveDataLen;

   readPageCnt = SignalSaveContentStruct.BlkHead[blkIndex].DataHead.EndPageIndex;
   readPageCnt = readPageCnt - SignalSaveContentStruct.BlkHead[blkIndex].DataHead.BeginPageIndex + 1;
   SignalSaveContentStruct.BlkContent[blkIndex].readPageCnt = readPageCnt;
   //---------------------------
   if (FALSE == SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag)
   {
      SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag = TRUE;

      SignalSaveContentStruct.BlkContent[blkIndex].Buffer = new HALFWORD_TYPE[saveDataLen / 2];
      SignalSaveContentStruct.BlkContent[blkIndex].PageBuf = new PAGE_CONTENT_STRUCT[readPageCnt];
   }
   else
   {
      delete[] SignalSaveContentStruct.BlkContent[blkIndex].PageBuf;
      delete[] SignalSaveContentStruct.BlkContent[blkIndex].Buffer;

      SignalSaveContentStruct.BlkContent[blkIndex].InitialFlag = TRUE;
      SignalSaveContentStruct.BlkContent[blkIndex].Buffer = new HALFWORD_TYPE[saveDataLen / 2];
      SignalSaveContentStruct.BlkContent[blkIndex].PageBuf = new PAGE_CONTENT_STRUCT[readPageCnt];
   }
}
void CDeviceRecoverDetailedDlg::DisplaySignalSaveBlkInfo(int blkIndex)
{
   CString str;
//------------------------------------------------------------------------------------
// first create id file folder
   str = CreateNewSaveRoute(FileHandler.CurModuleRoute,SignalSaveContentStruct.IdStr[blkIndex]);
   if (0 == str.IsEmpty())
   {
      DataSaveDirectoryStruct.IdDirectoryStr = str;
   }
   else
   {
      AfxMessageBox(_T("创建文件夹失败!"));
      return;
   }
//------------------------------------------------------------------------------------
// second create date file folder
   str = CreateNewSaveRoute(DataSaveDirectoryStruct.IdDirectoryStr,SignalSaveContentStruct.RecordedDateStr[blkIndex]);
   if (0 == str.IsEmpty())
   {
      DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex] = str;
   }
   else
   {
      AfxMessageBox(_T("创建文件夹失败!"));
      return;
   }
//--------------------------------------------------
// fourth create source data file folder
   str = CreateNewSaveRoute(DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex],SOURCE_DATA_FOLDER_STR);
   if (0 != str.IsEmpty())
   {
      AfxMessageBox(_T("创建文件夹失败!"));
      return;
   }

//-----------------------------------------------
// fifth eighth merge data file folder
   if (READ_ALL_BLK_DATA == BleReadProgressStruct.CmdIndex)
   {
      str = CreateNewSaveRoute(DataSaveDirectoryStruct.Date_DirectoryStr[blkIndex],MERGE_DATA_FOLDER_STR);
      if (0 != str.IsEmpty())
      {
         AfxMessageBox(_T("创建文件夹失败!"));
         return;
      }
   }
//-------------------------------------------
   CString fileName;
   fileName = GetSaveInforFileName_OnSourceData(blkIndex);
   FileHandler.SaveDataOverwriteWrite(fileName,SignalSaveContentStruct.BlkHeadStr[blkIndex]);
//--------------------------------------------------
}
//
//
void CDeviceRecoverDetailedDlg::GetHeadStrInfor(int blkIndex)
{
   int i;
   SAVE_DATA_HEAD *saveDataHeadPt;
   unsigned short actualSampleRate;
   CString blkInforStr, str;

   saveDataHeadPt = &SignalSaveContentStruct.BlkHead[blkIndex];
   blkInforStr.Empty();
   if (0 == saveDataHeadPt->DataHead.DivideVal)
   {
      actualSampleRate = saveDataHeadPt->DataHead.SampleRate;
   }
   else if (0xFF == saveDataHeadPt->DataHead.DivideVal)
   {
      actualSampleRate = saveDataHeadPt->DataHead.SampleRate;
   }
   else
   {
      actualSampleRate = saveDataHeadPt->DataHead.SampleRate / saveDataHeadPt->DataHead.DivideVal;
   }
   str.Format(_T("samplerate = %d,divideVal = %d,actualSamplerate = %d,LeadCnt = %d,"), saveDataHeadPt->DataHead.SampleRate,
      saveDataHeadPt->DataHead.DivideVal,
      actualSampleRate,
      saveDataHeadPt->DataHead.LeadCnt);
   blkInforStr = blkInforStr + str;

   if (DONGLE_CHOOSE_ECG == saveDataHeadPt->DataHead.LeadType)
      str = _T("ECG\n");
   else if (DONGLE_CHOOSE_EMG == saveDataHeadPt->DataHead.LeadType)
      str = _T("EMG\n");
   else if (DONGLE_CHOOSE_EEG == saveDataHeadPt->DataHead.LeadType)
      str = _T("EEG\n");
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   str.Format(_T("Mac ADDR = %02X,%02X,%02X,%02X,%02X,%02X,%02X,%02X\r\n"), saveDataHeadPt->DataHead.MacAddr[0],
      saveDataHeadPt->DataHead.MacAddr[1],
      saveDataHeadPt->DataHead.MacAddr[2],
      saveDataHeadPt->DataHead.MacAddr[3],
      saveDataHeadPt->DataHead.MacAddr[4],
      saveDataHeadPt->DataHead.MacAddr[5],
      saveDataHeadPt->DataHead.MacAddr[6],
      saveDataHeadPt->DataHead.MacAddr[7]);
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   time_t timevalue;
   struct tm *mytm;
   timevalue = saveDataHeadPt->DataHead.StartTimeStamp;
   mytm = localtime(&timevalue);
   blkInforStr = blkInforStr + _T("start time stamp = 0x");
   SignalSaveContentStruct.StartTimeStampStr[blkIndex].Empty();
   str.Format(_T("%08X"), saveDataHeadPt->DataHead.StartTimeStamp);   //20211102
   SignalSaveContentStruct.StartTimeStampStr[blkIndex] = str;
   blkInforStr = blkInforStr + str + _T("\r\n");
   blkInforStr = blkInforStr + _T("开始保存的时间 ");

   str.Format(_T("%04d-%02d-%02d"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday);
   SignalSaveContentStruct.RecordedDateStr[blkIndex] = str;
   blkInforStr = blkInforStr + str;

   str.Format(_T(",%02d:%02d:%02d\r\n"), mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   timevalue = saveDataHeadPt->DataHead.EndTimeStamp;
   mytm = localtime(&timevalue);
   str.Format(_T("end time stamp = 0x%08X\r\n"), saveDataHeadPt->DataHead.EndTimeStamp);
   blkInforStr = blkInforStr + str;
   str.Format(_T("结束保存的时间 %04d-%02d-%02d,%02d:%02d:%02d\r\n"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday, mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   str.Format(_T("save data len = %d,data len = %d\r\n"), saveDataHeadPt->DataHead.SaveDataLen, saveDataHeadPt->DataHead.SaveDataLen / 2);
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   str.Format(_T("ID num = "));
   blkInforStr = blkInforStr + str;
   SignalSaveContentStruct.IdStr[blkIndex].Empty();
   for (i = 0; i < saveDataHeadPt->DataHead.IDNumberLen; i++)
   {
      str = (char)(saveDataHeadPt->DataHead.IdNumberBuf[i]);
      blkInforStr = blkInforStr + str;
      SignalSaveContentStruct.IdStr[blkIndex] = SignalSaveContentStruct.IdStr[blkIndex] + str;
   }
   str = _T(",");
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   str.Format(_T("user ID = "));
   blkInforStr = blkInforStr + str;
   for (i = 0; i < saveDataHeadPt->DataHead.UserIdLen; i++)
   {
      str = (char)(saveDataHeadPt->DataHead.UserIDStrBuf[i]);
      blkInforStr = blkInforStr + str;
   }
   str.Format(_T("\n"));
   blkInforStr = blkInforStr + str;
   //--------------------------------------------------
   str.Format(_T("保存唯一标识 = "));
   blkInforStr = blkInforStr + str;
   for (i = 0; i < saveDataHeadPt->DataHead.SaveDataOnlyFlagLen; i++)
   {
      str = (char)(saveDataHeadPt->DataHead.SaveDataOnlyFlagBuf[i]);
      blkInforStr = blkInforStr + str;
   }
   str.Format(_T("\n"));
   blkInforStr = blkInforStr + str;
   //------------------------------------------------
   str.Format(_T("数据属性 = 0x%02X:"), saveDataHeadPt->DataHead.EndBlkValue);
   blkInforStr = blkInforStr + str;
   if (0x00 == saveDataHeadPt->DataHead.EndBlkValue)
   {
      str.Format(_T("中间数据\n"));
   }
   else if (0x55 == saveDataHeadPt->DataHead.EndBlkValue)
   {
      str.Format(_T("结尾数据\n"));
   }
   blkInforStr = blkInforStr + str;
   SignalSaveContentStruct.BlkHeadStr[blkIndex] = blkInforStr;

   if(READ_SINGLE_BLK_DATA == BleReadProgressStruct.CmdIndex )
   {
      AfxMessageBox(SignalSaveContentStruct.BlkHeadStr[blkIndex]);
   }
//--------------------------------------------------------------------------------------
}
void CDeviceRecoverDetailedDlg::GetEveryBlkInfor(int blkIndex)
{
   CString nameStr, ageStr, genderStr, weightStr, heightStr, phoneNoStr;
   char *nameBuf, *showNameBuf;
   char tmp;
   int i;
   CString str;
   CDataHandle dataHandle;
   //---------------------------
   nameStr.Empty();
   nameBuf = new char[USER_NAME_STR_LEN];
   memset(nameBuf, 0, USER_NAME_STR_LEN);
   for (i = 0; i < USER_NAME_STR_LEN; i++)
   {
      nameBuf[i] = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.Name[i];
   }
   showNameBuf = dataHandle.Utf8_To_GB2312(nameBuf);
   i = 0;
   nameStr.Empty();
   while (1)
   {
      if (i > USER_NAME_STR_LEN)
         break;

      if ('\0' == showNameBuf[i])
         break;

      str = showNameBuf[i];
      nameStr = nameStr + str;
      i++;
   }
   delete[] showNameBuf;
   delete[] nameBuf;
   ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_NAME_STR))->SetWindowText(nameStr);
//-------------------------------------------------------------------------------
   phoneNoStr.Empty();
   for (i = 0; i < USER_PHONE_NUMBER_STR_LEN; i++)
   {
      tmp = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.PhoneNo[i];
      if ('\0' == tmp)
         break;
      str = tmp;
      phoneNoStr = phoneNoStr + str;
   }
   ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_PHONE_NO))->SetWindowText(phoneNoStr);
//-------------------------------------------------------------------------------
   ageStr.Empty();
   for (i = 0; i < USER_AGE_STR_LEN; i++)
   {
      tmp = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.Age[i];
      if ('\0' == tmp)
         break;
      str = tmp;
      ageStr = ageStr + str;
   }
   ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_AGE))->SetWindowText(ageStr);
//-------------------------------------------------------------------------------
   heightStr.Empty();
   for (i = 0; i < USER_HEIGHT_STR_LEN; i++)
   {
      tmp = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.Height[i];
      if ('\0' == tmp)
         break;
      str = tmp;
      heightStr = heightStr + str;
   }
   ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_HEIGHT))->SetWindowText(heightStr);
//-------------------------------------------------------------------------------
   weightStr.Empty();
   for (i = 0; i < USER_WEIGHT_STR_LEN; i++)
   {
      tmp = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.Weight[i];
      if ('\0' == tmp)
         break;
      str = tmp;
      weightStr = weightStr + str;
   }
   ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_WEIGHT))->SetWindowText(weightStr);
//-------------------------------------------------------------------------------
   unsigned char gender;
   gender = SignalSaveContentStruct.BlkInfor[blkIndex].Content.UserInfor.Detailed.Gender[0];
   if ('0' == gender)
      ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_GENDER))->SetWindowText(_T("女"));
   else if ('1' == gender)
      ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_GENDER))->SetWindowText(_T("男"));
   else if ('2' == gender)
      ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_GENDER))->SetWindowText(_T("其他"));
   else
      ((CStatic *)GetDlgItem(IDC_DETAILED_RECOVER_GENDER))->SetWindowText(_T(""));
}
//
//
void CDeviceRecoverDetailedDlg::HandleGetEcgContentInfor(COMMUNICATE_BAG_STRUCT *receivebag)
{
   int pageIndex, savePageIndex;
   int internalIndex;
   int i;
   unsigned short chckTmp;
   int rBlkIndex;

   pageIndex = receivebag->Buffer[0];
   pageIndex = pageIndex + (receivebag->Buffer[1] << 7);
   pageIndex = pageIndex + (receivebag->Buffer[2] << 14);

   rBlkIndex = SignalSaveContentStruct.rBlkIndex;

   SignalSaveContentStruct.readPageIndex = pageIndex;
   savePageIndex = pageIndex - SignalSaveContentStruct.BlkHead[rBlkIndex].DataHead.BeginPageIndex;
   SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex = pageIndex;

   internalIndex = receivebag->Buffer[3];

   chckTmp = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp;
   chckTmp = chckTmp | (1 << internalIndex);
   SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].CheckTmp = chckTmp;
   //if ((0x02 == internalIndex) && (0x5FA == savePageIndex))
   //{
//	   AfxMessageBox(_T("Test"));
//   }
   //-------------------------------------------
   unsigned char *targetPt;
   targetPt = SignalSaveContentStruct.BlkContent[rBlkIndex].PageBuf[savePageIndex].DataInfor.LongBag[internalIndex];
   for (i = 0; i<DeviceStatusStruct.MidUploadBagDataLen; i++)
   {
      targetPt[i] = receivebag->Buffer[5 + i];
   }
   BleReadProgressStruct.ReadPageIndex = SignalSaveContentStruct.BlkContent[rBlkIndex].PageIndex;
   if ((DeviceStatusStruct.MaxUploadBagInternalCnt - 1) != internalIndex)
   {
      return;
   }
   BleReadProgressStruct.UpdateUploadProgressFlag = TRUE;

   CheckEnterIntoNextStep(READ_EVERY_BLK_CONTENT);
}
//
//
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetDevVerBtn()
{
   DataProtocolHandler.GenerateGetDongleVerCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_DEVICE_VER)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetSaveSta()
{
   DataProtocolHandler.GenerateGetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVE_STA)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverQuitSaveStatus()
{
   DataProtocolHandler.GenerateSetDongleSaveStaCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
                                                   CMD_QUIT_INTO_SAVE_MODE, 0, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_CUR_SAVE_STA)))->SetWindowText(_T(""));
}

void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetSaveblkCnt()
{
   DataProtocolHandler.GenerateGetSaveBlkCnt(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_SAVEBLK_CNT)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverEnterReadMode()
{
   DataProtocolHandler.GenerateEnterReadMode(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T(""));
}
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverQuitReadMode()
{
   DataProtocolHandler.GenerateQuitReadMode(TYPE_COMMUNICATE_WITHOUT_MACADDR, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);

   ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_READMODE_TIP)))->SetWindowText(_T(""));
}

void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetBlkInfor()
{
   unsigned char ecgBlkIndex;
   unsigned char internalIndex;
   internalIndex = ((CComboBox *)(GetDlgItem(IDC_DETAILED_RECOVER_BLK_INTERNAL_INDEX)))->GetCurSel();
   if( (FALSE == GetHandleBlkIndex(&ecgBlkIndex)) || (internalIndex >= MAX_BLK_INTERNAL_INDEX) )
   {
      AfxMessageBox(_T("未获取到有效的块索引"));
      return;
   }
   DataProtocolHandler.GenerateGetEveryBlkInforCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, ecgBlkIndex, internalIndex, NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//
//
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverGetBlkContent()
{
   unsigned char blkIndex;
   if (FALSE == GetHandleBlkIndex(&blkIndex))
   {
      AfxMessageBox(_T("未获取到有效的块索引"));
      return;
   }
   BleReadProgressStruct.BeginTimeTick = GetTickCount();
   BleReadProgressStruct.CmdIndex = READ_SINGLE_BLK_DATA;
   ActiveSingleReadBlkCmd(blkIndex);
}
void CDeviceRecoverDetailedDlg::ActiveSingleReadBlkCmd(int blkIndex)
{
   SAVE_DATA_HEAD *saveDataHeadPt;
   int i;
   unsigned char startCmd;

   startCmd = START_TRANSMIT_CMD | ACK_LONG_BAG_FLAG;

   SignalSaveContentStruct.rBlkIndex = blkIndex;
   BleReadProgressStruct.ReadBlkIndex = blkIndex;
   //---------------------------------
   saveDataHeadPt = &SignalSaveContentStruct.BlkHead[blkIndex];
   BleReadProgressStruct.StartPageIndex = saveDataHeadPt->DataHead.BeginPageIndex;
   BleReadProgressStruct.EndPageIndex = saveDataHeadPt->DataHead.EndPageIndex;
   //SignalSaveContentStruct.BlkHead[blkIndex].DataHead.BeginPageIndex = 0x5F31;
   //BleReadProgressStruct.StartPageIndex = 0x5F31;
   //BleReadProgressStruct.EndPageIndex = 0x5F33;// saveDataHeadPt->DataHead.EndPageIndex;
   BleReadProgressStruct.ReadPageIndex = BleReadProgressStruct.StartPageIndex;
   BleReadProgressStruct.ReadAllPageCnt = BleReadProgressStruct.EndPageIndex - BleReadProgressStruct.StartPageIndex + 1;

   for (i = 0; i<BleReadProgressStruct.ReadAllPageCnt; i++)
   {
      SignalSaveContentStruct.BlkContent[blkIndex].PageBuf[i].CheckTmp = 0;
   }
   DataProtocolHandler.GenerateGetEveryBlkContentCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, 
                                                     startCmd,
                                                     BleReadProgressStruct.StartPageIndex,
                                                     BleReadProgressStruct.EndPageIndex,
                                                     NULL);
   SendDataToLowerMachine(DataProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                          DataProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//
//
BOOL CDeviceRecoverDetailedDlg::GetHandleBlkIndex(unsigned char *blkIndex)
{
   CString ecgBlkIndexStr;
   int length;
   int ecgBlkIndex;
   int ecgBlkCnt;
   int i;
   unsigned char tmp;

   ((CEdit *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_BLK_INDEX)))->GetWindowText(ecgBlkIndexStr);  //20191217
   ecgBlkIndex = 0;

   length = ecgBlkIndexStr.GetLength();

   if ((length>2) || (0 == length))
   {
      ((CEdit *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_BLK_INDEX)))->SetWindowText(_T(""));  //20191217
      return FALSE;
   }
   for (i = 0; i<ecgBlkIndexStr.GetLength(); i++)
   {
      tmp = ecgBlkIndexStr[i] - '0';
      ecgBlkIndex = ecgBlkIndex * 10 + tmp;
   }
   ecgBlkCnt = SignalSaveContentStruct.BlkCnt;
   if ((0 == ecgBlkCnt) || (ecgBlkIndex >= ecgBlkCnt))
   {
      ((CEdit *)(GetDlgItem(IDC_DETAILED_RECOVER_READ_BLK_INDEX)))->SetWindowText(_T(""));  //20191217
      return FALSE;
   }
   *blkIndex = ecgBlkIndex;
   return TRUE;
}
void CDeviceRecoverDetailedDlg::OnBnClickedAutoEraseFlag()
{
   int value;
   value = ((CButton *)(GetDlgItem(IDC_AUTO_ERASE_FLAG)))->GetCheck();
   BleReadProgressStruct.AutoEraseDataFlag = value;
}
void CDeviceRecoverDetailedDlg::OnBnClickedDetailedRecoverReadAllContent()
{
   BleReadProgressStruct.CmdIndex = READ_ALL_BLK_DATA;
   BleReadProgressStruct.ReadStep = FIRST_QUIT_READ_MODE;
   BleReadProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   BleReadProgressStruct.RepeatSendCnt = 0;

   BleReadProgressStruct.AckBagLenVal = ACK_LONG_BAG_FLAG;
   BleReadProgressStruct.BeginTimeTick = GetTickCount();
}
//
// 
void CDeviceRecoverDetailedDlg::CheckRepeatSendCmdHandler(void)
{
   BleReadProgressStruct.DirectStatus = WAIT_ACK_CMD_STEP;
   BleReadProgressStruct.WaitAckTimeCnt = 0;
   BleReadProgressStruct.RepeatSendCnt++;
   if (BleReadProgressStruct.RepeatSendCnt > REPEART_SEND_CNT)
   {
      BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
      BleReadProgressStruct.ReadStep = INVALID_READ_STEP;
      ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_UPLOAD_INFOR)))->SetWindowText(_T("超时退出,读取状态"));
   }
   else
   {
      CString str;
      str.Format(_T("%d:repeat cnt = %d"), BleReadProgressStruct.ReadStep, BleReadProgressStruct.RepeatSendCnt);
      ((CStatic *)(GetDlgItem(IDC_DETAILED_RECOVER_UPLOAD_INFOR)))->SetWindowText(str);
   }
}
//
//
void CDeviceRecoverDetailedDlg::CheckWaitAckCmdHandler(int waitAckTimeCnt)
{
   BleReadProgressStruct.WaitAckTimeCnt++;
   if (BleReadProgressStruct.WaitAckTimeCnt > waitAckTimeCnt)
   {
      BleReadProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
   }
}
//
//
void CDeviceRecoverDetailedDlg::CheckEnterIntoNextStep(int step)
{
   if (READ_ALL_BLK_DATA != BleReadProgressStruct.CmdIndex)
      return;

   if (step == BleReadProgressStruct.ReadStep)
   {
      if (READ_EVERY_BLK_CONTENT != BleReadProgressStruct.ReadStep)
      {
         BleReadProgressStruct.DirectStatus = SEND_BLE_CMD_STEP;
         BleReadProgressStruct.RepeatSendCnt = 0;
      }
   }
   else
   {
      BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
      BleReadProgressStruct.ReadStep = INVALID_READ_STEP;
   }
   //-----------------------------------------------
   switch (step)
   {
      case INVALID_READ_STEP:break;
//--------------------------------------
      case FIRST_QUIT_READ_MODE:BleReadProgressStruct.ReadStep++;
                                break;
      case READ_DEVICE_SAVE_STATUS:if (DEVICE_IN_SAVE_STATUS == BleReadProgressStruct.DeviceSaveSta)
                                      BleReadProgressStruct.ReadStep++;
                                   else
                                      BleReadProgressStruct.ReadStep += 2;
                                   break;
      case QUIT_DEVICE_SAVE_STATUS:BleReadProgressStruct.ReadStep++;
                                   break;
//--------------------------------------
      case READ_DEVICE_SAVE_BLK_CNT:BleReadProgressStruct.ReadStep++;
                                    BleReadProgressStruct.ReadBlkIndex = 0;
                                    BleReadProgressStruct.InternalIndex = 0;
                                    break;
      case READ_EVERY_SAVE_BLK_INFO:BleReadProgressStruct.InternalIndex++;
                                    if(2 == BleReadProgressStruct.InternalIndex)
                                    { 
                                       BleReadProgressStruct.ReadBlkIndex++;
                                       BleReadProgressStruct.InternalIndex = 0;
                                       if (BleReadProgressStruct.ReadBlkIndex == SignalSaveContentStruct.BlkCnt)
                                       {
                                          BleReadProgressStruct.ReadStep++;
                                       }
                                    }
                                    break;
      case ENTER_INTO_READ_MODE:BleReadProgressStruct.ReadStep++;
                                BleReadProgressStruct.ReadBlkIndex = 0;
                                break;
      case READ_EVERY_BLK_CONTENT:BleReadProgressStruct.WaitAckTimeCnt = 0;
                                  break;
      case ERASE_DEVICE_BLK_CONTENT:BleReadProgressStruct.ReadStep++;
                                    BleReadProgressStruct.ReadBlkIndex = 0;
                                    break;
      case QUIT_READ_MODE:BleReadProgressStruct.CmdIndex = READ_DEVICE_DATA_IDLE;
                          BleReadProgressStruct.ChangeDevStaStep = INVALID_READ_STEP;
                          break;
      default:break;
   }
}
//============================================================
//
//
void CDeviceRecoverDetailedDlg::SendDataToLowerMachine(unsigned char *buffer, unsigned short length)
{
   if (NULL == ParentPt)
      return;

   unsigned type;
   CEcgSecreeningSystemDlg *dlgPt;
   dlgPt = (CEcgSecreeningSystemDlg *)(ParentPt);

   type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
   type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
   type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
   dlgPt->SendBuffer(type, buffer, length);
}


