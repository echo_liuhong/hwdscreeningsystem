#pragma once
#include "ProjDefine.h"

#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "FileHandler.h"

// CDeviceConfigDlg 对话框

class CDeviceConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CDeviceConfigDlg)
public:
	BOOL CompletePageFlag;

	int UploadRange;
	void *ParentPt;

	CDataProtocolHandler DataProtocolHandler;
	SIGNAL_SAVE_CONTENT_STRUCT SignalSaveContentStruct;
	DATA_SAVE_DIRECTORY_STRUCT DataSaveDirectoryStruct;
	CFileHandler FileHandler;
	BLE_READ_PROGRESS_STRUCT BleReadProgressStruct;
	BLE_DEVICE_STATUS_STRUCT DeviceStatusStruct;

	void InitialDataSaveDirectoryStruct(void);
	void InitialBleReadProgressStruct(void);
	void ConfigureDiffBagLenCommunicate(unsigned char bagLenFlag);
//----------------------------------------------------------
	BOOL GetHandleBlkIndex(unsigned char *blkIndex);

	void InitialAllControl(void);
	void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleSendTimeStampCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleDevSelfCheckingCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetEcgBlkCnt(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetEcgContentInfor(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetCompressSaveContent(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetCompleteSaveContent(COMMUNICATE_BAG_STRUCT *receivebag);

	void ApplyMemoryForSaveContent(int blkIndex);
	void GetHeadStrInfor(int blkIndex);
	void DisplaySignalSaveBlkInfo(int blkIndex);
	CString CreateNewSaveRoute(CString route, CString newFileFolder);
	CString GetSaveInforFileName_OnSourceData(int blkIndex);
	CString GetSaveTxtFileName_OnSourceData(int blkIndex);
	CString GetSaveBinFileName_OnSourceData(int blkIndex);
	void SaveDetailedInfor(CString fileName,int bagSaveIndex, EVERY_SEND_BAG_STRUCT *detailInfor);

	void ActiveSingleReadBlkCmd(int blkIndex);

	void UpdateUploadProgressAndCheck(void);
	void UpdateProgress_ReadSingleBlkData(void);
	void DisplayUpdateSpendTimeCnt(void);
	void UpdateReadProgressControlBar(void);

	void SaveEverySendBagInfor(int rBlkIndex);

	//CString GetSaveDataFileName_OnSourceData(int blkIndex, int saveIndex, CString startTimeStr);

	//void SaveDetailedInfor(CString fileName, int cmdIndex, EVERY_SEND_BAG_STRUCT *detailInfor);

	void SaveAndChangeSignalFile(int rBlkIndex);
	void GetAllSignalContentBuf(int blkIndex);
	void GetSinglePageContent(unsigned char *resultPt, PAGE_CONTENT_STRUCT *pagePt);
	void GetOriginalBuffer(unsigned char *originalBuffer, PAGE_CONTENT_STRUCT *pagePt);
	void GetResultBuffer(unsigned char *originalBuffer, unsigned char *resultPt);
	void SaveSingleBlkForBinFile(int blkIndex);
	void SaveSingleBlkForTxtFile(int blkIndex);
//-------------------------------------------------
	void HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetDongleVersionCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetDongleMacAddrCmd(COMMUNICATE_BAG_STRUCT *receivebag);
	void HandleGetDeviceInfo(COMMUNICATE_BAG_STRUCT *receivebag);

	void SendDataToLowerMachine(unsigned char *buffer, unsigned short length);

	CDeviceConfigDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDeviceConfigDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEVICE_CONFIG_DLG };
#endif

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSendTimeStamp();
	afx_msg void OnBnClickedGetTimeStamp();
	afx_msg void OnBnClickedSetRunStatus();
	afx_msg void OnBnClickedGetRunStatus();
	afx_msg void OnBnClickedGetDevVersion();
	afx_msg void OnBnClickedGetDevMacaddr();
	afx_msg void OnBnClickedGetDevIdnum();
	afx_msg void OnBnClickedSetSaveStatus();
	afx_msg void OnBnClickedGetSaveStatus();
	afx_msg void OnBnClickedGetDevSaveBlkCnt();
	afx_msg void OnBnClickedGetSaveblkInfor();
	afx_msg void OnBnClickedGetSaveblkContent();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedDevSelfCheck();
	afx_msg void OnBnClickedCompletePageContent();
};
