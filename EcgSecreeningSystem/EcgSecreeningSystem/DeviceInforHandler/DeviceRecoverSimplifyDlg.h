#pragma once

#include "ProjDefine.h"

#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "FileHandler.h"
#include "ExcelHandler.h"

// CDeviceRecoverSimplifyDlg 对话框
class CDeviceRecoverSimplifyDlg : public CDialog
{
	DECLARE_DYNAMIC(CDeviceRecoverSimplifyDlg)
public:
   void *ParentPt;
   int UploadRange;
   CDataProtocolHandler DataProtocolHandler;
   BLE_INTERACTIVE_STRUCT BleInteractiveStruct;
   BLE_READ_PROGRESS_STRUCT BleReadProgressStruct;

   BLE_DEVICE_STATUS_STRUCT DeviceStatusStruct;
   SIGNAL_SAVE_CONTENT_STRUCT SignalSaveContentStruct;       //Include ecg,emg

   DATA_SAVE_DIRECTORY_STRUCT DataSaveDirectoryStruct;
   CFileHandler FileHandler;
   MERGE_DATA_STRUCT MergeDataStruct;
//----------------------------------------------------------
   DEVICE_USER_RELATION_INFOR DeviceUserRelationInfor;
   CExcelHandler ExcelHandle;

   void GetSingleBlkStartAndEndTime(void);
   void ClearDeviceUserRelationInfor();
   void AddNewItemForActiveDevice();
   void CreateNewActiveList(CString fileName);
   void AddNewActiveInfor(CString fileName);
   void WriteUserInforToExcelFile(int nRowCnt);
//---------------------------------------------------
   void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleChangeDeviceStatus(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDevicePowerAndCapcityStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceInfo(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleSetDeviceSaveStaCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetEcgBlkCnt(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetEcgBlkInfor(COMMUNICATE_BAG_STRUCT *receivebag);
   void ApplyMemoryForSaveContent(int blkIndex);
   void DisplaySignalSaveBlkInfo(int blkIndex);
   void GetHeadStrInfor(int blkIndex);
   void GetEveryBlkInfor(int blkIndex);
   void HandleGetEcgContentInfor(COMMUNICATE_BAG_STRUCT *receivebag);
//------------------------------------------------------
   void ReadDataProgressHandler(void);
   void InvalidReadStepHandler(void);
   void BleDiffReadStepHandler(int readStep);
   void CheckRepeatSendCmdHandler(void);
   void CheckWaitAckCmdHandler(int waitAckTimeCnt);
   
   void UpdateUploadProgressAndCheck(void);
   void UpdateProgress_ReadAllBlkData(void);
   void DisplayUpdateSpendTimeCnt(void);
   void UpdateReadProgressControlBar(void);
   void SaveAndChangeSignalFile(int rBlkIndex);
   void GetAllSignalContentBuf(int blkIndex);
   void GetSinglePageContent(unsigned char *resultPt, PAGE_CONTENT_STRUCT *pagePt);
   void GetOriginalBuffer(unsigned char *originalBuffer, PAGE_CONTENT_STRUCT *pagePt);
   void GetResultBuffer(unsigned char *originalBuffer, unsigned char *resultPt);

   void SaveSingleBlkForBinFile(int blkIndex);
   void SaveSingleBlkForTxtFile(int blkIndex);

   void MergeAllReadBlk(void);
   void ReadBlkSourceDataFile_DataContent(int blkIndex);
   void SaveMergeDataHeadInfor(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt);
   void ChangeData_DiffType(int beginIndex, int mergeIndex, SAVE_DATA_HEAD *headPt);
   void SaveMergeDataFile(unsigned char createFileFlag, int index, int mergeIndex);
   CString GetMergeDataFileName(int beginIndex, int mergeIndex, int suffixIndex, int typeIndex);
   void SaveSingleBlkContent_ForBaihui(CString fileName, HALFWORD_TYPE *buf, int len);
//---------------------------------------------------
   void CheckEnterIntoNextStep(int step);
   void QuitReadmodeHandler(void);
   void ReadDeviceSaveStatusHandler(void);
   void QuitDeviceSaveStatusHandler(void);
   void ReadDeviceSaveBlkCntHandler(void);
   void ReadEverySaveBlkInforHandler(void);
   void EnterIntoReadmodeHandler(void);
   void BeginReadBlkContentHandler(void);
   void EraseDeviceBlkContentHandler(void);

   CString CreateNewSaveRoute(CString route, CString newFileFolder);
   CString GetSaveDataFileName_OnSourceData(int blkIndex, int saveIndex);
//---------------------------------------------------
   void SendDataToLowerMachine(unsigned char *buffer, unsigned short length);

   void InitialAllControl(void);
   void InitialDataSaveDirectoryStruct(void);
   void InitialBleReadProgressStruct(void);
   void ConfigureDiffBagLenCommunicate(unsigned char bagLenFlag);
//---------------------------------------------------
   CDeviceRecoverSimplifyDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDeviceRecoverSimplifyDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEVICE_RECOVER_SIMPLIFY_DLG };
#endif

protected:

   BOOL PreTranslateMessage(MSG* pMsg);
   BOOL HandleKeyPressDown(MSG *pMsg);

   virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnBnClickedSimplifyRecoverGetDevIdBtn();
   afx_msg void OnBnClickedSimplifyDataRecover();
   afx_msg void OnTimer(UINT_PTR nIDEvent);
};
