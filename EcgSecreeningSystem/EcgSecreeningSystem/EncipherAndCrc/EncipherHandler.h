//#include "windows.h"

#ifndef __ENCIPHER_HANDLER_H_H__
#define __ENCIPHER_HANDLER_H_H__

#include "stdint.h"

#define ECCIPHER_CHECK_CNT                         10
//----------------------------------------------------
#define EVERY_ENCIPHER_CNT_BYTE                    16
#define COMMUNICATE_INTERACTIVE_BUF_LEN            (EVERY_ENCIPHER_CNT_BYTE * 4)
class CEncipherHandler
{
public:
	unsigned char CheckCnt;

	static const unsigned char DefaultEncipherKey[EVERY_ENCIPHER_CNT_BYTE];
	unsigned char CommunicateEncipherKey[EVERY_ENCIPHER_CNT_BYTE];
	unsigned char InteractiveBuffer[COMMUNICATE_INTERACTIVE_BUF_LEN];
	unsigned char FixSerialNumBuf[EVERY_ENCIPHER_CNT_BYTE];
//------------------------------
	void UseDefaultCommunicateEncipherKey(void);
	void SetCommunicateEncipherKey(unsigned char *keyTab);
	void SetRandomCommunicateEncipherKey(void);

	void GetEncipherSerialNumAddr(void);           //32
	void CheckBulkSerialNumValidtyCmd(void);

	void GetFixSerialNumBuffer(unsigned char *buffer);
	void GetSingleUint32_tData(uint32_t keyValue, uint8_t *keyTab);
private:
	unsigned char ByteToAscii(unsigned char changedata);
	unsigned char AsciiToByte(unsigned char changedata);

protected:
public:
	CEncipherHandler();
	virtual ~CEncipherHandler();
};
#endif //__ENCIPHER_HANDLER_H_H__