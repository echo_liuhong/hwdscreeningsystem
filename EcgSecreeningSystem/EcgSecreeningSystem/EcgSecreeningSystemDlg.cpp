
// EcgSecreeningSystemDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "EcgSecreeningSystemDlg.h"
#include "afxdialogex.h"

#include "AboutDlg.h"
#include "DlgControlPos.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
//-------------------------------------------------------
#define DIALOG_CLOSE_TIMECNT                         200
#define PROGRAM_START_TIME_CNT                       1
//-------------------------------------------------------
#define PROGRAM_VERSION_STR                          _T("EcgHandleSystem Ver:1.00.02")

#define AUTO_OPEN_USB_DEVICE_TIMER_INDEX             1
#define AUTO_OPEN_USB_DEVICE_TIMER_INTERVAL          250    //UNIT:ms
#define BLE_INFOR_FLUSH_INTERVAL_CNT                 4
//-------------------------------------------------------------
#define SCAN_BLE_DEVICE_INFOR_CMD_INDEX              0x01
#define CONNECT_OR_DISCONNECT_DEVICE_CMD_INDEX       0x02
#define DATA_TRANSPARENT_CMD_INDEX                   0x03
#define ACTIVE_USB_BULK_CMD_INDEX                    0x04

#define BLE_MODULE_NON_EXISTENT_TIP_STR              _T("未找到插件")
#define BLE_MODULE_EXISTENT_TIP_STR                  _T("插件已连接")
//----------------------------------------------------------------------
#define MAX_SEARCH_BLE_DEVICE_CNT                    16
//-------------------------------
#define MAX_SHOWEDIT_LIMITTEXTCNT                    100000     
//----------------------------------------------------------------------
#define RAPID_SCREEN_SIMPLIFY_STR                    _T("筛查精简版")
#define DEVICE_RECOVER_SIMPLIFY_STR                  _T("回收精简版")
#define RAPID_SCREEN_DELTAILED_STR                   _T("筛查详细版")
#define DEVICE_RECOVER_DETAILED_STR                  _T("回收详细版")
#define DEVICE_CONFIGURE_DETAILED_STR                _T("设备配置")
#define DEFAULT_CHOOSE_DLG_INDEX                     RAPID_SCREEN_SIMPLIFY_INDEX

#define ECG_DISPLAY_STR                              _T("心电波形显示")
#define DEFAULT_DISPLAY_INFOR_DLG_INDEX              ECG_DISPLAY_INDEX
//----------------------------------------------------------------------
// CEcgSecreeningSystemDlg 对话框
CEcgSecreeningSystemDlg::CEcgSecreeningSystemDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ECGSECREENINGSYSTEM_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	InitialBulkReceiveContentStruct();
}
void CEcgSecreeningSystemDlg::InitialBulkReceiveContentStruct(void)
{
	BulkShowContent.HexShowFlag = FALSE;
	BulkShowContent.DisplayStatusFlag = FALSE; //20220309
	BulkShowContent.Cnt = 0;

	BulkShowContent.ProgramStartTimeCnt = PROGRAM_START_TIME_CNT;
	BulkShowContent.FirstConnectHandleStep = INVALID_CONNECT_STEP;
	BulkShowContent.FlushIntervalCnt = BLE_INFOR_FLUSH_INTERVAL_CNT;
}

void CEcgSecreeningSystemDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CEcgSecreeningSystemDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_DEVICECHANGE, OnDeviceChange)
    ON_NOTIFY(TCN_SELCHANGE, IDC_CHANGE_DEVICE_HANDLER_TYPE, &CEcgSecreeningSystemDlg::OnTcnSelchangeChangeDeviceHandleType)
    ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CONNECT_BLE_DEVICE, &CEcgSecreeningSystemDlg::OnBnClickedConnectBleDevice)
	ON_BN_CLICKED(IDC_DISCONNECT_ECG_DEVICE, &CEcgSecreeningSystemDlg::OnBnClickedDisconnectEcgDevice)
	ON_BN_CLICKED(IDC_CLEAR_BTN, &CEcgSecreeningSystemDlg::OnBnClickedClearBtn)
	ON_BN_CLICKED(IDC_PAUSE_BTN, &CEcgSecreeningSystemDlg::OnBnClickedPauseBtn)
	ON_BN_CLICKED(IDC_HEXSHOWFLAG, &CEcgSecreeningSystemDlg::OnBnClickedHexshowflag)
END_MESSAGE_MAP()


// CEcgSecreeningSystemDlg 消息处理程序
BOOL CEcgSecreeningSystemDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// 将“关于...”菜单项添加到系统菜单中。
	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	InitialAllControl();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}
//
//
void CEcgSecreeningSystemDlg::InitialAllControl(void)
{
   int i;
   CString str;

   InitialUsbBulkDriverPart();
   InitialDlgWindowInfo();
   InitialTabControl();
   InitialEcgAndUserTabControl();
   MakeCrc32Table();
   SetTimer(AUTO_OPEN_USB_DEVICE_TIMER_INDEX, AUTO_OPEN_USB_DEVICE_TIMER_INTERVAL, NULL);

   for (i = 0; i<MAX_SEARCH_BLE_DEVICE_CNT; i++)
   {
	  str.Format(_T("%02d"), i);
	  ((CComboBox *)(GetDlgItem(IDC_CONNECT_DEVICE_INDEX)))->AddString(str);
   }
   ((CComboBox *)(GetDlgItem(IDC_CONNECT_DEVICE_INDEX)))->SetCurSel(0);
   //--------------------------------------------
   ((CStatic *)(GetDlgItem(IDC_USB_BULK_OPEN_STATUS)))->SetWindowText(BLE_MODULE_NON_EXISTENT_TIP_STR);
   ((CEdit *)(GetDlgItem(IDC_RECEIVE_CONTENT)))->SetLimitText(MAX_SHOWEDIT_LIMITTEXTCNT);

   ((CStatic *)(GetDlgItem(IDC_PROGRAM_VER)))->SetWindowText(PROGRAM_VERSION_STR);
}
//
//
void CEcgSecreeningSystemDlg::InitialUsbBulkDriverPart()
{
   UsbBulkControl.MPData = this;                    // 用于处理usb bulk命令的函数
   UsbBulkControl.FunctionBack = UsbBulkDataProc;

   UsbBulkControl.UsbBulkDeviceInfor.OpenDeviceFlag = TRUE;

   UsbBulkControl.UsbBulkDeviceInfor.MyVid = USB_ID_VENDOR;
   UsbBulkControl.UsbBulkDeviceInfor.MyPid = USB_ID_PRODUCT;
   UsbBulkControl.UsbBulkDeviceInfor.MyPvn = USB_ID_PVN;
   UsbBulkControl.InitialUsbBulkRWThread();
}
void CEcgSecreeningSystemDlg::InitialDlgWindowInfo(void)
{
   CRect rt;
   int height;
   SystemParametersInfo(SPI_GETWORKAREA, 0, &rt, 0);                // get work area size 

   ShowWindow(SW_SHOWMAXIMIZED);
   GetClientRect(&ViewWindowInfor.myWindowRect);
   //GetWindowRect(&ShowRect);

   ((CStatic *)(GetDlgItem(IDC_USB_BULK_OPEN_STATUS)))->MoveWindow(USB_BULK_OPEN_STATUS_X_POS,
                                                                   USB_BULK_OPEN_STATUS_X_POS,
                                                                   USB_BULK_OPEN_STATUS_WIDTH,
                                                                   USB_BULK_OPEN_STATUS_HEIGHT);
//--------------------------------
   ((CEdit *)(GetDlgItem(IDC_RECEIVE_CONTENT)))->MoveWindow(RECEIVE_CONTENT_X_POS,
                                                            RECEIVE_CONTENT_Y_POS,
                                                            RECEIVE_CONTENT_WIDTH,
                                                            RECEIVE_CONTENT_HEIGHT);

//--------------------------------
   ((CButton *)(GetDlgItem(IDC_CONNECT_BLE_DEVICE)))->MoveWindow(CONNECT_BLE_DEVICE_X_POS,
                                                                 CONNECT_BLE_DEVICE_Y_POS,
                                                                 CONNECT_BLE_DEVICE_WIDTH,
                                                                 CONNECT_BLE_DEVICE_HEIGHT);
//--------------------------------
   ((CButton *)(GetDlgItem(IDC_DISCONNECT_ECG_DEVICE)))->MoveWindow(DISCONNECT_BLE_DEVICE_X_POS,
                                                                    DISCONNECT_BLE_DEVICE_Y_POS,
                                                                    DISCONNECT_BLE_DEVICE_WIDTH,
                                                                    DISCONNECT_BLE_DEVICE_HEIGHT);
//--------------------------------
   ((CComboBox *)(GetDlgItem(IDC_CONNECT_DEVICE_INDEX)))->MoveWindow(CONNECT_BLE_DEVICE_INDEX_X_POS,
                                                                     CONNECT_BLE_DEVICE_INDEX_Y_POS,
                                                                     CONNECT_BLE_DEVICE_INDEX_WIDTH,
                                                                     CONNECT_BLE_DEVICE_INDEX_HEIGHT);
//--------------------------------
   ((CButton *)(GetDlgItem(IDC_CLEAR_BTN)))->MoveWindow(CLEAR_BTN_X_POS, CLEAR_BTN_Y_POS, CLEAR_BTN_WIDTH, CLEAR_BTN_HEIGHT);
   ((CButton *)(GetDlgItem(IDC_PAUSE_BTN)))->MoveWindow(PAUSE_BTN_X_POS, PAUSE_BTN_Y_POS, PAUSE_BTN_WIDTH, PAUSE_BTN_HEIGHT);
   ((CButton *)(GetDlgItem(IDC_HEXSHOWFLAG)))->MoveWindow(HEXSHOW_BTN_X_POS, HEXSHOW_BTN_Y_POS, HEXSHOW_BTN_WIDTH, HEXSHOW_BTN_HEIGHT);
//---------------------------------
   height = rt.bottom - TABCTRL_Y_POS - 50;
   ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_UPDATE_DEVICE)))->MoveWindow(TABCTRL_X_POS,TABCTRL_Y_POS,TABCTRL_WIDTH,height);

//---------------------------------
   CRect rect;
   rect.left = TABCTRL_RIGHT_POS + SHOW_TABCTRL_X_OFFSET;
   rect.right = ViewWindowInfor.myWindowRect.right - SHOW_TABCTRL_X_OFFSET;

   rect.top = RECEIVE_CONTENT_Y_POS;
   rect.bottom = ViewWindowInfor.myWindowRect.bottom - SHOW_TABCTRL_Y_BOTTOM_OFFSET;
   ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_SHOW_CONTENT)))->MoveWindow(&rect);
}
//
//
void CEcgSecreeningSystemDlg::InitialTabControl(void)
{
   CTabCtrl *tabCtrl;
   int i;

   CTabCtrlParameter.RapidScreenSimplifyDlg = new CRapidScreenSimplifyDlg;
   CTabCtrlParameter.DeviceRecoverSimplifyDlg = new CDeviceRecoverSimplifyDlg;
   CTabCtrlParameter.RapidScreenDetailedDlg = new CRapidScreenDetailedDlg;
   CTabCtrlParameter.DeviceRecoverDetailedDlg = new CDeviceRecoverDetailedDlg;
   CTabCtrlParameter.DeviceConfigDlg = new CDeviceConfigDlg;

   CTabCtrlParameter.RapidScreenSimplifyDlg->ParentPt = this;
   CTabCtrlParameter.DeviceRecoverSimplifyDlg->ParentPt = this;
   CTabCtrlParameter.RapidScreenDetailedDlg->ParentPt = this;
   CTabCtrlParameter.DeviceRecoverDetailedDlg->ParentPt = this;
   CTabCtrlParameter.DeviceConfigDlg->ParentPt = this;

   CTabCtrlParameter.InsertItemCnt = MAX_INSERT_DLG_COUNT;

   CTabCtrlParameter.TabNameStr = new CString[MAX_INSERT_DLG_COUNT];
   CTabCtrlParameter.DlgId = new int[MAX_INSERT_DLG_COUNT];

   CTabCtrlParameter.TabNameStr[0] = RAPID_SCREEN_SIMPLIFY_STR;
   CTabCtrlParameter.TabNameStr[1] = DEVICE_RECOVER_SIMPLIFY_STR;
   CTabCtrlParameter.TabNameStr[2] = RAPID_SCREEN_DELTAILED_STR;
   CTabCtrlParameter.TabNameStr[3] = DEVICE_RECOVER_DETAILED_STR;
   CTabCtrlParameter.TabNameStr[4] = DEVICE_CONFIGURE_DETAILED_STR;

   CTabCtrlParameter.DlgPt[0] = CTabCtrlParameter.RapidScreenSimplifyDlg;
   CTabCtrlParameter.DlgPt[1] = CTabCtrlParameter.DeviceRecoverSimplifyDlg;
   CTabCtrlParameter.DlgPt[2] = CTabCtrlParameter.RapidScreenDetailedDlg;
   CTabCtrlParameter.DlgPt[3] = CTabCtrlParameter.DeviceRecoverDetailedDlg;
   CTabCtrlParameter.DlgPt[4] = CTabCtrlParameter.DeviceConfigDlg;

   CTabCtrlParameter.DlgId[0] = IDD_RAPID_SCREEN_SIMPLIFY_DLG;
   CTabCtrlParameter.DlgId[1] = IDD_DEVICE_RECOVER_SIMPLIFY_DLG;
   CTabCtrlParameter.DlgId[2] = IDD_RAPID_SCREEN_DETAILED_DLG;
   CTabCtrlParameter.DlgId[3] = IDD_DEVICE_RECOVER_DETAILED_DLG;
   CTabCtrlParameter.DlgId[4] = IDD_DEVICE_CONFIG_DLG;

   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_UPDATE_DEVICE)));
   for (i = 0; i<CTabCtrlParameter.InsertItemCnt; i++)
   {
      tabCtrl->InsertItem(i, CTabCtrlParameter.TabNameStr[i]);
      CTabCtrlParameter.DlgPt[i]->Create(CTabCtrlParameter.DlgId[i], tabCtrl);
      CTabCtrlParameter.DlgPt[i]->ShowWindow(SW_HIDE);
   }
   CTabCtrlParameter.SelectIndex = DEFAULT_CHOOSE_DLG_INDEX;
   tabCtrl->SetCurSel(CTabCtrlParameter.SelectIndex);

   ChangeTabControlShow();
   CTabCtrlParameter.DlgPt[CTabCtrlParameter.SelectIndex]->ShowWindow(SW_SHOW);
}
//
//
void CEcgSecreeningSystemDlg::ChangeTabControlShow(void)
{
   CTabCtrl *tabCtrl;
   CRect rect;
   CRect moveRect;

   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_UPDATE_DEVICE)));
   tabCtrl->GetClientRect(&rect);
   tabCtrl->GetItemRect(0,moveRect);
   //------------------------------------------------------
   rect.top += moveRect.bottom + 2;
   rect.bottom -= 2;
   rect.left += 2;
   rect.right -= 3;
   //------------------------------------------------------
   CTabCtrlParameter.RapidScreenSimplifyDlg->MoveWindow(&rect);
   CTabCtrlParameter.DeviceRecoverSimplifyDlg->MoveWindow(&rect);
   CTabCtrlParameter.RapidScreenDetailedDlg->MoveWindow(&rect);
   CTabCtrlParameter.DeviceRecoverDetailedDlg->MoveWindow(&rect);
   CTabCtrlParameter.DeviceConfigDlg->MoveWindow(&rect);
   //------------------------------------------------------
}

void CEcgSecreeningSystemDlg::InitialEcgAndUserTabControl(void)
{
   CTabCtrl *tabCtrl;
   int i;
   CTabCtrlParameter.EcgShowDlg = new CEcgShowDlg;
   CTabCtrlParameter.EcgShowDlg->ParentPt = this;

   CTabCtrlParameter.ShowInsertItemCnt = MAX_INSERT_SHOW_DLG_COUNT;

   CTabCtrlParameter.ShowTabNameStr = new CString[MAX_INSERT_SHOW_DLG_COUNT];
   CTabCtrlParameter.ShowDlgId = new int[MAX_INSERT_SHOW_DLG_COUNT];

   CTabCtrlParameter.ShowTabNameStr[0] = ECG_DISPLAY_STR;

   CTabCtrlParameter.ShowDlgPt[0] = CTabCtrlParameter.EcgShowDlg;

   CTabCtrlParameter.ShowDlgId[0] = IDD_ECG_SHOW_DLG;

   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_SHOW_CONTENT)));
   for (i = 0; i <CTabCtrlParameter.ShowInsertItemCnt; i++)
   {
      tabCtrl->InsertItem(i, CTabCtrlParameter.ShowTabNameStr[i]);
      CTabCtrlParameter.ShowDlgPt[i]->Create(CTabCtrlParameter.ShowDlgId[i], tabCtrl);
      CTabCtrlParameter.ShowDlgPt[i]->ShowWindow(SW_HIDE);
   }
   CTabCtrlParameter.ShowSelectIndex = DEFAULT_DISPLAY_INFOR_DLG_INDEX;
   tabCtrl->SetCurSel(CTabCtrlParameter.ShowSelectIndex);

   ChangeEcgAndUserTabControlShow();
   CTabCtrlParameter.ShowDlgPt[CTabCtrlParameter.ShowSelectIndex]->ShowWindow(SW_SHOW);
}
//
//
void CEcgSecreeningSystemDlg::ChangeEcgAndUserTabControlShow(void)
{
   CTabCtrl *tabCtrl;
   CRect rect;

   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_SHOW_CONTENT)));
   tabCtrl->GetClientRect(&rect);
   //------------------------------------------------------
   rect.top += 25;
   rect.bottom -= 2;
   rect.left += 2;
   rect.right -= 3;
   //------------------------------------------------------
   CTabCtrlParameter.EcgShowDlg->MoveWindow(&rect);
   CTabCtrlParameter.EcgShowDlg->InitialDlgArea();    //Here call ecgshow dlg function 20220107
}
//
//
//
void CEcgSecreeningSystemDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}
// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。
void CEcgSecreeningSystemDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
      //DrawHandler.DrawSignalWave();
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CEcgSecreeningSystemDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
//
//
LRESULT CEcgSecreeningSystemDlg::OnDeviceChange(WPARAM nEventType, LPARAM dwData)
{
	if (FALSE == UsbBulkControl.UsbBulkDeviceInfor.MyDevFound)
	{
		UsbBulkControl.UsbBulkDeviceInfor.OpenDeviceFlag = TRUE;
		return TRUE;
	}
	//-------------------------------------------
	UsbBulkControl.ScanAllUsbBulkDevice();
	if (0 == UsbBulkControl.UsbBulkDeviceInfor.DeviceNum)
	{
		UsbBulkControl.CloseRelativeUsbBulkDev();
		if (FALSE == UsbBulkControl.UsbBulkDeviceInfor.MyDevFound)
		{
			((CStatic *)GetDlgItem(IDC_USB_BULK_OPEN_STATUS))->SetWindowText(BLE_MODULE_NON_EXISTENT_TIP_STR);
		}
	}
	return TRUE;
}
//
// shield esc and enter key
BOOL CEcgSecreeningSystemDlg::PreTranslateMessage(MSG* pMsg)
{
	if (TRUE == HandleKeyPressDown(pMsg))
		return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}
//
// shield esc and enter
//
BOOL CEcgSecreeningSystemDlg::HandleKeyPressDown(MSG *pMsg)
{
	if ((WM_KEYFIRST <= pMsg->message) && (pMsg->message <= WM_KEYLAST))
	{
		if ((SHIELD_KEYBOARD_KEY1 == pMsg->wParam) || (SHIELD_KEYBOARD_KEY2 == pMsg->wParam))
			return TRUE;
	}
	return FALSE;
}
void CEcgSecreeningSystemDlg::OnTimer(UINT_PTR nIDEvent)
{
	if( AUTO_OPEN_USB_DEVICE_TIMER_INDEX == nIDEvent )
	{
		if( 0 == BulkShowContent.ProgramStartTimeCnt )
           PollingOpenUsbBulkDevice();
		else
           BulkShowContent.ProgramStartTimeCnt--;

		ConnectCommunicateHandler();
	}
	CDialogEx::OnTimer(nIDEvent);
}
//
//
void CEcgSecreeningSystemDlg::ConnectCommunicateHandler(void)
{
	if (READ_USB_BULK_DATA_MODE == BulkShowContent.FirstConnectHandleStep)
	{
		//GetBleDevInforHandler();
		//if ( NO_CONNECT_DEVICE == DeviceListStruct.ScanBleDevInfo.BleDevConnectStaus )
		//{
		//	 DisplayBleScanInfor();
		//}
		return;
	}
	//---------------------------------
	if (WRITE_READ_FIX_ADDR == BulkShowContent.FirstConnectHandleStep)
	{
		if( 0 == EncipherHandler.CheckCnt )
			return;

		EncipherHandler.CheckCnt--;
		GetUsbBulkFixSerialNumHandler();
	}
	else if (CHECK_BULK_SERIAL_NUMBER == BulkShowContent.FirstConnectHandleStep)
	{
		if( 0 == EncipherHandler.CheckCnt )
			return;

		EncipherHandler.CheckCnt--;
		CheckUsbBulkFixSerialNumHandler();
	}
	else if (ACTIVE_USB_BULK_HANDLE == BulkShowContent.FirstConnectHandleStep)
	{
		BulkShowContent.activeVal = ACTIVE_USB_BULK_READ;
		ActiveUsbBulkDevReadHandler();
	}
}
//
//
void CEcgSecreeningSystemDlg::GetUsbBulkFixSerialNumHandler(void)
{
	EncipherHandler.GetEncipherSerialNumAddr();
	ProtocolHandler.GenerateGetBulkFixSerialNumCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
		                                           EncipherHandler.InteractiveBuffer,
		                                           NULL);
	SendBufToUsbBulkDev();
}
//
//
void CEcgSecreeningSystemDlg::CheckUsbBulkFixSerialNumHandler(void)
{
	EncipherHandler.CheckBulkSerialNumValidtyCmd();
	ProtocolHandler.GenerateCheckBulkFixSerialNumValidityCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR,
	                                                         EncipherHandler.InteractiveBuffer,
		                                                     NULL);

	SendBufToUsbBulkDev();
}
//
//
void CEcgSecreeningSystemDlg::ActiveUsbBulkDevReadHandler(void)
{
	ProtocolHandler.GenerateWithBulkInteractiveCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, ACTIVE_USB_BULK_READ, NULL);
	SendBufToUsbBulkDev();
}
//
//
void CEcgSecreeningSystemDlg::InActiveUsbBulkDevReadHandler(void)
{
	ProtocolHandler.GenerateWithBulkInteractiveCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, INACTIVE_USB_BULK_READ, NULL);
	SendBufToUsbBulkDev();
}
//
//
void CEcgSecreeningSystemDlg::PollingOpenUsbBulkDevice(void)
{
	CString str;
	if (FALSE == UsbBulkControl.UsbBulkDeviceInfor.OpenDeviceFlag)
		return;

	UsbBulkControl.UsbBulkDeviceInfor.OpenDeviceFlag = FALSE;
	//------------------------
	if( TRUE == UsbBulkControl.UsbBulkDeviceInfor.MyDevFound )
		return;

	UsbBulkControl.OpenRelativeUsbBulkDevice();
	if( FALSE == UsbBulkControl.UsbBulkDeviceInfor.MyDevFound )
		return;

    if( INVALID_CONNECT_STEP == BulkShowContent.FirstConnectHandleStep )
	{
		BulkShowContent.FirstConnectHandleStep = WRITE_READ_FIX_ADDR;
	}
	((CStatic *)GetDlgItem(IDC_USB_BULK_OPEN_STATUS))->SetWindowText(BLE_MODULE_EXISTENT_TIP_STR);
}
//----------------------------------------
// DataProc
void __stdcall UsbBulkDataProc(char *buffer, DWORD cnt, LPVOID pData)
{
	if (NULL == pData)
		return;

	CEcgSecreeningSystemDlg *pClass = (CEcgSecreeningSystemDlg *)pData;
	pClass->ReceiveBufferData(buffer, cnt);
}
//-------------------------------------
// receive data
void CEcgSecreeningSystemDlg::ReceiveBufferData(char *buffer, DWORD cnt)
{
   BulkProtocolPart.HandlerReceiveBuffer(buffer, cnt);
   if( 0 == BulkProtocolPart.UsbBulkReceiveBag.Flag )
       return;

   BulkProtocolPart.UsbBulkReceiveBag.Flag = 0;
   unsigned char *bufPt;
   int length;
   int i;
   BOOL handleFlag;
   BOOL showFlag;

   bufPt = BulkProtocolPart.UsbBulkReceiveBag.SingleBag.Buffer;
   length = BulkProtocolPart.UsbBulkReceiveBag.SingleBagCntLen;
   //----------------------
   showFlag = FALSE;
   handleFlag = FALSE;
   for (i = 0; i<length; i++)
   {
      ProtocolHandler.HandlerCommunicateBag(bufPt[i]);
      if (HAVE_RECEIVE_BAG_RECEIVE == ProtocolHandler.DataCommunicateStruct.ReceiveFlag)
      {
         ProtocolHandler.DataCommunicateStruct.ReceiveFlag = NO_RECEIVE_BAG_RECEIVE;
         handleFlag = HandleCommunicateBag(&ProtocolHandler.DataCommunicateStruct.ReceiveBag);
         if (FALSE == handleFlag)
         {
            showFlag = TransformCommunicateBagToDiffDlg(&ProtocolHandler.DataCommunicateStruct.ReceiveBag);
         }
      }
   }
//------------------
   if ((FALSE == handleFlag) && (FALSE == showFlag))
   {
      ShowAllReceiveContent(bufPt, length);
   }
}
//
// handle communciate bag
//
BOOL CEcgSecreeningSystemDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;
   BOOL flag;

   cmdtype = receivebag->Type;
   flag = FALSE;
   switch (cmdtype)
   {
       //------------------------------------------
       case CMD_BLE_MODULE_INTERACTIVE:HandleDisplayDataCmd(receivebag);
                                       flag = TRUE;
                                       break;
       //------------------------------------------
       default:break;
   }
   return flag;
}
//
// handle display data cmd
void CEcgSecreeningSystemDlg::HandleDisplayDataCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
	int length;
	unsigned char cmdIndex;
	CDataHandle dataHandle;

	length = receivebag->Length;
//	length = length + CMD_HEAD_AND_CHECKSUM_CNT;

	cmdIndex = dataHandle.AsciiToByte(receivebag->Buffer[0]) * COUNT_MODE_DECIMAL_CNT;
	cmdIndex = cmdIndex + dataHandle.AsciiToByte(receivebag->Buffer[1]);
	if (READ_USB_BULK_DATA_MODE != BulkShowContent.FirstConnectHandleStep)
	{
		if( ACTIVE_USB_BULK_CMD_INDEX == cmdIndex )
		{
			HandleActiveUsbBulkCmd(&receivebag->Buffer[2], length - 2);
		}
		else if (GET_BULK_FIX_SERIAL_NUM_CMD_INDEX == cmdIndex)
		{
			HandleGetFixSerialNumCmd(&receivebag->Buffer[2], length - 2);
		}
		else if (GET_CHCK_BULK_FIX_SERIAL_NUM_CMD_INDEX == cmdIndex)
		{
			HandleFixSerialNumValidtyCmd(&receivebag->Buffer[2], length - 2);
		}
		return;
	}
//-------------------------------------------------
	if (DATA_TRANSPARENT_CMD_INDEX == cmdIndex)
	{
		ShowAllReceiveContent(&receivebag->Buffer[2],length - 2);
	}
}
//
//
void CEcgSecreeningSystemDlg::HandleActiveUsbBulkCmd(unsigned char *ribuf, DWORD cnt)
{
	CDataHandle dataHandle;
	unsigned char activeFlag;
	activeFlag = dataHandle.AsciiToByte(ribuf[0]) * COUNT_MODE_DECIMAL_CNT;
	activeFlag = activeFlag + dataHandle.AsciiToByte(ribuf[1]);
	if (BulkShowContent.activeVal == activeFlag)
	{
		if (ACTIVE_USB_BULK_READ == BulkShowContent.activeVal)
		{
			BulkShowContent.FirstConnectHandleStep = READ_USB_BULK_DATA_MODE;
		}
	}
}
void CEcgSecreeningSystemDlg::HandleGetFixSerialNumCmd(unsigned char *ribuf, DWORD cnt)
{
	EncipherHandler.GetFixSerialNumBuffer(ribuf);
	BulkShowContent.FirstConnectHandleStep = CHECK_BULK_SERIAL_NUMBER;
}
//
//
void CEcgSecreeningSystemDlg::HandleFixSerialNumValidtyCmd(unsigned char *ribuf, DWORD cnt)
{
	if (0x01 == ribuf[0])
	{
		BulkShowContent.FirstConnectHandleStep = ACTIVE_USB_BULK_HANDLE;
	}
}
//
// transform communicate bag to diff dlg
BOOL CEcgSecreeningSystemDlg::TransformCommunicateBagToDiffDlg(COMMUNICATE_BAG_STRUCT *receivebag)
{
	if( RAPID_SCREEN_SIMPLIFY_INDEX == CTabCtrlParameter.SelectIndex )
	{
      CRapidScreenSimplifyDlg *dlgpt;
      dlgpt = CTabCtrlParameter.RapidScreenSimplifyDlg;
      dlgpt->HandleCommunicateBag(receivebag);
   }
	else if(DEVICE_RECOVERY_SIMPLIFY_INDEX == CTabCtrlParameter.SelectIndex)
	{
      CDeviceRecoverSimplifyDlg *dlgpt;
      dlgpt = CTabCtrlParameter.DeviceRecoverSimplifyDlg;
      dlgpt->HandleCommunicateBag(receivebag);

      if (READ_ALL_BLK_DATA == dlgpt->BleReadProgressStruct.CmdIndex)
         return TRUE;
	}
   else if(RAPID_SCREEN_DETAILED_INDEX == CTabCtrlParameter.SelectIndex)
   {
      CRapidScreenDetailedDlg *dlgpt;
      dlgpt = CTabCtrlParameter.RapidScreenDetailedDlg;
      dlgpt->HandleCommunicateBag(receivebag);
      //return dlgpt->BleInteractiveStruct.HandleFlag;     //20211230
   }
   else if (DEVICE_RECOVERY_DETAILED_INDEX == CTabCtrlParameter.SelectIndex)
   {
      CDeviceRecoverDetailedDlg *dlgpt;
      dlgpt = CTabCtrlParameter.DeviceRecoverDetailedDlg;
      dlgpt->HandleCommunicateBag(receivebag);

      if (READ_ALL_BLK_DATA == dlgpt->BleReadProgressStruct.CmdIndex)
         return TRUE;
   }
   else if ( DEVICE_CONFIG_DETAILED_INDEX == CTabCtrlParameter.SelectIndex )
   {
	   CDeviceConfigDlg *dlgpt;
	   dlgpt = CTabCtrlParameter.DeviceConfigDlg;
	   dlgpt->HandleCommunicateBag(receivebag);
   }
//-----------------------------------------------------------------------
   if (ECG_DISPLAY_INDEX == CTabCtrlParameter.ShowSelectIndex)
   {
      CEcgShowDlg *dlgpt;
      dlgpt = CTabCtrlParameter.EcgShowDlg;
      dlgpt->HandleCommunicateBag(receivebag);
   }
	return FALSE;
}
//
//
void CEcgSecreeningSystemDlg::ShowAllReceiveContent(unsigned char *ribuf, DWORD cnt)
{
   DWORD i;

   int len;
   char memory;
   CString str, nwritestr, strtmp;
   CEdit *receiveInfoEdit;

   nwritestr.Empty();
   BulkShowContent.Cnt = BulkShowContent.Cnt + cnt;

   if(FALSE == BulkShowContent.DisplayStatusFlag)
       return;

   for (i = 0; i<cnt; i++)
   {
      if (BulkShowContent.HexShowFlag)
      {
         str.Format(_T("%02X,"), ribuf[i]);
         nwritestr = nwritestr + str;
      }
      else
      {
         memory = ribuf[i];
         str = memory;
         nwritestr = nwritestr + str;
      }
   }
   receiveInfoEdit = (CEdit *)(GetDlgItem(IDC_RECEIVE_CONTENT));
   len = receiveInfoEdit->GetWindowTextLength();
   receiveInfoEdit->SetSel(len, len);
   receiveInfoEdit->ReplaceSel(nwritestr);
}
//
//===================================================================
//
void CEcgSecreeningSystemDlg::SendBuffer(unsigned char type, unsigned char *sbuf, DWORD cnt)
{
   BulkProtocolPart.ChangeSendBufferToBulkProtocol(type, sbuf, cnt);
   if (0 == BulkProtocolPart.UsbBulkSendBag.SendBagCnt)
	   return;
//------------------------
   int i, j;
   char *sourceBuf, *targetBuf;
   for (i = 0; i<BulkProtocolPart.UsbBulkSendBag.SendBagCnt; i++)
   {
      sourceBuf = BulkProtocolPart.UsbBulkSendBag.BulkBagBuffer[i].sBulkBuf;
      targetBuf = UsbBulkControl.UsbBulkDriverCommunicateStruct.WriteContent[i];
      for (j = 0; j<SINGLE_USB_BULK_DATA_CNT; j++)
      {
          targetBuf[j] = sourceBuf[j];
      }
	}
	UsbBulkControl.UsbBulkDriverCommunicateStruct.SendBagCnt = BulkProtocolPart.UsbBulkSendBag.SendBagCnt;
	UsbBulkControl.UsbBulkDriverCommunicateStruct.SendBagIndex = 0;

	targetBuf = UsbBulkControl.UsbBulkDriverCommunicateStruct.WriteContent[0];
	if (SINGLE_USB_BULK_DATA_CNT == UsbBulkControl.BulkWriteBuffer(targetBuf, SINGLE_USB_BULK_DATA_CNT))
	{
		if (1 != UsbBulkControl.UsbBulkDriverCommunicateStruct.SendBagCnt)
		{
			UsbBulkControl.UsbBulkDriverCommunicateStruct.SendBagIndex = 1;
			UsbBulkControl.UsbBulkDriverCommunicateStruct.SendFlag = HAVE_DATA_NEED_SEND;
		}
		else
		{
			UsbBulkControl.UsbBulkDriverCommunicateStruct.SendFlag = NO_DATA_NEED_SEND;
		}
	}
	else
	{
		UsbBulkControl.UsbBulkDriverCommunicateStruct.SendFlag = NO_DATA_NEED_SEND;
	}
}
//
//
void CEcgSecreeningSystemDlg::OnBnClickedConnectBleDevice()
{
   int cursel;
   cursel = ((CComboBox *)(GetDlgItem(IDC_CONNECT_DEVICE_INDEX)))->GetCurSel();

   unsigned type;

   ProtocolHandler.GenerateConnectBleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, cursel, NULL);

   type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
   type = type | (BLE_MODULE_OPERATION_VAL << BLE_DATA_OBJECT_INDEX);
   type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
   SendBuffer(type, ProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
	           ProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//
//
void CEcgSecreeningSystemDlg::OnBnClickedDisconnectEcgDevice()
{
   unsigned type;

   ProtocolHandler.GenerateDisconnectBleCmd(TYPE_COMMUNICATE_WITHOUT_MACADDR, 0, NULL);

   type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
   type = type | (BLE_MODULE_OPERATION_VAL << BLE_DATA_OBJECT_INDEX);
   type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
   SendBuffer(type, ProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
              ProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//
//
void CEcgSecreeningSystemDlg::OnBnClickedClearBtn()
{
   SetDlgItemText(IDC_RECEIVE_CONTENT, _T(""));
}
void CEcgSecreeningSystemDlg::OnBnClickedPauseBtn()
{
   if (TRUE == BulkShowContent.DisplayStatusFlag)
   {
      BulkShowContent.DisplayStatusFlag = FALSE;
      SetDlgItemText(IDC_PAUSE_BTN, _T("继续显示"));
   }
   else
   {
      BulkShowContent.DisplayStatusFlag = TRUE;
      SetDlgItemText(IDC_PAUSE_BTN, _T("停止显示"));
   }
}
//
//
void CEcgSecreeningSystemDlg::OnBnClickedHexshowflag()
{
   if (FALSE == BulkShowContent.HexShowFlag)
   {
      BulkShowContent.HexShowFlag = TRUE;
      ((CButton *)(GetDlgItem(IDC_HEXSHOWFLAG)))->SetCheck(1);
   }
   else
   {
      BulkShowContent.HexShowFlag = FALSE;
      ((CButton *)(GetDlgItem(IDC_HEXSHOWFLAG)))->SetCheck(0);
   }
}

void CEcgSecreeningSystemDlg::OnTcnSelchangeChangeDeviceHandleType(NMHDR *pNMHDR, LRESULT *pResult)
{
   *pResult = 0;
   CTabCtrl *tabCtrl;
   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_DEVICE_HANDLER_TYPE)));
   if (DEVICE_RECOVERY_SIMPLIFY_INDEX == CTabCtrlParameter.SelectIndex)
   {
      CDeviceRecoverSimplifyDlg *dlgpt;
      dlgpt = CTabCtrlParameter.DeviceRecoverSimplifyDlg;
      if (READ_ALL_BLK_DATA == dlgpt->BleReadProgressStruct.CmdIndex)
      {
          tabCtrl->SetCurSel(CTabCtrlParameter.SelectIndex);
          return;
      }
   }
   else if( DEVICE_RECOVERY_DETAILED_INDEX == CTabCtrlParameter.SelectIndex )
   {
      CDeviceRecoverDetailedDlg *dlgpt;
      dlgpt = CTabCtrlParameter.DeviceRecoverDetailedDlg;
      if (READ_ALL_BLK_DATA == dlgpt->BleReadProgressStruct.CmdIndex)
      {
         tabCtrl->SetCurSel(CTabCtrlParameter.SelectIndex);
         return;
      }
   }

   CTabCtrlParameter.DlgPt[CTabCtrlParameter.SelectIndex]->ShowWindow(SW_HIDE);
   tabCtrl = ((CTabCtrl *)(GetDlgItem(IDC_CHANGE_DEVICE_HANDLER_TYPE)));
   CTabCtrlParameter.SelectIndex = tabCtrl->GetCurSel();

   CTabCtrlParameter.DlgPt[CTabCtrlParameter.SelectIndex]->ShowWindow(SW_SHOW);
}
//
//===================================================================
//
void CEcgSecreeningSystemDlg::SendBufToBleDevice(void)
{
	unsigned type;

	type = (RECEIVE_DATA_FOR_BLE_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
	type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
	type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
	SendBuffer( type, ProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                ProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//
void CEcgSecreeningSystemDlg::SendBufToUsbBulkDev(void)
{
    unsigned type;

    type = (RECEIVE_DATA_FOR_USB_PART << RECEIVE_DATA_FOR_OBJECT_INDEX);
    type = type | (BLE_TRANSPARENT_TRANSIMISSION_VAL << BLE_DATA_OBJECT_INDEX);
    type = type | (MASTER_TO_DEVICE_VAL << BULK_DATA_DIRECTION_INDEX);
    SendBuffer( type, ProtocolHandler.DataCommunicateStruct.SendBag.BufferForMac,
                ProtocolHandler.DataCommunicateStruct.SendSingleBagLen);
}
//

