#pragma once
#include "DataHandle.h"  
#include "SaveStruct.h"
#include "DataProtocolHandler.h"
#include "ProjDefine.h"
#include "DrawPicHandler.h"

#include "FileHandler.h"
// CEcgShowDlg 对话框

//-------------------------------------------------------------
#define ECG_CHANNEL_TMP_PART                       0x0F

#define WORD_LENGTH_PART                           0x30  //bit5 4 = 00:word length = 2,01:word length = 3

#define WORD_LENGTH_TWO                            0x00  //bit5 4 = 00:word length = 2,11:reserved
#define WORD_LENGTH_THREE                          0x10  //bit5 4 = 10:word length = 3,11:reserved
#define WORD_LENGTH_ONE_AND_A_HALF                 0x20  //bit5 4 = 20:word length = 1.5,11:reserved
#define WORD_LENGTH_SPECIAL_TYPE                   0x30  //bit5 4 = 30:special type
//-------------------------------------------------------------
#define SPECIAL_CMD_PART                           0x40  //
//-------------------------------------------------------------
#define ECG_SINGLE_CHANNEL_CMD                     0x01 
#define ECG_TWO_CHANNEL_CMD                        0x02 
#define ECG_THREE_CHANNEL_CMD                      0x03 
#define ECG_FOUR_CHANNEL_CMD                       0x04 
#define ECG_EIGHT_CHANNEL_CMD                      0x08 
#define ECG_NINE_LEAD_CHANNEL_CMD                  0x09
//-------------------------------------------------------------
#define BASE_OFFSET                                0x8000
//-------------------------------------------------------------
#define SAVE_DATA_MULTIPLE                         4
typedef struct 
{
   BOOL DataBufCreateFlag;

   int XDrawLen;                          // x draw length
   int YDrawLen;                          // y draw height

   int ShowCnt;
   int ShowStartIndex;
   int MaxSavePtCnt;

   WORD_TYPE *LeadSignalData;

}ECG_LEAD_SIGNAL;
//-------------------------------------

class CEcgShowDlg : public CDialog
{
	DECLARE_DYNAMIC(CEcgShowDlg)

public:
   CString SaveStr;
//------------------------   
   void *ParentPt;
   BOOL DrawFlag;
   BOOL SaveFlag;
   CRect ShowRect;

   unsigned int TimeStamp,BagIndex;

   ECG_LEAD_SIGNAL EcgLeadSignal;
   CFileHandler FileOperation;
//----------------------------------------
   void InitialAllControl(void);
   void InitialDrawInfo(void);

   void InitialDlgArea(void);
   void BrushEcgSignalBuffer(void);

   CDrawPicHandler DrawHandler;
   void BrushDrawInfo(void);

//-------------------------------------------------------------------
   void HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleSpecialCmd(COMMUNICATE_BAG_STRUCT *receivebag);
   void HandleGetSaveLeftTime(COMMUNICATE_BAG_STRUCT *receivebag);
   void GetSingleDisplayData(COMMUNICATE_BAG_STRUCT *receivebag);

   void GetSpecialDisplayData(COMMUNICATE_BAG_STRUCT *receivebag);
   void GetSpecialEcgDisplayData(COMMUNICATE_BAG_STRUCT *receivebag);
   void GetSpecialEmgDisplayData(COMMUNICATE_BAG_STRUCT *receivebag);

   void HandleSaveDisplayData(void);
   //-------------------------------------------------------------------
   CEcgShowDlg(CWnd* pParent = NULL);   // 标准构造函数
   virtual ~CEcgShowDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ECG_SHOW_DLG };
#endif
public:
   BOOL PreTranslateMessage(MSG* pMsg);
   BOOL HandleKeyPressDown(MSG *pMsg);
protected:
   virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnPaint();
   afx_msg void OnBnClickedSaveFlag();
};
