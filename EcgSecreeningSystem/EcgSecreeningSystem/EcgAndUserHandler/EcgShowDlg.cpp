// EcgShowDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "EcgSecreeningSystem.h"
#include "EcgShowDlg.h"
#include "afxdialogex.h"

#include "EcgSecreeningSystemDlg.h"
// CEcgShowDlg 对话框
#define X_DISPLAY_OFFSET_VALUE       5
#define Y_DISPLAY_OFFSET_VALUE       90

#define NEW_FILEFOLD_NAME_STR        _T("SaveData")
#define SAVE_FILE_NAME_STR           _T("SignalData.txt")

#pragma warning(disable:4996)               // for localtime warning

IMPLEMENT_DYNAMIC(CEcgShowDlg, CDialog)

CEcgShowDlg::CEcgShowDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_ECG_SHOW_DLG, pParent)
{
   DrawFlag = TRUE;
   SaveFlag = FALSE;

   EcgLeadSignal.DataBufCreateFlag = FALSE;

   EcgLeadSignal.ShowCnt = 0;
   EcgLeadSignal.ShowStartIndex = 0;

   SaveStr.Empty();
}

CEcgShowDlg::~CEcgShowDlg()
{
}

void CEcgShowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CEcgShowDlg, CDialog)
   ON_WM_PAINT()
	ON_BN_CLICKED(IDC_SAVE_FLAG, &CEcgShowDlg::OnBnClickedSaveFlag)
END_MESSAGE_MAP()

//
// shield esc and enter key
BOOL CEcgShowDlg::PreTranslateMessage(MSG* pMsg)
{
   if (TRUE == HandleKeyPressDown(pMsg))
      return TRUE;

   return CDialog::PreTranslateMessage(pMsg);
}
//
// shield esc and enter
//
BOOL CEcgShowDlg::HandleKeyPressDown(MSG *pMsg)
{
   if ((WM_KEYFIRST <= pMsg->message) && (pMsg->message <= WM_KEYLAST))
   {
      if ((SHIELD_KEYBOARD_KEY1 == pMsg->wParam) || (SHIELD_KEYBOARD_KEY2 == pMsg->wParam))
         return TRUE;
   }
   return FALSE;
}
//
//
BOOL CEcgShowDlg::OnInitDialog(void)
{
   CDialog::OnInitDialog();
   InitialAllControl();
   return TRUE;
}
void CEcgShowDlg::OnPaint()
{
   CPaintDC dc(this); // device context for painting
                      // TODO: 在此处添加消息处理程序代码
                      // 不为绘图消息调用 CDialog::OnPaint()
   if (TRUE == DrawFlag)
   {
      DrawFlag = FALSE;
      DrawHandler.DrawSignalWave();
   }
}
//
//
void CEcgShowDlg::InitialAllControl(void)
{
	FileOperation.CurModuleRoute = FileOperation.GetCurrentProgramPath();
	InitialDrawInfo();
    if ( FALSE == SaveFlag )
    {
	   ((CButton *)(GetDlgItem(IDC_SAVE_FLAG)))->SetCheck(0);
    }
}
//
//
void CEcgShowDlg::InitialDrawInfo(void)
{
   DrawHandler.ParentPt = this;
   DrawHandler.DisplayBuf.ShowFlag = SHOW_ECG_SIGNAL_WAVE;
   DrawHandler.PicDriver.m_hwnd = this->m_hWnd;
}
//
// for parent dlg call 
void CEcgShowDlg::InitialDlgArea(void)
{
   GetClientRect(&ShowRect);
   BrushDrawInfo();
   BrushEcgSignalBuffer();
}
//
//
void CEcgShowDlg::BrushDrawInfo(void)
{
   DRAW_AREA_SIZE_DETAILS *areaSizeDetails;

   areaSizeDetails = &DrawHandler.PicDriver.AreaSizeDetails;
   
   areaSizeDetails->DrawMappingRect.left = ShowRect.left + X_DISPLAY_OFFSET_VALUE;
   areaSizeDetails->DrawMappingRect.right = ShowRect.right - X_DISPLAY_OFFSET_VALUE;
   areaSizeDetails->DrawMappingRect.top = ShowRect.top + Y_DISPLAY_OFFSET_VALUE;
   areaSizeDetails->DrawMappingRect.bottom = ShowRect.bottom - Y_DISPLAY_OFFSET_VALUE;
   DrawHandler.GetEveryShowLen();
}
//
//
void CEcgShowDlg::BrushEcgSignalBuffer(void)
{
   if (TRUE == EcgLeadSignal.DataBufCreateFlag)
   {
      delete[] EcgLeadSignal.LeadSignalData;
   }
   EcgLeadSignal.XDrawLen = DrawHandler.PicDriver.AreaSizeDetails.X_EveryLen;
   EcgLeadSignal.YDrawLen = DrawHandler.PicDriver.AreaSizeDetails.Y_EveryLen;

   EcgLeadSignal.MaxSavePtCnt = EcgLeadSignal.XDrawLen * SAVE_DATA_MULTIPLE;
   EcgLeadSignal.LeadSignalData = new WORD_TYPE[EcgLeadSignal.MaxSavePtCnt];

   EcgLeadSignal.DataBufCreateFlag = TRUE;
}
//
//
void CEcgShowDlg::HandleCommunicateBag(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;
   unsigned char ecgType,channelCntContent;

   cmdtype = receivebag->Type;
   if (0 != (cmdtype & SPECIAL_CMD_PART))
   {
      HandleSpecialCmd(receivebag);
      return;
   }
   ecgType = receivebag->Type & WORD_LENGTH_PART;
   channelCntContent = receivebag->Type & ECG_CHANNEL_TMP_PART;
   if (WORD_LENGTH_TWO == ecgType)
   {
      switch (channelCntContent)
      {
         case ECG_SINGLE_CHANNEL_CMD:GetSingleDisplayData(receivebag);
                                     break;
         default:break;
      }
   }
   else if (WORD_LENGTH_SPECIAL_TYPE == ecgType)
   {
	   switch (channelCntContent)
	   {
	      case ECG_SINGLE_CHANNEL_CMD:GetSpecialDisplayData(receivebag);
			                          break;
	   }
   }
}
void CEcgShowDlg::HandleSpecialCmd(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char cmdtype;

   cmdtype = receivebag->Type;
   switch (cmdtype)
   {
      case CMD_GET_SAVE_LEFTTIME:HandleGetSaveLeftTime(receivebag);
                                 break;
   }
}
//
//
void CEcgShowDlg::HandleGetSaveLeftTime(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char value;
   CString str;
   CString saveStr;
   unsigned int lefttime;
   unsigned char lefthour, leftminute, leftsecond;

   value = receivebag->Buffer[0];
   
   lefttime = receivebag->Buffer[1];
   lefttime = lefttime * 128 + receivebag->Buffer[2];
   lefttime = lefttime * 128 + receivebag->Buffer[3];

   lefthour = lefttime / 3600;
   leftminute = (lefttime % 3600 / 60);
   leftsecond = (lefttime % 60);
   str.Format(_T("保存剩余时间:%02d - %02d - %02d"), lefthour, leftminute, leftsecond);

   ((CStatic *)(GetDlgItem(IDC_SHOW_ACTIVE_TIME)))->SetWindowText(str);
}

void CEcgShowDlg::GetSingleDisplayData(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char receivelength;
   int i;
   WORD_TYPE midTmp;
   CString str;
   unsigned char *targetbuf;

   int saveindex;
   //------------------------------------------------------
   receivelength = receivebag->Length - 1;   //delete bag index
   receivelength = receivelength / 2;
   targetbuf = &receivebag->Buffer[1];
   saveindex = EcgLeadSignal.ShowStartIndex;

   for (i = 0; i<receivelength; i++)
   {
      midTmp.DataUint = targetbuf[i * 2];
      midTmp.DataUint = midTmp.DataUint + targetbuf[i * 2 + 1] * 256;
      midTmp.DataInt = midTmp.DataInt - BASE_OFFSET;
      EcgLeadSignal.LeadSignalData[saveindex].DataInt = midTmp.DataInt;

      saveindex++;
      if (saveindex >= EcgLeadSignal.MaxSavePtCnt)
         saveindex = 0;
      //---------------------------------------------------------------
   }
   EcgLeadSignal.ShowStartIndex = saveindex;
   EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt + receivelength;
   if (EcgLeadSignal.ShowCnt > (EcgLeadSignal.MaxSavePtCnt + EcgLeadSignal.XDrawLen))
   {
      EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt - EcgLeadSignal.MaxSavePtCnt;
   }
//--------------------------------------
   if (FALSE == DrawFlag)
   {
      DrawHandler.DrawSignalWave();
   }
//--------------------------------------
}

void CEcgShowDlg::GetSpecialDisplayData(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char type;

	type = receivebag->Buffer[0];

	if (CMD_ECG_ONEHALF_FOR_BAGINDEX == type)
	{
		GetSpecialEcgDisplayData(receivebag);
	}
	else if(CMD_EMG_ONEHALF_FOR_BAGINDEX == type)
	{
		GetSpecialEmgDisplayData(receivebag);
	}
}
//
//
void CEcgShowDlg::GetSpecialEcgDisplayData(COMMUNICATE_BAG_STRUCT *receivebag)
{
   unsigned char receiveLength;
   int i, cnt;
   WORD_TYPE midTmp;
   CString str;
   unsigned char *targetbuf;

   int saveIndex;
//------------------------
   TimeStamp = ((receivebag->Buffer[2] & 0x0F) << 28);
   TimeStamp = TimeStamp + ((receivebag->Buffer[3] & 0x7F) << 21);
   TimeStamp = TimeStamp + ((receivebag->Buffer[4] & 0x7F) << 14);
   TimeStamp = TimeStamp + ((receivebag->Buffer[5] & 0x7F) << 7);
   TimeStamp = TimeStamp + ((receivebag->Buffer[6] & 0x7F));
//------------------------------------------
   BagIndex = ((receivebag->Buffer[7] & 0x7F) << 21);
   BagIndex = BagIndex + ((receivebag->Buffer[8] & 0x7F) << 14);
   BagIndex = BagIndex + ((receivebag->Buffer[9] & 0x7F) << 7);
   BagIndex = BagIndex + (receivebag->Buffer[10] & 0x7F);
//------------------------------------------
   receiveLength = receivebag->Length - 11;
   targetbuf = &receivebag->Buffer[11];
   saveIndex = EcgLeadSignal.ShowStartIndex;

   cnt = 0;
   for (i = 0; i < receiveLength; i++)
   {
	   str.Format(_T("%02X"), targetbuf[i]);
	   SaveStr = SaveStr + str;
	   switch (i % 3)
	   {
	     case 0:midTmp.DataUint = targetbuf[i];
	            break;
	     case 1:midTmp.DataUint = midTmp.DataUint + (unsigned short)((targetbuf[i] & 0xF0) * 16);
	            midTmp.DataInt = midTmp.DataInt - 0x800;
		        EcgLeadSignal.LeadSignalData[saveIndex].DataInt = midTmp.DataInt;
		        saveIndex++;
		        cnt++;
	         	if (saveIndex >= EcgLeadSignal.MaxSavePtCnt)
		            saveIndex = 0;

        //      str.Format(_T("%d\r\n"), midTmp.DataInt);
        //      EcgLeadSignal.AllChannelData[0] = EcgLeadSignal.AllChannelData[0] + str;

                midTmp.DataUint = (targetbuf[i] & 0x0F) * 256;
		        break;
	     case 2:midTmp.DataUint = midTmp.DataUint + targetbuf[i];
	            midTmp.DataInt = midTmp.DataInt - 0x800;
		        EcgLeadSignal.LeadSignalData[saveIndex].DataInt = midTmp.DataInt;
                saveIndex++;
		        cnt++;
		        if (saveIndex >= EcgLeadSignal.MaxSavePtCnt)
                    saveIndex = 0;

		//		           str.Format(_T("%d\r\n"), midTmp.DataInt);
		//			       EcgLeadSignal.AllChannelData[0] = EcgLeadSignal.AllChannelData[0] + str;
                break;
     	}
     }
     EcgLeadSignal.ShowStartIndex = saveIndex;
     EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt + cnt;
     if (EcgLeadSignal.ShowCnt > (EcgLeadSignal.MaxSavePtCnt + EcgLeadSignal.XDrawLen))
     {
         EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt - EcgLeadSignal.MaxSavePtCnt;
     }
     if (FALSE == DrawFlag)
     {
        DrawHandler.DrawSignalWave();
     }
//-----------------------------------------------------
     CString showStr;
     time_t timevalue;
     struct tm *mytm;
     timevalue = TimeStamp;
     mytm = localtime(&timevalue);

     str.Format(_T("%04d-%02d-%02d"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday);
     showStr = str;
     str.Format(_T(",%02d:%02d:%02d "), mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
     showStr = showStr + str;
     str.Format(_T("bagIndex = %08d "), BagIndex);
     showStr = showStr + str;
     ((CStatic *)(GetDlgItem(IDC_RECEIVE_BAG_INFOR)))->SetWindowText(showStr);
//-----------------------------------------------------
	 SaveStr = showStr + SaveStr;
	 SaveStr = SaveStr + _T("\r\n");
	 HandleSaveDisplayData();
}
//
//
void CEcgShowDlg::GetSpecialEmgDisplayData(COMMUNICATE_BAG_STRUCT *receivebag)
{
	unsigned char receiveLength;
	int i, cnt;
	WORD_TYPE midTmp;
	CString str;
	unsigned char *targetbuf;

	int saveIndex;
	//------------------------
	TimeStamp = ((receivebag->Buffer[2] & 0x0F) << 28);
	TimeStamp = TimeStamp + ((receivebag->Buffer[3] & 0x7F) << 21);
	TimeStamp = TimeStamp + ((receivebag->Buffer[4] & 0x7F) << 14);
	TimeStamp = TimeStamp + ((receivebag->Buffer[5] & 0x7F) << 7);
	TimeStamp = TimeStamp + ((receivebag->Buffer[6] & 0x7F));
	//------------------------------------------
	BagIndex = ((receivebag->Buffer[7] & 0x7F) << 21);
	BagIndex = BagIndex + ((receivebag->Buffer[8] & 0x7F) << 14);
	BagIndex = BagIndex + ((receivebag->Buffer[9] & 0x7F) << 7);
	BagIndex = BagIndex + (receivebag->Buffer[10] & 0x7F);
	//------------------------------------------
	//------------------------------------------
	receiveLength = receivebag->Length - 17;
	targetbuf = &receivebag->Buffer[17];
	saveIndex = EcgLeadSignal.ShowStartIndex;

	cnt = 0;
	for (i = 0; i < receiveLength; i++)
	{
		str.Format(_T("%02X"),targetbuf[i]);
		SaveStr = SaveStr + str;
		switch (i % 3)
		{
            case 0:midTmp.DataUint = targetbuf[i];
		           break;
		    case 1:midTmp.DataUint = midTmp.DataUint + (unsigned short)((targetbuf[i] & 0xF0) * 16);
                   midTmp.DataInt = midTmp.DataInt - 0x800;
                   EcgLeadSignal.LeadSignalData[saveIndex].DataInt = midTmp.DataInt;
			       saveIndex++;
                   cnt++;
		           if ( saveIndex >= EcgLeadSignal.MaxSavePtCnt)
				        saveIndex = 0;

                   //str.Format(_T("%d\r\n"), midTmp.DataInt);
                   //EcgLeadSignal.AllChannelData[0] = EcgLeadSignal.AllChannelData[0] + str;

                   midTmp.DataUint = (targetbuf[i] & 0x0F) * 256;
                   break;
		    case 2:midTmp.DataUint = midTmp.DataUint + targetbuf[i];
	               midTmp.DataInt = midTmp.DataInt - 0x800;
                   EcgLeadSignal.LeadSignalData[saveIndex].DataInt = midTmp.DataInt;
			       saveIndex++;
                   cnt++;
		           if (saveIndex >= EcgLeadSignal.MaxSavePtCnt)
				       saveIndex = 0;

                   //str.Format(_T("%d\r\n"), midTmp.DataInt);
                   //EcgLeadSignal.AllChannelData[0] = EcgLeadSignal.AllChannelData[0] + str;
                   break;
		}
	}
	EcgLeadSignal.ShowStartIndex = saveIndex;
	EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt + cnt;
	if (EcgLeadSignal.ShowCnt > (EcgLeadSignal.MaxSavePtCnt + EcgLeadSignal.XDrawLen))
	{
		EcgLeadSignal.ShowCnt = EcgLeadSignal.ShowCnt - EcgLeadSignal.MaxSavePtCnt;
	}
	if (FALSE == DrawFlag)
	{
		DrawHandler.DrawSignalWave();
	}
//-----------------------------------------------------
	CString showStr;
	time_t timevalue;
	struct tm *mytm;
	timevalue = TimeStamp;
	mytm = localtime(&timevalue);

	str.Format(_T("%04d-%02d-%02d"), (mytm->tm_year + 1900), (mytm->tm_mon + 1), mytm->tm_mday);
	showStr = str;
	str.Format(_T(",%02d:%02d:%02d  "), mytm->tm_hour, mytm->tm_min, mytm->tm_sec);
	showStr = showStr + str;
	str.Format(_T("bagIndex = %08d "), BagIndex);
	showStr = showStr + str;

	((CStatic *)(GetDlgItem(IDC_RECEIVE_BAG_INFOR)))->SetWindowText(showStr);
	SaveStr = showStr + SaveStr;
	SaveStr = SaveStr + _T("\r\n");
	HandleSaveDisplayData();
//-----------------------------------------------------
}
void CEcgShowDlg::HandleSaveDisplayData(void)
{
	CString folderName, curRouteName;
	curRouteName = FileOperation.CurModuleRoute;
	if (0 == SaveStr.GetLength())
		return;

    if (TRUE == SaveFlag )
	{
		FileOperation.SaveDataAppendWrite(NEW_FILEFOLD_NAME_STR, curRouteName, SAVE_FILE_NAME_STR, SaveStr);
	}
	SaveStr.Empty();
}
//
//
void CEcgShowDlg::OnBnClickedSaveFlag()
{
	if (FALSE == SaveFlag)
	{
		SaveFlag = TRUE;
		((CButton *)(GetDlgItem(IDC_SAVE_FLAG)))->SetCheck(1);
	}
	else 
	{
		SaveFlag = FALSE;
		((CButton *)(GetDlgItem(IDC_SAVE_FLAG)))->SetCheck(0);
	}
}
