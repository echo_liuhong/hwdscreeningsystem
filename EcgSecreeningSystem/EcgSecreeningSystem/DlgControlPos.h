
#ifndef __DLG_CONTROL_POS_H_H__
#define __DLG_CONTROL_POS_H_H__

#include "windows.h"

#define CONTROL_START_POS                        10
//--------------------------------------------------------
//...1
#define USB_BULK_OPEN_STATUS_X_POS               CONTROL_START_POS 
#define USB_BULK_OPEN_STATUS_Y_POS               10 
#define USB_BULK_OPEN_STATUS_WIDTH               300
#define USB_BULK_OPEN_STATUS_HEIGHT              30
#define USB_BULK_OPEN_STATUS_BOTTOM_POS          ( USB_BULK_OPEN_STATUS_Y_POS + USB_BULK_OPEN_STATUS_HEIGHT )
//--------------------------------------------------------
//...2
#define RECEIVE_CONTENT_X_POS                    CONTROL_START_POS 
#define RECEIVE_CONTENT_Y_POS                    (USB_BULK_OPEN_STATUS_BOTTOM_POS)
#define RECEIVE_CONTENT_WIDTH                    560
#define RECEIVE_CONTENT_HEIGHT                   240
#define RECEIVE_CONTENT_BOTTOM_POS               (RECEIVE_CONTENT_Y_POS + RECEIVE_CONTENT_HEIGHT)
//--------------------------------------------------------
//...3
#define CONNECT_BLE_DEVICE_X_POS                 CONTROL_START_POS 
#define CONNECT_BLE_DEVICE_Y_POS                 (RECEIVE_CONTENT_BOTTOM_POS+10)
#define CONNECT_BLE_DEVICE_WIDTH                 80
#define CONNECT_BLE_DEVICE_HEIGHT                30
#define CONNECT_BLE_DEVICE_RIGHT_POS             ( CONNECT_BLE_DEVICE_X_POS + CONNECT_BLE_DEVICE_WIDTH )
#define CONNECT_BLE_DEVICE_BOTTOM_POS            ( CONNECT_BLE_DEVICE_Y_POS + CONNECT_BLE_DEVICE_HEIGHT )
//--------------------------------------------------------
#define DISCONNECT_BLE_DEVICE_X_POS              (CONNECT_BLE_DEVICE_RIGHT_POS + 5) 
#define DISCONNECT_BLE_DEVICE_Y_POS              (RECEIVE_CONTENT_BOTTOM_POS+10)
#define DISCONNECT_BLE_DEVICE_WIDTH              80
#define DISCONNECT_BLE_DEVICE_HEIGHT             30
#define DISCONNECT_BLE_DEVICE_RIGHT_POS          ( DISCONNECT_BLE_DEVICE_X_POS + DISCONNECT_BLE_DEVICE_WIDTH )
#define DISCONNECT_BLE_DEVICE_BOTTOM_POS         ( DISCONNECT_BLE_DEVICE_Y_POS + DISCONNECT_BLE_DEVICE_HEIGHT )
//--------------------------------------------------------
#define CONNECT_BLE_DEVICE_INDEX_X_POS           (DISCONNECT_BLE_DEVICE_RIGHT_POS + 5) 
#define CONNECT_BLE_DEVICE_INDEX_Y_POS           (RECEIVE_CONTENT_BOTTOM_POS+12)
#define CONNECT_BLE_DEVICE_INDEX_WIDTH           80
#define CONNECT_BLE_DEVICE_INDEX_HEIGHT          160
#define CONNECT_BLE_DEVICE_INDEX_RIGHT_POS       ( CONNECT_BLE_DEVICE_INDEX_X_POS + CONNECT_BLE_DEVICE_INDEX_WIDTH )
#define CONNECT_BLE_DEVICE_INDEX_BOTTOM_POS      ( CONNECT_BLE_DEVICE_INDEX_Y_POS + CONNECT_BLE_DEVICE_INDEX_HEIGHT )
//--------------------------------------------------------
#define CLEAR_BTN_X_POS                          ( CONNECT_BLE_DEVICE_INDEX_RIGHT_POS + 5 ) 
#define CLEAR_BTN_Y_POS                          ( RECEIVE_CONTENT_BOTTOM_POS + 10 )
#define CLEAR_BTN_WIDTH                          80
#define CLEAR_BTN_HEIGHT                         30
#define CLEAR_BTN_RIGHT_POS                      ( CLEAR_BTN_X_POS + CLEAR_BTN_WIDTH )
#define CLEAR_BTN_BOTTOM_POS                     ( CLEAR_BTN_Y_POS + CLEAR_BTN_HEIGHT )
//--------------------------------------------------------
#define PAUSE_BTN_X_POS                          ( CLEAR_BTN_RIGHT_POS + 5 ) 
#define PAUSE_BTN_Y_POS                          ( RECEIVE_CONTENT_BOTTOM_POS + 10 )
#define PAUSE_BTN_WIDTH                          80
#define PAUSE_BTN_HEIGHT                         30
#define PAUSE_BTN_RIGHT_POS                      ( PAUSE_BTN_X_POS + PAUSE_BTN_WIDTH )
#define PAUSE_BTN_BOTTOM_POS                     ( PAUSE_BTN_Y_POS + PAUSE_BTN_HEIGHT )
//--------------------------------------------------------
#define HEXSHOW_BTN_X_POS                        ( PAUSE_BTN_RIGHT_POS + 5 ) 
#define HEXSHOW_BTN_Y_POS                        ( RECEIVE_CONTENT_BOTTOM_POS + 10 )
#define HEXSHOW_BTN_WIDTH                        140
#define HEXSHOW_BTN_HEIGHT                       30
#define HEXSHOW_BTN_RIGHT_POS                    ( HEXSHOW_BTN_X_POS + HEXSHOW_BTN_WIDTH )
#define HEXSHOW_BTN_BOTTOM_POS                   ( HEXSHOW_BTN_Y_POS + HEXSHOW_BTN_HEIGHT )
//---------------------------------------------------------
//...4
#define TABCTRL_X_POS                            CONTROL_START_POS 
#define TABCTRL_Y_POS                            ( CONNECT_BLE_DEVICE_BOTTOM_POS + 10 )
#define TABCTRL_WIDTH                            RECEIVE_CONTENT_WIDTH
#define TABCTRL_HEIGHT                           600
#define TABCTRL_RIGHT_POS                        ( TABCTRL_X_POS + TABCTRL_WIDTH )
#define TABCTRL_BOTTOM_POS                       ( TABCTRL_Y_POS + TABCTRL_HEIGHT )

#define SHOW_TABCTRL_X_OFFSET                    25
#define SHOW_TABCTRL_Y_BOTTOM_OFFSET             76
//---------------------------------------------------------
#endif   //__USB_BULK_DRIVER_CONFIG_H_H__
//---------------------------------------

