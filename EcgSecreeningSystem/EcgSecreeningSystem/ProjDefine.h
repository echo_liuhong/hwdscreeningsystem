
#ifndef __PROJECT_DEFINE_H_H__
#define __PROJECT_DEFINE_H_H__

#include "windows.h"
#include "SaveStruct.h"
#include "CompleteCommunicateProtocol.h"
#include "DataHandle.h"

#define MAX_BLK_INTERNAL_INDEX                  2
//------------------------------------------------------
#define OVERFLOW_TIMER_INDEX                    2
#define UPLOAD_OVERFLOW_TIME_INTERVAL           1000

#define READ_PROGRESS_TIMER_INDEX               3
#define READ_PROGRESS_TIME_INTERVAL             400

#define ACTIVE_DEV_TIMER_INDEX                  3
#define ACTIVE_DEV_PROGRESS_TIME_INTERVAL       500
//------------------------------------------------------
#define SAVE_DATA_INFOR_INDEX                   0
#define SAVE_DATA_TXT_INDEX                     1
#define SAVE_DATA_BIN_INDEX                     2

#define BIN_SUFFIX_FILE_NAME                    _T(".bin")
#define TXT_SUFFIX_FILE_NAME                    _T(".txt")
#define DAT_SUFFIX_FILE_NAME                    _T(".dat")
#define FOLDER_NAMER_STR                        _T("_Infor")
#define SIGNAL_PREFIX_VAL                       _T("Signal_")
#define INTERPOLATION_PREFIX_VAL                _T("Interpolation_")
#define SAMPLE_PREFIX_VAL                       _T("Sample_")
#define BAIHUI_SIGNAL_PREDIX                    _T("BAIHUI_")
#define BOSHENG_SIGNAL_PREDIX                   _T("BOSHENG_")

#define SOURCE_DATA_FOLDER_STR                  _T("SourceData")
#define INTERPOLATION_DATA_FOLDER_STR           _T("InterpolationData")
#define SAMPLE_DATA_FOLDER_STR                  _T("SampleData")
#define BAIHUI_DATA_FOLDER_STR                  _T("BaihuiData")
#define BOSHENG_DATA_FOLDER_STR                 _T("BoshengData")
#define MERGE_DATA_FOLDER_STR                   _T("MergeData")

#define TXT_FILE_SUFFIX_TIP                     0
#define NO_TXT_FILE_SUFFIX_TIP                  1

#define DATA_FILE_TYPE_BAIHUI                   0
#define DATA_FILE_TYPE_BOSHENG                  1

#define BAIHUI_SCALE_VAL                        2
#define BAIHUI_DC_OFFSET                        1870

#define BOSHENG_SCALE_VAL                       (float)(0.838 * 4.62)
#define BOSHENG_DC_OFFSET                       3200
//--------------------------------------------------
//SHIELD KEY
#define SHIELD_KEYBOARD_KEY1                    VK_ESCAPE
#define SHIELD_KEYBOARD_KEY2                    VK_RETURN
//--------------------------------------------------
#define FEMALE_GENDER_INDEX                     0
#define MALE_GENDER_INDEX                       1
#define OTHER_GENDER_INDEX                      2
#define DEFAULT_GENDER_CHOOSE_INDEX             MALE_GENDER_INDEX
//--------------------------------------------------
#define BAIHUI_ECG_SIGNAL_SAMPLERATE            200

#define WAIT_ACK_TIME_CNT                       5
#define WAIT_SAVE_BLK_CONTENT_TIME_CNT          60
#define REPEART_SEND_CNT                        5 
//--------------------------------------------------
typedef struct
{
   BOOL HandleFlag;
   unsigned char RumMode;
   int InternalIndex;
   int type;
   CString VersionStr;
   
}BLE_INTERACTIVE_STRUCT;

typedef struct
{
   unsigned char InitialFlag;
   unsigned int readPageCnt;
   unsigned int PageIndex;

   PAGE_CONTENT_STRUCT *PageBuf;
   HALFWORD_TYPE *Buffer;

}SAVE_DATA_CONTENT;

//----------------------------------------
typedef struct
{
   BOOL GetBufferFlag;
   int BlkCnt;
   int rBlkIndex;
   unsigned int readPageIndex;
   int ReadFlag[MAX_SAVE_BLK_CNT];

   CString PreFileName[MAX_SAVE_BLK_CNT];
   CString IdStr[MAX_SAVE_BLK_CNT];
   CString StartTimeStampStr[MAX_SAVE_BLK_CNT];
   CString RecordedDateStr[MAX_SAVE_BLK_CNT];

   CString BlkHeadStr[MAX_SAVE_BLK_CNT];
   CString BlkInfoStr[MAX_SAVE_BLK_CNT];
   //--------------
   SAVE_DATA_HEAD BlkHead[MAX_SAVE_BLK_CNT];
   SAVE_BLK_DETAILED_INFOR BlkInfor[MAX_SAVE_BLK_CNT];
   SAVE_DATA_CONTENT BlkContent[MAX_SAVE_BLK_CNT];
   //--------------

}SIGNAL_SAVE_CONTENT_STRUCT;
//----------------------------------------------
#define  VALID_CHECKTMP_FOR_16_PART             0xFFFF     
#define  VALID_CHECKTMP_FOR_11_PART             0x7FF     
#define  VALID_CHECKTMP_FOR_7_PART              0x7F    
enum {
   SHORT_BAG_COMMUNICATE = 0,
   LONG_BAG_COMMUNICATE,
};
typedef struct
{
   unsigned char AddType;
   unsigned char AddIndex;
   unsigned char SetSaveStatus;
   unsigned char SetSaveHours;
   unsigned int timeStamp;
   //--------------------------------------------
   unsigned char UploadBagLenFlag;             //
   unsigned char UploadBagDataLen;             //
   unsigned char MaxUploadBagInternalCnt;
   unsigned char MidUploadBagDataLen;          //
   unsigned char LastUploadBagDataLen;         //
   unsigned short CheckTmp;
   //--------------------------------------------
}BLE_DEVICE_STATUS_STRUCT;
//----------------------------------
enum {
   DEVICE_NOT_IN_SAVE_STATUS = 0,
   DEVICE_IN_SAVE_STATUS,
};
enum {
   INVALID_TRANSMIT_CMD = 0,
   START_TRANSMIT_CMD,
   END_TRANSMIT_CMD,
   SPECIAL_PAGE_TRANSMIT_CMD,
};
#define ACK_SHORT_BAG_FLAG               0     //
#define ACK_LONG_BAG_FLAG                0x20  //

#define ACK_COMPLETE_PAGE_CONTENT_FLAG   0x40
//----------------
enum {
   READ_DEVICE_DATA_IDLE = 0,
   READ_SINGLE_BLK_DATA,
   READ_ALL_BLK_DATA,
};
enum {
   INVALID_READ_STEP = 0,
   FIRST_QUIT_READ_MODE,
   READ_DEVICE_SAVE_STATUS,    //FIRST
   QUIT_DEVICE_SAVE_STATUS,
   READ_DEVICE_SAVE_BLK_CNT,
   READ_EVERY_SAVE_BLK_INFO,
   ENTER_INTO_READ_MODE,
   READ_EVERY_BLK_CONTENT,
   ERASE_DEVICE_BLK_CONTENT,
   QUIT_READ_MODE,
};

#define STEP_INVALID                    0
#define STEP_FIRST_QUIT_READMODE        1
#define STEP_ENTER_INTO_READMODE        2
#define STEP_QUIT_READ_MODE             3
#define STEP_QUIT_SAVE_MODE             4
#define STEP_ERASE_BLK_CONTENT          5 
#define STEP_ENTER_INTO_RUN_MODE        6
enum {
   SEND_BLE_CMD_STEP = 0,
   WAIT_ACK_CMD_STEP,
};
enum {
   RESERVE_DATA_IN_READ_ALL_BLK = 0,
   AUTO_ERASE_DATA_IN_READ_ALL_BLK,
};
typedef struct
{
   BOOL UpdateUploadProgressFlag;
   BOOL SaveRecoverInforFileFlag;

   unsigned char DirectStatus : 2;
   unsigned char DataErrorFlag : 2;
   unsigned char AutoEraseDataFlag : 1;
   unsigned char DeviceSaveSta : 1;
   unsigned char ReserveBits : 2;

   unsigned char WaitAckTimeCnt;
   unsigned char RepeatSendCnt;

   unsigned char CmdIndex;
   unsigned char ReadStep;
   unsigned char AckBagLenVal;

   int ChangeDevStaStep;
   //----------------------------   
   int ReadBlkIndex;
   int InternalIndex;

   int StartPageIndex;
   int ReadPageIndex;
   int EndPageIndex;
   int ReadAllPageCnt;
   int BeginTimeTick;

   int StartBagIndex;
   int EndBagIndex;
   int ReadBagCnt;

}BLE_READ_PROGRESS_STRUCT;
//-----------------------------------------------
typedef struct
{
   CString IdDirectoryStr;
   CString Date_DirectoryStr[MAX_SAVE_BLK_CNT];

}DATA_SAVE_DIRECTORY_STRUCT;

//-----------------------------------------------
typedef struct
{
   CString BoshengFileName;
   CString BaihuiFileName;
   CString SourceBinFileName;
   CString SourceTxtFileName;

   HALFWORD_TYPE *sourceData;
   DATA_CONTENT_STRUCT SourceBinContent;
   DATA_CONTENT_STRUCT SourceTxtContent;

   int beginIndex;
   SAVE_DATA_HEAD SaveHead;

}MERGE_DATA_STRUCT;

enum {
   INVALID_ACTIVE_DEV_STEP = 0,
   FIRST_GET_DEV_RUN_STA,
   SECOND_QUIT_DEV_READ_MODE,
   THIRD_GET_DEV_SAVE_STA,
   FOUTH_WRITE_USER_INFOR,
   FIFTH_GET_DEV_ID,
   SIXTH_SEND_TIMSTAMP,
   SEVENTH_ACTIVE_DEV,
   EIGHTH_ACTIVE_DEV_SAVE,
};
typedef struct
{
   BOOL UpdateUploadProgressFlag;
   BOOL SaveStatusFlag;
   BOOL ActiveDevFlag;
   BOOL SaveActiveFileFlag;

   unsigned char DirectStatus : 2;
   unsigned char DataErrorFlag : 2;
   unsigned char DeviceSaveSta : 1;
   unsigned char ReserveBits : 3;

   unsigned char WaitAckTimeCnt;
   unsigned char RepeatSendCnt;

   unsigned char ActiveDevStep;
   unsigned char RunMode;
   unsigned char SaveMode;
//----------------------------   
   unsigned char InternalIndex;
   unsigned char Type;

   int ChangeDevStaStep;

}ACTIVE_DEVICE_PROGRESS_STRUCT;

typedef struct
{
   CString DeviceId;
   CString StartTime;
   CString EndTime;
   CString BlkCnt;
   CString NameStr;
   CString Gender;
   CString Age;
   CString Height;
   CString Weight;
   CString PhoneNumber;
   CString FileRoute;

}DEVICE_USER_RELATION_INFOR;
//-----------------------------------------------
#endif   //__PROJECT_DEFINE_H_H__
//---------------------------------------

