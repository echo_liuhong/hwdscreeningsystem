// UpdateBleCenterDlg.cpp : implementation file
//
#include "stdafx.h"
#include "AboutDlg.h"

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
	
}
BOOL CAboutDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
    ((CStatic *)GetDlgItem(IDC_ECG_SCREENING_SYSTEM_VER))->SetWindowText(ECG_SCREENING_SYSTEM_VERSION_STR); 
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()
