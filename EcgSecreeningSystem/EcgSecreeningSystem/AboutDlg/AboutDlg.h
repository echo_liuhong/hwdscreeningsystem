#pragma once

#include "Resource.h"		// main symbols

#define ECG_SCREENING_SYSTEM_VERSION_STR    _T("EcgSecreeningSystem Version 1.00.05")

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX};

protected:
    virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};
